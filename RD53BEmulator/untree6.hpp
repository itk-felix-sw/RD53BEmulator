#ifndef UNTREE6_H
#define UNTREE6_H

#include <cinttypes>
#include <cstdlib>
#include <utility>

#include <iostream>
#include <bitset>

#include "lut_untree_6.h"

#define USE_LUT

template <uint8_t index=0>
inline void huffman_decode_2(uint8_t& val, uint8_t& len) {
    switch (val & (0b11 << index)) { 
        case (0b11 << index):
            return;
        case (0b10 << index):
            return;
        case (0b01 << index):
        case (0b00 << index):
            len -= 1;
            //std::cout << "hword " << std::bitset<8>((val)) << std::endl;
            //std::cout << "umask " << std::bitset<8>((0xFE << index)) << std::endl;
            //std::cout << "lmask " <<  std::bitset<8>(0xFF >> (7 - index)) << std::endl;
            //std::cout << "lval  " <<  std::bitset<8>((val & (0xFF >> (7 - index)))) << std::endl;
            //std::cout << "lvaln " <<  std::bitset<8>((val & (0xFF >> (7 - index))) >> 1) << std::endl;
            val = (val & (0xFE << index)) | (0x1 << index) | (((val & (0xFF >> (7 - index))) >> 1));
            //std::cout << "word  " << std::bitset<8>((val)) << std::endl;
            return;
    }
}

inline void huffman_decode_8(uint8_t& val, uint8_t& len) {
    // start from "left" side
    huffman_decode_2<6>(val, len);
    huffman_decode_2<4>(val, len);
    huffman_decode_2<2>(val, len);
    huffman_decode_2<0>(val, len);
}

template <uint8_t index=0>
inline void huffman_decode_6(uint8_t& val, uint8_t& len) {
    // start from "left" side
    huffman_decode_2<index+4>(val, len);
    huffman_decode_2<index+2>(val, len);
    huffman_decode_2<index+0>(val, len);
}

template <uint8_t index=0>
inline void huffman_decode_4(uint8_t& val, uint8_t& len) {
    // start from "left" side
    huffman_decode_2<index+2>(val, len);
    huffman_decode_2<index+0>(val, len);
}

inline uint8_t _untree6_reverse8(uint8_t b) {
   b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
   b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
   b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
   return b;
}

inline std::pair<uint8_t, uint8_t> untree_6(uint16_t side) {
    uint8_t hm_e = 0;
    uint8_t hitmap = 0;
    uint8_t len = 0;
    // assume side is left-aligned 14 bits
    switch (side & (0b11 << 12)) { // bits 13,14

        case (0b11 << 12): // top node is 11

            // bits 12-9 encode rest of tree
            switch (side & (0b1111 << 8)) {
                case (0b1111 << 8): // 1111 == 11,11
                    hm_e = side & (0b11111111);
                    len = 6 + 8;
                    huffman_decode_8(hm_e, len);
                    hitmap = hm_e;
                    break;

                case (0b1110 << 8): // 1110 == 11,10
                    hm_e = (side >> 2) & 0b111111;
                    len = 6 + 6;
                    huffman_decode_6(hm_e, len);
                    hitmap = hm_e << 2;
                    break;

                case (0b1101 << 8): // 1101 == 11,01
                case (0b1100 << 8): // 1100 == 11,01
                    hm_e = (side >> 3) & 0b111111;
                    len = 5 + 6;
                    huffman_decode_6(hm_e, len);
                    hitmap = ((hm_e & 0b111100) << 2) | (hm_e & 0b000011);
                    break;

                case (0b1011 << 8): // 1011 == 10,11
                    hm_e = (side >> 2) & 0b111111;
                    len = 6 + 6;
                    huffman_decode_6(hm_e, len);
                    hitmap = ((hm_e & 0b110000) << 2) | (hm_e & 0b001111);
                    break;

                case (0b1010 << 8): // 1010 == 10,10
                    hm_e = (side >> 4) & 0b1111;
                    len = 6 + 4;
                    huffman_decode_4(hm_e, len);
                    hitmap = ((hm_e & 0b1100) << 4) | ((hm_e & 0b0011) << 2);
                    break;

                case (0b1001 << 8): // 1001 == 10,01
                case (0b1000 << 8): // 1000 == 10,01
                    hm_e = (side >> 5) & 0b1111;
                    len = 5 + 4;
                    huffman_decode_4(hm_e, len);
                    hitmap = ((hm_e & 0b1100) << 4) | (hm_e & 0b0011);
                    break;

                case (0b0111 << 8): // 0111 == 01,11
                case (0b0110 << 8): // 0110 == 01,11
                    hm_e = (side >> 3) & 0b111111;
                    len = 5 + 6;
                    huffman_decode_6(hm_e, len);
                    hitmap = hm_e & 0b111111;
                    break;

                case (0b0101 << 8): // 0101 == 01,10
                case (0b0100 << 8): // 0100 == 01,10
                    hm_e = (side >> 5) & 0b1111;
                    len = 5 + 4;
                    huffman_decode_4(hm_e, len);
                    hitmap = hm_e << 2;
                    break;

                case (0b0011 << 8): // 0011 == 01,01
                case (0b0010 << 8): // 0010 == 01,01
                case (0b0001 << 8): // 0001 == 01,01
                case (0b0000 << 8): // 0000 == 01,01
                    hm_e = (side >> 6) & 0b1111;
                    len = 4 + 4;
                    huffman_decode_4(hm_e, len);
                    hitmap = ((hm_e & 0b1100) << 2) | (hm_e & 0b0011);
                    break;

            }
            break; 

        case (0b10 << 12): // top node is 10
                     
            // only left tree in bits 14,13
            switch (side & 0b11 << 10) {
                case (0b11 << 10): // 11-- == 11,00
                    hm_e = (side >> 6) & 0b1111;
                    len = 4 + 4;
                    huffman_decode_4(hm_e, len);
                    hitmap = hm_e << 4;
                    break;

                case (0b10 << 10): // 10-- == 10,00
                    hm_e = (side >> 8) & 0b11;
                    len = 4 + 2;
                    huffman_decode_2(hm_e, len);
                    hitmap = hm_e << 6;
                    break;

                case (0b01 << 10): // 01-- == 01,00
                case (0b00 << 10): // 00-- == 01,00
                    hm_e = (side >> 9) & 0b11;
                    len = 3 + 2;
                    huffman_decode_2(hm_e, len);
                    hitmap = hm_e << 4;
                    break;
            }
            break; 
        
        case (0b01 << 12): // top node is 01
        case (0b00 << 12): // top node is 00

            // only right tree in bits 15,14
            switch (side & 0b11 << 11) {
                case (0b11 << 11): // --11 == 00,11
                    hm_e = (side >> 7) & 0b1111;
                    //std::cout << "hm_e " << std::bitset<4>(hm_e) << std::endl;
                    len = 3 + 4;
                    huffman_decode_4(hm_e, len);
                    hitmap = hm_e;
                    break;

                case (0b10 << 11): // --10 == 00,10
                    hm_e = (side >> 9) & 0b11;
                    len = 3 + 2;
                    huffman_decode_2(hm_e, len);
                    hitmap = hm_e << 2;
                    break;

                case (0b01 << 11): // --01 == 00,01
                case (0b00 << 11): // --00 == 00,01
                    hm_e = (side >> 10) & 0b11;
                    //std::cout << "hm_e " << std::bitset<2>(hm_e) << std::endl;
                    len = 2 + 2;
                    huffman_decode_2(hm_e, len);
                    hitmap = hm_e;
                    break;

            }
            break; 
    }
    //std::cout << "hitmap: " << std::bitset<8>(hitmap) << std::endl;
    return std::make_pair(_untree6_reverse8(hitmap), len);
}

std::pair<uint16_t, uint8_t> untree_30(uint32_t tree) {
    // Assume tree is currently right-aligned 30 bits
    uint16_t hitmap = 0;
    uint8_t len = 0;
    uint8_t offset;

    //std::cout << "here" << std::endl;

#ifdef USE_LUT
    int16_t hm1 = 0, hm2 = 0;
    uint8_t len1 = 0, len2 = 0;
    switch (tree & (0b11 << 28)) {
        case (0b11 << 28):
            hm1 = _untree_6_lut[(tree & (0x3FFF << 14)) >> 14];
            hitmap |= hm1 & 0xFF;
            len1 = ((hm1 & 0xFF00) >> 8);
            offset = 14 - len1;
            hm2 = _untree_6_lut[(tree & (0x3FFF << offset)) >> offset];
            len2 = ((hm2 & 0xFF00) >> 8);
            hitmap |= (hm2 & 0xFF) << 8;
            len = 2 + len1 + len2;
            break;

        case (0b10 << 28):
            hm1 = _untree_6_lut[(tree & (0x3FFF << 14)) >> 14];
            hitmap |= hm1 & 0xFF;
            len1 = ((hm1 & 0xFF00) >> 8);
            len = 2 + len1;
            break;

        case (0b01 << 28):
        case (0b00 << 28):
            hm2 = _untree_6_lut[(tree & (0x3FFF << 15)) >> 15];
            hitmap |= (hm2 & 0xFF) << 8;
            len2 = ((hm2 & 0xFF00) >> 8);
            len = 1 + len2;
            break;
    }
#else
    std::pair<uint8_t, uint8_t> hm1;
    std::pair<uint8_t, uint8_t> hm2;
    switch (tree & (0b11 << 28)) {
        case (0b11 << 28):
            hm1 = untree_6((tree & (0x3FFF << 14)) >> 14);
            hitmap |= hm1.first;
            //std::cout << "length of first: " << (uint32_t) hm1.second << std::endl;
            offset = 14 - hm1.second;
            //std::cout << "hm2 " << std::bitset<14>((tree & (0x3FFF << offset)) >> offset) << std::endl;
            //std::cout << "gate " << std::bitset<28>(0xFFFC000 & (0x3FFF << offset)) << std::endl;
            hm2 = untree_6((tree & (0x3FFF << offset)) >> offset);
            hitmap |= uint16_t(hm2.first) << 8;
            len = 2 + hm1.second + hm2.second;
            break;

        case (0b10 << 28):
            hm1 = untree_6((tree & (0x3FFF << 14)) >> 14);
            hitmap |= hm1.first;
            len = 2 + hm1.second;
            break;

        case (0b01 << 28):
        case (0b00 << 28):
            hm2 = untree_6((tree & (0x3FFF << 15)) >> 15);
            hitmap |= uint16_t(hm2.first) << 8;
            len = 1 + hm2.second;
            break;
    }
#endif

    return std::make_pair(hitmap, len);


}
#endif
