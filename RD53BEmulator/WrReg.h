#ifndef RD53B_WRITE_REGISTER_H
#define RD53B_WRITE_REGISTER_H

#include "RD53BEmulator/Command.h"

namespace RD53B{

/**
 * Write Register Command given the address and Chip ID.
 * Command is identified by symbol 0x66, followed by the Chip ID,
 * a mode bit, the 9-bit address, and the payload data.
 * In the case the mode is 0 (single) the payload is 16 bits followed by four padding bits.
 * In the case the mode is 1 (multiple), the address must be set to 0,
 * and it is possible to send as many 10-bit data frames to consecutive pixel addresses.
 *
 * In the case mode is 0, the command encoding uses 30 bits and it looks like the following:
 *
 * | RBit  |  0 |  1 |  4 |  5 |  9 | 10 | 14 | 15 | 19 | 20 | 24 | 25 | 26 | 29 |
 * | ---   | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
 * | Rel   |  0 |  1 |  4 |  0 |  4 |  0 |  4 |  0 |  4 |  0 |  4 |  0 |  1 |  4 |
 * | Value |  M | Address        |||| Payload                    ||||||| 0b0000 ||
 * | Size  |  1 |  9             |||| 16                         |||||||  4     ||
 * | Field |  1         |||  2     ||  3     ||  4     ||  5     ||  6         |||
 * 
 * In the case mode is 1, the command encoding uses 10x(N+1) bits and it looks like the following:
 *
 * | RBit  |  0 |  1 |  4 |  5 |  9 | 10*i+10 | 10*i+14 | 10*i+15 | 10*i+19 |
 * | ---   | -- | -- | -- | -- | -- | ------- | ------- | ------- | ------- |
 * | Rel   |  0 |  1 |  4 |  0 |  4 |  0      |  4      |  0      |  4      |
 * | Value |  M |  0x0           |||| Payload_i                          ||||
 * | Size  |  1 |  9             |||| 10                                 ||||
 * | Field |  1         |||  2     ||  2*i+3           ||  2*i+4           ||
 * 
 *
 * @brief RD53B Write Register Command
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class WrReg: public Command{

 public:
  
  /**
   * Create a WrReg
   **/
  WrReg();

  /**
   * Create a WrReg from another one
   * @param copy WrReg to copy
   **/
  WrReg(WrReg * copy);

  /**
   * Create a WrReg with parameters
   * @param chipid Chip ID [0:3] + bcast [4]
   * @param address the address [0:9]
   * @param value the value to write [0:15]
   **/
  WrReg(uint32_t chipid, uint32_t address, uint32_t value);

  
  /**
   * Empty destructor just for completeness.
   **/
  ~WrReg();
  
  /**
   * Return a clone of the current Command
   * @return A clone of the current Command
   **/
  Command * Clone();

  /**
   * Return a human readable representation of the symbol
   * @return The human readable representation of the symbol
   **/
  std::string ToString();

  /**
   * Parse a human readable representation of the command
   * @param str The human readable representation of the command
   * @return true if it was parsed correctly
   **/
  bool FromString(std::string str);

  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the chipid for the pulse
   * @param chipid Chip ID
   **/
  void SetChipId(uint32_t chipid);

  /**
   * Get the chipid for the pulse
   * @return chipid Chip ID
   **/
  uint32_t GetChipId();

  /**
   * Set the address for the readout
   * @param address the address [0:9]
   **/
  void SetAddress(uint32_t address);

  /**
   * Get the address for the readout
   * @return the address [0:9]
   **/
  uint32_t GetAddress();

  /**
   * Set the value to write 
   * @param value the value to write [0:15]
   **/
  void SetValue(uint32_t value);

  /**
   * Set the value to write
   * @param index index to write [0:9]
   * @param value the value to write [0:15]
   **/
  void SetValue(uint32_t index, uint32_t value);

  /**
   * Get the value to write 
   * @param index index to write [0:9]
   * @return the value to write [0:15]
   **/
  uint32_t GetValue(uint32_t index=0);

  /**
   * Set the mode (0: short, 1: long) 
   * @param mode the write mode (0: short, 1: long)
   **/
  void SetMode(uint32_t mode);

  /**
   * Get the mode (0: short, 1: long) 
   * @return the write mode (0: short, 1: long)
   **/
  uint32_t GetMode();

 private:

  uint32_t m_symbol;
  uint32_t m_chipid;
  uint32_t m_address;
  uint32_t m_mode;
  std::vector<uint32_t> m_values;
};


}

#endif
