#ifndef RD53B_EMULATOR_H
#define RD53B_EMULATOR_H

#include "RD53BEmulator/Decoder.h"
#include "RD53BEmulator/Encoder.h"
#include "RD53BEmulator/Configuration.h"
#include <queue>
#include <vector>
#include <mutex>

namespace RD53B{

/**
 * This class emulates an RD53B pixel detector that is identified by a chip ID
 * by makes use of a Command Encoder, a Frame Decoder, and a Field Configuration.
 *
 * A byte stream can be passed to the Emulator (Emulator::HandleCommand),
 * that will be decoded into a list of commands, and added to a queue.
 * If the chip ID of the Command does not match the one for the Emulator,
 * the Command will be ignored. Messages with chip ID >7 will be always interpreted.
 *
 * Commands are processed (Emulator::ProcessQueue) from the queue starting
 * from the oldest one received.
 * If the emulator is configured in auto-trigger mode through a configuration command,
 * it will automatically generate a Data Stream without the reception of the Trigger Command.
 *
 * @verbatim

   uint8_t * in_bytes =...
   uint32_t in_length =...

   Emulator emu(0);

   emu.HandleCommand(in_bytes,in_length);
   emu.ProcessQueue();

   uint8_t * out_bytes = emu.GetBytes();
   uin32_t out_length = emu.GetLength();

   @endverbatim
 *
 *
 * @brief RD53B Emulator
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class Emulator {

 public:

  Emulator(uint32_t chipid=0);

  ~Emulator();
  
  /**
    * Process byte stream of commands in the emulator.
    * The command will be first decoded by the Encoder,
    * and then interpreted by the emulator.
    * Commands are then placed in a command queue.
    * If the chip ID of the commands does not match the one
    * for this emulator, the command will be ignored.
    * Messages with ChipID>7 will be always interpreted.
    * @param recv_data Byte stream of commands.
    * @param recv_size Size of the byte stream.
    **/
  void HandleCommand(uint8_t *recv_data, uint32_t recv_size);

  /**
    * Process commands pending queue starting from the oldest received.
    * If the emulator is configured in auto-trigger mode through a configuration command.
    * It will result in the generation of data events.
    **/
  void ProcessQueue();
  
  /**
   * Enable the verbose mode
   * @param enable Enable verbose mode if true
   **/
  void SetVerbose(bool enable);

  /**
   * Get the output byte stream from the emulator.
   * @return Byte stream output from the emulator.
   **/
  uint8_t * GetBytes();
  
  /**
   * Get the length of the output byte stream from the emulator.
   * @return Length of the byte stream output from the emulator.
   **/
  uint32_t  GetLength();

 private:
  
  uint32_t m_chipid;
  bool m_verbose;
  uint32_t m_register_index;
  Decoder *m_decoder;
  Encoder *m_encoder;
  Configuration *m_config;
  uint32_t m_mon_frame_skip;
  std::mutex m_read_mutex;
  std::mutex m_reg_mutex;
  uint32_t m_l0id;
  uint32_t m_l1id;
  uint32_t m_nhits;
  std::queue<Command*> m_cmds;
  std::queue<uint32_t> m_read_reqs;
  uint32_t GetNextRegister();
  bool IsMsgForMe(uint32_t chipid);

};

}
#endif
