#ifndef RD53B_DIGITALSCAN_H
#define RD53B_DIGITALSCAN_H

#include "RD53BEmulator/Handler.h"
#include "RD53BEmulator/HitTree.h"
#include "TH2I.h"

namespace RD53B{

  /**
   * A DigitalScan injects a signal in the digital circuit of the pixel, and
   * checks for the corresponding hit in the data.
   * Because the output data bandwidth is limited, the injection is done in a
   * few pixels at a time.
   * The analysis of the digital scan checks that every single pixel has the
   * required number of hits.
   * If there is a discrepancy in the number of hits in a given set of pixels,
   * it is possible to generate an enable mask to ignore those pixels in
   * subsequent scans.
   * The digital injection pulse is ORed with the comparator output.
   * Therefore, the analog state must be below threshold, or the discriminator
   * must be off (bias current set to zero) for digital injection to work.
   * This is accomplished by setting the threshold to a high value
   * (SetGlobalThreshold(Pixel::XXX,500)) and enabling digital injection 
   * (Configuration::INJ_MODE_DIG=1).
   *
   * @brief RD53B DigitalScan
   * @author Carlos.Solans@cern.ch
   * @author Alessandra.Palazzo@cern.ch
   * @date June 2020
   */

  class DigitalScan: public Handler{

  public:

    /**
     * Create the scan
     */
    DigitalScan();

    /**
     * Delete the Handler
     */
    virtual ~DigitalScan();

    /**
     * Set the value of the variables for this scan, create histograms. 
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled, that is required
     * to have the same amount of entries or less than the number of triggers.
     */
    virtual void Run();

    /**
     * Write and delete histograms. 
     * Create the enable map, in which pixels have value 1 if the number
     * of hits is equal to the number of triggers, 0 otherwise. 
     * Pixels with enable=0 will be masked.
     */
    virtual void Analysis();


    /**
     * Return the quality of the scan 
     **/
    bool CheckComm();

    /**
     * Change digital scan parameters to CommMode
     **/
    void SetCommMode();


  private:
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;

    bool m_CommScan;
    std::map<std::string,std::vector<int>> m_mem_thres;
    std::map<std::string,TH2I*> m_occ;
    std::map<std::string,TH2I*> m_enable;
    std::map<std::string,TH1I*> m_tot;
    std::map<std::string,TH1I*> m_tid;
    std::map<std::string,TH1I*> m_ttag;
    std::map<std::string,TH1I*> m_bcid;  
    TH1I* m_Time;  
    TH1F* m_TimeNorm;  
    std::map<std::string,HitTree*> m_hits;

    std::map<std::string,double> m_Nhits;
    std::map<std::string,double> m_ExpectNhits;


  };

}

#endif
