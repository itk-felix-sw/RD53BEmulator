#ifndef RD53B_Sync_H
#define RD53B_Sync_H

#include "RD53BEmulator/Command.h"

namespace RD53B{

/**
 * Filler for the space between other commands.
 * Unlike other commands, the Sync does not have a symbol identifier
 * that is repeated twice. In this case the 16-bit frame content is
 * 0x817E, which corresponds to the space reserved for the symbol identifier
 * of other commands. The Sync command does not contain any data.
 *
 * Bit     |  0 |  7 |  8 | 15 |
 * ----    | -- | -- | -- | -- |
 * Byte    |  0     ||  1     ||
 * Payload |  0x81  ||  0x7E  ||
 *
 *
 * @brief RD53B Sync Command
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class Sync: public Command{
  
 public:
  
  /**
   * Create a Sync
   **/
  Sync();
  
  /**
   * Empty destructor just for completeness.
   **/
  ~Sync();
  
  /**
   * Return a clone of the current Command
   * @return A clone of the current Command
   **/
  Command * Clone();

  /**
   * Return a human readable representation of the symbol
   * @return The human readable representation of the symbol
   **/
  std::string ToString();

  /**
   * Parse a human readable representation of the command
   * @param str The human readable representation of the command
   * @return true if it was parsed correctly
   **/
  bool FromString(std::string str);

  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

 private:

  uint32_t m_symbol;

};

}
#endif
