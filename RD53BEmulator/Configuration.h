#ifndef RD53B_CONFIGURATION_H
#define RD53B_CONFIGURATION_H

#include "RD53BEmulator/Field.h"
#include "RD53BEmulator/Register.h"
#include <map>
#include <vector>
#include <string>

namespace RD53B{

  /**
   * The Configuration of the RD53B is contained in a Register table of 16-bits.
   * A Field is associated to a number of bits of a Register. 
   * The behavior of the RD53B is determined by the values of the registers.
   * The default value is the same one as on the real RD53B.
   * 
   * Each Register can be accessed (Configuration::GetRegister, Configuration::SetRegister),
   * and also each Field (Configuration::GetField). 
   * Note the index to access a given Field is given by Configuration::FieldType.
   *
   * @brief RD53B Configuration
   * @author Carlos.Solans@cern.ch
   * @date May 2020
   **/

  class Configuration{
	
  public:

    enum FieldType{
      PIX_PORTAL,           /**< Address  0, bits 15 to 0, default 0. Virtual register to access the pixel configuration. **/
      REGION_COL,           /**< Address  1, bits  7 to 0, default 0. Pixel column pair connected to the pixel portal. **/
      REGION_ROW,           /**< Address  2, bits  8 to 0, default 0. Pixel row conected to the pixel portal. **/
      PIX_BCAST_EN,         /**< Address  3, bit   2,      default 0. Pixel broadcast mode. 1=enabled. **/
      PIX_CONF_MODE,        /**< Address  3, bit   1,      default 1. Enable Pixel Mask (0) or TDAC (1) mode. **/
      PIX_AUTO_ROW,         /**< Address  3, bit   0,      default 0. Enable row number auto increment during configuration. **/
      PIX_DEFAULT_CONFIG,   /**< Address  4, bits 15 to 0, default 0. LSB of the default pixel config. 0x9CE2 to exit default config. **/
      PIX_DEFAULT_CONFIG_B, /**< Address  5, bits 15 to 0, default 0. MSB of the default pixel config. 0x631D to exit default config. **/
      GCR_DEFAULT_CONFIG,   /**< Address  6, bits 15 to 0, default 0. LSB of the default GCR config. 0xAC75 to exit default config. **/
      GCR_DEFAULT_CONFIG_B, /**< Address  7, bits 15 to 0, default 0. MSB of the default GCR config. 0x538A to exit default config. **/
      DAC_PREAMP_L_DIFF,    /**< Address  8, bits  9 to 0, default 50. Input transistor bias for left 2 cols. **/
      DAC_PREAMP_R_DIFF,    /**< Address  9, bits  9 to 0, default 50. Input transistor bias for right 2 cols. **/
      DAC_PREAMP_TL_DIFF,   /**< Address 10, bits  9 to 0, default 50. Input transistor bias for top left 2x2. **/
      DAC_PREAMP_TR_DIFF,   /**< Address 11, bits  9 to 0, default 50. Input transistor bias for top right 2x2. **/
      DAC_PREAMP_T_DIFF,    /**< Address 12, bits  9 to 0, default 50. Input transistor bias for top 2 rows. **/
      DAC_PREAMP_M_DIFF,    /**< Address 13, bits  9 to 0, default 50. Input transistor bias for all other pixels. **/
      DAC_PRECOMP_DIFF,     /**< Address 14, bits  9 to 0, default 50. Precomparator tail current bias. **/
      DAC_COMP_DIFF,        /**< Address 15, bits  9 to 0, default 50. Comparator total current bias. **/
      DAC_VFF_DIFF,         /**< Address 16, bits  9 to 0, default 100. Preamp feedback (return to baseline). **/
      DAC_TH1_L_DIFF,       /**< Address 17, bits  9 to 0, default 100. Negative Vth offset for left 2 cols. **/
      DAC_TH1_R_DIFF,       /**< Address 18, bits  9 to 0, default 100. Negative Vth offset for right 2 cols. **/ 
      DAC_TH1_M_DIFF,       /**< Address 19, bits  9 to 0, default 100. Negative Vth offset for all other pixels. **/
      DAC_TH2_DIFF,         /**< Address 20, bits  9 to 0, default 0. Positive Vth offset for all other pixels. **/
      DAC_LCC_DIFF,         /**< Address 21, bits  9 to 0, default 100. Leakage current compensation bias. **/
      EN_LCC_DIFF,          /**< Address 37, bit   1, default 0. Leakage current compensation enable. **/
      EN_CAP_FB_DIFF,       /**< Address 37, bit   0, default 0. Enable leakage current low gain mode. **/
      EN_SHUNT_ANA,         /**< Address 38, bit   9, default 0. Enable undershunt regulator for Analog voltage. **/ 
      EN_SHUNT_DIG,         /**< Address 38, bit   8, default 0. Enable undershunt regulator for Digital voltage. **/ 
      DAC_SHUNT_ANA,        /**< Address 38, bits  7 to 4, default 8. Undershunt regulator for Analog voltage trim value. **/ 
      DAC_SHUNT_DIG,        /**< Address 38, bits  3 to 0, default 8. Undershunt regulator for Digital voltage trim value. **/ 
      EnCoreCol_3,          /**< Address 39, bits  6, default 0. Enable Core Columns 53:48 **/
      EnCoreCol_2,          /**< Address 40, bits 16, default 0. Enable Core Columns 47:32 **/
      EnCoreCol_1,          /**< Address 41, bits 16, default 0. Enable Core Columns 31:16 **/
      EnCoreCol_0,          /**< Address 42, bits 16, default 0. Enable Core Columns 15:0 **/
      EnCoreColReset_3,     /**< Address 43, bits  6, default 0. Enable reset for Core Columns 53:48 **/
      EnCoreColReset_2,     /**< Address 44, bits 16, default 0. Enable reset for Core Columns 47:32 **/ 
      EnCoreColReset_1,     /**< Address 45, bits 16, default 0. Enable reset for Core Columns 31:16 **/
      EnCoreColReset_0,     /**< Address 46, bits 16, default 0. Enable reset for Core Columns 15:0 **/
      TwoLevelTrigger,      /**< Address 47, bit  10, default 0. Enable two level Trigger **/
      Latency,              /**< Address 47, bits  9 to 0, default 500. Trigger latency **/
      SelfTrigEn,           /**< Address 48, bit   5, default 0. Enable self trigger mode **/
      EnSelfTrigTot,        /**< Address 48, bit   4, default 1. Enable self trigger TOT mode **/
      SelfTrigTot,          /**< Address 48, bits  3 to 0, default 1. Self trigger TOT threshold **/
      SelfTrigDelay,        /**< Address 49, bits 14 to 5, default 100. Self trigger delay **/
      SelfTrigMult,         /**< Address 49, bits  4 to 0, default 1. Self trigger multiplier **/
      HitOrPatternLUT,      /**< Address 50, bits 15 to 0, default 0. HitOR logic program **/
      ReadTriggerColDelay,  /**< Address 51, bits 13 to 12, default 0. Column Read delay **/
      ReadTriggerClearTime, /**< Address 51, bits 11 to 0, default 1000. Read trigger decision time in BCs **/
      TruncationTimeoutConf,/**< Address 52, bits 11 to 0, default 0. Event truncation timeout in BCs (0=off) **/
      CalMode,              /**< Address 53, bit   7, default 0. Calibration injection mode (0=Ana, 1=Dig) **/ 
      CalAnaMode,           /**< Address 53, bit   6, default 0. Analog injection mode. **/
      CalFineDelay,         /**< Address 53, bits  5 to 0, default 0. Calibration fine delay **/
      CmdClockFineDelay,    /**< Address 54, bits 11 to 6, default 0. Clock fine delay. **/
      CmdDataFineDelay,     /**< Address 54, bits  5 to 0, default 0. Data fine delay. **/
      VCAL_HIGH,            /**< Address 55, bits 11 to 0, default 500. Calibration pulse high level voltage (DACs). **/
      VCAL_MED,             /**< Address 56, bits 11 to 0, default 500. Calibration pulse mdium level voltage (DACs). **/
      CapMeasEnPar,         /**< Address 57, bit 2, default 0. Enable par of cap meas (whatever this is). **/
      CapMeasPar,           /**< Address 57, bit 1, default 0. Enable cap meas (whatever this is) **/
      InjVcalRange,         /**< Address 57, bit 0, default 0. Vcal range bit **/
      OverwriteCdrLimit,    /**< Address 58, bit 4, default 0. CDR overwrite limit. **/
      CdrSelPd,             /**< Address 58, bit 3, default 0. CDR phase det sel. **/
      SER_CLK,              /**< Address 58, bits 2 to 0, default 0. Clock sel **/
      ChSyncConf,           /**< Address 59, bits 4 to 0, default 31. Channel sync lock threshold **/
      GlobalPulseConf,      /**< Address 60, bits 15 to 0, default 0. Global pulse route **/
      GlobalPulseWidth,     /**< Address 61, bits 7 to 0, default 0. Global pulse width. **/
      ServiceDataEnable,    /**< Address 62, bit 8, default 0. Service data block enable. **/
      ServiceDataPeriod,    /**< Address 62, bits 7 to 0, default 50. Service data periodicity (N_D) **/
      ToTEnable,            /**< Address 63, bit 12, default 0. Enable TOT **/
      ToAEnable,            /**< Address 63, bit 11, default 0. Enable TOA **/
      Enable80MHz,          /**< Address 63, bit 10, default 0. Enable 80 MHz clock **/
      Enable6b4b,           /**< Address 63, bit  9, default 0. Enable 6b to 4b **/
      ToTLatency,           /**< Address 63, bits  8 to 0, default 500. TOT latency **/
      PrecisionToTEnable_3, /**< Address 64, bits  5 to 0, default 0. Enable TOT for core columns 53:48 **/
      PrecisionToTEnable_2, /**< Address 65, bits 15 to 0, default 0. Enable TOT for core columns 47:32 **/
      PrecisionToTEnable_1, /**< Address 66, bits 15 to 0, default 0. Enable TOT for core columns 31:16 **/
      PrecisionToTEnable_0, /**< Address 67, bits 15 to 0, default 0. Enable TOT for core columns 15:0 **/
      DataMergePol,         /**< Address 68, bits 11 to 8, default 0. Data merge in pol (whatever it is) **/         
      EnChnId,              /**< Address 68, bit 7, default 0. Enable channel id for data merging **/
      DataMergeEnClkGate,   /**< Address 68, bit 6, default 0. Enable clock gate for data merging **/
      DataMergeSelClk,      /**< Address 68, bit 5, default 0. Select clock for data merging **/
      DataMergeEn,          /**< Address 68, bits 4 to 1, default 0. Enable data merging **/
      ChnBond,              /**< Address 68, bit 0, default 0. Enable channel bonding **/
      DataMergeInMux_3,     /**< Address 69, bits 15 to 14, default 3. Input lane mapping **/
      DataMergeInMux_2,     /**< Address 69, bits 13 to 12, default 2. Input lane mapping **/
      DataMergeInMux_1,     /**< Address 69, bits 11 to 10, default 1. Input lane mapping **/
      DataMergeInMux_0,     /**< Address 69, bits  9 to  8, default 0. Input lane mapping **/
      DataMergeOutMux_3,    /**< Address 69, bits  7 to  6, default 0. Output lane mapping **/
      DataMergeOutMux_2,    /**< Address 69, bits  5 to  4, default 1. Output lane mapping **/
      DataMergeOutMux_1,    /**< Address 69, bits  3 to  2, default 2. Output lane mapping **/
      DataMergeOutMux_0,    /**< Address 69, bits  1 to  0, default 3. Output lane mapping **/
      EnCoreColCal_3,       /**< Address 70, bits  5 to  0, default 0. Enable calibration pulse for core columns 53:48. **/
      EnCoreColCal_2,       /**< Address 71, bits  5 to  0, default 0. Enable calibration pulse for core columns 47:32. **/
      EnCoreColCal_1,       /**< Address 72, bits  5 to  0, default 0. Enable calibration pulse for core columns 31:16. **/
      EnCoreColCal_0,       /**< Address 73, bits  5 to  0, default 0. Enable calibration pulse for core columns 15:0.  **/
      EnBCID,               /**< Address 74, bit  10, default 0. Enable BCID in data stream. **/
      EnL1ID,               /**< Address 74, bit   9, default 0. Enable L1ID in data stream. **/
      EnEOS,                /**< Address 74, bit   8, default 1. Enable End Of Stream bit in data stream. **/
      NumOfEventsInStream,  /**< Address 74, bits  7 to  0, default 16. Number of events in stream. **/
      DropToT,              /**< Address 75, bits 10 to  0, default 0. Enable binary read-out (drop the TOT). **/
      EnRawMap,             /**< Address 75, bit   9 to  0, default 0. Enable raw map mode (no compression). **/
      EnHitRemoval,         /**< Address 75, bit   8 to  0, default 0. Enable hit removal. **/
      MaxHits,              /**< Address 75, bits  7 to  4, default 0. Max hits per event. **/
      EnIsoHitRemoval,      /**< Address 75, bit   3 to  0, default 0. Enable isolated hit removal. **/
      MaxToT,               /**< Address 75, bits  2 to  0, default 0. Enable binary read-out (drop the TOT). **/
      EvenMask,             /**< Address 76, bits 15 to  0, default 0. Isolated hit filter mask for even columns. **/
      OddMask,              /**< Address 77, bits 15 to  0, default 0. Isolated hit filter mask for odd columns. **/
      EfuseConfig,          /**< Address 78, bits 15 to  0, default 0. Enable Efuses (0x0F0F=read, 0xF0F0=write). **/
      EfuseWriteData1,      /**< Address 79, bits 15 to  0, default 0. Data to be written to Efuses (1 of 2). **/
      EfuseWriteData0,      /**< Address 80, bits 15 to  0, default 0. Data to be written to Efuses (2 of 2). **/
      AURORA_EN_PRBS,       /**< Address 81, bit  12,       default 0. Enable PRBS (Pseudo Random Bit Stream) in the Aurora block. **/
      AURORA_EN_LANES,      /**< Address 81, bits 11 to  8, default 1. Number of enabled Aurora lanes **/
      AURORA_CC_WAIT,       /**< Address 81, bits  7 to  2, default 25. Aurora CC wait **/
      AURORA_CC_SEND,       /**< Address 81, bits  1 to  0, default 3. Aurora CC send **/
      AURORA_CB_WAIT1,      /**< Address 82, bits  7 to  0, default 255. Aurora Channel Bonding wait (bits 19:12). **/
      AURORA_CB_WAIT0,      /**< Address 83, bits 15 to  4, default 4095. Aurora Channel Bonding wait (bits 11:0). **/
      AURORA_CB_SEND,       /**< Address 83, bits  3 to  0, default 0. Aurora Channel Bonding send **/
      AURORA_INIT_WAIT,     /**< Address 84, bits 10 to  0, default 32. Aurora initialization delay (units?) **/
      GP_VAL_REG,           /**< Address 85, bits 12 to  9, default 5. Global Pulse pattern **/
      GP_CMOS_EN,           /**< Address 85, bit   8, default 1. Global Pulse CMOS enable **/
      GP_CMOS_DS,           /**< Address 85, bit   7,       default 0. Global Pulse CMOS DS **/
      GP_LVDS_EN,           /**< Address 85, bits  6 to  3, default 15. Global Pulse LVDS enable **/
      GP_LVDS_BIAS,         /**< Address 85, bits  2 to  0, default 7. Global Pulse LVDS strength **/
      GP_CMOS_ROUTE,        /**< Address 86, bits  5 to  0, default 34. Route for the CMOS global pulse **/ 
      GP_LVDS_PAD_3,        /**< Address 87, bits 11 to  6, default 35. Global Pulse LVDS MUX select 3 **/
      GP_LVDS_PAD_2,        /**< Address 87, bits  5 to  0, default 33. Global Pulse LVDS MUX select 2 **/
      GP_LVDS_PAD_1,        /**< Address 88, bits 11 to  6, default 1. Global Pulse LVDS MUX select 1 **/
      GP_LVDS_PAD_0,        /**< Address 88, bits  5 to  0, default 0. Global Pulse LVDS MUX select 0 **/
      DAC_CP_CDR,           /**< Address 89, bits  9 to  0, default 40. CDR CP Bias (values smaller than 15 are set to 15) **/
      DAC_CP_FD_CDR,        /**< Address 90, bits  9 to  0, default 400. CDR FD CP Bias (values smaller than 100 are set to 100) **/
      DAC_CP_BUFF_CDR,      /**< Address 91, bits  9 to  0, default 200. CDR unity gain buffer bias **/
      DAC_VCO_CDR,          /**< Address 92, bits  9 to  0, default 1023. CDR VCO Bias (values smaller than 700 are set to 700) **/
      DAC_VCOBUFF_CDR,      /**< Address 93, bits  9 to  0, default 500. CDR VCO Buffer Bias (values smaller than 200 are set to 200) **/
      SER_SEL_OUT_3,        /**< Address 94, bits  7 to  6, default 1. CML 3 output. 0=CK, 1=Aurora, 2=PRBS6, 3=0 **/
      SER_SEL_OUT_2,        /**< Address 94, bits  5 to  4, default 1. CML 2 output. 0=CK, 1=Aurora, 2=PRBS6, 3=0 **/
      SER_SEL_OUT_1,        /**< Address 94, bits  3 to  2, default 1. CML 1 output. 0=CK, 1=Aurora, 2=PRBS6, 3=0 **/
      SER_SEL_OUT_0,        /**< Address 94, bits  1 to  0, default 1. CML 0 output. 0=CK, 1=Aurora, 2=PRBS6, 3=0 **/
      SER_INV_TAP,          /**< Address 95, bits  7 to  6, default 0. Invert tap on CML output **/
      SER_EN_TAP,           /**< Address 95, bits  5 to  4, default 0. Enable tap on CML output **/
      SER_EN_LANE,          /**< Address 95, bits  3 to  0, default 1. Enable lane (3,2,1,0) **/
      DAC_CML_BIAS_2,       /**< Address 96, bits  9 to  0, default 0. CML driver tap 2 amplitude (pre-emphasis) **/ 
      DAC_CML_BIAS_1,       /**< Address 97, bits  9 to  0, default 0. CML driver tap 1 amplitude (pre-emphasis) **/
      DAC_CML_BIAS_0,       /**< Address 98, bits  9 to  0, default 0. CML driver tap 0 amplitude (main) **/
      MonitorPinEn,         /**< Address 99, bit  12,       default 0.  Enable monitor pin **/
      MonitorIMuxSel,       /**< Address 99, bits 11 to  6, default 63. Monitor current MUX select **/ 
      MonitorVMuxSel,       /**< Address 99, bits  5 to  0, default 63. Monitor voltage MUX select **/
      ErrWngMask,           /**< Address 100, bits  7 to  0, default 0. Error and Warning Message disable Mask **/
      MON_SENS_SLDO_DIG_EN,  /**< Address 101, bit  11,       default 0. Enable monitoring for analog shunt LDO analog **/
      MON_SENS_SLDO_DIG_DEM, /**< Address 101, bits 10 to  7, default 0. Enable DEM for analog shunt LDO analog **/
      MON_SENS_SLDO_DIG_BIAS,/**< Address 101, bit   6,       default 0. Enable bias for shunt LDO digital sensing **/
      MON_SENS_SLDO_ANA_EN,  /**< Address 101, bit   5,       default 0. Enable monitoring for analog shunt LDO analog **/
      MON_SENS_SLDO_ANA_DEM, /**< Address 101, bits  4 to  1, default 0. Enable DEM for analog shunt LDO analog **/
      MON_SENS_SLDO_ANA_BIAS,/**< Address 101, bit   0,       default 0. Enable bias for shunt LDO analog sensing **/
      MON_SENS_ACB_EN,      /**< Address 102, bit   5,       default 0. Enable monitoring for Tsense center **/
      MON_SENS_ACB_DEM,     /**< Address 102, bits  4 to  1, default 0. Enable DEM for Tsense center **/
      MON_SENS_ACB_BIAS,    /**< Address 102, bit   0,       default 0. Enable bias for Tsense center **/
      VREF_RSENSE_BOT,      /**< Address 103, bit   8,       default 0. Enable reference voltage for resistance monitoring at the bottom **/
      VREF_RSENSE_TOP,      /**< Address 103, bit   7,       default 0. Enable reference voltage for resistance monitoring at the top **/
      VREF_IN,              /**< Address 103, bit   6,       default 1. Enable reference voltage **/
      MON_ADC_TRIM,         /**< Address 103, bits  5 to  0, default 0. Monitoring ADC trim bits **/
      DAC_NTC,              /**< Address 104, bits  9 to  0, default 100. Current output DAC for external NTC **/
      HITOR_MASK_3,         /**< Address 105, bits  5 to  0, default 0. HitOR enable for core columns 53:48. **/
      HITOR_MASK_2,         /**< Address 106, bits 15 to  0, default 0. HitOR enable for core columns 47:32. **/
      HITOR_MASK_1,         /**< Address 107, bits 15 to  0, default 0. HitOR enable for core columns 31:16. **/
      HITOR_MASK_0,         /**< Address 108, bits 15 to  0, default 0. HitOR enable for core columns 15:0. **/
      AutoRead0,            /**< Address 109, bits  8 to  0, default 137. Auto-Read register address A for lane 0. **/
      AutoRead1,            /**< Address 110, bits  8 to  0, default 133. Auto-Read register address B for lane 0. **/
      AutoRead2,            /**< Address 111, bits  8 to  0, default 121. Auto-Read register address A for lane 1. **/
      AutoRead3,            /**< Address 112, bits  8 to  0, default 122. Auto-Read register address B for lane 1. **/
      AutoRead4,            /**< Address 113, bits  8 to  0, default 124. Auto-Read register address A for lane 2. **/
      AutoRead5,            /**< Address 114, bits  8 to  0, default 127. Auto-Read register address B for lane 2. **/
      AutoRead6,            /**< Address 115, bits  8 to  0, default 126. Auto-Read register address A for lane 3. **/
      AutoRead7,            /**< Address 116, bits  8 to  0, default 125. Auto-Read register address B for lane 3. **/
      RING_OSC_B_CLR,       /**< Address 117, bit  14,       default 0. Ring oscillator B clear. **/
      RING_OSC_B_LEFT_EN,   /**< Address 117, bit  13,       default 0. Ring oscillator B enable left. **/
      RING_OSC_B_RIGHT_EN,  /**< Address 117, bit  12,       default 0. Ring oscillator B enable right. **/
      RING_OSC_B_CAPA_EN,   /**< Address 117, bit  11,       default 0. Ring oscillator B enable CAPA (whatever it is). **/
      RING_OSC_B_FF_EN,     /**< Address 117, bit  10,       default 0. Ring oscillator B enable FF. **/
      RING_OSC_B_LVT_EN,    /**< Address 117, bit   9,       default 0. Ring oscillator B enable LVT. **/
      RING_OSC_A_CLR,       /**< Address 117, bit   8,       default 0. Ring oscillator A clear. **/
      RING_OSC_A_EN,        /**< Address 117, bits  7 to  0, default 0. Ring oscillator A enable bits. **/
      RING_OSC_A_ROUTE,     /**< Address 118, bits  8 to  6, default 0. Ring oscillator A route. **/
      RING_OSC_B_ROUTE,     /**< Address 118, bits  5 to  0, default 0. Ring oscillator B route. **/
      RING_OSC_A_OUT,       /**< Address 119, bits 15 to  0, default 0. Ring oscillator A output. **/
      RING_OSC_B_OUT,       /**< Address 120, bits 15 to  0, default 0. Ring oscillator B output. **/
      BCIDCnt,              /**< Address 121, bits 15 to  0, default 0. Bunch Counter ID counter. **/
      TrigCnt,              /**< Address 122, bits 15 to  0, default 0. Counter of number triggers received. **/
      ReadTrigCnt,          /**< Address 123, bits 15 to  0, default 0. Counter of number internal triggers received. **/
      LockLossCnt,          /**< Address 124, bits 15 to  0, default 0. Channel Sync lost lock counter. **/
      BitFlipWngCnt,        /**< Address 125, bits 15 to  0, default 0. Bit Flip Warning counter. **/
      BitFlipErrCnt,        /**< Address 126, bits 15 to  0, default 0. Bit Flip Error counter. **/
      CmdErrCnt,            /**< Address 127, bits 15 to  0, default 0. Command Decoder error message counter. **/
      RdWrFifoErrCnt,       /**< Address 128, bits 15 to  0, default 0. Writes and Reads when FIFO was full counter. **/
      AI_REGION_ROW,        /**< Address 129, bits 15 to  0, default 0. Auto Increment current row value. **/
      HITOR_3_CNT,          /**< Address 130, bits 15 to  0, default 0. Counter of HitOr_3. **/
      HITOR_2_CNT,          /**< Address 131, bits 15 to  0, default 0. Counter of HitOr_2. **/
      HITOR_1_CNT,          /**< Address 132, bits 15 to  0, default 0. Counter of HitOr_1. **/
      HITOR_0_CNT,          /**< Address 133, bits 15 to  0, default 0. Counter of HitOr_0. **/
      SkippedTriggerCnt,    /**< Address 134, bits 15 to  0, default 0. Skipped Trigger counter. **/
      EfusesReadData1,      /**< Address 135, bits 15 to  0, default 0. Read-back of efuses 1 of 2. **/
      EfusesReadData0,      /**< Address 136, bits 15 to  0, default 0. Read-back of efuses 2 of 2. **/
      MonitoringDataADC,    /**< Address 137, bits 15 to  0, default 0. ADC value. **/
    };


    enum Registers{
      ADDR_PIX_PORTAL = 0,
      ADDR_REGION_COL = 1,
      ADDR_REGION_ROW = 2,

      ADDR_DAC_TH1_L_DIFF = 17,
      ADDR_DAC_TH1_R_DIFF = 18,
      ADDR_DAC_TH1_M_DIFF = 19,
      ADDR_DAC_TH2_DIFF = 20,

      ADDR_EnCoreCol_3 = 39,
      ADDR_EnCoreCol_2 = 40,
      ADDR_EnCoreCol_1 = 41,
      ADDR_EnCoreCol_0 = 42,
      ADDR_EnCoreColReset_3 = 43,
      ADDR_EnCoreColReset_2 = 44,
      ADDR_EnCoreColReset_1 = 45,
      ADDR_EnCoreColReset_0 = 46,

      ADDR_GCR_DEFAULT_CONFIG = 6,
      ADDR_GCR_DEFAULT_CONFIG_B = 7 ,
      ADDR_GlobalPulseConf = 60,
      ADDR_GlobalPulseWidth = 61,
      ADDR_MonitoringDataADC = 137
    };


    static const uint32_t IMUX_NTC_PAD_CURRENT = 9;
    static const uint32_t IMUX_HIGHZ = 63;
    static const uint32_t VMUX_PAD_VOLTAGE = 37; //BM: was 1 before
    static const uint32_t VMUX_NTC_PAD_VOLTAGE = 37; //BM: was 2 before
    static const uint32_t NUM_REGISTERS = 138;
    static const uint32_t REGISTER0 = 3;

    /**
     * Create a new configuration object. Initialize the fields to their default values.
     **/
    Configuration();

    /**
     * Coppy of configuration object
     **/  
    Configuration( const Configuration & obj);
    
    /**
     * Clear internal arrays
     **/
    ~Configuration();

    /**
     * Enable the verbose mode
     * @param enable Enable the verbose mode if true
     */
    void SetVerbose(bool enable);

    /**
     * Get the number of registers in the configuration
     * @return The configuration size
     **/
    uint32_t Size();

    /**
     * Get the register object corresponsing to the given index
     * @param index Register index
     * @return The register object
     **/
    Register * GetRegister(uint32_t index);

    /**
     * Get the whole 16-bit value of a register
     * @param index Register index
     * @param update force the update of the register. Default false.
     * @return The 16-bit register value
     **/
    uint16_t GetRegisterValue(uint32_t index, bool update);

    /**
     * Get the whole 16-bit value of a register without updating, this is defined as const
     * @param index Register index
     * @return The 16-bit register value
     **/
    uint16_t GetRegisterValue(uint32_t index) const;

    /**
     * Set the whole 16-bit value of a register
     * @param index Register index
     * @param value The 16-bit register value
     **/
    void SetRegisterValue(uint32_t index, uint16_t value);

    /**
     * Set the Field value from a string. Note it is not necessarily the whole register
     * @param name Field name
     * @param value The new value
     **/
    void SetField(std::string name, uint16_t value);

    /**
     * Set the Field value from a string. Note it is not necessarily the whole register
     * @param index Field index
     * @param value The new value
     **/
    void SetField(uint32_t index, uint16_t value);

    /**
     * Get a Field inside the Configuration given
     * its Field index (ie: Configuration::GateHitOr).
     * @param index The Field index (Configuration::FieldType)
     * @return pointer to the field
     **/
    Field * GetField(uint32_t index);

    /**
     * Get a Field inside the Configuration by name
     * @param name The Field name (Configuration::m_name2index)
     * @return pointer to the field
     **/
    Field * GetField(std::string name);

    /**
     * Get list of addresses that have been updated
     * @return vector of addresses that have been updated
     **/
    std::vector<uint32_t> GetUpdatedRegisters();

    /**
     * Get map of addresses
     * @return map of strings to addresses
     **/
    std::map<std::string,uint32_t> GetRegisters();

    /**
     * Get map of addresses
     * @return map of strings to addresses
     **/
    void SetRegisters(std::map<std::string,uint32_t> config);

    /**
     * Make a back-up copy of the registers.
     * This is called by the constructor and contains the default values
     * The back-up is restored by calling Configuration::Restore().
     **/
    void Backup();

    /**
     * Restore the back-up copy of the registers.
     * This updates the update flag of the registers.
     * Then the registers that have been updated can be retrieved with Configuration::GetUpdatedRegisters().
     * The back-up can be made anytime by calling Configuration::Backup().
     **/
    void Restore();

  private:

    bool m_verbose;
    std::vector<uint16_t> m_backup;
    std::vector<Register> m_registers;
    std::map<uint32_t, Field*> m_fields;
    static std::map<std::string, uint32_t> m_name2index; //Temproraly made this non static as it needs to be filled
  
  };

}

#endif
