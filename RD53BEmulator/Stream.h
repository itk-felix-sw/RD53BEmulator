#ifndef RD53B_STREAM_H
#define RD53B_STREAM_H

#include <cstdint>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <utility>
#include "RD53BEmulator/HitMap.h"

namespace RD53B{

/**
 * A Data Stream is a variable length bit Stream made out of one or many 64-bit Aurora Frames that contains event Data.
 * This stream plus Idle frames are multiplexed with the Service stream every Number of Data (ND) Aurora frames.
 * There is always one Service frame every ND Data or Idle frames.
 *
 * The RD53B data always begins with a New-Stream (NS=1) bit, followed by an 8-bit tag, and zero or many hit data blocks,
 * each one followed by a TOT block if enabled, and, if the Number of Events (NE) per stream is greater than one (NE>1),
 * is followed by further events with the same structure up to NE, starting by a 3-bit new-tag identifier (0b111).
 * Finally, the stream is terminated, only if enabled, by a 6-bit End Of Stream (EOS) marker (0b000000).
 * Additional padding zeroes will be added at the end of the stream up to the next multiple of 64-bit,
 * which is the length of the underlying Aurora frame. For every triggered event, there will be a Stream
 * containing the event tag, even if there are no hits, and the TOT fields are sorted in increasing row (top to down!),
 * and increasing column index (left to right). In the case one Stream spans multiple Aurora frames,
 * the subsequent frames start with NS=0.
 *
 * The hit data is encoded and compressed per core-column (ccol) and quarter-core-row (qrow).
 * An optional 6-bit ccol identifier is followed by a 1-bit is-last marker, a 1-bit is-neighbor marker,
 * an optional 8-bit qrow identifier, and the encoded and compressed hit map.
 * The ccol addresses range from 1 to 55 (0b110111), thus a ccol address of 0b000000 corresponds to the EOS marker,
 * and a ccol larger than 56 (0b111000) corresponds to a new-tag identifier.
 * The first hit in the data always contains the ccol identifier, but if the is-last marker is zero,
 * the next ccol identifier is suppressed, meaning that there are more hits from the same core-column in the event.
 * The is-neighbor marker is set for contiguous qrows, if the is-neighbor bit is set qrow identifier is
 * suppressed, it indicates that the next hit belongs to the next qrow.
 *
 * The hit map represents a 8x2 pixel region per core-column and quarter-core-row.
 * The hit map is encoded in a breadth first binary tree that enumerates the elements from left to right, and top to bottom.
 * First the region is split in halves (left to right), then it is split in halves again (top, bottom), finally
 * in halves again (left to right). The smallest element are 2 adjacent pixels. For each split of the binary tree,
 * a 2-bit identifier is used to mark which of the two halves has a hit, including the 2 adjacent pixels.
 * The hit map is encoded in the following way: A 2-bit marker for step 1, up to 2 2-bit markers for step 2,
 * up to 4 2-bit markers for step 3, and up to 8 2-bit markers for the pixel maps.
 * Therefore the maximum number of bits that the hit map uses is 30.
 * The resulting bit pattern of the hit map is compressed in the byte stream replacing 0b01 bit pattern by 0b0.
 *
 * Field      | Value     | Followed by                       | Decided by
 * -----      | -----     | -----------                       | ----------
 * NS bit     | 0         | anything                          | data
 * NS bit     | 1         | Tag[8] OR Chip ID[2] then tag[8]  | configuration
 * Tag        | 0-251     | Tag[11] OR ccol[6] OR orphan bits | data
 * Tag        | 251, 252  | 32 bits of metadata               | always
 * Tag        | 253       | Tag[11] OR ccol[6] OR orphan bits | data 
 * Tag        | 254       | Tag[11] OR orphan bits            | data
 * Tag        | 255       | Tag[11] OR orphan bits            | config and data
 * ccol       | 1-55      | islast[1] then isneighbor[1]      | always
 * isneighbor | 0         | qrow[8]                           | always
 * isneighbor | 1         | compressed map[var] OR map[16]    | configuration
 * qrow       | 0-193     | compressed map[var] OR map[16]    | configuration
 * map        | any       | ToT[Nhit x 4] OR lines below      | configuration
 * ToT(islast)| any(0)    | qrow[6]                           | always
 * ToT(islast)| any(1)    | Tag[11] OR ccol[6] OR orphan bits | data
 *
 *
 * The Stream class is a tool to encode (Stream::Pack) and decode (Stream::UnPack) the Stream
 * (byte array) contained in a Data Frame array.
 * The Stream does not handle the NS and channel ID bits at the beginning of the Data Frame.
 * A bit array is decoded into hits (Stream::GetTag, Stream::GetQcol, Stream::GetQrow, Stream::GetHitMap, Stream::GetTotMap),
 * and encoded from them (Stream::AddHit).
 *
 * Additional settings include dropping the TOT (Stream::SetTot), and the hitmap encoding (Stream::SetEncoding).
 *
 * @verbatim

   Stream stream;
   
   //decode a byte stream
   uint8_t bytes = ...;

   //decode bits
   stream.UnPack(bytes);

   @endverbatim
 *
 *
 * @brief RD53B Data Stream
 * @author Carlos.Solans@cern.ch
 * @author Ismet.Siral@cern.ch
 * @date May 2020
 **/

class Stream{
  
 public:

  /**
   * Create the look-up-tables as static objects
   **/
  Stream();

  /**
   * Clear internal memory, except the look-up-tables
   **/
  ~Stream();

  /**
   * Clear internal memory
   **/
   void Clear();

   /**
   * Return a human readable representation of the Stream
   * @return The human readable representation of the Stream
   **/
  std::string ToString();

  /**
   * Extract the contents of the Stream from the byte array
   * @param bytes pointer to the bit array
   * @param maxlen maximum number of bytes available
   * @return the number of bits processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t num_bytes);
 

  /**
   * Write the contents of the Stream to a byte array
   * @param bytes pointer to the byte array
   * @return the number of bits processed
   **/
  uint32_t Pack(uint8_t * bytes);
  
  /**
   * Enable or disable the ChipID at the beginning of each frame
   * @param enable Enable the ChipID if true
   **/
  void EnableChipId(bool enable);

  /**
   * Return if the ChipID at the beginning of each frame is enabled
   * @return true if the ChipID is enabled
   **/
  bool HasChipId();

  /**
   * Enable or disable the TOT at the end of the event
   * @param enable Enable the TOT if true
   **/
  void EnableTot(bool enable);

  /**
   * Return if the TOT at the end of the event is enabled or disabled
   * @return true if the TOT is enabled
   **/
  bool HasTot();

  /**
   * Enable or disable the encoding of the hit maps.
   * @param enable Enable the encoding if true
   **/
  void EnableEnc(bool enable);

  /**
   * Return if the encoding of the hit maps is enabled or disabled
   * @return true if the encoding is enabled
   **/
  bool HasEnc();

  /**
   * Add a hit to the stream
   * @param tag tag
   * @param qcol quarter-core-column
   * @param qrow quarter-core-row
   * @param hitmap the hit map
   * @param totmap the TOT map
   **/
  void AddHit(uint32_t tag, uint32_t qcol, uint32_t qrow, uint32_t hitmap, uint64_t totmap);

  /**
   * Get the number of hits in this Stream
   * @return the number of hits in the Stream
   **/
  uint32_t GetNhits();

  /**
   * Get the tag of the given hit
   * @param hit the hit index
   * @return the tag of the hit
   **/
  uint32_t GetTag(uint32_t hit);

  /**
   * Get the tag of the Stream header
   * @return the tag of the header
   **/
  uint32_t GetStreamTag();

  /**
   * Set the tag of the Stream header
   * @param tag the tag of the header
   **/
  void SetStreamTag(uint32_t tag);

  /**
   * Get the quarter-core-column of the given hit
   * @param hit the hit index
   * @return the quarter-core-column of the hit
   **/
  uint32_t GetQcol(uint32_t hit);

  /**
   * Get the quarter-core-row of the given hit
   * @param hit the hit index
   * @return the quarter-core-row of the hit
   **/
  uint32_t GetQrow(uint32_t hit);

  /**
   * Get the hit-map of the given hit
   * @param hit the hit index
   * @return the hit-map of the hit
   **/
  uint32_t GetHitMap(uint32_t hit);

  /**
   * Get the tot-map of the given hit
   * @param hit the hit index
   * @return the tot-map of the hit
   **/
  uint64_t GetTotMap(uint32_t hit);

  /**
   * Get the human readable representation of a given hit
   * @param hit the hit index
   * @return the human readable representation of a given hit
   **/
  std::string GetHitString(uint32_t hit);

  /**
   * Set the verbose mode
   * @param enable Enable the verbose mode if true
   */
  void SetVerbose(bool enable);
 

 protected:

  bool m_verbose;
  bool m_enable_tot;
  bool m_enable_enc;
  bool m_enable_cid;
  std::vector<uint32_t> m_tags;
  std::vector<uint32_t> m_qcols;
  std::vector<uint32_t> m_qrows;
  std::vector<uint32_t> m_hitmaps;
  std::vector<uint64_t> m_totmaps;
  std::vector<uint32_t> m_islast;
  std::vector<uint32_t> m_isneigbor;
  uint32_t m_tag;
  HitMap m_hm;

  /**
   * Write part of a 64-bit integer into a byte array
   * @param bytes pointer to the byte array
   * @param pos first bit to start writing
   * @param value the value to write
   * @return the position in the byte array after writing
   **/
  uint32_t writeBytes(uint8_t * bytes, uint32_t pos, uint64_t value, uint32_t len);

  /**
   * Write a bit into a byte array
   * @param bytes pointer to the byte array
   * @param pos first bit to start writing
   * @param value the value to write
   * @return the position in the byte array after writing
   **/
  uint32_t writeBytes(uint8_t * bytes, uint32_t pos, bool value);

  /**
   * Read a 64-bit integer from a byte array
   * @param bytes pointer to the byte array
   * @param pos first bit to start reading
   * @param value reference to the 64-bit integer that will be filled starting from MSB
   * @param len number of bits to write counting from the LSB
   * @return the position in the byte array after reading
   **/
  uint32_t readBytes(uint8_t * bytes, uint32_t pos, uint64_t &value, uint32_t len=1);

  /**
   * Read a 32-bit integer from a byte array
   * @param bytes pointer to the byte array
   * @param pos first bit to start reading
   * @param value reference to the 32-bit integer that will be filled starting from MSB
   * @param len number of bits to write counting from the LSB
   * @return the position in the byte array after reading
   **/
  uint32_t readBytes(uint8_t * bytes, uint32_t pos, uint32_t &value, uint32_t len=1);

  /**
   * Read a boolean from a byte array
   * @param bytes pointer to the byte array
   * @param pos first bit to start reading
   * @param value reference to the boolean that will be filled starting from MSB
   * @param len number of bits to write counting from the LSB
   * @return the position in the byte array after reading
   **/
  uint32_t readBytes(uint8_t * bytes, uint32_t pos, bool &value);


};

}

#endif 
