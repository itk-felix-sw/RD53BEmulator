#ifndef RD53B_HANDLER_H
#define RD53B_HANDLER_H

#include "RD53BEmulator/FrontEnd.h"
#include "RD53BEmulator/Masker.h"

#include <vector>
#include <cstdint>
#include <string>
#include <thread>
#include <fstream>
#include <felix_proxy/ClientThread.h>

class TH1I;
class TH2I;
class TFile;

namespace RD53B{

  class RunNumber;

  /**
   * A Handler is a tool to communicate with a FrontEnd through NETIO.
   * The configuration requires a mapping file, that contains a list of
   * front-ends described by the following:
   *
   * | Variable |   Type | Description                              |
   * | -------- |   ---- | -----------                              |
   * |     name | string | Front-end position (A_BM_01_1)           |
   * |   config | string | JSON configuration file (A_BM_01_1.json) |
   * |       rx |    int | Data e-link                              |
   * |       tx |    int | Command e-link                           |
   * |     host | string | FELIX host                               |
   * | cmd_port |    int | FELIX command port                       |
   * | data_port|    int | FELIX data port                          |
   *
   * The Handler allows to add any combination of front-ends (Handler::AddFE).
   * The configuration of each FrontEnd will be read out from the file
   * specified in the mapping file.
   * And retrieved from a search path that built by the following directories 
   * in decreasing order of priority, the tuned version of the configuration 
   * files is preferred, which is searched in the tuned folder relative to the
   * current working directory.
   *
   *  - Current working directory
   *  - ITK_PATH environment variable
   *  - share/data/config in the installed directory

   *
   * It is necessary to connect to FELIX (Handler::Connect) once all the 
   * FrontEnd objects have been added.
   * Once connected, all the FrontEnd objects can be configured 
   * (Handler::Config).
   * This will also create an output ROOT file that can contain the results of
   * the scan.
   * Finally the run method (Handler::Run) implements the specific scan
   * procedure.
   * At the end of the scan, the output of each FrontEnd can be saved to the
   * output directory. This includes any ROOT histograms created during the 
   * scan, that are written to the output ROOT file.
   * The output data path is defined by the ITK_DATA_PATH, and can be
   * overwritten by the user (Handler::SetOutPath).
   * This path contains
   *
   *  - The tuned configuration file (A_BM_01_1.json)
   *  - The results ROOT file (output.root)
   *  - The metadata file (metadata.txt)
   *
   * An example on how to use the Handler class is the following:
   *
   * @verbatim

   Handler * handler = new Handler();

   handler->SetVerbose(true);
   handler->SetContext("posix");
   handler->SetInterface("eth0");
   handler->SetMapping("mapping.json");

   handler->AddFE("A_BM_01_1");
   handler->AddFE("A_BM_01_2");
   handler->AddFE("A_BM_01_3");
   handler->AddFE("A_BM_01_4");

   //Create the output directory
   handler->InitRun();

   //Connect to FELIX
   handler->Connect();

   //Configure the front-ends
   handler->Config();

   //Set the specific configuration for the run
   handler->PreRun();

   //Save the configuration at the beginning of the run
   handler->SaveConfig("before");

   //Run the Handler
   handler->Run();

   //Perform the analysis
   handler->Analysis();

   //Disconnect from FELIX
   handler->Disconnect();

   //Save the results and config files
   handler->Save();

   delete handler;
   @endverbatim
   *
   * @brief RD53B Handler
   * @author Carlos.Solans@cern.ch
   * @author Enrico.Junior.Schioppa@cern.ch
   * @author Alessandra.Palazzo@cern.ch
   & @author ismet.siral@cern.ch
   * @date June 2020
   */

  class Handler{

  public:

    /**
     * Create an empty Handler
     */
    Handler();

    /**
     * Delete the Handler
     */
    virtual ~Handler();

    /**
     * Enable the verbose mode
     * @param enable Enable the verbose mode if true
     */
    void SetVerbose(bool enable);

    /**
     * Set the netio::context as a string
     * @param context Back-end for the netio communication: posix or rdma
     */
    void SetContext(std::string context);

    /**
     * Set the network interface to use
     * @param interface The network interface to use.
     */
    void SetInterface(std::string interface);

    /**
     * Set the network connectivity bus from FELIX-Client
     * @param interface The network folder from FELIX-Client
     */
    void SetBusPath(std::string BusFolder);


    /**
     * Load a connectivity map file to the Handler. Structure should be the
     * following:
     *
     * - mapping : List of FrontEnd configurations
     *   - []
     *     - name : Chip name (A_BM_05_1)
     *     - config : Relative path to the config file (A_BM_05_1.json)
     *     - rx : Data e-link (integer)
     *     - tx : Command e-link (integer)
     *     - host : FELIX host name
     *     - cmd_port : FELIX command port number (integer)
     *     - data_port : FELIX data port number (integer)
     *
     *
     * The search path is relative to the current working directory,
     * then to the ITK_PATH environment variable,
     * and finally to the installed directory.
     *
     * @param mapping path to the file to load
     * @param auto_load automatially load front-ends from the mapping file
     */
    void SetMapping(std::string mapping, bool auto_load=false);

    /**
     * Add a FrontEnd to the mapping, but do not load it into memory.
     * @param name Name of the FrontEnd
     * @param config Path to the configuration file
     * @param cmd_elink Command e-link number
     * @param data_elink Data e-link number
     * @param cmd_host Command host name
     * @param cmd_port Command port number
     * @param data_host Data host name
     * @param data_port Data port number
     */
    void AddMapping(std::string name, std::string config,
		    uint64_t cmd_elink, uint64_t data_elink,
		    std::string cmd_host="", uint32_t cmd_port=0,
		    std::string data_host="", uint32_t data_port=0);

    /**
     * Add to the list of enabled front-ends the given FrontEnd object with
     * the given name.
     * @param name Name of the FrontEnd
     * @param fe FrontEnd pointer
     */
    void AddFE(std::string name, FrontEnd* fe);

    /**
     * Add to the list of enabled front-ends a new FrontEnd object with the
     * given name, and a given configuration path.
     * If the configuration path is not given the name must be available in
     * the mapping file.
     * Search path is relative to the current working directory,
     * then to the ITK_PATH environment variable,
     * and finally to the installed directory.
     *
     * The configuration file is a JSON file with the following contents:
     *
     * - RD53B
     *   - name : Chip name (A_BM_05_1)
     *   - Parameter
     *     - chipId : Wire-bonded chip ID
     *   - GlobalConfig : Map of field and value (see fei4b::Configuration::FieldType)
     *   - PixelConfig : List of pixel settings per column
     *     - []
     *       - Col : Row number (from 1 to 399)
     *       - Enable : Enable the pixel in the matrix 
     *       - Hitbus : Enable the pixel in the hit bus
     *       - InjEn : Enable the pixel for injection
     *       - TDAC : Pixel threshold setting 
     *       
     * @param name Name of the FrontEnd
     * @param path Path to the configuration file (Optional).
     */
    void AddFE(std::string name, std::string path="");

    /**
     * Add to the list of enabled front-ends a new FrontEnd object with the
     * given name, and a given configuration.
     * The configuration should be given as a json string with the following
     * contents:
     *
     * - RD53B
     *   - name : Chip name (A_BM_05_1)
     *   - Parameter
     *     - chipId : Wire-bonded chip ID
     *   - GlobalConfig : Map of field and value (see fei4b::Configuration::FieldType)
     *   - PixelConfig : List of pixel settings per column
     *     - []
     *       - Col : Row number (from 1 to 399)
     *       - Enable : Enable the pixel in the matrix 
     *       - Hitbus : Enable the pixel in the hit bus
     *       - InjEn : Enable the pixel for injection
     *       - TDAC : Pixel threshold setting 
     *       
     * @param name Name of the FrontEnd
     * @param config_str Configuration as string in json format
     */
    void AddFEbyString(std::string name, std::string config_str);

    /**
     * Save the FrontEnd configuration to the given path.
     * The tuned version of the configuration files is preferred,
     * thus the configuration file will be searched in the tuned
     * folder before the current working directory.
     * @param fe Pointer to the FrontEnd
     * @param path Path to the configuration file (Optional).
     */
    void SaveFE(FrontEnd * fe, std::string path="");

    /**
     * Configure the FrontEnd objects added to the Handler.
     * Start a new run by invoking the RunNumber class.
     * Set the front-ends in configuration mode (FrontEnd::SetRunMode).
     * Write the global registers (FrontEnd::ConfigGlobal).
     * Write the pixel registers one double column at a time (FrontEnd::ConfigDoubleColumn).
     * Reset the front-end ECR and BCR counters (FrontEnd::Reset)
     * Create the output directory in the output data path followed by the run
     * number.
     * Create the output ROOT file inside the output directory.
     */
    void Config();

    /**
     * Create a netio::low_latency_send_socket to send commands (Command) to
     * the FrontEnd, and a netio::low_latency_subscribe_socket to receive data
     * (Record) from the FrontEnd.
     * Subscribe to the data elink of each FrontEnd.
     * The received data is handled directly by the corresponding FrontEnd
     * object (FrontEnd::HandleData), the AddressRecord and ValueRecord are
     * parsed automatically into the FrontEnd Configuration, and the rest of
     * the Record fragments are converted into Hit objects (FrontEnd::GetHit,
     * FrontEnd::NextHit).
     */
    void Connect();

    /**
     * Disconnect the netio::low_latency_send_socket, and unsubscribe from the
     * data elink of each FrontEnd.
     */
    void Disconnect();

    /**
     * If GetEnableOutput is true, create a new output directory and open the 
     * output root and log files.
     */
    void InitRun();

    /**
     * Virtual method to  implement a PreRun sequence.
     */
    virtual void PreRun();

    /**
     * Virtual method to implement a Scan sequence by an extended class.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     */
    virtual void Analysis();

    /**
     * Save the configuration for each FrontEnd in the output folder.
     * @param configuration If "before" saves the configuration used at the beginning
     * of the scan/tuning, if "after" the one resulting from the scan/tuning.
     */
    void SaveConfig(std::string configuration);

    /**
     * Save the configuration for each FrontEnd in the tuned folder.
     */
    void Save();

    /**
     * Prepare the Trigger sequence for all the modules from the first FrontEnd.
     * Build the corresponding netio::message.
     * Here we assume that the front-ends are all connected to the same TX.
     * @param cal_delay Delay between the Command::Cal and the Command::Trigger in ns
     * @param triggerMultiplier The number of consecutive triggers to send at each trigger command
     * @param bc1 Trigger on the first BC of the Trigger command.
     * @param bc2 Trigger on the second BC of the Trigger command.
     * @param bc3 Trigger on the third BC of the Trigger command.
     * @param bc4 Trigger on the fourth BC of the Trigger command.
     */
    void PrepareTrigger(uint32_t cal_delay=49, uint32_t triggerMultiplier=1, bool bc1=1, bool bc2=0, bool bc3=0, bool bc4=0, uint32_t PulseInj=0);

    /**
     * Prepare the Trigger sequence for all the modules using a given trigger sequence.
     * Build the corresponding netio::message.
     * Here we assume that the front-ends are all connected to the same TX.
     * @param latency Delay between the Command::Cal and the Command::Trigger in ns
     * @param pulse_inj Enables/Disables some Cal pulse in every sequnece. Works better for digital scan
     * @param trigger Sequence of triggers in increasing order of BC.
     */
    void PrepareTrigger(uint32_t latency, bool pulse_inj, std::vector<uint32_t> trigger);

    /**
     * Prepare the Trigger sequence for all the modules from the given Encoder.
     * Build the corresponding netio::message.
     * @param encoder The Encoder holding the Trigger sequence
     */
    void PrepareTrigger(Encoder * encoder);

    /**
     * Send a Trigger sequence to the FrontEnd through netio
     */
    void Trigger();

    /**
     * Send the pending Command messages to the selected FrontEnd
     * @param fe FrontEnd to send the pending messages to
     */
    void Send(FrontEnd *fe);

    /**
     * Get the list of FrontEnd objects in this Handler
     * @return the list of FrontEnd objects
     */
    std::vector<FrontEnd*> GetFEs();

    /**
     * Get a single FrontEnd objects from this Handler
     * @param name of the FrontEnd
     * @return a pointer to the FrontEnd
     */
    FrontEnd* GetFE(std::string name);

    /**
     * Get the data elink of a given FrontEnd
     * @param name of the FrontEnd
     * @return the FrontEnd data elink
     */
    uint64_t GetFEDataElink(std::string name);

    /**
     * Get the command elink of a given FrontEnd
     * @param name of the FrontEnd
     * @return the FrontEnd command elink
     */
    uint64_t GetFECmdElink(std::string name);

    /**
     * Define the path for the output of the results
     * @param path to the output
     */
    void SetOutPath(std::string path);

    /**
     * Get the output path
     * @return The path to the output
     */
    std::string GetOutPath();

    /**
     * Get the full output path
     * @return The full path to the output directory
     */
    std::string GetFullOutPath();

    /**
     * Get the network connectivity bus from FELIX-Client
     * @return interface The network folder from FELIX-Client
     */
    std::string GetBusPath();



    /**
     * Enable or disable the re-tune flag for the scans.
     * This flag controls the initial values of the tuning.
     * If enabled, the initial values will be read from the configuration
     * files.
     * @param enable Enable or disable the re-tune flag
     */
    void SetRetune(bool enable);

    /**
     * Get the re-tune flag for the scans.
     * This flag controls the initial values of the tuning.
     * If enabled, the initial values will be read from the configuration
     * files.
     * @return The re-tune flag
     */
    bool GetRetune();

    /**
     * Enable/Disable the output of the Handler
     * @param enable Enable the output if true
     */
    void SetEnableOutput(bool enable);

    /**
     * Get the output enabled of the Handler
     * @return True if the output is enabled
     */
    bool GetEnableOutput();

    /**
     * Set the threshold charge.
     * Maximum charge is 100k electrons.
     * @param charge The charge in electrons.
     */
    void SetCharge(uint32_t charge);

    /**
     * Get the threshold charge.
     * Maximum charge is 100k electrons.
     * @return The charge in electrons.
     */
    uint32_t GetCharge();

    /**
     * Set if you want to save a masking file for noise scan.
     * If 1 save the mask file, otherwise load it
     */
    void SetMaskOpt(int maskopt);

   /**
     * Get the variable if you want to save a masking file for noise scan.
     * If 1 save the mask file, otherwise load it
     */
    uint32_t GetMaskOpt();

    /**
     * Set the time over threshold.
     * Maximum ToT is 15 bc.
     * @param ToT The ToT in bc.
     */
    void SetToT(uint32_t ToT);

    /**
     * Get the time over threshold.
     * Maximum ToT is 15 bc.
     * @return The ToT in bc.
     */
    uint32_t GetToT();
  
    /**
     * Set the requested scan.
     * @param scan The scan name.
     */
    void SetScan(std::string scan);

    /**
     * Set the FrontEnd type to be scanned.
     * Parameter accepts a string with the name of the front-ends to scan.
     * FrontEnd type will be searched inside the string.
     * Only some scans use this parameter.
     * Examples: "sync+lin","sync","all"  
     * @param scan_fe The front-end region name.
     */
    void SetScanFE(std::string scan_fe);

    /**
     * Enable or disable the StoreHits flag
     * In some scans it allows the hits for each frontEnd to be stored in the root file
     * @param enable Enable the store hits flag if true
     **/
    void StoreHits(bool enable);
    
    /**
     * Set the threshold.
     * @param threshold The threshold target in electrons.
     **/
    void SetThreshold(uint32_t threshold);

    /**
     * Get the threshold.
     * @return The threshold target in electrons.
     **/
    uint32_t GetThreshold();

    /**
     * Set the latency. Only used by selected scans.
     * @param latency the latency for the L1A
     **/
    void SetLatency(uint32_t latency);

    /**
     * Get the latency. Only used by selected scans.
     * @return latency the latency for the L1A
     **/
    uint32_t GetLatency();

    /**
     * Set the command line to run the scan/tuning
     * @param commandLine Command line
     */
    void SetCommandLine(std::string commandLine);

    /**
     * Configure only the pixels of the frontends
     **/
    void ConfigurePixels();

    void ConfigurePixels(FrontEnd * fe);

    /**
     * Configure the mask step for the enabled frontends.
     * Used twice per mask step during a scan.
     * First used with enable=true to enable the pixels that will scanned,
     * second used with enable=false to disble the pixels for the next iteration.
     * @param enable enable or disable the pixels of the mask
     * @param doPrecisionToT Toggle doing precision ToT or not 
     */
    void ConfigMask(bool enable=true, bool doPrecisionToT=false);

    /**
     * Helper method that calls ConfigMask(false)
     */
    void UnconfigMask();

    /**
     * Get the Masker
     */
    Masker * GetMasker();

    /**
     * Function to save the FE configs to the daqapi database
     * @param configuration "before" of "after", indicated wether config was saved before or after the scan
     * @return The configs of all FEs in one string 
     */
    std::string GetConfig(std::string configuration);

    /**
     * Function to save the scan results to the daqapi database
     * @return the histograms of the scans as json objects 
     */
    std::string GetResults();

    /**
     * FrontEnd type mask to be able to select different Sync FE for the scans
     **/
    enum FrontEndTypes{ 
      DiffFE= 1, // All  
      DiffLeftFE = 2,
      DiffRightFE = 3,
      DiffMidFE = 4
    };

    /**
     * Set the total elapsed time during data taking.
     * @param The elapsed time in seconds.
     */
    void SetElapsedTime(std::string frontEndName, float elapsedTime);

    /**
     * Get the total elapsed time during data taking.
     * @return The elapsed time in seconds.
     */
    float GetElapsedTime(std::string frontEndName);

    void ResetAllTagCounters();

  protected:

    bool m_verbose;
    bool m_exit;
    std::string m_backend;
    std::string m_interface;
    felix_proxy::ClientThread * m_NextClient;
    felix_proxy::ClientThread::Config  *m_NextConfig;
    std::vector<std::string> m_config_path;
    TFile * m_rootfile;
    RunNumber * m_rn;
    Masker * m_masker;
    bool m_retune;
    uint32_t m_charge;
    int m_maskopt;
    uint32_t m_ToT;
    uint32_t m_threshold;
    uint32_t m_scan_fe;
    bool m_storehits;
    uint32_t m_latency;
    int32_t m_ChipId_status;

    std::string m_scan;
    std::string m_buspath;
    std::string m_outpath;
    std::string m_fullOutPath;
    std::string m_commandLine;
    std::ofstream m_logFile;
    bool m_output;
    std::string m_connectivityPath;
    std::ofstream m_outFile;
    std::ofstream m_outFileCMD;
    std::vector<FrontEnd*> m_fes;
    std::map<std::string, FrontEnd*> m_fe;
    std::map<std::string, bool> m_enabled;
    std::map<uint64_t, std::map<uint8_t, std::mutex> > m_mutex;
    std::map<std::string, std::string> m_configs;
    std::map<std::string, uint64_t> m_fe_tx;
    std::map<std::string, uint64_t> m_fe_rx;
    std::map<std::string, uint64_t> m_fe_ser;
    std::map<uint64_t, std::string> m_cmd_host;
    std::map<uint64_t, uint32_t>    m_cmd_port;
    std::map<uint64_t, std::string> m_data_host;
    std::map<uint64_t, uint32_t>    m_data_port;
    std::map<uint64_t, std::vector<FrontEnd*> > m_tx_fes;
    std::map<uint64_t, std::map<uint8_t, FrontEnd*> >m_rx_fes;
    std::map<uint64_t, std::map<uint8_t, FrontEnd*> >m_ser_fes;
    std::map<uint64_t, std::vector<uint8_t> > m_trigger_msgs;
    std::map<std::string, float> m_elapsedTime;
  };

}

#endif
