#ifndef RD53B_ANALOGSCAN_H
#define RD53B_ANALOGSCAN_H

#include "RD53BEmulator/Handler.h"
#include "RD53BEmulator/HitTree.h"
#include "TH2I.h"
#include "TH1I.h"
#include "TH2F.h"

namespace RD53B{

  /**
   * An AnalogScan injects a signal in the analog circuit of the pixel, and
   * checks for the corresponding hit in the data, according to whether the
   * signal is above threshold or not.
   * Because the output data bandwidth is limited, the injection is done in a
   * few pixels at a time.
   * The analysis of the analog scan checks that every single pixel has the
   * required number of hits. It is thus crucial to set the injection signal
   * well above the global threshold.
   * If there is a discrepancy in the number of hits in a given set of pixels,
   * it is possible to generate an enable mask to ignore those pixels in
   * subsequent scans.
   *
   * @brief RD53B AnalogScan
   * @author Carlos.Solans@cern.ch
   * @author Enrico.Junior.Schioppa@cern.ch
   * @author Alessandra.Palazzo@cern.ch
   * @author ismet.siral@cern.ch
   * @date January 2023
   */

  class AnalogScan: public Handler{

  public:

    /**
     * Create the scan
     */
    AnalogScan();

    /**
     * Delete the Handler
     */
    virtual ~AnalogScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled, that is required to have the same
     * amount of entries or less than the number of triggers.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     * Create the enable map, in which pixels have value 1 if the number
     * of hits is equal to the number of triggers, 0 otherwise.
     * Pixels with enable=0 will be masked.
     */
    virtual void Analysis();

  private:
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;
    std::map<std::string,std::vector<int>> m_mem_thres;
    std::map<std::string,TH2I*> m_occ;
    std::map<std::string,TH2I*> m_occC;
    //    std::map<std::string,TH2F*> m_tot;
    std::map<std::string,TH1I*> m_tot;
    std::map<std::string,TH2I*> m_enable;
    std::map<std::string,TH1I*> m_tid;
    std::map<std::string,TH1I*> m_ttag;
    std::map<std::string,TH1I*> m_bcid;
    std::map<std::string,TH2I*> m_bcidmap;
    std::map<std::string,TH2I*> m_ttagmap;
    std::map<std::string,HitTree*> m_hits;
    uint32_t m_colMin;
    uint32_t m_colMax;

  };

}

#endif
