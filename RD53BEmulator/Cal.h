#ifndef RD53B_Cal_H
#define RD53B_Cal_H

#include "RD53BEmulator/Command.h"

namespace RD53B{

/**
 * This command controls the generation of 2 signals
 * CAL_EDGE (mode: 1 bit, delay: 5 bit, duration: 8 bit),
 * and CAL_AUX (mode: 1 bit, delay: 5 bit).
 * Send a calibration command to a given chip ID.
 * The command starts with the symbol identifier 0x63,
 * followed by the encoded 5-bit chip ID, and encoded 20-bit payload:
 *
 * | Bit   | 19 | 18 | 15 | 14 | 13 | 10 | 9  | 6  | 5  | 4  | 0  |
 * | ----  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
 * | Rel   | 4  | 3  | 0  | 4  | 3  | 0  | 4  | 1  | 0  | 4  | 0  |
 * | Value | EM | E delay    ||| E duration     |||| AM | Adelay ||
 * | Size  | 1  | 5          ||| 8              |||| 1  | 5      ||
 * | Field | 1          ||| 2          ||| 3          ||| 4      ||
 * 
 * @brief RD53B Calibration Command
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class Cal: public Command{
  
 public:
  
  /**
   * Create a Cal
   **/
  Cal();
  
  /**
   * Empty destructor just for completeness.
   **/
  ~Cal();
  
  /**
   * Create a Cal with settings
   * @param chipid Chip ID [0:3] + bcast [4]
   * @param edge_mode the mode for the CAL_EDGE
   * @param edge_delay the delay for the CAL_EDGE
   * @param edge_width the width for the CAL_EDGE
   * @param aux_mode the mode for the CAL_AUX
   * @param aux_delay the delay for the CAL_AUX
   **/
  Cal(uint32_t chipid, uint32_t edge_mode, uint32_t edge_delay, uint32_t edge_width, uint32_t aux_mode, uint32_t aux_delay);

  /**
   * Create a new Cal from another one
   * @param copy A Cal to be copied
   **/
  Cal(Cal * copy);


  /**
   * Return a clone of the current Command
   * @return A clone of the current Command
   **/
  Cal * Clone();

  /**
   * Return a human readable representation of the symbol
   * @return The human readable representation of the symbol
   **/
  std::string ToString();

  /**
   * Parse a human readable representation of the command
   * @param str The human readable representation of the command
   * @return true if it was parsed correctly
   **/
  bool FromString(std::string str);

  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the chipid for the pulse
   * @param chipid Chip ID
   **/
  void SetChipId(uint32_t chipid);

  /**
   * Get the chipid for the pulse
   * @return chipid Chip ID
   **/
  uint32_t GetChipId();

  /**
   * Set the CAL_EDGE mode (1 bit)
   * @param edge_mode the mode for the CAL_EDGE
   **/
  void SetEdgeMode(uint32_t edge_mode);

  /**
   * Get the CAL_EDGE mode (1 bit)
   * @return the mode for the CAL_EDGE
   **/
  uint32_t GetEdgeMode();
  
  /**
   * Set the CAL_EDGE delay (5 bits)
   * @param edge_delay the delay for the CAL_EDGE
   **/
  void SetEdgeDelay(uint32_t edge_delay);
  
  /**
   * Get the CAL_EDGE delay (5 bits)
   * @return the delay for the CAL_EDGE
   **/
  uint32_t GetEdgeDelay();
  
  /**
   * Set the CAL_EDGE width (8 bits)
   * @param edge_duration the duration for the CAL_EDGE
   **/
  void SetEdgeWidth(uint32_t edge_duration);
  
  /**
   * Get the CAL_EDGE width (8 bits)
   * @return the duration for the CAL_EDGE
   **/
  uint32_t GetEdgeWidth();
  
  /**
   * Set the CAL_AUX mode (1 bit)
   * @param aux_mode the mode for the CAL_AUX
   **/
  void SetAuxMode(uint32_t aux_mode);
  
  /**
   * Get the CAL_AUX mode (1 bit)
   * @return the mode for the CAL_AUX
   **/
  uint32_t GetAuxMode();
  
  /**
   * Set the CAL_AUX delay (5 bits)
   * @param aux_delay the delay for the CAL_AUX
   **/
  void SetAuxDelay(uint32_t aux_delay);
  
  /**
   * Get the CAL_AUX delay (5 bits)
   * @return the delay for the CAL_AUX
   **/
  uint32_t GetAuxDelay();

 private:

  uint32_t m_symbol;
  uint32_t m_chipid;
  uint32_t m_edge_mode;
  uint32_t m_edge_delay;
  uint32_t m_edge_width;
  uint32_t m_aux_mode;
  uint32_t m_aux_delay;
  
};

}

#endif
