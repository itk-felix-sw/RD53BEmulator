#ifndef RD53B_READ_REGISTER_H
#define RD53B_READ_REGISTER_H

#include "RD53BEmulator/Command.h"

namespace RD53B{

/**
 * Command to initiate the readout of the register given a chip ID and address.
 * The address is 9 bits (RdReg::SetAddress, RdReg::GetAddress).
 * Command is identified by symbol 0x65, followed by the Chip ID,
 * a padding bit, and the 9-bit address.
 * 
 * Bit     |  0 |  7 |  8 | 15 | 16 | 23 | 24 | 31 |
 * ----    | -- | -- | -- | -- | -- | -- | -- | -- |
 * Byte    |  0     ||  1     ||  2     ||  3     ||
 * Payload | 0x65   ||  0 |  4 |  5 |  9 | 10 | 14 |
 *
 * The Payload in this case is the following:
 *
 * Payload |  0 |  4 |  5 |  6 |  9 | 10 | 14 |
 * ------- | -- | -- | -- | -- | -- | -- | -- |
 * Rel     |  0 |  4 |  0 |  1 |  4 |  0 |  4 |
 * Byte    |  1     ||  2         |||  3     ||
 * Descrip | ChipID ||  0 |  Address       ||||
 * Size    |  5     ||  1 |  9             ||||
 *
 * 
 * @brief RD53B Read Register Command
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class RdReg: public Command{
  
 public:
  
  /**
   * Create a read register command
   **/
  RdReg();

  /**
   * Create a RdReg from another one
   * @param copy RdReg to copy
   **/
  RdReg(RdReg * copy);

  /**
   * Create a read register Command with parameters
   * @param chipid Chip ID [0:3] + bcast [4]
   * @param address the address [0:9]
   **/
  RdReg(uint32_t chipid, uint32_t address);
  
  /**
   * Empty destructor just for completeness.
   **/
  ~RdReg();
  
  /**
   * Return a clone of the current Command
   * @return A clone of the current Command
   **/
  Command * Clone();

  /**
   * Return a human readable representation of the symbol
   * @return The human readable representation of the symbol
   **/
  std::string ToString();

  /**
   * Parse a human readable representation of the command
   * @param str The human readable representation of the command
   * @return true if it was parsed correctly
   **/
  bool FromString(std::string str);

  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the chipid for the pulse
   * @param chipid Chip ID [0:3] + bcast [4]
   **/
  void SetChipId(uint32_t chipid);

  /**
   * Get the chipid for the pulse
   * @return chipid Chip ID [0:3] + bcast [4]
   **/
  uint32_t GetChipId();

  /**
   * Set the address for the readout
   * @param address the address [0:9]
   **/
  void SetAddress(uint32_t address);

  /**
   * Get the address for the readout
   * @return the address [0:9]
   **/
  uint32_t GetAddress();


 private:

  uint32_t m_symbol;
  uint32_t m_chipid;
  uint32_t m_address;

};

}

#endif
