#ifndef RD53B_SELFTRIGGERSCAN_H
#define RD53B_SELFTRIGGERSCAN_H

#include "RD53BEmulator/Handler.h"
#include "TH2I.h"

namespace RD53B{

  /**
   * A Source scan is designed to see hits from a source.
   * Many random triggers are sent with the hope to see a hit from the source.
   *
   * @brief RD53B SelfTriggerScan
   * @author Brian.Moser@cern.ch
   * @date March 2023
   */

  class SelfTriggerScan: public Handler{

  public:

    /**
     * Create the scan
     */
    SelfTriggerScan();

    /**
     * Delete the Handler
     */
    virtual ~SelfTriggerScan();

    /**
     * Set the value of the variables for this scan, create histograms. 
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Collect the hits per trigger.
     * An occupancy histogram is filled.
     */
    virtual void Run();

    /**
     * Write and delete histograms. 
     */
    virtual void Analysis();

    /**
     * Method to call the stop of the scan from inside the loop
     * @param signal the signal thrown by the signal catch
     */
    static void StopScan(int32_t signal);

  private:

    static bool m_scanStop;

    float m_TriggerFrequency;
    double m_frequency;
    double m_duration;
    TH2I* m_totalHits;
    std::map<std::string,TH2I*> m_occ;
    std::map<std::string,TH2I*> m_occHigh;
    std::map<std::string,TH2I*> m_enable;
    std::map<std::string,TH2I*> m_mask;
    std::map<std::string,TH1I*> m_tot;
    std::map<std::string,TH1I*> m_bcid;
    TH1I* m_triggers;
    std::map<std::string,TH2I*> m_deltaBCID;
    
    uint32_t m_colMin;
    uint32_t m_colMax;

  public:
    uint32_t m_TriggerLatency;

  };

}

#endif
