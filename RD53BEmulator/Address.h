#ifndef RD53B_Address_H
#define RD53B_Address_H

#include <cstdint>
#include <string>

namespace RD53B{

/**
 * Address is a tool to convert from pixel row (Address::SetRow, Address::GetRow)
 * and column (Address::SetCol, Address::GetCol)
 * into core row (Address::SetCoreRow, Address::GetCoreRow),
 * core column (Address::SetCoreCol, Address::GetCoreCol),
 * region in core (Address::SetRegion, Address::GetRegion),
 * and pair in region (Address::SetPair, Address::GetPair).
 *
 * @brief RD53B Pixel Address
 * @author Carlos.Solans@cern.ch
 * @date June 2020
 **/

class Address{

 public:

  /**
   * Create an Address
   **/
  Address();

  /**
   * Empty destructor just for completeness.
   **/
  ~Address();

  std::string ToString();

  void SetPixel(uint32_t col, uint32_t row);

  void SetRow(uint32_t row);

  void SetCol(uint32_t col);

  void SetCore(uint32_t core_col, uint32_t core_row, uint32_t region, uint32_t pair);

  void SetCoreRow(uint32_t core_row);

  void SetCoreCol(uint32_t core_col);

  void SetRegion(uint32_t region);

  void SetPair(uint32_t pair);

  void SetRegister1(uint32_t register1);

  void SetRegister2(uint32_t register2);

  uint32_t GetRow();

  uint32_t GetCol();

  uint32_t GetCoreRow();

  uint32_t GetCoreCol();

  uint32_t GetRegion();

  uint32_t GetPair();

  uint32_t GetRegister1();

  uint32_t GetRegister2();

 private:

  uint16_t m_row;
  uint16_t m_col;
  uint16_t m_core_row;
  uint16_t m_core_col;
  uint16_t m_region;
  uint16_t m_pair;

  void pack();
  void unpack();

};

}
#endif
