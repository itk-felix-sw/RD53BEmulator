#ifndef RD53B_PIXELTHRESHOLDTUNE_H
#define RD53B_PIXELTHRESHOLDTUNE_H

#include "RD53BEmulator/Handler.h"
#include "TH2.h"
#include "TF1.h"
#include "TGraph.h"

namespace RD53B{

  /**
   * 
   * 
   * 
   * @brief RD53A PixelThresholdTune
   * @author Alessandra.Palazzo@cern.ch
   * @author ismet.siral@cern.ch
   * @date January 2021
   */

  class PixelThresholdTune: public Handler{

  public:

    /**
     * Create the scan
     */
    PixelThresholdTune();

    /**
     * Delete the Handler
     */
    virtual ~PixelThresholdTune();

    /**
     * Set the value of the variables needed for this tuning.
     * Create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the occupancy map per trigger and per
     * frontend.
     * An occupancy map is filled for each value of the DAC.
     */
    virtual void Run();

    /**
     * Delete histograms.
     * Interpolate the values of the local DACs which allow to obtain the
     * desired target and save them in the configuration file.
     */
    virtual void Analysis();

  private:
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;
    uint32_t m_nSteps;
    float m_Acceptance;
    std::vector<int> m_Steps;
    uint32_t m_vcalMed;
    uint32_t m_vcalHigh;
    uint32_t m_vcalDiff;
    std::vector<std::vector<TH2I*>> m_occ;
    std::vector<std::vector<TH2I*>> m_sign;
  };

}

#endif
