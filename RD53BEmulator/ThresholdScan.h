#ifndef RD53B_THRESHOLDSCAN_H
#define RD53B_THRESHOLDSCAN_H

#include "RD53BEmulator/Handler.h"
#include "TH2I.h"
#include "TH2F.h"
#include "TH1I.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "HitTree.h"

namespace RD53B{

  /**
   * A ThresholdScan measures the threshold in each pixel by varying the
   * injected analog charge and looking at the turning point of the s-curve.
   * Because the output data bandwidth is limited, the injection is done in
   * a few pixels at a time.
   *
   * @brief RD53A ThresholdScan
   * @author Carlos.Solans@cern.ch
   * @author Enrico.Junior.Schioppa@cern.ch
   * @author Alessandra.Palazzo@cern.ch
   * @date November 2020
   */

  class ThresholdScan: public Handler{

  public:

    /**
     * Create the scan
     */
    ThresholdScan();

    /**
     * Delete the Handler
     */
    virtual ~ThresholdScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop over the injected analog charge.
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger and per FrontEnd.
     * An occupancy histogram at each value of the injected charge is filled, 
     * that is required to have the same amount of entries or less than the
     * number of triggers.
     */
    virtual void Run();
  
    /**
     * Write and delete histograms.
     */
    virtual void Analysis();

  private:
    uint32_t m_mask_step;
    uint32_t m_cc_max;
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;
    uint32_t m_vcalMed;
    //uint32_t m_vcalHigh;
    //uint32_t m_vcalHigh_Final;
    uint32_t m_vcalDiffStart;
    uint32_t m_vcalDiffEnd;
    uint32_t m_vcalStep;
    double m_chargeMin;
    double m_chargeMax;
    double m_chargeStep;
    uint32_t m_nSteps;

    std::map<std::string,TH1I*> m_tot;    
    std::map<std::string,TH1I*>  m_scur;
    std::map<std::string,TH2F*>  m_scur2D;
    std::map<std::string,TH2I**> m_occ;
    std::map<std::string,std::map<uint32_t,std::map<uint32_t,TH1I*> > > m_curves;
    std::map<std::string,HitTree*> m_hits;

  };

}

#endif
