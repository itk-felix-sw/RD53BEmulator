#ifndef RD53B_DATAFRAME_H
#define RD53B_DATAFRAME_H

#include "RD53BEmulator/Frame.h"
#include "RD53BEmulator/Stream.h"

namespace RD53B{

/**
 * A Data Frame is a type of 64-bit Aurora Frame that contains the event Data.
 * Data plus Idle Frames are multiplexed with the Service Frame every Number-of-Data (ND) Frames.
 * There is always one Service frame every ND Data or Idle frames.
 *
 * A Data Stream always begins with a New-Stream (NS) bit, and is possibly followed by a 2-bit channel ID,
 * and possibly ends with a 6-bit End Of Stream (EOS) marker (0b000000),
 * which not necessarily will be at the end of the Aurora Frame.
 * Additional padding zeroes (orphan bits) will be added up to the end of the Frame.
 * The channel ID allows to differentiate between front-ends.
 *
 * A Data Frame is decoded by detecting a Stream in the payload bits of the Frame.
 *
 * | Bit  |  0 |  1 |  N   |  N+1 | N+6 | N+7 | 64 |
 * | ---  | -- | -- | ---- | ---- | --- | --- | -- |
 * | Desc | NS | Payload  || EOS       || Orphan  ||
 * | Size |  1 | Variable ||  6        || 64-6-3  ||
 *
 *
 * | Bit  |  0 |  1 |  2 |  3 |  N   |  N+1 | N+6 | N+7 | 64 |
 * | ---  | -- | -- | -- | -- | ---- | ---- | --- | --- | -- |
 * | Desc | NS | ChnId  || Payload  || EOS       || Orphan  ||
 * | Size |  1 |  2     || Variable ||  6        || 64-6-3  ||
 *
 * @brief RD53B Data Frame
 * @author Carlos.Solans@cern.ch
 * @author Ismet.Siral@cern.ch
 * @date May 2020
 **/

class DataFrame: public Frame{
  
 public:

  /**
   * Create an empty data frame
   **/
  DataFrame();

  /**
   * Delete the data frame
   **/
  ~DataFrame();

  /**
   * Return a human readable representation of the data
   * @return The human readable representation of the data
   **/
  std::string ToString();
  
  /**
   * Return the byte stream representation of the data
   * @param bytes the byte array
   * @param len the number of bytes to print
   * @return The hex string representation of the data
   **/
  static std::string ToByteStream(uint8_t * bytes, uint32_t len);

  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);
  
  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Clear the internal memory
   */
  void Clear();

  /**
   * Set the number of events per stream, by default it is 1.
   * @param nes Number of events per stream
   **/
  void SetNumEventsPerStream(uint32_t nes);

  /**
   * Enable or disable the end of stream marker
   * @param enable Enable the end of stream marker if true
   **/
  void SetEOS(bool enable);

  /**
   * Enable or disable the binary tree encoding
   * @param enable Enable the binary tree encoding if true
   **/
  void SetEnc(bool enable);

  /**
   * Enable or disable the channel ID
   * @param enable Enable the channel ID after the NS bit
   **/
  void SetChnID(bool enable);

  /**
   * Enable or disable the end of stream marker
   * @param enable Enable the end of stream marker if true
   **/
  void EnableEOS(bool enable);

  /**
   * Enable or disable the TOT on the data
   * @param enable Enable the TOT if true
   **/
  void EnableTOT(bool enable);

  /**
   * Enable or disable the binary tree encoding
   * @param enable Enable the binary tree encoding if true
   **/
  void EnableEnc(bool enable);

  /**
   * Enable or disable the channel ID
   * @param enable Enable the channel ID after the NS bit
   **/
  void EnableChnID(bool enable);

  /**
   * Add a Stream into the DataFrame
   * @param stream Pointer to the Stream to add
   **/
  void AddStream(Stream * stream);

  /**
   * Get a Stream in the DataFrame
   * @param index The index for the Stream to retrieve
   * @return the Stream given the index
   **/
  Stream * GetStream(uint32_t index);

  /**
   * Get the number of Streams in the DataFrame for a given channel (optional)
   * @return the number of Streams
   **/
  uint32_t GetNStreams();

  /**
   * Set the verbose mode
   * @param enable Enable the verbose mode if true
   */
  void SetVerbose(bool enable);

 private:
 
  bool m_enable_eos; //! End of Stream marker
  bool m_enable_tot; //! TOT enabled
  bool m_enable_cid; //! Channel ID enabled
  bool m_enable_enc; //! Binary tree encoding
  uint32_t m_nod; //! Number of Data per stream
  bool m_verbose;

  std::vector<Stream*> m_streams; //! Stream array

};

}

#endif 
