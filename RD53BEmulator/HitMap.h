#ifndef _RD53B_HITMAP_H
#define _RD53B_HITMAP_H
  
#include <iostream>
#include <cstdint>
#include <string>

namespace RD53B{
  
/**
 * A hitmap in the RD53B::Stream can be compressed using a binary tree.
 * The minimum size of the compressed map is 4, the maximum is 30 bits.
 * The compression is enabled by RD53B::Configuration::EnRawMap (addr: 75, bit: 9).
 *
 * A group of 8 cols x 2 rows (16-bits) is encoded using 4 cuts
 * - S1 which row has a hit (top, bot)
 * - S2 which 4-pixel side has a hit (left, right)
 * - S3 which 2-pixel side has a hit (left, right)
 * - M where in the 2-pixel is the hit (left, right)
 *
 * The format follows the the convention: 
 * - S1, S2, S3, [S3], map, [3x[map]], [S2, S3, [S3], map, [3x[map]]]
 * - First the row (S1), then the side (S2), then the pairs (2xS3), and then the maps (3xM)
 * - If S1 is 0b01 or 0b10 there is only one S2
 * - If S2 is 0b01 or 0b10 there is only one S3 
 *
 * Then bit code 0b01 is replaced by 0b0 to reduce the output
 *
 * HitMap::Decode decodes the hitmap into 8 pixel pair maps following the rules, and
 * composes the 8x2 pixel region based on those. HitMap::Encode encodes the hitmap 
 * by reconstructing the S1, S2, S3 values and assembling the compressed tree again.
 * In both cases, the encoded value is stored as a 32-bit integer padded to the left (bit 30), 
 * and the decoded value is a 32-bit integer padded to the right.
 *
 * @brief RD53B HitMap binary encoding/decoding tool
 * @author Carlos.Solans@cern.ch
 * @date September 2023
**/

class HitMap{
  
public:
  
  HitMap();
  ~HitMap();

  /**
   * @brief Encode a 8x2 pixel region into a hitmap
   * @param decoded the 8x2 pixel region stored in a 32-bit integer padded to the right
   * @param encoded pointer to a 32-bit integer where to store the encoded map padded to the left (bit 30)
   * @return the length of the encoded map in bits
   */
  uint32_t Encode(uint32_t decoded, uint32_t * encoded);
 
 /**
   * @brief Decode a hitmap into a 8x2 pixel region
   * @param encoded the 32-bit integer containing the encoded map padded to the left (bit 30)
   * @param decoded pointer to a 32-bit integer where to store the 8x2 pixel region padded to the right
   * @return the length of the encoded map in bits
   */
  uint32_t Decode(uint32_t encoded, uint32_t * decoded);

  std::string to_string();
  
private:
  
  uint32_t S1;
  uint32_t S2t;
  uint32_t S3tl;
  uint32_t S3tr;
  uint32_t Mtll;
  uint32_t Mtlr;
  uint32_t Mtrl;
  uint32_t Mtrr;
  uint32_t S2b;
  uint32_t S3bl;
  uint32_t S3br;
  uint32_t Mbll;
  uint32_t Mblr;
  uint32_t Mbrl;
  uint32_t Mbrr;

  uint32_t readTwo(uint32_t src, uint32_t pos, uint32_t * dst);
  uint32_t writeTwo(uint32_t src, uint32_t pos, uint32_t * dst);
  std::string to_string(uint32_t src);  
  
};

}
#endif
