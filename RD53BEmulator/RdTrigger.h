#ifndef RD53B_READ_TRIGGER_H
#define RD53B_READ_TRIGGER_H

#include "RD53BEmulator/Command.h"

namespace RD53B{

/**
 * In dual trigger mode, the read trigger bit for a given trigger tag
 * needs to be set before the egg timer expires. Otherwise the readout
 * of this event is not triggered.
 * Set the Read Trigger bit for a given Trigger Tag and Chip ID.
 * Command is identified by symbol 0x69, followed by the Chip ID,
 * two padding bits, and the 8-bit trigger tag.
 * 
 * | Bit   | 9  | 7  | 5  | 4  | 0  |
 * | ----  | -- | -- | -- | -- | -- |
 * | Rel   | 4  | 2  | 0  | 4  | 0  |
 * | Value | 0  | Trigger tag    ||||
 * | Field | 1          ||| 2      ||
 * 
 * @brief RD53B Read Trigger Command
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class RdTrigger: public Command{
  
 public:
  
  /**
   * Create a read register command
   **/
  RdTrigger();
  
  /**
   * Empty destructor just for completeness.
   **/
  ~RdTrigger();
  
  /**
   * Return a clone of the current Command
   * @return A clone of the current Command
   **/
  Command * Clone();

  /**
   * Return a human readable representation of the symbol
   * @return The human readable representation of the symbol
   **/
  std::string ToString();

  /**
   * Parse a human readable representation of the command
   * @param str The human readable representation of the command
   * @return true if it was parsed correctly
   **/
  bool FromString(std::string str);

  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the chipid for the pulse
   * @param chipid Chip ID [0:4]
   **/
  void SetChipId(uint32_t chipid);

  /**
   * Get the chipid for the pulse
   * @return chipid Chip ID [0:4]
   **/
  uint32_t GetChipId();

  /**
   * Set the trigger tag
   * @param tag the trigger tag [0:7]
   **/
  void SetTag(uint32_t tag);

  /**
   * Get the trigger tag
   * @return the trigger tag [0:7]
   **/
  uint32_t GetTag();


 private:

  uint32_t m_symbol;
  uint32_t m_chipid;
  uint32_t m_tag;

};

}
#endif
