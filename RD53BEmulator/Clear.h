#ifndef RD53B_CLEAR_H
#define RD53B_CLEAR_H

#include "RD53BEmulator/Command.h"

namespace RD53B{

/**
 * Clears the entire data path. All pending triggers and stored hits will be erased.
 * This command should be issued prior to sending triggers for the first time since power-up.
 * The symbol identifier of this command is 0x5A and doesn't contain any data.
 *
 * @brief RD53B Clear Command
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class Clear: public Command{
  
 public:
  
  /**
   * Create an RD53B ECR
   **/
  Clear();

  /**
   * Create an RD53B ECR
   * Param chipid
   **/
  Clear(uint32_t chipid);
  
  /**
   * Empty destructor just for completeness.
   **/
  ~Clear();
  
  /**
   * Return a clone of the current Command
   * @return A clone of the current Command
   **/
  Command * Clone();

  /**
   * Return a human readable representation of the ECR
   * @return The human readable representation of the ECR
   **/
  std::string ToString();

  /**
   * Parse a human readable representation of the command
   * @param str The human readable representation of the command
   * @return true if it was parsed correctly
   **/
  bool FromString(std::string str);
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the chipid for the pulse
   * @param chipid Chip ID [0:3] + bcast [4]
   **/
  void SetChipId(uint32_t chipid);

  /**
   * Set the chipid for the pulse
   * @return chipid Chip ID [0:3] + bcast [4]
   **/
  uint32_t GetChipId();

 private:

  uint32_t m_symbol;
  uint32_t m_chipid;
};

}

#endif
