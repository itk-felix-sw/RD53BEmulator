#ifndef RD53B_BINARYTREE_H
#define RD53B_BINARYTREE_H

#include <map>
#include <vector>
#include <cstdint>

namespace RD53B{

/**
 * The hit maps in a quarter-core region of the RD53B are encoded into
 * patterns (BinaryTree::ccs) of different sizes (BinaryTree::szs).
 * The position of the hitmap in the look-up-table corresponds to the binary
 * representation of the hit following the numbering of the pixels in the matrix
 * (row increases from top to bottom down, and column from left to right).
 * 
 * 
 * The pixels in the quad-core-region can be numbered like the following:
 * 
 * | R/C | 1 | 2 | 3 | 4 |  5 |  6 |  7 |  8 |
 * | --- | - | - | - | - | -- | -- | -- | -- |
 * | 1   | 1 | 2 | 3 | 4 |  9 | 10 | 11 | 12 |
 * | 2   | 5 | 6 | 7 | 8 | 13 | 14 | 15 | 16 |
 *
 * And assigned the following code that can be obtained
 * by first splitting the region left to right, and
 * assigning a 1 to the region where the selected pixel
 * is located, and a 0 otherwise, keeping left first,
 * then splitting top and bottom, and assigning a 1 to
 * the region with the hit, and finally left right again,
 * keeping left first. 
 *
 *
 * | Pix | Code     |
 * | --- | -------- |
 * |   1 | 10101010 |
 * |   2 | 10101001 |
 * |   3 | 10100110 |
 * |   4 | 10100101 |
 * |   5 | 10011010 |
 * |   6 | 10011001 |
 * |   7 | 10010110 |
 * |   8 | 10010101 |
 * |   9 | 01101010 |
 * |  10 | 01101001 |
 * |  11 | 01100110 |
 * |  12 | 01100101 |
 * |  13 | 01011010 |
 * |  14 | 01011001 |
 * |  15 | 01010110 |
 * |  16 | 01010101 |
 * 
 * @brief RD53B BinaryTree look-up-tables for quarter-core hitmap encoding and decoding.
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class BinaryTree{
  public:
  /** @brief Compressed bit-maps **/
  static const uint32_t ccs[65536];
  /** @brief Uncompressed bit-maps **/
  static const uint32_t bbs[65536];
  /** @brief Size of the compressed bit-maps **/
  static const uint32_t szs[65536];
};

}
#endif
