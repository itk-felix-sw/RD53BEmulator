#ifndef RD53B_DISCONNECTEDBUMPSCAN_H
#define RD53B_DISCONNECTEDBUMPSCAN_H

#include "RD53BEmulator/Handler.h"
#include "RD53BEmulator/HitTree.h"
#include "TH2I.h"
#include "TH1I.h"
#include "TH2F.h"

namespace RD53B{

  /**
   * An DisconnectedBumpScan injects a signal in the analog circuit of the pixel one by one
   * while enableing injection in pixels around them. 
   * If there is a discrepancy in the number of hits in a given set of pixels,
   * it is possible to understand that there is a cross talk.
   *
   * @brief RD53B DisconnectedBumpScan
   * @author ismet.siral@cern.ch
   * @date March 2023
   */

  class DisconnectedBumpScan: public Handler{

  public:

    /**
     * Create the scan
     */
    DisconnectedBumpScan();

    /**
     * Delete the Handler
     */
    virtual ~DisconnectedBumpScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger.
     * An occupancy histogram is filled, that is required to have the same
     * amount of entries or less than the number of triggers.
     */
    virtual void Run();

    /**
     * Write and delete histograms.
     * Create the enable map, in which pixels have value 1 if the number
     * of hits is equal to the number of triggers, 0 otherwise.
     * Pixels with enable=0 will be masked.
     */
    virtual void Analysis();

  private:
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;
    std::map<std::string,std::vector<int>> m_mem_thres;
    std::map<std::string,TH2I*> m_occ;
    std::map<std::string,TH2I*> m_occC;
    //    std::map<std::string,TH2F*> m_tot;
    std::map<std::string,TH1I*> m_tot;
    std::map<std::string,TH2I*> m_enable;
    std::map<std::string,TH1I*> m_tid;
    std::map<std::string,TH1I*> m_ttag;
    std::map<std::string,TH1I*> m_bcid;
    std::map<std::string,TH2I*> m_bcidmap;
    std::map<std::string,TH2I*> m_ttagmap;
    std::map<std::string,HitTree*> m_hits;
    uint32_t m_colMin;
    uint32_t m_colMax;

  };

}

#endif
