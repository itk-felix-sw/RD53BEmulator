#ifndef RD53BCOMMAND_H
#define RD53BCOMMAND_H

#include <cstdint>
#include <string>
#include <vector>
#include <map>

namespace RD53B{

/**
 * This class represents a generic RD53B command, and the 
 * specific commands (Trigger, Sync, Pulse, Cal...) extend this class.
 * All commands are built in 16-bit frames made out of two 8-bit symbols.
 * There are short commands that are executed immediately after being recognized,
 * and long commands, that use the ChipID to address to a given chip.
 * Custom encoding of 5-bit to DC-balanced 8-bit symbols is the following:
 * 
 * Symbol  | Encoding  | Hex
 * ------- | --------- | ----
 * Data_00 | 0110_1010 | 0x6A
 * Data_01 | 0110_1100 | 0x6C
 * Data_02 | 0111_0001 | 0x71
 * Data_03 | 0111_0010 | 0x72
 * Data_04 | 0111_0100 | 0x74
 * Data_05 | 1000_1011 | 0x8B
 * Data_06 | 1000_1101 | 0x8D
 * Data_07 | 1000_1110 | 0x8E
 * Data_08 | 1001_0011 | 0x93
 * Data_09 | 1001_0101 | 0x95
 * Data_10 | 1001_0110 | 0x96
 * Data_11 | 1001_1001 | 0x99
 * Data_12 | 1001_1010 | 0x9A 
 * Data_13 | 1001_1100 | 0x9C
 * Data_14 | 1010_0011 | 0xA3 
 * Data_15 | 1010_0101 | 0xA5
 * Data_16 | 1010_0110 | 0xA6
 * Data_17 | 1010_1001 | 0xA9
 * Data_18 | 0101_1001 | 0x59
 * Data_19 | 1010_1100 | 0xAC
 * Data_20 | 1011_0001 | 0xB1
 * Data_21 | 1011_0010 | 0xB2
 * Data_22 | 1011_0100 | 0xB6
 * Data_23 | 1100_0011 | 0xC3
 * Data_24 | 1100_0101 | 0xC5
 * Data_25 | 1100_0110 | 0xC6
 * Data_26 | 1100_1001 | 0xC9
 * Data_27 | 1100_1010 | 0xCA
 * Data_28 | 1100_1100 | 0xCC
 * Data_29 | 1101_0001 | 0xD1
 * Data_30 | 1101_0010 | 0xD2
 * Data_31 | 1101_0100 | 0xD4
 *
 * This encoding is stored in the symbol to value (Command::m_symbol2data),
 * and value to symbol (Command::m_data2symbol) look up tables
 * contained in this class.
 *
 * The method to decode a byte array is Command::SetBytes,
 * and to encode a it Command::GetBytes.
 * Command::ToString returns a human readable representation
 * of the command. Command::CheckSymbol is used to identify if
 * the byte array can be decoded into the given command.
 * And Command::GetType is used to return the command type.
 *
 * @brief Abstract RD53B Command
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class Command{
  
 public:

  static const uint32_t UNKNOWN=0; /**< Default command type */
  static const uint32_t SYNC=1;    /**< Type for the Sync command */
  static const uint32_t PLOCK=2;   /**< Type for the PLL Lock command */
  static const uint32_t RDTRG=3;   /**< Type for the Read Trigger command */
  static const uint32_t RDREG=4;   /**< Type for the Read Register command */
  static const uint32_t WRREG=5;   /**< Type for the Write Register command */
  static const uint32_t CAL=6;     /**< Type for the Calibration command */
  static const uint32_t PULSE=7;   /**< Type for the Pulse command */
  static const uint32_t CLEAR=8;   /**< Type for the Clear command */
  static const uint32_t TRIGGER=9; /**< Type for the Trigger command */

  /**
   * Empty constructor
   **/
  Command();

  /**
   * Empty destructor
   **/
  virtual ~Command(){};

  /**
   * Return a clone of the current Command
   * @return A clone of the current Command
   **/
  virtual Command * Clone()=0;

  /**
   * Return a human readable representation of the command
   * @return The human readable representation of the command
   **/
  virtual std::string ToString()=0;

  /**
   * Parse a human readable representation of the command
   * @param str The human readable representation of the command
   * @return true if it was parsed correctly
   **/
  virtual bool FromString(std::string str)=0;
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  virtual uint32_t UnPack(uint8_t * bytes, uint32_t maxlen)=0;
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  virtual uint32_t Pack(uint8_t * bytes)=0;

  /**
   * Get the type of the command
   * @return the command type
   **/
  virtual uint32_t GetType()=0;

 protected:

  /** Lookup table from 5-bit data (0-31) to custom 8-bit symbols */
  static std::vector<uint32_t> m_data2symbol;

  /** Lookup table from custom 8-bit symbols to 5-bit data (0-31) */
  static std::map<uint32_t,uint32_t> m_symbol2data;
  
};

}

#endif 
