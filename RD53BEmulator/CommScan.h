#ifndef RD53B_COMMSCAN_H
#define RD53B_COMMSCAN_H

#include "RD53BEmulator/Handler.h"
#include "TH2I.h"
#include "TH2F.h"
#include "TH1I.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "HitTree.h"

namespace RD53B{

  /**
   * A Communication Scan measures the fraction of goo hits reported in a bunch of pixels when varying some key chip communicatoin parameters (pre-enphasis).
   *
   * @brief RD53B CommAScan
   * @author Valerio.Dao@cern.ch
   * @date February 2023
   */

  class CommScan: public Handler{

  public:

    /**
     * Create the scan
     */
    CommScan();

    /**
     * Delete the Handler
     */
    virtual ~CommScan();

    /**
     * Set the value of the variables for this scan, create histograms.
     */
    virtual void PreRun();

    /**
     * Loop over the injected analog charge.
     * Loop the matrix in groups of pixels and core columns.
     * Trigger few events and collect the hits per trigger and per FrontEnd.
     * An occupancy histogram at each value of the injected charge is filled, 
     * that is required to have the same amount of entries or less than the
     * number of triggers.
     */
    virtual void Run();
  
    /**
     * Write and delete histograms.
     */
    virtual void Analysis();

  private:
    
    uint32_t m_ntrigs;
    uint32_t m_TriggerLatency;

    uint32_t m_par0_Min;
    uint32_t m_par0_Max;
    uint32_t m_par0_nSteps;
    uint32_t m_par1_Min;
    uint32_t m_par1_Max;
    uint32_t m_par1_nSteps;
    std::string   m_par0_Label;
    std::string   m_par1_Label;
    
    std::map<std::string,TH2I**> m_occ;
    std::map<std::string,TH2F*>  m_results;


  };

}

#endif
