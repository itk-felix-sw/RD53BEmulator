#ifndef RD53B_ENCODER_H
#define RD53B_ENCODER_H

#include "RD53BEmulator/Command.h"
#include "RD53BEmulator/Cal.h"
#include "RD53BEmulator/Clear.h"
#include "RD53BEmulator/PllLock.h"
#include "RD53BEmulator/Pulse.h"
#include "RD53BEmulator/RdReg.h"
#include "RD53BEmulator/RdTrigger.h"
#include "RD53BEmulator/Sync.h"
#include "RD53BEmulator/Trigger.h"
#include "RD53BEmulator/WrReg.h"

#include <cstdint>
#include <vector>

namespace RD53B{

/**
 * The RD53B Encoder unpacks a byte stream into a Command list,
 * and packs a Command list into a byte stream preserving the order.
 *
 * The byte stream contents and length are accessible through 
 * Encoder::GetBytes and Encoder::GetLength.
 * Similarly, a byte stream can be decoded by the Encoder::SetBytes.
 * The Command list is available from Encoder::GetCommands. 
 *
 * For performance reasons, any Command added to the Encoder 
 * should not be deleted by the user. It will be deleted by
 * Encoder::Clear.
 *
 * @verbatim
 
   Encoder encoder;
   encoder.AddRecord(new Cal());
   encoder.AddRecord(new Pulse());
   encoder.AddRecord(new Trigger());
 
   //Encode into bytes
   encoder.Encode();
   uint8_t * bytes = decoder.GetBytes();
   uin32_t length = decoder.GetLength();

   //Decode into Commands
   encoder.SetBytes(bytes, length);
   encoder.Decode();
   vector<Command*> commands = decoder.GetCommands();

   @endverbatim
 *
 * @brief RD53B input byte stream Encoder
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/


class Encoder{
    
 public:

  /**
   * Initialize the Encoder
   **/
  Encoder();
  
  /**
   * Delete the commands in memory
   **/
  ~Encoder();
  
  /**
   * Add bytes to the already existing byte array
   * @param bytes byte array
   * @param pos starting index of the byte array
   * @param len number of bytes to add
   **/
  void AddBytes(uint8_t *bytes, uint32_t pos, uint32_t len);

  /**
   * Replace the bytes of the byte array.
   * @param bytes byte array
   * @param len number of bytes to add
   **/
  void SetBytes(uint8_t *bytes, uint32_t len);
  
  /**
   * Add an RD53B Command to the end of the Command list
   * @param command The RD53B specific Command
   **/
  void AddCommand(Command *command);

  /**
   * Add an RD53B Command to the end of the Command list from a string
   * @param str String representation of the command
   **/
  void AddCommand(std::string str);
    
  /**
   * Get the Command list 
   * @return vector of Command pointers
   **/
  std::vector<Command*> & GetCommands();

  /**
   * Get a string representation of the bytes. 
   * @return a string in hexadecimal
   **/
  std::string GetByteString();

  /**
   * Get the byte array pointer. 
   * This pointer cannot be deleted by the user.
   * @return the byte array as a pointer
   **/
  uint8_t * GetBytes();
  
  /**
   * @return the size of the byte array
   **/
  uint32_t GetLength();

  /**
   * Encode the Command list into a byte array
   **/
  void Encode();

  /**
   * Decode the byte array into a Command list
   **/
  void Decode();
  
  /**
   * Clear the byte array
   **/
  void ClearBytes();
  
  /**
   * Clear the Command list by deleting each
   * object in the list.
   **/
  void ClearCommands();
  
  /**
   * Clear the byte array and the Command list
   **/
  void Clear();

 private:
  
  std::vector<Command*> m_commands;
  std::vector<uint8_t> m_bytes;
  uint32_t m_length;
  
  Cal * m_Cal;
  RD53B::Clear * m_Cle;
  PllLock* m_Pll;
  Pulse * m_Pul;
  RdReg * m_RdR;
  RdTrigger * m_RdT;
  Sync * m_Syn;
  RD53B::Trigger * m_Trg;
  WrReg * m_WrR;

};

}
#endif
