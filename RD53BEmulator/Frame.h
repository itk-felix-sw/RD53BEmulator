#ifndef RD53B_FRAME_H
#define RD53B_FRAME_H

#include <cstdint>
#include <string>
#include <vector>
#include <map>

namespace RD53B{

/**
 * This class represents a generic RD53B output Frame.
 * The specific RD53B Frame (Data, Idle, Service,...)
 * extend this class.
 *
 * @brief Abstract RD53B Frame
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class Frame{
  
 public:

  /**
   * Type values
   **/
  static const uint32_t UNKNOWN=0;
  static const uint32_t DATA=1;
  static const uint32_t IDLE=3;
  static const uint32_t REGISTER=3;

  /**
   * Empty constructor
   **/
  Frame();

  /**
   * Virtual destructor
   **/
  virtual ~Frame();

  /**
   * Return a human readable representation of the frame
   * @return The human readable representation of the frame
   **/
  virtual std::string ToString()=0;

  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  virtual uint32_t UnPack(uint8_t * bytes, uint32_t maxlen)=0;
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  virtual uint32_t Pack(uint8_t * bytes)=0;

  /**
    * Get the type of the command
    * @return the command type
    **/
   virtual uint32_t GetType()=0;

  /**
    * Get the aurora header
    * @return the aurora header in byte form
    **/
   uint8_t GetAuroraHeader();


 protected:

  uint32_t m_aheader;
  
};

}

#endif 
