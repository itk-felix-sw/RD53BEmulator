#ifndef RD53B_PLL_LOCK_H
#define RD53B_PLL_LOCK_H

#include "RD53BEmulator/Command.h"

namespace RD53B{

/**
 * A clock pattern that allows the front-end PLL to lock to the proper frequency.
 * During normal operation it is equivalent to sending a No-Opertation (NOOP) command.
 * The same 8-bit symbol (0xAA) is repeated twice to produce a clock pattern.
 *
 * @brief RD53B PLL Lock Command
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class PllLock: public Command{
  
 public:
  
  /**
   * Create a Sync
   **/
  PllLock();
  
  /**
   * Empty destructor just for completeness.
   **/
  ~PllLock();
  
  /**
   * Return a clone of the current Command
   * @return A clone of the current Command
   **/
  Command * Clone();

  /**
   * Return a human readable representation of the symbol
   * @return The human readable representation of the symbol
   **/
  std::string ToString();

  /**
   * Parse a human readable representation of the command
   * @param str The human readable representation of the command
   * @return true if it was parsed correctly
   **/
  bool FromString(std::string str);

  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

 private:

  uint32_t m_symbol;

};

}
#endif
