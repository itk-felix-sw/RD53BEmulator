#ifndef RD53B_PULSE_H
#define RD53B_PULSE_H

#include "RD53BEmulator/Command.h"

namespace RD53B{

/**
 * Send a Pulse with duration of 2(N+1) bunch crossings to the given chip ID.
 * The duration is controlled by the GlobalPulseWidth 8-bit register.
 * The symbol identifier of this command is 0x5C, followed by the Chip ID.
 * 
 * @brief RD53B Pulse Command
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class Pulse: public Command{
  
 public:
  
  /**
   * Create a Pulse
   **/
  Pulse();
  /**
   * Create a Pulse with settings
   * @param chipid Chip ID [0:3] + bcast [4]
   * @param length Pulse duration in powers of 2 of the 160 MHz clock
   **/
  Pulse(uint32_t chipid, uint32_t length=4);

  /**
   * Create a Pulse from another one
   * @param copy A Pulse to copy
   */
  Pulse(Pulse * copy);
  
  /**
   * Empty destructor just for completeness.
   **/
  ~Pulse();
  
  /**
   * Return a clone of the current Command
   * @return A clone of the current Command
   **/
  Command * Clone();

  /**
   * Return a human readable representation of the symbol
   * @return The human readable representation of the symbol
   **/
  std::string ToString();

  /**
   * Parse a human readable representation of the command
   * @param str The human readable representation of the command
   * @return true if it was parsed correctly
   **/
  bool FromString(std::string str);

  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the chipid for the pulse
   * @param chipid Chip ID [0:3] + bcast [4]
   **/
  void SetChipId(uint32_t chipid);

  /**
   * Set the chipid for the pulse
   * @return chipid Chip ID [0:3] + bcast [4]
   **/
  uint32_t GetChipId();

 private:

  uint32_t m_symbol;
  uint32_t m_chipid;

};

}

#endif
