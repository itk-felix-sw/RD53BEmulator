#ifndef RD53B_IDLEFRAME_H
#define RD53B_IDLEFRAME_H

#include "RD53BEmulator/Frame.h"

namespace RD53B{


/**
 * The RD53B IdleFrame block is a Frame used to space contiguous Data Frames.
 * It is composed by the 2-bit Aurora header (0b10), followed by an 8-bit
 * identifier (0x78), Aurora IdleFrame code (IdleFrame::SetAuroraCode, IdleFrame::GetAuroraCode),
 * that defines the nature of the IdleFrame, followed by 48-bit padding zeroes.
 * When bits CC, CB, and NR are low, the IdleFrame is considered a regular idle.
 * Possible Aurora IdleFrame codes are:
 *
 * Bit | Description
 * --- | -----------
 *   8 | Clock Compensation (CC). When high all other bits should be zero.
 *   9 | Channel Bonding (CB). When high all other bits should be zero.
 *  10 | Not Ready (NR). When high CC and CB cannot be high.
 *  11 | Strict Alignment (SA). Can be set independently of NR.
 *
 * The format of the Register frame is the following:
 *
 * | Bit  |  0 |  7 |  8 |  9 | 10 | 11 | 12 | 15 | 16 | 23 | 24 | 31 | 32 | 39 | 40 | 47 | 48 | 53 | 54 | 63 |
 * | ---  | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
 * | Rel  |  7 |  0 |  7 |  6 |  5 |  4 |  3 |  0 |  7 |  0 |  7 |  0 |  7 |  0 |  7 |  0 |  7 |  0 |  7 |  0 |
 * | Desc |   0x78 || CC | CB | NR | SA |  0x00  ||  0x00                                          ||||||||||||
 * | Size |  8     ||  1 |  1 |  1 |  1 |  4     ||  48                                            ||||||||||||
 * | Byte |  0     ||  1                     ||||||  2     ||  3     ||  4     ||  5     ||  6     ||  7     ||
 * 
 *
 * @brief RD53B IdleFrame Frame
 * @author Carlos.Solans@cern.ch
 * @date May 2020
 **/

class IdleFrame: public Frame{
  
 public:

  /**
   * Empty constructor
   **/
  IdleFrame();

  /**
   * Create a new object from another one
   * @param copy Pointer to copy
   **/
  IdleFrame(IdleFrame *copy);

  /**
   * Destructor
   **/
  ~IdleFrame();

  /**
   * Create a new object from this one
   * @return copy Pointer to copy of this one
   **/
  IdleFrame * Clone();

  /**
   * Return a human readable representation of the register
   * @return The human readable representation of the register
   **/
  std::string ToString();
  
  /**
   * Extract the contents of this command from the bytes
   * @param bytes the byte array
   * @param maxlen the maximum number of bytes than can be read
   * @return the number of bytes processed
   **/
  uint32_t UnPack(uint8_t * bytes, uint32_t maxlen);
  
  /**
   * Set the contents of this command to bytes
   * @param bytes the byte array
   * @return the number of bytes processed
   **/
  uint32_t Pack(uint8_t * bytes);

  /**
   * Get the type of command
   * @return the type of the command
   **/
  uint32_t GetType();

  /**
   * Set the Aurora code of the frame.
   * @param code Aurora code to set
   **/
  void SetAuroraCode(uint32_t code);

  /**
   * Get the Aurora code
   * @return the Aurora code in byte form
   **/
  uint32_t GetAuroraCode();

  /**
   * Set the CC bit.
   * @param enable Enable the CC bit if true
   **/
  void SetCC(bool enable);

  /**
   * Get the CC bit
   * @return true if CC bit is enabled
   **/
  bool GetCC();

  /**
   * Set the CB bit.
   * @param enable Enable the CB bit if true
   **/
  void SetCB(bool enable);

  /**
   * Get the CB bit
   * @return true if CB bit is enabled
   **/
  bool GetCB();

  /**
   * Set the NR bit.
   * @param enable Enable the NR bit if true
   **/
  void SetNR(bool enable);

  /**
   * Get the NR bit
   * @return true if NR bit is enabled
   **/
  bool GetNR();

  /**
   * Set the SA bit.
   * @param enable Enable the SA bit if true
   **/
  void SetSA(bool enable);

  /**
   * Get the SA bit
   * @return true if SA bit is enabled
   **/
  bool GetSA();
  
 protected:

  uint8_t m_acode;
  uint8_t m_kcode;

  static const uint8_t kCC=0x80;
  static const uint8_t kCB=0x40;
  static const uint8_t kNR=0x20;
  static const uint8_t kSA=0x10;

};

}

#endif 
