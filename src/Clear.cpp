#include "RD53BEmulator/Clear.h"
#include <sstream>
#include <iomanip>

using namespace std;
using namespace RD53B;

Clear::Clear(){
  m_symbol = 0x5A;
  m_chipid = 0;
}

Clear::Clear(uint32_t chipid){
  m_symbol = 0x5A;
  m_chipid = chipid;
}



Clear::~Clear(){}

Command * Clear::Clone(){
  Clear * clone = new Clear();
  clone->m_chipid = m_chipid;
  return clone;
}

string Clear::ToString(){
  ostringstream os;
  os << "Clear 0x" << hex << m_symbol << dec
     << " ChipId:" << m_chipid
     << " (0x" << hex << m_chipid << dec << ")";
  return os.str();
}

bool Clear::FromString(string str){
  istringstream is(str);
  uint32_t hsym;
  string hcid, tmp;
  is >> tmp >> hex >> hsym >> dec;
  if(hsym!=m_symbol){return false;}
  is >> hcid >> m_chipid >> tmp;
  return true;
}

uint32_t Clear::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<2) return 0;
  m_symbol = bytes[0];
  if(m_symbol!=0x5A) return 0;
  m_chipid = m_symbol2data[bytes[1]];
  return 2;
}

uint32_t Clear::Pack(uint8_t * bytes){
  bytes[0]=m_symbol;
  bytes[1]=m_data2symbol[m_chipid];
  return 2;
}

uint32_t Clear::GetType(){
  return Command::CLEAR;
}

void Clear::SetChipId(uint32_t chipid){
  m_chipid=chipid&0x1F;
}

uint32_t Clear::GetChipId(){
  return m_chipid;
}


