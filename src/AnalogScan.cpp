
#include "RD53BEmulator/AnalogScan.h"
#include "RD53BEmulator/Masker.h"
#include "RD53BEmulator/Tools.h"
#include "TCanvas.h"
#include "TStyle.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace RD53B;

AnalogScan::AnalogScan(){
  m_ntrigs = 50;
  m_TriggerLatency = 50;
  m_colMin = 0; 
  m_colMax = 400;
}

AnalogScan::~AnalogScan(){}

void AnalogScan::PreRun(){

  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
  m_logFile << "\"m_TriggerLatency\": " << m_TriggerLatency << endl;
  
  if(m_verbose){
    cout << "AnalogScan: m_ntrigs = " << m_ntrigs << endl;
    cout << "AnalogScan: m_TriggerLatency = " << m_TriggerLatency << endl;
  }

  for(auto fe : GetFEs()){

    fe->BackupConfig();

    cout << "AnalogScan: Configure Analog injection" << endl;
    fe->GetConfig()->SetField(Configuration::CalMode,0); // Analog Mode
    //fe->GetConfig()->SetField(Configuration::CalAnaMode,1); // Analog fine delay
    fe->GetConfig()->SetField(Configuration::Latency,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::PIX_BCAST_EN,0);
    fe->GetConfig()->SetField(Configuration::PIX_AUTO_ROW,0);

    //fe->SetGlobalThreshold(Pixel::Diff,Tools::chargeToThr(100,2));

    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,3000);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,200);
    fe->GetConfig()->SetField(Configuration::InjVcalRange,1);
    //fe->GetConfig()->SetField(Configuration::DiffTh1M,0);


    fe->WriteGlobal();
    Send(fe);
       
    cout << "AnalogScan: Mask all" << endl;
    fe->DisableAll();
    ConfigurePixels(fe);
    //Send(fe);

    m_occ[fe->GetName()]    =new TH2I(("occ_"+fe->GetName()).c_str(),"Occupancy map;Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS,0, Matrix::NUM_ROWS);
    m_occC[fe->GetName()]   =new TH2I(("occC_"+fe->GetName()).c_str(),"Occupancy map (with cut);Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS,0, Matrix::NUM_ROWS);

    m_tot[fe->GetName()]    =new TH1I(("tot_"+fe->GetName()).c_str(),(fe->GetName()+";TOT").c_str(), 16, 0, 16);
    m_enable[fe->GetName()] =new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tid[fe->GetName()]    =new TH1I(("tid_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger ID").c_str(), 16, 0, 16);
    m_ttag[fe->GetName()]   =new TH1I(("ttag_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger Tag").c_str(), 16, 0, 16);
    m_bcid[fe->GetName()]   =new TH1I(("bcid_"+fe->GetName()).c_str(),(fe->GetName()+";BCID").c_str(),0xFFFF+1,-0.5,0xFFFF+0.5);
    m_ttagmap[fe->GetName()]=new TH2I(("ttagmap_"+fe->GetName()).c_str(),"TTag map;Column;Row;TTag accumulated", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_bcidmap[fe->GetName()]=new TH2I(("bcidmap_"+fe->GetName()).c_str(),"BCID map;Column;Row;BCID accumulated", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_hits[fe->GetName()]   =new HitTree(("hits_"+fe->GetName()).c_str(),fe->GetName());

  }

  //GetMasker()->SetRange(40,40,48,42);
  //GetMasker()->SetRange(0,0,200,200);
  //GetMasker()->SetRange(0,0,10,10);
  // //GetMasker()->SetShape(1,1); //8,2

  GetMasker()->SetVerbose(false);
  GetMasker()->SetRange(0,0,400,384);
  GetMasker()->SetShape(8,64);
  GetMasker()->SetFrequency(1); //50
  GetMasker()->Build();

}

void AnalogScan::Run(){

  //Prepare the trigger
  cout << "AnalogScan: Prepare the TRIGGER" << endl;
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // This is needed for diff in the disconnected bump config.
  PrepareTrigger(m_TriggerLatency-10, 4, true, true, true, true,1);
  
  //cout << "AnalogScan: Create the mask" << endl;
  for(auto fe : GetFEs()){ // RD53B is always a bit noisy at initialisation so clearing hits
    if(!fe->HasHits()) continue;
    fe->ClearHits();
  }

  GetMasker()->GetStep(0);
  ConfigMask();

  for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
    GetMasker()->GetStep(st);
    
    if (st==0) {
      for(auto fe : GetFEs()){ 
	if(!fe->HasHits()) continue;
	fe->ClearHits();
      }
    }

    //Do the mask step
    ConfigMask();
    ConfigMask();
    //std::this_thread::sleep_for(std::chrono::milliseconds(50)); //100
    
    //accumulate hits
    uint32_t ExpectedHits=0;
    for(auto fe : GetFEs()){ 
      ExpectedHits+=m_ntrigs*fe->GetNPixelsEnabled();
    }
    


    //histogram hit
    bool LastLoop=false;
    uint32_t nhits=0;
    uint32_t nRhits=0;
    uint32_t nGoodHists=0;


    //Trigger and read-out
    for(uint32_t trig=0;trig<m_ntrigs; trig++) Trigger();

    auto start = chrono::steady_clock::now();
    vector<Hit*> hits;
    uint32_t nH=0;
    while(true){
	
      for(auto fe : GetFEs()){
	hits=fe->GetHits(10000);
	nH=hits.size();
	if(nH>0) start = chrono::steady_clock::now();
	if (nH==0) {
	  this_thread::sleep_for(chrono::milliseconds( 1 ));
	
	} else {
	  for (unsigned int h=0; h<nH; h++) {
	
	    Hit* hit = hits.at(h);
	    if(hit==0) continue;
	    hit->SetEvNum(st);
	    //cout << "Hit V: " << fe->GetName() << ": " << hit->ToString() << endl;
	  
	    if(hit->GetTOT()>0)  { //added by VD to control the noise
	      m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	      nRhits++;
	    
	      //cout << " ::: " << hit->ToString() << endl;
	      if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
		//cout << "Hit not expected in mask step: " << fe->GetName() << ": " << hit->ToString() << endl;
		// fe->NextHit();
		// continue;
	      } else {
		m_occC[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
		nGoodHists++;
	      }
	      m_tot[fe->GetName()] ->Fill(hit->GetTOT());
	      m_tid[fe->GetName()] ->Fill(hit->GetTID());
	      m_ttag[fe->GetName()]->Fill(hit->GetTTag());
	      m_bcid[fe->GetName()]->Fill(hit->GetBCID());
	      m_bcidmap[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),hit->GetBCID());
	      m_ttagmap[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),hit->GetTTag());
	    } else {
	    }
	    nhits++;
	    //tree->Set(hit);
	    //tree->Fill();
	   
	  }
	  fe->ClearHits(nH);
	}
	//
      }
      auto end = chrono::steady_clock::now();
      uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
      if(LastLoop) break;
      if(ms>200 or nGoodHists>=ExpectedHits ){LastLoop=true;}
      //if(ms>300) {LastLoop=true;}
    }
    cout << "AnalogScan:: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() 
	 << " ==== NRealHits in step: " << nRhits << " / " << nGoodHists << " / " << ExpectedHits  
	 <<" :: ALL Hits: " << nhits << endl;
    
    //Undo the mask step
    UnconfigMask();
    UnconfigMask();
    //////std::this_thread::sleep_for(std::chrono::microseconds(1000000));
  }
  //UnconfigMask();
}
 
void AnalogScan::Analysis(){
  gStyle->SetOptStat(0);
  TCanvas* can=new TCanvas("plot","plot",800,600);
  for(auto fe : GetFEs()){
    cout << "AnalogScan: Restore the original threshold value" << endl;
    fe->RestoreBackupConfig();


    m_occ[fe->GetName()]->Write();
    m_occC[fe->GetName()]->Write();
    m_tot[fe->GetName()]->Write();
    m_enable[fe->GetName()]->Write();
    m_tid[fe->GetName()]->Write();
    m_ttag[fe->GetName()]->Write();
    m_bcid[fe->GetName()]->Write();
    m_ttagmap[fe->GetName()]->Write();
    m_bcidmap[fe->GetName()]->Write();
    if(m_storehits){m_hits[fe->GetName()]->Write();}

    m_occ[fe->GetName()]->Draw("COLZ");
    m_occ[fe->GetName()]->SetTitle( (fe->GetName()+" Occ").c_str() );
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC.pdf").c_str() );
    m_occC[fe->GetName()]->SetTitle( (fe->GetName()+" Occ (cut)").c_str() );
    m_occC[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCCcut.pdf").c_str() );    


    //Applying Analog Mask if maskopt is not 0
    std::cout<<"Mask Opt "<<m_maskopt<<std::endl;
    if(m_maskopt!=0){
      uint32_t nDisablePix=0;
      for(unsigned binX=0; binX<Matrix::NUM_COLS; binX++)
	for(unsigned binY=0; binY<Matrix::NUM_ROWS; binY++)
	  {
	    uint32_t BinVal= m_occ[fe->GetName()]->GetBinContent(binX+1,binY+1);
	    if( (m_ntrigs * 0.9) < BinVal && (m_ntrigs * 1.1) > BinVal ){
	      //std::cout<<"col,row: "<<binX<<","<<binY<<" = "<<BinVal<<" Accepted "<<std::endl;
	      fe->SetPixelEnable(binX, binY ,1); 
	      fe->SetPixelMask(binX, binY ,1); 
	    }
	    else {
	      nDisablePix+=1;
	      fe->SetPixelEnable(binX, binY ,0); 
	      fe->SetPixelMask(binX, binY ,0); 
	    }
	  }
      std::cout<<nDisablePix<<" Pixels have been disabled"<<std::endl;
    }



    delete m_occ[fe->GetName()];
    delete m_tot[fe->GetName()];
    delete m_enable[fe->GetName()];
    delete m_tid[fe->GetName()];
    delete m_ttag[fe->GetName()];
    delete m_bcid[fe->GetName()];
    delete m_ttagmap[fe->GetName()];
    delete m_bcidmap[fe->GetName()];
    delete m_hits[fe->GetName()];
    
  }
  delete can;
}
