#include "RD53BEmulator/Cal.h"
#include <sstream>
#include <iomanip>

using namespace std;
using namespace RD53B;

Cal::Cal(){
  m_symbol = 0x63;
  m_chipid = 0;
  m_edge_mode = 0;
  m_edge_delay = 0;
  m_edge_width = 0;
  m_aux_mode = 0;
  m_aux_delay = 0;
}

Cal::~Cal(){}


Cal::Cal(Cal * copy){
  m_symbol = copy->m_symbol;
  m_chipid = copy->m_chipid;
  m_edge_mode = copy->m_edge_mode;
  m_edge_delay = copy->m_edge_delay;
  m_edge_width = copy->m_edge_width;
  m_aux_mode = copy->m_aux_mode;
  m_aux_delay = copy->m_aux_delay;
}

Cal::Cal(uint32_t chipid, uint32_t edge_mode, uint32_t edge_delay, uint32_t edge_width, uint32_t aux_mode, uint32_t aux_delay){
  m_symbol = 0x63;
  m_chipid = chipid;
  m_edge_mode = edge_mode;
  m_edge_delay = edge_delay;
  m_edge_width = edge_width;
  m_aux_mode = aux_mode;
  m_aux_delay = aux_delay;
}



Cal * Cal::Clone(){
  Cal * clone = new Cal();
  clone->m_chipid = m_chipid;
  clone->m_edge_mode = m_edge_mode;
  clone->m_edge_delay = m_edge_delay;
  clone->m_edge_width = m_edge_width;
  clone->m_aux_mode = m_aux_mode;
  clone->m_aux_delay = m_aux_delay;
  return clone;
}

string Cal::ToString(){
  ostringstream os;
  os << "Cal 0x" << hex << m_symbol << dec
     << " ChipId: " << m_chipid
     << " (0x" << hex << m_chipid << dec << ")"
     << " edge_mode: " << m_edge_mode
     << " edge_delay: " << m_edge_delay
     << " (0x" << hex << m_edge_delay << dec << ")"
     << " edge_duration: " << m_edge_width
     << " (0x" << hex << m_edge_width << dec << ")"
     << " aux_mode: " << m_aux_mode
     << " aux_delay: " << m_aux_delay
     << " (0x" << hex << m_aux_delay << dec << ")";
  return os.str();
}

bool Cal::FromString(string str){
  istringstream is(str);
  uint32_t hsym;
  string hchid, hemod, hedel, hedur, hamod, hadel, tmp;
  is >> tmp >> hex >> hsym >> dec;
  if(hsym!=m_symbol){return false;}
  is >> hchid >> m_chipid >> tmp 
     >> hemod >> m_edge_mode 
     >> hedel >> m_edge_delay >> tmp
     >> hedel >> m_edge_width >> tmp
     >> hamod >> m_aux_mode
     >> hadel >> m_aux_delay >> tmp;
  return true;
}

uint32_t Cal::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<6) return 0;
  if(bytes[0]!=0x63) return 0;
  m_chipid        = m_symbol2data[bytes[1]];
  m_edge_mode     =(m_symbol2data[bytes[2]]>>4)&0x1;
  m_edge_delay    =(m_symbol2data[bytes[2]]&0xF)<<1;
  m_edge_delay   |=(m_symbol2data[bytes[3]]>>4)&0x1;
  m_edge_width =(m_symbol2data[bytes[3]]&0xF)<<4;
  m_edge_width|=(m_symbol2data[bytes[4]]>>1)&0xF;
  m_aux_mode      =(m_symbol2data[bytes[4]]>>0)&0x1;
  m_aux_delay     =(m_symbol2data[bytes[5]]>>0)&0x1F;
  return 6;
}

uint32_t Cal::Pack(uint8_t * bytes){
  bytes[0]=((m_symbol>>0)&0xFF);
  bytes[1]=m_data2symbol[((m_chipid       &0x1F)) ];
  bytes[2]=m_data2symbol[((m_edge_mode    &0x01)<<4)|((m_edge_delay   >>1)&0x0F)];
  bytes[3]=m_data2symbol[((m_edge_delay   &0x01)<<4)|((m_edge_width>>4)&0x0F)];
  bytes[4]=m_data2symbol[((m_edge_width&0x0F)<<1)|((m_aux_mode     >>0)&0x01)];
  bytes[5]=m_data2symbol[((m_aux_delay    &0x1F)<<0)];
  return 6;
}

uint32_t Cal::GetType(){
  return Command::CAL;
}

void Cal::SetChipId(uint32_t chipid){
  m_chipid=chipid&0x1F;
}

uint32_t Cal::GetChipId(){
  return m_chipid;
}

void Cal::SetEdgeMode(uint32_t edge_mode){
  m_edge_mode=edge_mode&0x1;
}

uint32_t Cal::GetEdgeMode(){
  return m_edge_mode;
}

void Cal::SetEdgeDelay(uint32_t edge_delay){
  m_edge_delay=edge_delay&0x1F;
}

uint32_t Cal::GetEdgeDelay(){
  return m_edge_delay;
}

void Cal::SetEdgeWidth(uint32_t edge_duration){
  m_edge_width=edge_duration&0xFF;
}

uint32_t Cal::GetEdgeWidth(){
  return m_edge_width;
}

void Cal::SetAuxMode(uint32_t aux_mode){
  m_aux_mode=aux_mode&0x1;
}

uint32_t Cal::GetAuxMode(){
  return m_aux_mode;
}

void Cal::SetAuxDelay(uint32_t aux_delay){
  m_aux_delay=aux_delay&0x1F;
}

uint32_t Cal::GetAuxDelay(){
  return m_aux_delay;
}
