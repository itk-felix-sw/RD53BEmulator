#include "RD53BEmulator/Stream.h"

#include <iostream>
#include <ctime>
#include <vector>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cmdl/cmdargs.h>

using namespace std;
using namespace RD53B;

int main(int argc, char ** argv) {
  
  cout << "#################################" << endl
       << "# profile_rd53b_stream          #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgInt  cPattern0('p',"pattern0","pattern","first pattern (default=0)");
  CmdArgInt  cPatternN('P',"patternN","pattern","last pattern (default=0xFFFF)");
  CmdArgInt  cNumHits( 'n',"num-hits","number","number of hits per event (default=1)");
  CmdArgStr  cOutfile( 'o',"outfile", "filename","output file");
  CmdArgBool cNoTot(   'T',"no-tot","turn off tot");
  CmdArgBool cNoEnc(   'E',"no-enc","turn off encoding");

  CmdLine cmdl(*argv,&cVerbose,&cPattern0,&cPatternN,&cNumHits,&cOutfile,&cNoTot,&cNoEnc,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  ostringstream clog;
  for(int32_t pattern=cPattern0;pattern<=cPatternN;pattern++){
    cout << "Pattern: 0x" << hex << pattern << dec << endl;

    if(cVerbose) cout << "Create a new stream" << endl;
    Stream * stream = new Stream();
    stream->EnableEnc(!cNoEnc);
    stream->EnableTot(!cNoTot);
    if(cVerbose) cout << "Adding hits" << endl;
    for(int32_t hit=1;hit<cNumHits;hit++){
      stream->AddHit(1,2,3,pattern,pattern);
    }

    vector<bool> bits(1000,0);

    if(cVerbose) cout << "Encode" << endl;
    //stream->Pack(&bits);

    if(cVerbose) cout << "Decode" << endl;
    //stream->UnPack(&bits);

    vector<bool> bits2=bits;

    uint32_t n_test=1E6;
    clock_t start;
    double duration, frequency;

    clog << "Nhits: " << cNumHits << ", "
         << "Pattern: 0x" << hex << pattern << dec << ", ";

    if(cVerbose) cout << "Encoding performance" << endl;
    start = clock();
    for(uint32_t i=0; i<n_test; i++){
      //stream->Pack(&bits);
    }
    duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
    frequency = (double) n_test / duration;
  
    if(cVerbose) cout << "CPU time  [s] : " << duration << endl;
    if(cVerbose) cout << "Frequency [MHz]: " << frequency/1E6 << endl;
    clog << "Encoding [MHz]: " << fixed << setprecision(3) << frequency/1E6 << ", ";

    if(cVerbose) cout << "Decoding performance" << endl;
    start = clock();
    for(uint32_t i=0; i<n_test; i++){
      //stream->UnPack(&bits2);
    }
    duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
    frequency = (double) n_test / duration;
  
    if(cVerbose) cout << "CPU time  [s] : " << duration << endl;
    if(cVerbose) cout << "Frequency [MHz]: " << frequency/1E6 << endl;
    clog << "Decoding [MHz]: " << fixed << setprecision(3) << frequency/1E6 << endl;

    if(cVerbose) cout << "Cleaning the house" << endl;
    delete stream;
  
  }
  
  cout << clog.str() << endl;

  ofstream ofs(cOutfile,ofstream::trunc);
  ofs << clog.str() << endl;
  ofs.close();

  cout << "Have a nice day" << endl;
  return 0;
}

