#include "RD53BEmulator/DataFrame.h"

#include <iostream>
#include <ctime>
#include <vector>
#include <iomanip>
#include <chrono>
#include <fstream>
#include <cmdl/cmdargs.h>

using namespace std;
using namespace RD53B;

int main(int argc, char ** argv) {
  
  cout << "#################################" << endl
       << "# profile_rd53b_data            #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgIntList cRow(  'r',"row","quad-core-row","0 to 191");
  CmdArgIntList cCol(  'c',"col","core-col","0 to 50");
  CmdArgInt  cPattern( 'p',"pattern0","pattern","16-bit pattern");
  CmdArgInt  cNumEvts( 'n',"num-events","","number of hits per event");
  CmdArgStr  cOutfile( 'o',"outfile", "filename","output file");
  CmdArgBool cNoTot(   'T',"no-tot","turn off tot");
  CmdArgBool cNoBin(   'B',"no-bin","turn off binary tree");
  CmdArgBool cNoDec(   'D',"no-dec","turn off event decoding");

  cNumEvts=1;

  CmdLine cmdl(*argv,&cVerbose,&cRow,&cCol,&cPattern,&cNumEvts,&cOutfile,&cNoTot,&cNoBin,&cNoDec,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  cout << "Create a new stream" << endl;
  Stream * stream = new Stream();
  stream->EnableTot(!cNoTot);
  stream->EnableEnc(!cNoBin);
  cout << "Adding hits" << endl;
  for(int32_t evt=0; evt<cNumEvts; evt++){
    for(uint32_t i=0; i<cRow.count(); i++){
      stream->AddHit(evt+1,cCol[i],cRow[i],cPattern,cPattern);
    }
  }

  cout << "Create new data" << endl;
  DataFrame * data = new DataFrame();
  data->SetVerbose(cVerbose);
  data->SetEnc(!cNoBin);

  cout << "Add stream to data" << endl;
  data->AddStream(stream);

  cout << "Data to string" << endl;
  cout << data->ToString() << endl;

  cout << "Create a byte stream" << endl;
  vector<uint8_t> bytes(1E8,0);

  cout << "Encode" << endl;
  uint32_t sz=data->Pack(&bytes[0]);

  if(cVerbose){
    cout << "Byte stream (" << sz << "): " << DataFrame::ToByteStream(&bytes[0],sz) << endl;
  }

  cout << "Clear" << endl;
  data->Clear();

  cout << "Decode" << endl;
  data->UnPack(&bytes[0],sz);

  cout << data->ToString() << endl;

  if(cVerbose){
    cout << "Cleaning the house" << endl;
    delete data;

    cout << "Have a nice day" << endl;
    return 0;
  }

  vector<uint8_t> bytes2=bytes;
  uint32_t n_test=1E5;
  
  cout << "Decode performance measurement" << endl;
  auto start = std::chrono::steady_clock::now();
  for(uint32_t i=0; i<n_test; i++){
    data->UnPack(&bytes2[0],sz);
  }
  auto end = std::chrono::steady_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
  float frequency = (float) n_test / duration;
  
  ostringstream clog;

  clog << "Quad Core Rows : "; for(uint32_t i=0; i<cRow.count(); i++){ clog << cRow[i] << " "; }; clog << endl;
  clog << "Core Cols      : "; for(uint32_t i=0; i<cCol.count(); i++){ clog << cCol[i] << " "; }; clog << endl;
  clog << "NumQuadCoreRows: " << cRow.count() << endl;
  clog << "NumCoreCols    : " << cCol.count() << endl;
  clog << "Pattern        : 0x" << hex << cPattern << dec << endl;
  clog << "NumEvents      : " << cNumEvts << endl;
  clog << "NumTries       : " << n_test << endl;
  clog << "Enable Decoding: " << !cNoDec << endl;
  clog << "Enable TOT     : " << !cNoTot << endl;
  clog << "Enable BinTree : " << !cNoBin << endl;
  clog << "CPU time  [s]  : " << fixed << setprecision(3) << duration/1E6 << endl;
  clog << "Frequency [MHz]: " << fixed << setprecision(3) << frequency << endl;

  cout << clog.str();

  ofstream ofs(cOutfile,ofstream::trunc);
  ofs << clog.str();
  ofs.close();

  cout << "Cleaning the house" << endl;
  delete data;

  cout << "Have a nice day" << endl;
  return 0;
}

