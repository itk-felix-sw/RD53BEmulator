#include "RD53BEmulator/Configuration.h"
#include <iostream>

using namespace std;
using namespace RD53B;


std::map<std::string, uint32_t> Configuration::m_name2index={
  {"PixPortal",PIX_PORTAL},
  {"PixRegionCol",REGION_COL},  
  {"PixRegionRow",REGION_ROW},  
  {"PixBroadcast",PIX_BCAST_EN},
  {"PixConfMode",PIX_CONF_MODE},
  {"PixAutoRow",PIX_AUTO_ROW}, 
  {"PixDefaultConfig",PIX_DEFAULT_CONFIG},
  {"PixDefaultConfigB",PIX_DEFAULT_CONFIG_B}, 
  {"GcrDefaultConfig",GCR_DEFAULT_CONFIG},   
  {"GcrDefaultConfigB",GCR_DEFAULT_CONFIG_B}, 
  {"DiffPreampL",DAC_PREAMP_L_DIFF},    
  {"DiffPreampR",DAC_PREAMP_R_DIFF},    
  {"DiffPreampTL",DAC_PREAMP_TL_DIFF},   
  {"DiffPreampTR",DAC_PREAMP_TR_DIFF},   
  {"DiffPreampT",DAC_PREAMP_T_DIFF},    
  {"DiffPreampM",DAC_PREAMP_M_DIFF},    
  {"DiffPreComp",DAC_PRECOMP_DIFF},     
  {"DiffComp",DAC_COMP_DIFF},        
  {"DiffVff",DAC_VFF_DIFF},         
  {"DiffTh1L",DAC_TH1_L_DIFF},       
  {"DiffTh1R",DAC_TH1_R_DIFF},       
  {"DiffTh1M",DAC_TH1_M_DIFF},       
  {"DiffTh2",DAC_TH2_DIFF},         
  {"DiffLcc",DAC_LCC_DIFF},         
  {"DiffLccEn",EN_LCC_DIFF},          
  {"DiffFbCapEn",EN_CAP_FB_DIFF},       
  {"SldoEnUndershuntA",EN_SHUNT_ANA},         
  {"SldoEnUndershuntD",EN_SHUNT_DIG},         
  {"SldoTrimA",DAC_SHUNT_ANA},        
  {"SldoTrimD",DAC_SHUNT_DIG},        
  {"EnCoreCol3",EnCoreCol_3},          
  {"EnCoreCol2",EnCoreCol_2},          
  {"EnCoreCol1",EnCoreCol_1},          
  {"EnCoreCol0",EnCoreCol_0},          
  {"RstCoreCol3",EnCoreColReset_3},     
  {"RstCoreCol2",EnCoreColReset_2},     
  {"RstCoreCol1",EnCoreColReset_1},     
  {"RstCoreCol0",EnCoreColReset_0},     
  {"TwoLevelTrig",TwoLevelTrigger},      
  {"Latency",Latency},              
  {"SelfTrigEn",SelfTrigEn},           
  {"SelfTrigDigThrEn",EnSelfTrigTot},        
  {"SelfTrigDigThr",SelfTrigTot},          
  {"SelfTrigDelay",SelfTrigDelay},        
  {"SelfTrigMulti",SelfTrigMult},         
  {"SelfTrigPattern",HitOrPatternLUT},      
  {"DataReadDelay",ReadTriggerColDelay},  
  {"ReadTrigLatency",ReadTriggerClearTime}, 
  {"TruncTimeoutConf",TruncationTimeoutConf},
  {"InjDigEn",CalMode},              
  {"InjAnaMode",CalAnaMode},           
  {"InjFineDelay",CalFineDelay},         
  {"FineDelayClk",CmdClockFineDelay},    
  {"FineDelayData",CmdDataFineDelay},     
  {"InjVcalHigh",VCAL_HIGH},            
  {"InjVcalMed",VCAL_MED},             
  {"CapMeasEnPar",CapMeasEnPar},         
  {"CapMeasEn",CapMeasPar},           
  {"InjVcalRange",InjVcalRange},         
  {"CdrOverwriteLimit",OverwriteCdrLimit},    
  {"CdrPhaseDetSel",CdrSelPd},             
  {"CdrClkSel",SER_CLK},              
  {"ChSyncLockThr",ChSyncConf},           
  {"GlobalPulseConf",GlobalPulseConf},      
  {"GlobalPulseWidth",GlobalPulseWidth},     
  {"ServiceBlockEn",ServiceDataEnable},    
  {"ServiceBlockPeriod",ServiceDataPeriod},    
  {"TotEnPtot",ToTEnable},            
  {"TotEnPtoa",ToAEnable},            
  {"TotEn80",Enable80MHz},          
  {"TotEn6b4b",Enable6b4b},           
  {"TotPtotLatency",ToTLatency},           
  {"PtotCoreColEn3",PrecisionToTEnable_3}, 
  {"PtotCoreColEn2",PrecisionToTEnable_2}, 
  {"PtotCoreColEn1",PrecisionToTEnable_1}, 
  {"PtotCoreColEn0",PrecisionToTEnable_0}, 
  {"DataMergeInPol",DataMergePol},         
  {"EnChipId",EnChnId},              
  {"DataMergeEnClkGate",DataMergeEnClkGate},   
  {"DataMergeSelClk",DataMergeSelClk},      
  {"DataMergeEn",DataMergeEn},          
  {"DataMergeEnBond",ChnBond},              
  {"DataMergeInMux3",DataMergeInMux_3},     
  {"DataMergeInMux2",DataMergeInMux_2},     
  {"DataMergeInMux1",DataMergeInMux_1},     
  {"DataMergeInMux0",DataMergeInMux_0},     
  {"DataMergeOutMux3",DataMergeOutMux_3},    
  {"DataMergeOutMux2",DataMergeOutMux_2},    
  {"DataMergeOutMux1",DataMergeOutMux_1},    
  {"DataMergeOutMux0",DataMergeOutMux_0},    
  {"EnCoreColCal3",EnCoreColCal_3},       
  {"EnCoreColCal2",EnCoreColCal_2},       
  {"EnCoreColCal1",EnCoreColCal_1},       
  {"EnCoreColCal0",EnCoreColCal_0},       
  {"DataEnBcid",EnBCID},               
  {"DataEnL1id",EnL1ID},               
  {"DataEnEos",EnEOS},                
  {"NumOfEventsInStream",NumOfEventsInStream},  
  {"DataEnBinaryRo",DropToT},              
  {"DataEnRaw",EnRawMap},             
  {"DataEnHitRemoval",EnHitRemoval},         
  {"DataMaxHits",MaxHits},              
  {"DataEnIsoHitRemoval",EnIsoHitRemoval},      
  {"DataMaxTot",MaxToT},               
  {"EvenMask",EvenMask},             
  {"OddMask",OddMask},              
  {"EfuseConfig",EfuseConfig},          
  {"EfuseWriteData1",EfuseWriteData1},      
  {"EfuseWriteData0",EfuseWriteData0},      
  {"AuroraEnPrbs",AURORA_EN_PRBS},       
  {"AuroraActiveLanes",AURORA_EN_LANES},      
  {"AuroraCCWait",AURORA_CC_WAIT},       
  {"AuroraCCSend",AURORA_CC_SEND},       
  {"AuroraCBWait1",AURORA_CB_WAIT1},      
  {"AuroraCBWait0",AURORA_CB_WAIT0},      
  {"AuroraCBSend",AURORA_CB_SEND},       
  {"AuroraInitWait",AURORA_INIT_WAIT},
  {"GpValReg",GP_VAL_REG},
  {"GpCmosEn",GP_CMOS_EN},
  {"GpCmosDs",GP_CMOS_DS},
  {"GpLvdsEn",GP_LVDS_EN},
  {"GpLvdsBias",GP_LVDS_BIAS},
  {"GpCmosRoute",GP_CMOS_ROUTE},
  {"GpLvdsPad3",GP_LVDS_PAD_3},
  {"GpLvdsPad2",GP_LVDS_PAD_2},
  {"GpLvdsPad1",GP_LVDS_PAD_1},
  {"GpLvdsPad0",GP_LVDS_PAD_0},
  {"CdrCp",DAC_CP_CDR},
  {"CdrCpFd",DAC_CP_FD_CDR},
  {"CdrCpBuff",DAC_CP_BUFF_CDR},
  {"CdrVco",DAC_VCO_CDR},
  {"CdrVcoBuff",DAC_VCOBUFF_CDR},
  {"SerSelOut3",SER_SEL_OUT_3},
  {"SerSelOut2",SER_SEL_OUT_2},
  {"SerSelOut1",SER_SEL_OUT_1},
  {"SerSelOut0",SER_SEL_OUT_0},
  {"SerInvTap",SER_INV_TAP},
  {"SerEnTap",SER_EN_TAP},
  {"SerEnLane",SER_EN_LANE},  
  {"CmlBias2",DAC_CML_BIAS_2},
  {"CmlBias1",DAC_CML_BIAS_1},
  {"CmlBias0",DAC_CML_BIAS_0},
  {"MonitorEnable",MonitorPinEn}, 
  {"MonitorI",MonitorIMuxSel},
  {"MonitorV",MonitorVMuxSel}, 
  {"ErrWngMask",ErrWngMask},
  {"MonSensSldoDigEn",MON_SENS_SLDO_DIG_EN},
  {"MonSensSldoDigDem",MON_SENS_SLDO_DIG_DEM},
  {"MonSensSldoDigSelBias",MON_SENS_SLDO_DIG_BIAS},
  {"MonSensSldoAnaEn",MON_SENS_SLDO_ANA_EN},
  {"MonSensSldoAnaDem",MON_SENS_SLDO_ANA_DEM},
  {"MonSensSldoAnaSelBias",MON_SENS_SLDO_ANA_BIAS},
  {"MonSensAcbEn",MON_SENS_ACB_EN},
  {"MonSensAcbDem",MON_SENS_ACB_DEM},
  {"MonSensAcbSelBias",MON_SENS_ACB_BIAS},
  {"VrefRsensBot",VREF_RSENSE_BOT},
  {"VrefRsensTop",VREF_RSENSE_TOP},
  {"VrefIn",VREF_IN},
  {"MonAdcTrim",MON_ADC_TRIM},
  {"NtcDac",DAC_NTC},
  {"HitOrMask3",HITOR_MASK_3},
  {"HitOrMask2",HITOR_MASK_2},
  {"HitOrMask1",HITOR_MASK_1},
  {"HitOrMask0",HITOR_MASK_0},
  {"AutoRead0",AutoRead0},
  {"AutoRead1",AutoRead1},
  {"AutoRead2",AutoRead2},
  {"AutoRead3",AutoRead3},
  {"AutoRead4",AutoRead4},
  {"AutoRead5",AutoRead5},
  {"AutoRead6",AutoRead6},
  {"AutoRead7",AutoRead7},
  {"RingOscBClear",RING_OSC_A_ROUTE},
  {"RingOscBEnBl",RING_OSC_B_LEFT_EN},
  {"RingOscBEnBr",RING_OSC_B_RIGHT_EN},
  {"RingOscBEnCapA",RING_OSC_B_CAPA_EN},
  {"RingOscBEnFf",RING_OSC_B_FF_EN},
  {"RingOscBEnLvt",RING_OSC_B_LVT_EN},
  {"RingOscAClear",RING_OSC_A_CLR},
  {"RingOscAEn",RING_OSC_A_EN},
  {"RingOscARoute",RING_OSC_A_ROUTE},
  {"RingOscBRoute",RING_OSC_B_ROUTE},
  {"RingOscAOut",RING_OSC_A_OUT},
  {"RingOscBOut",RING_OSC_B_OUT},
  {"BcidCnt",BCIDCnt},
  {"TrigCnt",TrigCnt},
  {"ReadTrigCnt",ReadTrigCnt},
  {"LockLossCnt",LockLossCnt},
  {"BitFlipWngCnt",BitFlipWngCnt},
  {"BitFlipErrCnt",BitFlipErrCnt},
  {"CmdErrCnt",CmdErrCnt},
  {"RdWrFifoErrCnt",RdWrFifoErrCnt},
  {"AiRegionRow",AI_REGION_ROW},
  {"HitOrCnt3",HITOR_3_CNT},
  {"HitOrCnt2",HITOR_2_CNT},
  {"HitOrCnt1",HITOR_1_CNT},
  {"HitOrCnt0",HITOR_0_CNT},
  {"SkippedTrigCnt",SkippedTriggerCnt},
  {"EfuseReadData1",EfusesReadData1},
  {"EfuseReadData0",EfusesReadData0},
  {"MonitoringDataAdc",MonitoringDataADC}
};

Configuration::Configuration(){
  m_verbose=false;
  m_registers.resize(138);
  m_backup.resize(138);

  m_fields[PIX_PORTAL]           = new Field(&m_registers[ 0], 0,16,0);
  m_fields[REGION_COL]           = new Field(&m_registers[ 1], 0, 8,0);
  m_fields[REGION_ROW]           = new Field(&m_registers[ 2], 0, 9,0);
  m_fields[PIX_BCAST_EN]         = new Field(&m_registers[ 3], 2, 1,0);
  m_fields[PIX_CONF_MODE]        = new Field(&m_registers[ 3], 1, 1,1);
  m_fields[PIX_AUTO_ROW]         = new Field(&m_registers[ 3], 0, 1,0);
  m_fields[PIX_DEFAULT_CONFIG]   = new Field(&m_registers[ 4], 0,16,0);
  m_fields[PIX_DEFAULT_CONFIG_B] = new Field(&m_registers[ 5], 0,16,0);
  m_fields[GCR_DEFAULT_CONFIG]   = new Field(&m_registers[ 6], 0,16,0);
  m_fields[GCR_DEFAULT_CONFIG_B] = new Field(&m_registers[ 7], 0,16,0);
  m_fields[DAC_PREAMP_L_DIFF]    = new Field(&m_registers[ 8], 0,10,50);
  m_fields[DAC_PREAMP_R_DIFF]    = new Field(&m_registers[ 9], 0,10,50);
  m_fields[DAC_PREAMP_TL_DIFF]   = new Field(&m_registers[10], 0,10,50);
  m_fields[DAC_PREAMP_TR_DIFF]   = new Field(&m_registers[11], 0,10,50);
  m_fields[DAC_PREAMP_T_DIFF]    = new Field(&m_registers[12], 0,10,50);
  m_fields[DAC_PREAMP_M_DIFF]    = new Field(&m_registers[13], 0,10,50);
  m_fields[DAC_PRECOMP_DIFF]     = new Field(&m_registers[14], 0,10,50);
  m_fields[DAC_COMP_DIFF]        = new Field(&m_registers[15], 0,10,50);
  m_fields[DAC_VFF_DIFF]         = new Field(&m_registers[16], 0,10,50);
  m_fields[DAC_TH1_L_DIFF]       = new Field(&m_registers[17], 0,10,100);
  m_fields[DAC_TH1_R_DIFF]       = new Field(&m_registers[18], 0,10,100);
  m_fields[DAC_TH1_M_DIFF]       = new Field(&m_registers[19], 0,10,100);
  m_fields[DAC_TH2_DIFF]         = new Field(&m_registers[20], 0,10,100);
  m_fields[DAC_LCC_DIFF]         = new Field(&m_registers[21], 0,10,0);

  m_fields[EN_LCC_DIFF]          = new Field(&m_registers[37], 1, 1,0);
  m_fields[EN_CAP_FB_DIFF]       = new Field(&m_registers[37], 0, 1,0);
  m_fields[EN_SHUNT_ANA]         = new Field(&m_registers[38], 9, 1,0);
  m_fields[EN_SHUNT_DIG]         = new Field(&m_registers[38], 8, 1,0);
  m_fields[DAC_SHUNT_ANA]        = new Field(&m_registers[38], 4, 4,8);
  m_fields[DAC_SHUNT_DIG]        = new Field(&m_registers[38], 0, 4,8);

  m_fields[EnCoreCol_3]          = new Field(&m_registers[39], 0, 6,63);
  m_fields[EnCoreCol_2]          = new Field(&m_registers[40], 0,16,65535);
  m_fields[EnCoreCol_1]          = new Field(&m_registers[41], 0,16,65535);
  m_fields[EnCoreCol_0]          = new Field(&m_registers[42], 0,16,65535);
  m_fields[EnCoreColReset_3]     = new Field(&m_registers[43], 0, 6,0);
  m_fields[EnCoreColReset_2]     = new Field(&m_registers[44], 0,16,0);
  m_fields[EnCoreColReset_1]     = new Field(&m_registers[45], 0,16,0);
  m_fields[EnCoreColReset_0]     = new Field(&m_registers[46], 0,16,0);

  m_fields[TwoLevelTrigger]      = new Field(&m_registers[47], 9,1,0);
  m_fields[Latency]              = new Field(&m_registers[47], 0,9,500);
  m_fields[SelfTrigEn]           = new Field(&m_registers[48], 5,1,0);
  m_fields[EnSelfTrigTot]        = new Field(&m_registers[48], 4,1,1);
  m_fields[SelfTrigTot]          = new Field(&m_registers[48], 0,4,1);
  m_fields[SelfTrigDelay]        = new Field(&m_registers[49], 5,10,0);
  m_fields[SelfTrigMult]         = new Field(&m_registers[49], 0, 5,0);
  m_fields[HitOrPatternLUT]      = new Field(&m_registers[50], 0,16,0);
  m_fields[ReadTriggerColDelay]  = new Field(&m_registers[51], 12, 2,0);
  m_fields[ReadTriggerClearTime] = new Field(&m_registers[51], 0,12,1000);
  m_fields[TruncationTimeoutConf]= new Field(&m_registers[52], 0,12,0);
  m_fields[CalMode]              = new Field(&m_registers[53], 7, 1,0);
  m_fields[CalAnaMode]           = new Field(&m_registers[53], 6, 1,0);
  m_fields[CalFineDelay]         = new Field(&m_registers[53], 0, 6,0);
  m_fields[CmdClockFineDelay]    = new Field(&m_registers[54], 6, 6,0);
  m_fields[CmdDataFineDelay]     = new Field(&m_registers[54], 0, 6,0);
  m_fields[VCAL_HIGH]            = new Field(&m_registers[55], 0,12,500);
  m_fields[VCAL_MED]             = new Field(&m_registers[56], 0,12,300);
  m_fields[CapMeasEnPar]         = new Field(&m_registers[57], 2, 1,0);
  m_fields[CapMeasPar]           = new Field(&m_registers[57], 1, 1,0);
  m_fields[InjVcalRange]         = new Field(&m_registers[57], 0, 1,0);
  m_fields[OverwriteCdrLimit]    = new Field(&m_registers[58], 4, 1,0);
  m_fields[CdrSelPd]             = new Field(&m_registers[58], 3, 1,0);
  m_fields[SER_CLK]              = new Field(&m_registers[58], 0, 3,0);
  m_fields[ChSyncConf]           = new Field(&m_registers[59], 0, 5,16);
  m_fields[GlobalPulseConf]      = new Field(&m_registers[60], 0,16,300);
  m_fields[GlobalPulseWidth]     = new Field(&m_registers[61], 0,8,300);
  m_fields[ServiceDataEnable]    = new Field(&m_registers[62], 8, 1,0);
  m_fields[ServiceDataPeriod]    = new Field(&m_registers[62], 0, 8,50);
  m_fields[ToTEnable]            = new Field(&m_registers[63],12, 1,0);
  m_fields[ToAEnable]            = new Field(&m_registers[63],11, 1,0);
  m_fields[Enable80MHz]          = new Field(&m_registers[63],10, 1,0);
  m_fields[Enable6b4b]           = new Field(&m_registers[63], 9, 1,0);
  m_fields[ToTLatency]           = new Field(&m_registers[63], 0, 9,500);
  m_fields[PrecisionToTEnable_3] = new Field(&m_registers[64], 0, 6,0);
  m_fields[PrecisionToTEnable_2] = new Field(&m_registers[65], 0,16,0);
  m_fields[PrecisionToTEnable_1] = new Field(&m_registers[66], 0,16,0);
  m_fields[PrecisionToTEnable_0] = new Field(&m_registers[67], 0,16,0);
  m_fields[DataMergePol]         = new Field(&m_registers[68], 8, 4,0);
  m_fields[EnChnId]              = new Field(&m_registers[68], 7, 1,1);
  m_fields[DataMergeEnClkGate]   = new Field(&m_registers[68], 6, 1,1);
  m_fields[DataMergeSelClk]      = new Field(&m_registers[68], 5, 1,0);
  m_fields[DataMergeEn]          = new Field(&m_registers[68], 1, 4,0);
  m_fields[ChnBond]              = new Field(&m_registers[68], 0, 1,0);
  m_fields[DataMergeInMux_3]     = new Field(&m_registers[69],14, 2,3);
  m_fields[DataMergeInMux_2]     = new Field(&m_registers[69],12, 2,2);
  m_fields[DataMergeInMux_1]     = new Field(&m_registers[69],10, 2,1);
  m_fields[DataMergeInMux_0]     = new Field(&m_registers[69], 8, 2,0);
  m_fields[DataMergeOutMux_3]    = new Field(&m_registers[69], 6, 2,0);
  m_fields[DataMergeOutMux_2]    = new Field(&m_registers[69], 4, 2,1);
  m_fields[DataMergeOutMux_1]    = new Field(&m_registers[69], 2, 2,2);
  m_fields[DataMergeOutMux_0]    = new Field(&m_registers[69], 0, 2,3);
  m_fields[EnCoreColCal_3]       = new Field(&m_registers[70], 0, 6,0x3F);
  m_fields[EnCoreColCal_2]       = new Field(&m_registers[71], 0,16,0xFFFF);
  m_fields[EnCoreColCal_1]       = new Field(&m_registers[72], 0,16,0xFFFF);
  m_fields[EnCoreColCal_0]       = new Field(&m_registers[73], 0,16,0xFFFF);
  m_fields[EnBCID]               = new Field(&m_registers[74],10, 1,0);
  m_fields[EnL1ID]               = new Field(&m_registers[74], 9, 1,0);
  m_fields[EnEOS]                = new Field(&m_registers[74], 8, 1,1);
  m_fields[NumOfEventsInStream]  = new Field(&m_registers[74], 0, 8,1);
  m_fields[DropToT]              = new Field(&m_registers[75], 10, 1,0);
  m_fields[EnRawMap]             = new Field(&m_registers[75], 9, 1,0);
  m_fields[EnHitRemoval]         = new Field(&m_registers[75], 8, 1,0);
  m_fields[MaxHits]              = new Field(&m_registers[75], 4, 4,0);
  m_fields[EnIsoHitRemoval]      = new Field(&m_registers[75], 3, 1,0);
  m_fields[MaxToT]               = new Field(&m_registers[75], 0, 3,0);

  m_fields[EvenMask]             = new Field(&m_registers[76], 0,16,0);
  m_fields[OddMask]              = new Field(&m_registers[77], 0,16,0);
  m_fields[EfuseConfig]          = new Field(&m_registers[78], 0,16,0);
  m_fields[EfuseWriteData1]      = new Field(&m_registers[79], 0,16,0);
  m_fields[EfuseWriteData0]      = new Field(&m_registers[80], 0,16,0);
  m_fields[AURORA_EN_PRBS]       = new Field(&m_registers[81],12, 1,0);
  m_fields[AURORA_EN_LANES]      = new Field(&m_registers[81], 8, 4,0);
  m_fields[AURORA_CC_WAIT]       = new Field(&m_registers[81], 2, 6,0);
  m_fields[AURORA_CC_SEND]       = new Field(&m_registers[81], 0, 2,0);
  m_fields[AURORA_CB_WAIT1]      = new Field(&m_registers[82], 0, 8,255);
  m_fields[AURORA_CB_WAIT0]      = new Field(&m_registers[83], 4,12,4095);
  m_fields[AURORA_CB_SEND]       = new Field(&m_registers[83], 0, 4,0);
  m_fields[AURORA_INIT_WAIT]     = new Field(&m_registers[84], 0,11,32);
  m_fields[GP_VAL_REG]           = new Field(&m_registers[85], 9, 4,5);
  m_fields[GP_CMOS_EN]           = new Field(&m_registers[85], 8, 1,1);
  m_fields[GP_CMOS_DS]           = new Field(&m_registers[85], 7, 1,0);
  m_fields[GP_LVDS_EN]           = new Field(&m_registers[85], 3, 4,15);
  m_fields[GP_LVDS_BIAS]         = new Field(&m_registers[85], 0, 3,7);

  m_fields[GP_CMOS_ROUTE]        = new Field(&m_registers[86], 0, 7,34);
  m_fields[GP_LVDS_PAD_3]        = new Field(&m_registers[87], 6, 6,35);
  m_fields[GP_LVDS_PAD_2]        = new Field(&m_registers[87], 0, 6,34);
  m_fields[GP_LVDS_PAD_1]        = new Field(&m_registers[88], 6, 6,1);
  m_fields[GP_LVDS_PAD_0]        = new Field(&m_registers[88], 0, 6,0);
  m_fields[DAC_CP_CDR]           = new Field(&m_registers[89], 0,10,40);
  m_fields[DAC_CP_FD_CDR]        = new Field(&m_registers[90], 0,10,400);
  m_fields[DAC_CP_BUFF_CDR]      = new Field(&m_registers[91], 0,10,200);
  m_fields[DAC_VCO_CDR]          = new Field(&m_registers[92], 0,10,1023);
  m_fields[DAC_VCOBUFF_CDR]      = new Field(&m_registers[93], 0,10,500);
  m_fields[SER_SEL_OUT_3]        = new Field(&m_registers[94], 6,2,1);
  m_fields[SER_SEL_OUT_2]        = new Field(&m_registers[94], 4,2,1);
  m_fields[SER_SEL_OUT_1]        = new Field(&m_registers[94], 2,2,1);
  m_fields[SER_SEL_OUT_0]        = new Field(&m_registers[94], 0,2,1);
  m_fields[SER_INV_TAP]          = new Field(&m_registers[95], 6,2,0);
  m_fields[SER_EN_TAP]           = new Field(&m_registers[95], 4,2,0);
  m_fields[SER_EN_LANE]          = new Field(&m_registers[95], 0,4,1);

  m_fields[DAC_CML_BIAS_2]       = new Field(&m_registers[96], 0,10,0);
  m_fields[DAC_CML_BIAS_1]       = new Field(&m_registers[97], 0,10,0);
  m_fields[DAC_CML_BIAS_0]       = new Field(&m_registers[98], 0,10,500);
  m_fields[MonitorPinEn]         = new Field(&m_registers[99],12, 1,0);
  m_fields[MonitorIMuxSel]       = new Field(&m_registers[99], 6, 6,63);
  m_fields[MonitorVMuxSel]       = new Field(&m_registers[99], 0, 6,63);
  m_fields[ErrWngMask]           = new Field(&m_registers[100], 0,8,0);

  m_fields[MON_SENS_SLDO_DIG_EN]   = new Field(&m_registers[101],11,1,0);
  m_fields[MON_SENS_SLDO_DIG_DEM]  = new Field(&m_registers[101], 7,4,0);
  m_fields[MON_SENS_SLDO_DIG_BIAS] = new Field(&m_registers[101], 6,1,0);
  m_fields[MON_SENS_SLDO_ANA_EN]   = new Field(&m_registers[101], 5,1,0);
  m_fields[MON_SENS_SLDO_ANA_DEM]  = new Field(&m_registers[101], 1,4,0);
  m_fields[MON_SENS_SLDO_ANA_BIAS] = new Field(&m_registers[101], 0,1,0);

  m_fields[MON_SENS_ACB_EN]      = new Field(&m_registers[102], 5,1,0);
  m_fields[MON_SENS_ACB_DEM]     = new Field(&m_registers[102], 1,4,0);
  m_fields[MON_SENS_ACB_BIAS]    = new Field(&m_registers[102], 0,1,0);
  m_fields[VREF_RSENSE_BOT]      = new Field(&m_registers[103], 8, 1,0);
  m_fields[VREF_RSENSE_TOP]      = new Field(&m_registers[103], 7, 1,0);
  m_fields[VREF_IN]              = new Field(&m_registers[103], 6, 1,1);
  m_fields[MON_ADC_TRIM]         = new Field(&m_registers[103], 0, 6,0);

  m_fields[DAC_NTC]              = new Field(&m_registers[104], 0,10,100);
  m_fields[HITOR_MASK_3]         = new Field(&m_registers[105], 0, 6,0);
  m_fields[HITOR_MASK_2]         = new Field(&m_registers[106], 0,16,0);
  m_fields[HITOR_MASK_1]         = new Field(&m_registers[107], 0,16,0);
  m_fields[HITOR_MASK_0]         = new Field(&m_registers[108], 0,16,0);
  m_fields[AutoRead0]            = new Field(&m_registers[109], 0, 9,137);
  m_fields[AutoRead1]            = new Field(&m_registers[110], 0, 9,133);
  m_fields[AutoRead2]            = new Field(&m_registers[111], 0, 9,121);
  m_fields[AutoRead3]            = new Field(&m_registers[112], 0, 9,122);
  m_fields[AutoRead4]            = new Field(&m_registers[113], 0, 9,124);
  m_fields[AutoRead5]            = new Field(&m_registers[114], 0, 9,127);
  m_fields[AutoRead6]            = new Field(&m_registers[115], 0, 9,126);
  m_fields[AutoRead7]            = new Field(&m_registers[116], 0, 9,125);
  m_fields[RING_OSC_B_CLR]       = new Field(&m_registers[117],14, 1,0);
  m_fields[RING_OSC_B_LEFT_EN]   = new Field(&m_registers[117],13, 1,0);
  m_fields[RING_OSC_B_RIGHT_EN]  = new Field(&m_registers[117],12, 1,0);
  m_fields[RING_OSC_B_CAPA_EN]   = new Field(&m_registers[117],11, 1,0);
  m_fields[RING_OSC_B_FF_EN]     = new Field(&m_registers[117],10, 1,0);
  m_fields[RING_OSC_B_LVT_EN]    = new Field(&m_registers[117], 9, 1,0);
  m_fields[RING_OSC_A_CLR]       = new Field(&m_registers[117], 8, 1,0);
  m_fields[RING_OSC_A_EN]        = new Field(&m_registers[117], 0, 8,0);
  m_fields[RING_OSC_A_ROUTE]     = new Field(&m_registers[118], 6, 3,0);
  m_fields[RING_OSC_B_ROUTE]     = new Field(&m_registers[118], 0, 6,0);
  m_fields[RING_OSC_A_OUT]       = new Field(&m_registers[119], 0,16,0);
  m_fields[RING_OSC_B_OUT]       = new Field(&m_registers[120], 0,16,0);
  m_fields[BCIDCnt]              = new Field(&m_registers[121], 0,16,0);
  m_fields[TrigCnt]              = new Field(&m_registers[122], 0,16,0);
  m_fields[ReadTrigCnt]          = new Field(&m_registers[123], 0,16,0);
  m_fields[LockLossCnt]          = new Field(&m_registers[124], 0,16,0);
  m_fields[BitFlipWngCnt]        = new Field(&m_registers[125], 0,16,0);
  m_fields[BitFlipErrCnt]        = new Field(&m_registers[126], 0,16,0);
  m_fields[CmdErrCnt]            = new Field(&m_registers[127], 0,16,0);
  m_fields[RdWrFifoErrCnt]       = new Field(&m_registers[128], 0,16,0);
  m_fields[AI_REGION_ROW]        = new Field(&m_registers[129], 0, 9,0);
  m_fields[HITOR_3_CNT]          = new Field(&m_registers[130], 0,16,0);
  m_fields[HITOR_2_CNT]          = new Field(&m_registers[131], 0,16,0);
  m_fields[HITOR_1_CNT]          = new Field(&m_registers[132], 0,16,0);
  m_fields[HITOR_0_CNT]          = new Field(&m_registers[133], 0,16,0);
  m_fields[SkippedTriggerCnt]    = new Field(&m_registers[134], 0,16,0);
  m_fields[EfusesReadData1]      = new Field(&m_registers[135], 0,16,0);
  m_fields[EfusesReadData0]      = new Field(&m_registers[136], 0,16,0);
  m_fields[MonitoringDataADC]    = new Field(&m_registers[137], 0,12,0);
}


Configuration::Configuration( const Configuration& obj) {
  for (unsigned i=0;i<m_registers.size();i++){
    this->SetRegisterValue(i,obj.GetRegisterValue(i));
  }
}


Configuration::~Configuration(){
  for(auto it : m_fields){
    delete it.second;
  }
  m_fields.clear();
}

void Configuration::SetVerbose(bool enable){
  m_verbose = enable;
}

uint32_t Configuration::Size(){
  return m_registers.size();
}

Register * Configuration::GetRegister(uint32_t index){
  if(index>m_registers.size()-1){return 0;}
  return &m_registers[index];
}

uint16_t Configuration::GetRegisterValue(uint32_t index) const{
  if(index>m_registers.size()-1){return 0;}
  return m_registers[index].GetValue();
}

uint16_t Configuration::GetRegisterValue(uint32_t index, bool update){
  if(index>m_registers.size()-1){return 0;}
  if(update) m_registers[index].Update(false);
  return m_registers[index].GetValue();
}

void Configuration::SetRegisterValue(uint32_t index, uint16_t value){
  if(index>m_registers.size()-1){return;}
  m_registers[index].SetValue(value);
}

void Configuration::SetField(string name, uint16_t value){

  if(m_verbose) std::cout << __PRETTY_FUNCTION__ << "Setting field: " << name << ", " << value << std::endl;
  auto it=m_name2index.find(name);
  if(it==m_name2index.end()){
    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << " WARNING!!! - Reached end of name2index dictionary" << std::endl;
    return;
  }
  else{
    //    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << " Found in dictionary: " << it -> first << ", " << it -> second << std::endl;
  }
  m_fields[it->second]->SetValue(value);
}

void Configuration::SetField(uint32_t index, uint16_t value){
  m_fields[index]->SetValue(value);
}

Field * Configuration::GetField(uint32_t index){
  return m_fields[index];
}

Field * Configuration::GetField(string name){
  auto it=m_name2index.find(name);
  if(it==m_name2index.end()){return 0;}
  return m_fields[it->second];
}

vector<uint32_t> Configuration::GetUpdatedRegisters(){
  vector<uint32_t> ret;
  for(uint32_t i=1;i<m_registers.size();i++){
    if(m_registers[i].IsUpdated()){ret.push_back(i);m_registers[i].Update(false);}
  }
  if(m_registers[0].IsUpdated()){ret.push_back(0);m_registers[0].Update(false);}
  return ret;
}

map<string,uint32_t> Configuration::GetRegisters(){
  map<string,uint32_t> ret;
  for(auto it: m_name2index){
    string name=it.first;
    ret[name]=m_fields[m_name2index[name]]->GetValue();
  }
  return ret;
}

void Configuration::SetRegisters(map<string,uint32_t> config){
  for(auto it : config){
    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << "Setting field: " << it.first << ", " << it.second << std::endl;
    SetField(it.first,it.second);
  }
}

void Configuration::Backup(){
  
  for(uint32_t i=0; i<m_registers.size(); i++){
    m_backup[i]=m_registers[i].GetValue();
  }
  
}

void Configuration::Restore(){

  for(uint32_t i=0; i<m_registers.size(); i++){
    if(m_backup[i]==m_registers[i].GetValue()){continue;}
    m_registers[i].SetValue(m_backup[i]);
  }
  
}
