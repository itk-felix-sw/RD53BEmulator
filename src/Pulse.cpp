#include "RD53BEmulator/Pulse.h"
#include <sstream>
#include <iomanip>

using namespace std;
using namespace RD53B;

Pulse::Pulse(){
  m_symbol = 0x5C;
  m_chipid = 0;
}

Pulse::Pulse(uint32_t chipid, uint32_t length){ 
  m_symbol = 0x5C;
  m_chipid = chipid;
}



Pulse::~Pulse(){}

Command * Pulse::Clone(){
  Pulse * clone = new Pulse();
  clone->m_chipid = m_chipid;
  return clone;
}

string Pulse::ToString(){
  ostringstream os;
  os << "Pulse 0x" << hex << m_symbol << dec
     << " ChipId:" << m_chipid
     << " (0x" << hex << m_chipid << dec << ")";
  return os.str();
}

bool Pulse::FromString(string str){
  istringstream is(str);
  uint32_t hsym;
  string tmp;
  is >> tmp >> hex >> hsym >> dec;
  if(hsym!=m_symbol){return false;}
  is >> tmp >> m_chipid >> tmp;
  return true;
}

uint32_t Pulse::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<2) return 0;
  m_symbol = bytes[0];
  if(m_symbol!=0x5A) return 0;
  m_chipid = m_symbol2data[bytes[1]];
  return 2;
}

uint32_t Pulse::Pack(uint8_t * bytes){
  bytes[0]=m_symbol;
  bytes[1]=m_data2symbol[m_chipid];
  return 2;
}

uint32_t Pulse::GetType(){
  return Command::PULSE;
}

void Pulse::SetChipId(uint32_t chipid){
  m_chipid=chipid&0x1F;
}

uint32_t Pulse::GetChipId(){
  return m_chipid;
}

