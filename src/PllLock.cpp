#include "RD53BEmulator/PllLock.h"
#include <sstream>
#include <iomanip>

using namespace std;
using namespace RD53B;

PllLock::PllLock(){
  m_symbol = 0xAAAA;
}

PllLock::~PllLock(){}

Command * PllLock::Clone(){
  PllLock * clone = new PllLock();
  return clone;
}

string PllLock::ToString(){
  ostringstream os;
  os << "PLL_LOCK 0x" << hex << m_symbol << dec;
  return os.str();
}

bool PllLock::FromString(string str){
  istringstream is(str);
  uint32_t hsym;
  string tmp;
  is >> tmp >> hex >> hsym >> dec;
  if(hsym!=m_symbol){return false;}
  return true;
}

uint32_t PllLock::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<2) return 0;
  m_symbol=0;
  m_symbol|=bytes[0]<<0;
  m_symbol|=bytes[1]<<8;
  if(m_symbol!=0xAAAA) return 0;
  return 2;
}

uint32_t PllLock::Pack(uint8_t * bytes){
  bytes[0]=((m_symbol>>0)&0xFF);
  bytes[1]=((m_symbol>>8)&0xFF);
  return 2;
}

uint32_t PllLock::GetType(){
  return Command::PLOCK;
}

