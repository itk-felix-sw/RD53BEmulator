#include "RD53BEmulator/Sync.h"
#include <sstream>
#include <iomanip>

using namespace std;
using namespace RD53B;

Sync::Sync(){
  m_symbol = 0x817E;
}

Sync::~Sync(){}

Command * Sync::Clone(){
  Sync * clone = new Sync();
  return clone;
}

string Sync::ToString(){
  ostringstream os;
  os << "Sync 0x" << hex << m_symbol << dec;
  return os.str();
}

bool Sync::FromString(string str){
  istringstream is(str);
  string tmp;
  uint32_t hsym;
  is >> tmp >> hex >> hsym >> dec;
  if(hsym!=m_symbol){return false;}
  return true;
}

uint32_t Sync::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<2) return 0;
  if(bytes[0]!=((m_symbol>>8)&0xFF) || bytes[1]!=((m_symbol>>0)&0xFF)) return 0;
  return 2; 
}

uint32_t Sync::Pack(uint8_t * bytes){
  bytes[1]=((m_symbol>>0)&0xFF);
  bytes[0]=((m_symbol>>8)&0xFF);
  return 2;
}

uint32_t Sync::GetType(){
  return Command::SYNC;
}

