#include "RD53BEmulator/Emulator.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace RD53B;

Emulator::Emulator(uint32_t chipid){
  m_chipid=chipid;
  m_decoder = new Decoder();
  m_encoder = new Encoder();
  m_config = new Configuration();
  m_verbose = 0;
  m_register_index=0;
  m_l0id = 0;
  m_l1id = 0;
  m_nhits = 10;
}

Emulator::~Emulator(){
  delete m_decoder;
  delete m_encoder;
  delete m_config;
}

void Emulator::HandleCommand(uint8_t *recv_data, uint32_t recv_size){
  if(recv_size==0){return;}
  if(m_verbose) cout << "Emulator::HandleCommand size: " << recv_size << endl;
  m_encoder->Clear();
  m_encoder->SetBytes(recv_data, recv_size);
  if(m_verbose) cout << "Byte stream: " << m_encoder->GetByteString() << endl;
  m_encoder->Decode();
  
  //handle read register and write register commands immediately, else queue for later
  for(uint32_t i=0;i<m_encoder->GetCommands().size();i++){

    if(m_verbose) cout << setw(2) << i << " " << m_encoder->GetCommands()[i]->ToString() << endl;

    if(m_encoder->GetCommands()[i]->GetType()==Command::RDREG){
      RdReg * rdreg=dynamic_cast<RdReg*>(m_encoder->GetCommands()[i]);
      m_read_mutex.lock();
      m_read_reqs.push(rdreg->GetAddress());
      m_read_mutex.unlock();
    }
    else if(m_encoder->GetCommands()[i]->GetType()==Command::WRREG){
      WrReg * wrreg=dynamic_cast<WrReg*>(m_encoder->GetCommands()[i]);
      m_reg_mutex.lock();
      m_config->SetRegister(wrreg->GetAddress(),wrreg->GetValue());
      m_reg_mutex.unlock();
    }
    else if(m_encoder->GetCommands()[i]->GetType()==Command::TRIGGER or
            m_encoder->GetCommands()[i]->GetType()==Command::CAL or
            m_encoder->GetCommands()[i]->GetType()==Command::PULSE or
            m_encoder->GetCommands()[i]->GetType()==Command::SYNC){
      m_cmds.push(m_encoder->GetCommands()[i]->Clone());
    }
  }
}

void Emulator::ProcessQueue(){
  
  m_decoder->Clear();   

  if(m_config->GetField(Configuration::ServiceDataEnable)->GetValue()==1 and
      m_l1id % m_config->GetField(Configuration::ServiceDataPeriod)->GetValue()==0){
    //Send a service data frame
    Register *reg = new Register();
    m_read_mutex.lock();
    uint32_t sz=m_read_reqs.size();
    m_read_mutex.unlock();
    for(uint32_t i=0;i<2;i++){
      uint32_t addr;
      if(sz>i){
        m_read_mutex.lock();
        addr = m_read_reqs.front();
        m_read_reqs.pop();
        m_read_mutex.unlock();
        reg->SetAuto(i,0);
      }else{
        addr = GetNextRegister();
        reg->SetAuto(i,1);
      }
      reg->SetRegister(i,addr,m_config->GetRegister(addr));
    }
    m_decoder->AddFrame(reg);
  }

  //If self trigger
  if(m_config->GetField(Configuration::SelfTrigEn)->GetValue()==1){
    Data * data = new Data();
    Stream * ss = new Stream();
    data->AddStream(ss);
    for(uint32_t i=0;i<m_nhits;i++){
      //@FIXME Select the hits better
      uint32_t col = (rand() % 50)+1;
      uint32_t row = (rand() % 48)+1;
      uint32_t map = (rand() % 0xFFFF);
      uint32_t tot = (rand() % 0xFFFF)+1;
      ss->AddHit(0,col,row,map,tot);
    }
    m_decoder->AddFrame(data);
    m_l0id++;
  }else{
    //Process cmd queue
    while(m_cmds.empty()){
      Command * cmd = m_cmds.front();
      if(cmd->GetType()==Command::CAL){
        Cal * cal=dynamic_cast<Cal*>(cmd);
	cal=cal;
      }else if(cmd->GetType()==Command::PULSE){
        Pulse * pulse=dynamic_cast<Pulse*>(cmd);
	pulse=pulse;
      }else if(cmd->GetType()==Command::SYNC){
        Sync * sync=dynamic_cast<Sync*>(cmd);
	sync=sync;
      }else if(cmd->GetType()==Command::TRIGGER){
        Trigger * trg=dynamic_cast<Trigger*>(cmd);
	trg=trg;
      }
      m_cmds.pop();
      delete cmd;
    }
  }
    
  m_decoder->Encode();
  
  if(m_verbose){
    cout << "Emulator::processQueue" << endl;
    for(uint32_t i=0;i<m_decoder->GetFrames().size();i++)
      cout << setw(2) << i << " " << m_decoder->GetFrames()[i]->ToString() << endl;
  } 

}

void Emulator::SetVerbose(bool enable){
   m_verbose = enable;
}

uint8_t * Emulator::GetBytes(){
  return m_decoder->GetBytes();
}

uint32_t Emulator::GetLength(){
  return m_decoder->GetLength();
}

uint32_t Emulator::GetNextRegister(){
  uint32_t addr=m_config->GetField(Configuration::AutoRead0+m_register_index)->GetValue();
  m_register_index++;
  if(m_register_index>=8){m_register_index=0;}
  return addr;
}

bool Emulator::IsMsgForMe(uint32_t chipid){
  if(chipid==m_chipid or (chipid&0x8)==0x8) return true;
  return false;
}

