#include "RD53BEmulator/ExternalTriggerScan.h"
#include "RD53BEmulator/Tools.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TSysEvtHandler.h"

#include <iostream>
#include <chrono>
#include <signal.h>

using namespace std;
using namespace RD53B;

bool ExternalTriggerScan::m_scanStop = false;

TApplication* theAppETS;

struct chip_config_t {
  uint32_t chip_x;
  uint32_t chip_y; 
  bool flip_x;
  bool flip_y;
} typedef chip_config_t;

constexpr chip_config_t quad_map[4] = {
  {1, 1, true, false},
  {0, 1, true, false},
  {0, 0, false, true},
  {1, 0, false, true},
};

template <uint32_t chip_x, uint32_t chip_y, bool flip_x, bool flip_y>
inline std::pair<uint32_t, uint32_t> quad_transform(const std::pair<uint32_t, uint32_t>& hit) {
  return std::pair<uint32_t, uint32_t>(
				       (flip_x ? 399 - hit.first : hit.first) + (400 * chip_x),
				       (flip_y ? 383 - hit.second : hit.second) + (384 * chip_y)
				       );
}

ExternalTriggerScan::ExternalTriggerScan(){
  m_TriggerLatency = 50;
  m_TriggerFrequency = 40e3;
  m_colMin = 0;
  m_colMax = 400;
}

ExternalTriggerScan::~ExternalTriggerScan(){}

void ExternalTriggerScan::StopScan(int){
  cout << endl << "You pressed ctrl+c -> quitting" << endl;
  m_scanStop = true;
}

void ExternalTriggerScan::PreRun(){

  m_totalHits = new TH2I("occ_global",";Column;Row;Number of hits", Matrix::NUM_COLS*2, 0, Matrix::NUM_COLS*2, Matrix::NUM_ROWS*2, 0, Matrix::NUM_ROWS*2);

  for(auto fe : GetFEs()){

    cout << "ExternalTriggerScan: Configure" << endl;
    cout << "TRIGGER LATENCY IS : " << m_latency << endl;
    fe->GetConfig()->SetField(Configuration::CalMode,0);
    fe->GetConfig()->SetField(Configuration::Latency,m_TriggerLatency);
    

    fe->WriteGlobal();
    Send(fe);
    fe->EnableAll();
    ConfigurePixels(fe);
  
 
    
    //Create histograms
    m_occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_occHigh[fe->GetName()]=new TH2I(("occH_"+fe->GetName()).c_str(),";Column;Row;Number of Hi hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);

    m_enable[fe->GetName()]=new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_mask[fe->GetName()]=new TH2I(("mask_"+fe->GetName()).c_str(),"Mask map;Column;Row;Masked", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tot[fe->GetName()] =new TH1I(("tot_"+fe->GetName()).c_str(),"ToT"  , 16, -0.5, 15.5);
    m_bcid[fe->GetName()]=new TH1I(("bcid_"+fe->GetName()).c_str(),"BCID", 32, -0.5, 31.5);

    m_triggers = new TH1I("triggers", "Number of triggers", 1, 0.5, 1.5);
    m_deltaBCID[fe->GetName()]=new TH2I(("deltaBCID_"+fe->GetName()).c_str(),"DeltaBCID", 32, -0.5, 31.5, 16, -0.5, 15.5);
  }

  // GetMasker()->SetVerbose(m_verbose);
  // GetMasker()->SetRange(0,0,400,384);
  // GetMasker()->SetShape(400,384);
  // GetMasker()->Build();
  // GetMasker()->GetStep(0);

  bool enable=true;
  //ConfigMask();
  for(auto fe : GetFEs()){
    for(auto cc : GetMasker()->GetCoreColumns()){
      fe->EnableCoreColumn(cc,enable);
      fe->EnableCalCoreColumn(cc,enable);
    }
    for(auto dc : GetMasker()->GetDoubleColumns()){
      fe->EnableCalDoubleColumn(dc,enable);
    }
    fe->ProcessCommands();
    Send(fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(5));

  }

}

void ExternalTriggerScan::Run(){

  gROOT->SetBatch(false);

  theAppETS=new TApplication("myApp",0,0);
  TCanvas* myC=new TCanvas("can","can", 1000, 1000);
  //  myC->Divide(2,2);

  // RD53B is always a bit noisy at initialisation so clearing hits
  for(auto fe : GetFEs()){ 
    if(!fe->HasHits()) continue;
    fe->ClearHits();
  }
  
  auto scan_start = chrono::steady_clock::now();
  
  m_scanStop = false;
  signal(SIGINT, StopScan);
  signal(SIGTERM, StopScan);
  signal(SIGILL, StopScan);
  cout << "Please press Ctrl+C to stop the scan" << endl;

  uint32_t nhits_total = 0;
  uint32_t ev=0;
  while(!m_scanStop){

    // uint32_t nhits = 0;
    // do {
    //   nhits = 0;
    //   for(auto fe : GetFEs()){
    //     if(!fe->HasHits()) continue;
    //     Hit* hit = fe->GetHits(200);

    uint32_t nhits = 0;

    for(auto fe : GetFEs()){
      vector<Hit*> hits=fe->GetHits(2000);
      uint32_t nH=hits.size();
      //cout << " on FE: " << fe->GetName() << " with hit size: " << nH << endl;
      if (nH==0) {
	this_thread::sleep_for(chrono::milliseconds( 20 ));
      } else {
	for (unsigned int h=0; h<nH; h++) {
	  Hit* hit=hits.at(h);


	  //cout << "--> " << hit->ToString() << endl;
	  if(hit!=0 && hit->GetTOT()!=0x0){
	    //	  cout << "We are having a hit with TOT= " << hit->GetTOT() << endl;
	    //	  cout << hit->ToString() << endl;
	    //cout << "Delta = " << hit->GetBCID() << ", TOT = " << hit->GetTOT() << endl;

	    if( (hit->GetCol() > 400) || (hit->GetRow() > 385) ) continue;
	  
	    m_tot[fe->GetName()]->Fill(hit->GetTOT());
	    m_bcid[fe->GetName()]->Fill(hit->GetBCID());

	    if (hit->GetTOT()>10) m_occHigh[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	    m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());

	    //fill into the global histogram
	    int fe_idx = fe->GetChipID()-12;
	    std::pair<uint32_t, uint32_t> in_hits = std::make_pair<uint32_t, uint32_t>(hit->GetCol(), hit->GetRow());
	    std::pair<uint32_t, uint32_t> out_hits;
	  
	    switch (fe_idx) {
	    case 0:
	      out_hits = quad_transform<quad_map[0].chip_x, quad_map[0].chip_y, quad_map[0].flip_x, quad_map[0].flip_y>(in_hits);
	      break;
	    case 1:
	      out_hits = quad_transform<quad_map[1].chip_x, quad_map[1].chip_y, quad_map[1].flip_x, quad_map[1].flip_y>(in_hits);
	      break;
	    case 2:
	      out_hits = quad_transform<quad_map[2].chip_x, quad_map[2].chip_y, quad_map[2].flip_x, quad_map[2].flip_y>(in_hits);
	      break;
	    case 3:
	      out_hits = quad_transform<quad_map[3].chip_x, quad_map[3].chip_y, quad_map[3].flip_x, quad_map[3].flip_y>(in_hits);
	      break;
	    default:
	      std::cerr << "Invalid chip ID!" << std::endl;
	      exit(1);
	    }
	  
	    //std::cout << "FE " << fe_idx << "In  hit: (" <<  in_hits.first << ", " <<  in_hits.second << ")  " << "Out hit: (" << out_hits.first << ", " << out_hits.second << ")" << std::endl;
	    m_totalHits->Fill(out_hits.first, out_hits.second);

	    nhits++;
	    nhits_total++;
	    //if (hit->GetCol()>140) cout << "--> " << hit->ToString() << endl;
	  }
	  //        fe->NextHit();
	}
      }

      fe->ClearHits(nH);
      // auto end_read = chrono::steady_clock::now();
      // uint32_t ms = chrono::duration_cast<chrono::milliseconds>(end_read - start_read).count();
      // if(ms>30) break;

      //    }while (nhits > 0);
    }

    ev++;
    
    if( ev % 100000 == 0 ){ //100000
      //auto current_time = chrono::steady_clock::now();
      //      cout << "Run time:: "<< chrono::duration_cast<chrono::minutes>(current_time - scan_start).count() << "min ; NHits in trigger # " << ev  << ": " << nhits_total << "      "; // << endl;
      nhits_total = 0;
      myC->cd();
      m_totalHits->SetMaximum(50);
      m_totalHits->Draw("COLZ");

      // int c=0;
      // for(auto fe : GetFEs()){
      // 	c+=1;
      // 	myC->cd(c);	
      // 	m_occ[fe->GetName()]->SetMaximum(50);
      // 	m_occ[fe->GetName()]->Draw("COLZ");
      // 	// myC->cd(2);	
      // 	// m_occHigh[fe->GetName()]->SetMaximum(50);
      // 	// m_occHigh[fe->GetName()]->Draw("COLZ");
      // 	// myC->cd(3);	
      // 	// m_tot[fe->GetName()]->Draw("HIST");
      // 	// myC->cd(4);	
      // 	// m_bcid[fe->GetName()]->Draw("HIST");
      // 	//cout << " FE_" << c << ": " << m_occ[fe->GetName()]->Integral();
      // }
      //cout << endl;
      myC->Update();
      this_thread::sleep_for(chrono::microseconds( 100000 ));
      //exit(-1);
    }
    //this_thread::sleep_for(chrono::microseconds( (int)period ));
    ev++;
  }
  
  //UnconfigMask();
  
  cout << "Total scan time: " << chrono::duration_cast<chrono::seconds>(chrono::steady_clock::now() - scan_start).count() << " s" << endl;
}

void ExternalTriggerScan::Analysis(){
  gROOT->SetBatch(true);
  TCanvas* can=new TCanvas("plot","plot",800,600);

  for(auto fe : GetFEs()){

    //get total # of hits
    float thres = m_triggers->GetBinContent(1)*8e-6; //currently threshold = 1e-6 per BC
    //create a mask for noisy pixels, mask everything > ntot/npix
    if (m_maskopt == 0){
       for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
         for(uint32_t col = m_colMin; col < m_colMax; col++){
           if (m_occ[fe->GetName()]->GetBinContent(col+1,row+1) < thres){
	     fe->SetPixelEnable(col,row,true);
	     m_enable[fe->GetName()]->SetBinContent(col+1,row+1,1);
	   }else{
	     fe->SetPixelEnable(col,row,false);
             m_mask[fe->GetName()]->SetBinContent(col+1,row+1,1);
	     if (m_triggers->GetBinContent(1)>1e6) cout << " FE: " << fe->GetName() << " --> Masking pixel: " << col << " , " << row << endl;
	   }
         }
       }
       //m_enable=m_mask;
    }

    m_mask[fe->GetName()]->Write();
    m_enable[fe->GetName()]->Write();
    m_occ[fe->GetName()]->Write();
    m_tot[fe->GetName()]->Write();
    m_triggers->Write();
    m_totalHits->Write();

    m_occ[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC.pdf").c_str() );

    m_enable[fe->GetName()]->SetMaximum(2);
    m_enable[fe->GetName()]->SetMinimum(0);
    m_enable[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__ENABLE.pdf").c_str() );

    m_mask[fe->GetName()]->SetMaximum(2);
    m_mask[fe->GetName()]->SetMinimum(0);
    m_mask[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__MASKED.pdf").c_str() );

    m_tot[fe->GetName()]->Draw("hist");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__TOT.pdf").c_str() );

    delete m_occ[fe->GetName()];
    delete m_enable[fe->GetName()];
    delete m_mask[fe->GetName()];
    delete m_tot[fe->GetName()];
    delete m_deltaBCID[fe->GetName()];
  }

  delete can;

}
