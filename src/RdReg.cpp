#include "RD53BEmulator/RdReg.h"
#include <sstream>
#include <iomanip>

using namespace std;
using namespace RD53B;

RdReg::RdReg(){
  m_symbol = 0x65;
  m_chipid = 0;
  m_address = 0;
}


RdReg::RdReg(RdReg * copy){
  m_symbol = copy->m_symbol;
  m_chipid = copy->m_chipid;
  m_address = copy->m_address;
}

RdReg::RdReg(uint32_t chipid, uint32_t address){
  m_symbol = 0x65;
  m_chipid=chipid&0x1F;
  m_address=address&0x1FF;
}


RdReg::~RdReg(){}

Command * RdReg::Clone(){
  RdReg * clone = new RdReg();
  clone->m_chipid = m_chipid;
  clone->m_address = m_address;
  return clone;
}

string RdReg::ToString(){
  ostringstream os;
  os << "RdReg 0x" << hex << m_symbol << dec
     << " ChipId: " << m_chipid
     << " (0x" << hex << m_chipid << dec << ")"
     << " Address: " << m_address
     << " (0x" << hex << m_address << dec << ")";
  return os.str();
}

bool RdReg::FromString(string str){
  istringstream is(str);
  uint32_t hsym;
  string tmp;
  is >> tmp >> hex >> hsym >> dec;
  if(hsym!=m_symbol){return false;}
  is >> tmp >> m_chipid >> tmp 
     >> tmp >> m_address >> tmp;
  return true;
}

uint32_t RdReg::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<4) return 0;
  if(bytes[0]!=m_symbol) return 0;
  m_chipid   = m_symbol2data[bytes[1]];
  m_address  = m_symbol2data[bytes[2]]&0xF<<5;
  m_address |= m_symbol2data[bytes[3]];
  return 4;
}

uint32_t RdReg::Pack(uint8_t * bytes){
  bytes[0]=((m_symbol>>0)&0xFF);
  bytes[1]=m_data2symbol[(m_chipid  &0x1F)];
  bytes[2]=m_data2symbol[((m_address>>5)&0x0F)];
  bytes[3]=m_data2symbol[((m_address&0x1F))];
  return 4;
}

uint32_t RdReg::GetType(){
  return Command::RDREG;
}

void RdReg::SetChipId(uint32_t chipid){
  m_chipid=chipid&0x1F;
}

uint32_t RdReg::GetChipId(){
  return m_chipid;
}

void RdReg::SetAddress(uint32_t address){
  m_address=address&0x1FF;
}

uint32_t RdReg::GetAddress(){
  return m_address;
}



