#include "RD53BEmulator/WrReg.h"
#include <sstream>
#include <iomanip>
#include <iostream>

using namespace std;
using namespace RD53B;

WrReg::WrReg(){
  m_symbol = 0x66;
  m_chipid = 0;
  m_address = 0;
  m_mode = 0;
  m_values.resize(1,0);
}



WrReg::WrReg(WrReg * copy){
  m_symbol = copy->m_symbol;
  m_chipid = copy->m_chipid;
  m_address = copy->m_address;
  m_mode = copy->m_mode;
  m_values = copy->m_values;
}

WrReg::WrReg(uint32_t chipid, uint32_t address, uint32_t value){
  m_symbol = 0x66;
  m_chipid = chipid;
  m_address = address;
  m_mode = 0;
  m_values.resize(1,0);
  m_values[0]=value;
}





WrReg::~WrReg(){}

Command * WrReg::Clone(){
  WrReg * clone = new WrReg();
  clone->m_chipid = m_chipid;
  clone->m_address = m_address;
  clone->m_mode = m_mode;
  clone->m_values = m_values;
  return clone;
}

string WrReg::ToString(){
  ostringstream os;
  os << "WrReg 0x" << hex << m_symbol << dec
     << " ChipId: " << m_chipid
     << " (0x" << hex << m_chipid << dec << ")"
     << " Mode: " << m_mode
     << " (0x" << hex << m_mode << dec << ")"
     << " Address: " << m_address
     << " (0x" << hex << m_address << dec << ")"
     << " Value: ";
  for(uint32_t i=0;i<(m_mode==0?1:m_values.size());i++){
    os << m_values[i]
       << " (0x" << hex << m_values[i] << dec << "),";
  }
  return os.str();
}

bool WrReg::FromString(string str){
  istringstream is(str);
  uint32_t hsym;
  string hdr, hcid, vcid, hmod, vmod, hadd, vadd, hval, vval;
  is >> hdr >> hex >> hsym >> dec;
  if(hsym!=m_symbol){return false;}
  is >> hcid >> m_chipid >> vcid 
     >> hmod >> m_mode >> vmod
     >> hadd >> m_address >> vadd
     >> hval;
  uint32_t val;
  m_values.clear();
  while(is>>val>>vval){
    m_values.push_back(val);
  }
  return true;
}

uint32_t WrReg::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<8) return 0; 
  if(bytes[0]!=m_symbol) return 0;
  m_chipid   = m_symbol2data[bytes[1]];
  m_mode     = m_symbol2data[bytes[2]]>>4;
  m_address  = (m_symbol2data[bytes[2]]&0xF)<<5;
  m_address |= m_symbol2data[bytes[3]];
  m_values.resize(0);
  uint32_t dt=0;
  uint32_t pos=4;
  if(m_mode==0){
  	dt = m_symbol2data[bytes[4]]<<11;
  	dt|= m_symbol2data[bytes[5]]<<6;
  	dt|= m_symbol2data[bytes[6]]<<1;
  	dt|= m_symbol2data[bytes[7]]>>4;
  	m_values.push_back(dt);
    pos+=4;
  }else{
	//decode until we find a symbol2data that is not found?
    auto it=m_symbol2data.begin();
    while(pos<maxlen){
      it = m_symbol2data.find(bytes[pos]);
      if(it==m_symbol2data.end()){break;}
      dt  = (it->second<<5);
      dt |= m_symbol2data[bytes[pos+1]];
      m_values.push_back(dt);
      pos+=2;
    }
  }
  return pos;
}

uint32_t WrReg::Pack(uint8_t * bytes){

  bytes[0]=((m_symbol>>0)&0xFF);
  bytes[1]=m_data2symbol[(m_chipid  &0x1F)];
  bytes[2]=m_data2symbol[((m_mode   &0x01)<<4) | ((m_address>>5)&0x0F)];
  bytes[3]=m_data2symbol[((m_address&0x1F))];
  uint32_t pos=4;
  if(m_mode==0){
  	bytes[4]=m_data2symbol[(m_values[0]>>11)&0x1F];
		bytes[5]=m_data2symbol[(m_values[0]>> 6)&0x1F];
		bytes[6]=m_data2symbol[(m_values[0]>> 1)&0x1F];
		bytes[7]=m_data2symbol[(m_values[0]<< 4)&0x10];
		pos+=4;
  }else{
    for(uint32_t i=0;i<m_values.size();i+=2){
      bytes[i+4]=m_data2symbol[(m_values[i]<<5)|m_values[i+1]<<0];
      pos+=1;
    }
  }
  return pos;
}

uint32_t WrReg::GetType(){
  return Command::WRREG;
}

void WrReg::SetChipId(uint32_t chipid){
  m_chipid=chipid&0xF;
}

uint32_t WrReg::GetChipId(){
  return m_chipid;
}

void WrReg::SetAddress(uint32_t address){
  m_address=address&0x1FF;
}

uint32_t WrReg::GetAddress(){
  return m_address;
}

void WrReg::SetValue(uint32_t value){
  m_values[0]=value&0xFFFF;
}

void WrReg::SetValue(uint32_t index, uint32_t value){
  if(m_values.size()<index+1) m_values.resize(index+1);
  m_values[index]=value&0xFFFF;
  m_mode=0;
}

uint32_t WrReg::GetValue(uint32_t index){
  return m_values[index];
}

void WrReg::SetMode(uint32_t mode){
  m_mode=mode&0x1;
}

uint32_t WrReg::GetMode(){
  return m_mode;
}




