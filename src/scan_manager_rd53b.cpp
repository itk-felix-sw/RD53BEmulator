#include <RD53BEmulator/Handler.h>
#include <RD53BEmulator/DigitalScan.h>
#include <RD53BEmulator/AnalogScan.h>
#include <RD53BEmulator/ThresholdScan.h>
#include <RD53BEmulator/ToTScan.h>
#include <RD53BEmulator/ReadRegisterScan.h>
#include <RD53BEmulator/ConfigScan.h>
#include <RD53BEmulator/CommScan.h>
#include <RD53BEmulator/TrimScan.h>
#include <RD53BEmulator/NoiseScan.h>
#include <RD53BEmulator/ExternalTriggerScan.h>
#include <RD53BEmulator/SelfTriggerScan.h>
#include <RD53BEmulator/CrossTalkScan.h>
#include <RD53BEmulator/DisconnectedBumpScan.h>
#include <RD53BEmulator/GlobalThresholdTune.h>
#include <RD53BEmulator/PixelThresholdTune.h>

#include <cmdl/cmdargs.h>
#include <signal.h>
#include <sstream>
#include <chrono>
#include <map>
#include <fstream>

using namespace std;
using namespace RD53B;

int main(int argc, char *argv[]){

  cout << "#####################################" << endl
       << "# Welcome to scan_manager_rd53b     #" << endl
       << "#####################################" << endl;

  CmdArgStr  cScan(     's',"scan","scan","scan name",CmdArg::isREQ);
  CmdArgStrList cConfs( 'c',"config","chips","chip names");
  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgStr  cBackend(  'b',"backend","type","posix,rdma. Default posix");
  CmdArgStr  cInterface('i',"interface","network-card","Default enp24s0f0");
  CmdArgStr  cNetFile(  'n',"netfile","file","Network file. Default network.json");
  CmdArgStr  cBusPath('B',"buspath","folder","Network folder. Default /home/itkpix/bus/");
  CmdArgInt  cCharge(   'q',"charge","electrons","Injection charge");
  CmdArgInt  cMaskOpt(  'm',"maskfile","Type","If 0 disables masking (don't update), 1  reset masking, -1 (default) enable masking and update");
  CmdArgBool cRetune(   'r',"retune","enable retune mode");
  CmdArgStr  cOutPath(  'o',"outpath","path","output path. Default $ITK_DATA_PATH");
  CmdArgInt  cToT(      't',"ToT","bc","target ToT");
  CmdArgInt  cThreshold('a',"threshold","electrons","threshold target");
  CmdArgInt  cWait(     'w',"wait","seconds","time to wait between config and run. Default: 3");
  CmdArgBool cStoreHits('H',"hits","turn on the store hits flag");
  CmdArgInt  cLatency(  'L',"latency","integer","latency");
  CmdArgStr  cFrontEnd( 'f',"frontend","type","mid, left, right, all. Some scans accept frontend range");  
  CmdLine cmdl(*argv,&cScan,&cConfs,&cVerbose,&cBackend,&cInterface,&cNetFile,&cBusPath,&cCharge,&cMaskOpt,&cRetune,&cOutPath,&cToT,&cThreshold,&cWait,&cStoreHits,&cLatency,&cFrontEnd,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  std::string commandLineStr="";
  for (int i=1;i<argc;i++) commandLineStr.append(std::string(argv[i]).append(" "));

  string scan(cScan);
  string backend(cBackend.flags()&CmdArg::GIVEN?cBackend:"posix");
  string interface=(cInterface.flags()&CmdArg::GIVEN?cInterface:"lo");
  string netfile=(cNetFile.flags()&CmdArg::GIVEN?cNetFile:"connectivity-emu-rd53a.json");
  string buspath=(cBusPath.flags()&CmdArg::GIVEN?cBusPath:"/home/itkpix/bus/");

  Handler * handler = 0;
  if(scan.find("DigitalScan")!=string::npos)           { handler=new DigitalScan();     }
  else if(scan.find("AnalogScan")!=string::npos)       { handler=new AnalogScan();      }
  else if(scan.find("ReadRegisterScan")!=string::npos) { handler=new ReadRegisterScan();}
  else if(scan.find("ThresholdScan")!=string::npos)    { handler=new ThresholdScan();   }
  else if(scan.find("ToTScan")!=string::npos)          { handler=new ToTScan();         }
  else if(scan.find("CommScan")!=string::npos)         { handler=new CommScan();        }
  else if(scan.find("TrimScan")!=string::npos)         { handler=new TrimScan();        }
  else if(scan.find("NoiseScan")!=string::npos)        { handler=new NoiseScan();        }
  else if(scan.find("ExternalTriggerScan")!=string::npos)        { handler=new ExternalTriggerScan();        }
  else if(scan.find("CrossTalkScan")!=string::npos)        { handler=new CrossTalkScan();        }
  else if(scan.find("DisconnectedBumpScan")!=string::npos)        { handler=new DisconnectedBumpScan();        }
  else if(scan.find("GlobalThresholdTune")!=string::npos)        { handler=new GlobalThresholdTune();        }
  else if(scan.find("PixelThresholdTune")!=string::npos)        { handler=new PixelThresholdTune();        }

  else if(scan.find("SelfTriggerScan")!=string::npos)        { handler=new SelfTriggerScan();        }
  else{
    cout << "Scan Manager: Scan not recognized: " << scan << endl;
    cout << "Available scans are: " << endl
 	 << "DigitalScan" << endl
	 << "AnalogScan" << endl
	 << "ReadRegisterScan" << endl
	 << "ThresholdScan" << endl
	 << "ToTScan" << endl
	 << "CommScan" << endl
	 << "TrimScan" << endl
	 << "NoiseScan" << endl
	 << "CrossTalkScan" << endl
	 << "ExternalTriggerScan" << endl
	 << "SelfTriggerScan" << endl;
    return 0;
  }

  if(cFrontEnd.flags()&CmdArg::GIVEN){
    handler->SetScanFE(string(cFrontEnd));
  }
  else{
    handler->SetScanFE(string("all"));
  }
  handler->SetMaskOpt((cMaskOpt.flags()&CmdArg::GIVEN)?cMaskOpt:-1);
  handler->SetVerbose(cVerbose);
  handler->SetContext(backend);
  handler->SetInterface(interface);
  handler->SetMapping(netfile,(cConfs.count()==0));
  handler->SetBusPath(buspath);
  handler->SetRetune(cRetune);
  handler->SetScan(scan);
  handler->SetCommandLine(commandLineStr);

 

  if(cLatency.flags()&CmdArg::GIVEN && scan.find("ExternalTrigger")!=string::npos ) {
    handler->SetLatency(cLatency);
  }

  for(uint32_t i=0;i<cConfs.count();i++){
    cout << "Scan Manager: Adding front end #" << i << " " << string(cConfs[i]) << endl;
    handler->AddFE(string(cConfs[i]));
  }
  
  cout << "Scan Manager: Configure the scan" << endl;
  if(cOutPath.flags()&CmdArg::GIVEN){
    handler->SetOutPath(string(cOutPath));
  }
  if(cCharge.flags()&CmdArg::GIVEN){
    handler->SetCharge(cCharge);
  }
  if(cToT.flags()&CmdArg::GIVEN){
    handler->SetToT(cToT);
  }
  if(cThreshold.flags()&CmdArg::GIVEN){
    handler->SetThreshold(cThreshold);
  }

  if(cStoreHits.flags()&CmdArg::GIVEN){
    handler->StoreHits(cStoreHits);
  }

  cout << "Scan Manager: Connect" << endl;
  handler->Connect();

  cout << "Scan Manager: InitRun" << endl;
  handler->InitRun();

  cout << "Scan Manager: Save configuration before the run" << endl;
  handler->SaveConfig("before");

  cout << "Scan Manager: Config" << endl;
  handler->Config();

  cout << "Scan Manager: PreRun" << endl;
  handler->PreRun();
  
  cout << "Scan Manager: Wait for run to start: " << endl;
  sleep(cWait);

  cout << "Scan Manager: Resetting the tag counters" << endl;
  handler->ResetAllTagCounters();

  cout << "Scan Manager: Run" << endl;
  handler->Run();

  cout << "Scan Manager: Analysis" << endl;
  handler->Analysis();

  cout << "Scan Manager: Disconnect" << endl;
  handler->Disconnect();

  cout << "Scan Manager: Save configuration after the run" << endl;
  handler->SaveConfig("after");

  cout << "Scan Manager: Save the tuned files" << endl;
  handler->SaveConfig("tuned");
  

  cout << "Scan Manager: Save the histograms" << endl;
  handler->Save();

  cout << "Scan Manager: Cleaning the house" << endl;
  delete handler;

  cout << "Scan Manager: Have a nice day" << endl;
  return 0;
}
