#include "RD53BEmulator/DataFrame.h"

#include <iostream>
#include <ctime>
#include <vector>
#include <iomanip>

using namespace std;
using namespace RD53B;

int main() {
  
  cout << "#################################" << endl
       << "# test_rd53b_data               #" << endl
       << "#################################" << endl;

  cout << "Create a new stream" << endl;

  bool EnableCID=false;

  Stream * stream = new Stream();
  
  stream->EnableChipId(EnableCID);



  cout << "Adding hits" << endl;
  stream->AddHit(0x6A,2,3,4,0x5);
  stream->AddHit(0x6A,2,4,4,0x5);
  stream->AddHit(0x6A,3,5,4,0x5);
  stream->AddHit(0x6A,2,6,4,0x5);
  stream->AddHit(0x6A,2,3,4,0x5);
  stream->AddHit(0x6A,2,4,4,0x5);
  stream->AddHit(0x6A,2,5,4,0x5);
  stream->AddHit(0x6B,12,13,0x002F,15);
  stream->AddHit(0x6A,22,23,0xFF20,25);

  cout << "Create new data" << endl;
  DataFrame * data = new DataFrame();
  data->SetChnID(EnableCID);
  

  cout << "Add stream to data" << endl;
  data->AddStream(stream);
  cout<<"Checking chip id on stream: "<<data->GetStream(0)->HasChipId()<<endl;
  cout << "Data to string" << endl;
  cout << data->ToString() << endl;

  cout << "Create a byte stream" << endl;
  vector<uint8_t> bytes(10000,0);

  cout << "Encode" << endl;
  uint32_t sz=data->Pack(&bytes[0]);

  cout << "Byte stream (" << sz << "): " << DataFrame::ToByteStream(&bytes[0],sz) << endl;

  cout << "Clear" << endl;
  data->Clear();


  cout << "Decode" << endl;
  //data->UnPack(bytesReversed.data(),sz);
  data->UnPack(&bytes[0],sz);
  cout << data->ToString() << endl;



  vector<uint8_t> bytes2=bytes;
  uint32_t n_test=1E6;
  clock_t start;
  double duration, frequency;

  cout << "Encoding performance" << endl;
  start = clock();
  for(uint32_t i=0; i<n_test; i++){
    data->Pack(&bytes[0]);
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;

  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [MHz]: " << frequency/1E6 << endl;

  
  cout << "Decoding performance" << endl;
  start = clock();
  for(uint32_t i=0; i<n_test; i++){
    data->UnPack(&bytes2[0],sz);
  }
  duration = ( clock() - start ) / (double) CLOCKS_PER_SEC;
  frequency = (double) n_test / duration;
  
  cout << "CPU time  [s] : " << duration << endl;
  cout << "Frequency [MHz]: " << frequency/1E6 << endl;

  cout << "Cleaning the house" << endl;
  delete data;

  cout << "Have a nice day" << endl;
  return 0;
}

