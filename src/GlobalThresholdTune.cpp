#include "RD53BEmulator/GlobalThresholdTune.h"
#include "RD53BEmulator/Tools.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace RD53B;

GlobalThresholdTune::GlobalThresholdTune(){
  m_ntrigs = 100;
  m_TriggerLatency = 60;
  m_vcalMed  = 500;
  m_vcalHigh = 500;
  m_vcalDiff = 0;
  m_colMin   = 0;
  m_colMax   = 400;
  m_DACMax   = 600;
  m_DACMin   = 100;
  m_DACStep  = 1 ;
  m_FEStep.clear();
}

GlobalThresholdTune::~GlobalThresholdTune(){}

void GlobalThresholdTune::PreRun(){


  if(m_retune){ //This should only run for the Mid FE
    m_DACMin=000; 
    m_DACMax=360; 
    m_DACStep= 20; 
  }
  else{
    m_DACMin=000; 
    m_DACMax=360; 
    m_DACStep= 20; 
  }


  if(m_scan_fe==FrontEndTypes::DiffMidFE){
    // m_colMin   = Matrix::LEFT_COL_END+1;
    // m_colMax   = Matrix::RIGHT_COL_START;
    m_colMin   = Matrix::LEFT_COL_END+1;
    m_colMax   = 42;
  }
  else if(m_scan_fe==FrontEndTypes::DiffLeftFE){
    m_colMin   = Matrix::LEFT_COL_START;
    m_colMax   = Matrix::LEFT_COL_END+1;
  }
  else if(m_scan_fe==FrontEndTypes::DiffRightFE){
    m_colMin   = Matrix::RIGHT_COL_START;
    m_colMax   = Matrix::RIGHT_COL_END+1;
  }
  else{
    m_colMin   = 0;
    m_colMax   = Matrix::NUM_COLS;
  }
  std::cout<<"col Min: "<<m_colMin<<" Max: "<<m_colMax<<std::endl;
  m_vcalDiff = Tools::injToVcal(m_threshold);
  m_vcalHigh = m_vcalMed + m_vcalDiff;
  cout << " PRECONFIG: " << m_threshold << " correspond to VCAL diff: " << m_vcalDiff << endl;

  m_nSteps = 1 + (m_DACMax - m_DACMin) / m_DACStep;
  
  m_logFile << "ntrigs: " << m_ntrigs << endl;
  m_logFile << "latency: " << m_TriggerLatency << endl;
  m_logFile << "vcalMed: " << m_vcalMed << endl;
  m_logFile << "vcalHigh: " << m_vcalHigh << endl;
  m_logFile << "DACmin: " << m_DACMin << endl;
  m_logFile << "DACmax: " << m_DACMax << endl;
  m_logFile << "DACstep: " << m_DACStep << endl;
  m_logFile << "nsteps: " << m_nSteps << endl;


  for(auto fe : GetFEs()){
    m_meanTh[fe->GetName()].resize(m_nSteps,0);
    m_FEStep.push_back(0);
    fe->BackupConfig();

    //Configure the options for an threshold scan                                                                                     
    cout << "GlobalThresholdTune: Configure analog injection for " << fe->GetName() << endl;
    fe->GetConfig()->SetField(Configuration::CalMode,0); //was INJ_MODE_DIG
    fe->GetConfig()->SetField(Configuration::Latency, m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::PIX_BCAST_EN,0);
    fe->GetConfig()->SetField(Configuration::PIX_AUTO_ROW,0);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,m_vcalMed);
    fe->GetConfig()->SetField(Configuration::InjVcalRange,1);    
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,m_vcalHigh);


    if(m_scan_fe==FrontEndTypes::DiffLeftFE || m_scan_fe==FrontEndTypes::DiffFE)
      fe->SetGlobalThreshold(Pixel::DiffLeft,m_DACMax);
    if(m_scan_fe==FrontEndTypes::DiffRightFE || m_scan_fe==FrontEndTypes::DiffFE)
    fe->SetGlobalThreshold(Pixel::DiffRight,m_DACMax);
    if(m_scan_fe==FrontEndTypes::DiffMidFE || m_scan_fe==FrontEndTypes::DiffFE)
      fe->SetGlobalThreshold(Pixel::Diff,m_DACMax);

    fe->WriteGlobal();
    Send(fe);
    
    fe->DisableAll();
    ConfigurePixels(fe);

    //If retune config is called, refersh the pixel threshold
    unsigned counter=0;
    if(m_retune){
      for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){                                              	for(uint32_t col = 0; col < Matrix::NUM_COLS; col++){
	  counter++;
	  fe->SetPixelThreshold(col, row, 0);
	  if(counter%50==0){
	    fe->ProcessCommands();
	    Send(fe);
 	  }    	
	}
      }
    }



    
    m_occ[fe->GetName()] = new TH2I*[m_nSteps];
 
    for(uint32_t Step=0; Step < m_nSteps; Step++){
      //uint32_t GlobalDAC = m_DACMax - Step * m_DACStep;
      uint32_t GlobalDAC = m_DACMin + Step * m_DACStep;
      m_occ[fe->GetName()][Step] = new TH2I(("occ_"+to_string(Step)+"_"+fe->GetName()).c_str(),
					    ("Occupancy map, global DAC value: "+to_string(GlobalDAC)+";Column;Row;Number of hits").c_str(), 
					    Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    }

  }

  
  
  //Setting up the masking according to the Scan Type
  cout<<"Setting up mask for: " << m_colMin << " - " << m_colMax << endl;
  GetMasker()->SetVerbose(m_verbose);
  GetMasker()->SetRange(m_colMin,0,m_colMax,Matrix::NUM_ROWS);
  //GetMasker()->SetRange(0,0,100,100);
  GetMasker()->SetShape(8,4);
  GetMasker()->SetFrequency(10);

  GetMasker()->Build();

}

void GlobalThresholdTune::Run(){

  //Prepare the trigger
  cout << "GlobalThresholdTune: Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency-5, 4, true, true, true, true);
  std::vector<bool> breakStep;
  breakStep.resize(GetFEs().size(),false);
  
  int FECount=0;
  const int FESize=GetFEs().size();
  
  for(auto fe : GetFEs()){ // RD53B is always a bit noisy at initialisation so clearing hits
    if(!fe->HasHits()) continue;
    fe->ClearHits();
  }



  for (m_iStep = 0; m_iStep < m_nSteps; m_iStep++){
    bool AllBreak=true;
    
    for(FECount=0; FECount< FESize; FECount++){ 
      AllBreak = AllBreak && breakStep.at(FECount);
    }
    if (AllBreak) break;

    //Setting the global threshold DAC value
    //uint32_t GlobalDAC = m_DACMin + m_iStep * m_DACStep;
    uint32_t GlobalDAC = m_DACMax - m_iStep * m_DACStep;
    cout << endl << "GlobalThresholdTune: global threshold DAC = " << GlobalDAC << endl;
    for(auto fe : GetFEs()) {

      if(m_scan_fe==FrontEndTypes::DiffLeftFE || m_scan_fe==FrontEndTypes::DiffFE)
	fe->SetGlobalThreshold(Pixel::DiffLeft,GlobalDAC);
      if(m_scan_fe==FrontEndTypes::DiffRightFE || m_scan_fe==FrontEndTypes::DiffFE)
	fe->SetGlobalThreshold(Pixel::DiffRight,GlobalDAC);
      if(m_scan_fe==FrontEndTypes::DiffMidFE || m_scan_fe==FrontEndTypes::DiffFE)
	fe->SetGlobalThreshold(Pixel::Diff,GlobalDAC);

      fe->WriteGlobal();
      Send(fe);
      //std::this_thread::sleep_for(std::chrono::milliseconds(100)); 
    }
    
    std::vector<unsigned> NPixels;

    for(FECount=0; FECount< FESize; FECount++)
      NPixels.push_back(0);

    for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
      //if (st%8!=0) continue;
      //count++;
      GetMasker()->GetStep(st);
      ConfigMask();
      int FECount=-1;
      cout << "                         Mask Step: " << st << " with nPixel: ";
      for (auto fe: GetFEs()){
	FECount++;
	NPixels.at(FECount)+=fe->GetNPixelsEnabled();
	cout<<NPixels.at(FECount)<<" / ";
      }





      cout << endl; 

      //Trigger and read-out
      for(uint32_t trig=0;trig<m_ntrigs; trig++) Trigger();
      
      auto start = chrono::steady_clock::now();
      
      //histogram hit
      uint32_t nhits=0;
      bool LastLoop=false;
      uint32_t tmpTOT=0;
      while(true){
	
	for(auto fe : GetFEs()){
	  if(!fe->HasHits()) continue;
	  LastLoop=false;
	  Hit* hit = fe->GetHit();
	  if(hit!=0) {
	    tmpTOT=hit->GetTOT();
	    
	    if( tmpTOT == 0xF) {
	      fe->NextHit();
	      continue;
	    }
	    if( GetMasker()->Contains(hit->GetCol(),hit->GetRow()) ) {
	    m_occ[fe->GetName()][m_iStep]->Fill(hit->GetCol(),hit->GetRow());
	    nhits++;
	    }
	    else{
	      cout << "Hit not expected in mask step: " << fe->GetName() << ": " << hit->ToString() << endl;
	    }
	  }
	  fe->NextHit();
	}
	auto end = chrono::steady_clock::now();
	uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
	if(LastLoop) break;
	if(ms>20){LastLoop=true;}
      }
      UnconfigMask();
    }
  
    FECount=-1;
    for(auto fe: GetFEs()){
      FECount++;
      double meanTh = double(m_occ[fe->GetName()][m_iStep]->Integral())/NPixels.at(FECount);
      m_meanTh[fe->GetName()][m_iStep] = meanTh;
      std::cout << __PRETTY_FUNCTION__  << "Integral / NPixels: "<< m_occ[fe->GetName()][m_iStep]->Integral()<< "/"<<NPixels.at(FECount) << " => mean Occ: " << meanTh/m_ntrigs*100 << " % at m_iStep: " << m_iStep << endl;
    
      /*
	The DAC value for which the mean occupancy is 50% of m_ntrigs corresponds
	to the threshold target. Therefore we stop the scan when 
	m_meanTh>0.5*m_ntrigs and then we find the DAC value for which 
	m_meanTh=0.5*m_ntrigs by linear interpolation between the last DAC value
	and the previous one.
      */ 
      if(meanTh > 0.5 * m_ntrigs && !breakStep.at(FECount)) {
	std::cout<<"Breaking Mean Thr: "<< meanTh<<" Expect Triggers "<<0.5 * m_ntrigs<<std::endl;
	breakStep.at(FECount)=true;
	m_FEStep.at(FECount)=m_iStep; //Save the iStep that works out for this FE
	//we should skip this front-end from the next loop
      }
    }
  }
}

void GlobalThresholdTune::Analysis(){
  
  int FECount=-1;
  for(auto fe : GetFEs()){
    FECount++;
    //Doing Some input checks. 
    if(m_nSteps==m_FEStep.at(FECount)){ //If m_nSteps is m_FEStep.at(FECount) it means, for loop finnished naturally. This means the for loop push m_FEStep.at(FECount)s to outof range and this needs to be fixed.
      m_FEStep.at(FECount)+=-1; 
      std::cout<<__PRETTY_FUNCTION__<<" WARNING!!!! the DAC Range of is not enough, please decrease DAC Min"<<std::endl;

    } 
    else if(0==m_FEStep.at(FECount)) { //If m_FEStep.at(FECount)s is 0 it means, for loop finnished at first loop. This will cause issue with the remaining function. So will add one to prevent issues. 
      m_FEStep.at(FECount)+=1;
      std::cout<<__PRETTY_FUNCTION__<<" WARNING!!!! the DAC Range of is not enough, please increase DAC Max"<<std::endl;
    }


    std::cout<<"Writting Occupancy plots"<<std::endl;
    for(uint32_t Step=0; Step < m_nSteps; Step++){ 
      m_occ[fe->GetName()][Step]->Write();
    }
    
    
    //linear interpolation
    float meanOccTarg = 50;
    uint32_t DAC1 = m_DACMax - (m_FEStep.at(FECount) - 1) * m_DACStep; //second to last value 
    uint32_t DAC2 = m_DACMax - m_FEStep.at(FECount) * m_DACStep; //last value
    std::cout<<"DAC1: "<<DAC1<<" DAC2: "<<DAC2<<" iStep: "<<m_FEStep.at(FECount)<<"/"<<m_nSteps <<" m_DACStep "<<m_DACStep<<std::endl;
    
    float meanOcc1 = m_meanTh[fe->GetName()][m_FEStep.at(FECount)-1];
    float meanOcc2 = m_meanTh[fe->GetName()][m_FEStep.at(FECount)];
    std::cout<< "Mean Occ1 : "<<meanOcc1<<" Mean Occ2 :"<<meanOcc2<<" Mean Occ Target "<<meanOccTarg<<std::endl;
    uint32_t finalDAC = round(DAC1 + (meanOccTarg - meanOcc1) * signed((DAC2 - DAC1))/(meanOcc2 - meanOcc1));
    //min/max??
    std::cout << __PRETTY_FUNCTION__ << "m_FEStep.at(FECount): " << m_FEStep.at(FECount) << " Occ target: " << meanOccTarg << " DAC1: " << DAC1 << " DAC2: " << DAC2 << " meanOcc1: " << meanOcc1 << " meanOcc2: " << meanOcc2 << " finalDAC: " << finalDAC <<endl;
    
    fe->RestoreBackupConfig();
    if(m_scan_fe==FrontEndTypes::DiffLeftFE || m_scan_fe==FrontEndTypes::DiffFE)
      fe->SetGlobalThreshold(Pixel::DiffLeft,finalDAC);
    if(m_scan_fe==FrontEndTypes::DiffRightFE || m_scan_fe==FrontEndTypes::DiffFE)
    fe->SetGlobalThreshold(Pixel::DiffRight,finalDAC);
    if(m_scan_fe==FrontEndTypes::DiffMidFE || m_scan_fe==FrontEndTypes::DiffFE)
      fe->SetGlobalThreshold(Pixel::Diff,finalDAC);

    fe->WriteGlobal();
    Send(fe);
    
    for(uint32_t Step=0; Step < m_nSteps; Step++){
      delete m_occ[fe->GetName()][Step];
    }
    delete[] m_occ[fe->GetName()];
  }
}
