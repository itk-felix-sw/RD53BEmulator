#include <RD53BEmulator/Handler.h>
#include <RD53BEmulator/DigitalScan.h>

#include <cmdl/cmdargs.h>
#include <signal.h>
#include <sstream>
#include <chrono>
#include <map>
#include <fstream>

using namespace std;
using namespace RD53B;

#include "pybind11/pybind11.h"
#include "pybind11/stl.h"


class test_comm{
  DigitalScan * m_handler;
public:
  test_comm(uint64_t tx,uint64_t rx, std::string bus, std::string FEConf);
  ~test_comm();
  bool CheckCommScans();

};


namespace py = pybind11;
PYBIND11_MODULE(libtest_comm, m) {
  py::class_<test_comm>(m, "test_comm")
    .def(py::init<uint64_t ,uint64_t , std::string, std::string>())
    .def("CheckCommScans",(&test_comm::CheckCommScans),"Check if communication is good");
}



test_comm::test_comm(uint64_t tx,uint64_t rx, std::string bus, std::string FEConf){
  
  std::cout<<"TX: 0x"<<std::hex<<tx<<" RX: 0x"<<rx<<std::dec<<" Bus: "<<bus<<" FE: "<<FEConf<<std::endl;

  m_handler = new DigitalScan();
  (m_handler)->SetCommMode();

  m_handler->SetMaskOpt(1);
  //m_handler->SetVerbose(cVerbose);
  //m_handler->SetContext(backend);
  // m_handler->SetInterface(interface);
  // m_handler->SetMapping(netfile,(cConfs.count()==0));
  m_handler->SetBusPath(bus);
  m_handler->SetVerbose(false);
  m_handler->SetInterface("lo");
  m_handler->AddMapping("Chip",FEConf,tx,rx);
  m_handler->AddFE("Chip");

  // m_handler->SetRetune(cRetune);
  // m_handler->SetScan(scan);
  // m_handler->SetCommandLine(commandLineStr);

  cout << "Scan Manager: Connect" << endl;
  m_handler->Connect();

  cout << "Scan Manager: InitRun" << endl;
  m_handler->InitRun();

  cout << "Scan Manager: Config" << endl;
  m_handler->Config();

  cout << "Scan Manager: PreRun" << endl;
  m_handler->PreRun();

  cout << "Scan Manager: Resetting the tag counters" << endl;
  m_handler->ResetAllTagCounters();


}

test_comm::~test_comm(){
  delete m_handler;
}


bool test_comm::CheckCommScans(){


  cout << "Scan Manager: Run" << endl;
  m_handler->Run();

  cout << "Scan Manager: Analysis" << endl;
  m_handler->Analysis();

  cout << "Scan Manager: Disconnect" << endl;
  m_handler->Disconnect();


  cout << "Scan Manager: Have a nice day" << endl;

  bool retVal = m_handler->CheckComm();
  return retVal;

}

int main(int argc, char *argv[]){

  cout << "#####################################" << endl
       << "# Welcome to scan_manager_rd53b     #" << endl
       << "#####################################" << endl;

  CmdArgStr  cTX('t',"tx","tx","tx",CmdArg::isREQ);
  CmdArgStr  cRX('r',"rx","rx","rx",CmdArg::isREQ);
  CmdArgStr   cConfs( 'c',"config","chips","FE Config",CmdArg::isREQ);
  CmdArgStr  cBusPath('B',"buspath","folder","Network folder. Default /home/itkpix/bus/",CmdArg::isREQ);
  CmdLine cmdl(*argv,&cTX,&cRX,&cConfs,&cBusPath,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  std::string commandLineStr="";
  for (int i=1;i<argc;i++) commandLineStr.append(std::string(argv[i]).append(" "));


  string FEConf=(cConfs.flags()&CmdArg::GIVEN?cConfs:"");
  string buspath=(cBusPath.flags()&CmdArg::GIVEN?cBusPath:"/home/itkpix/bus/");
  
  uint64_t tx = std::stoul(string(cTX));
  uint64_t rx = std::stoul(string(cRX));
  

  test_comm Comm(tx,rx,buspath,FEConf);

  return Comm.CheckCommScans();

}
