#include "RD53BEmulator/Address.h"

#include <iostream>
#include <ctime>
#include <vector>
#include <cmdl/cmdargs.h>

using namespace std;
using namespace RD53B;

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# test_rd53b_address            #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose(  'v',"verbose","turn on verbose mode");
  CmdArgIntList cPixel( 'p',"pixel","col-row","col and row");
  CmdArgIntList cRegion('r',"region","col-row-region-pair","core col, core row, core region, region pair");

  CmdLine cmdl(*argv,&cVerbose,&cPixel,&cRegion,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  Address addr;

  if(cPixel.flags() & CmdArg::GIVEN){
    addr.SetPixel(cPixel[0],cPixel[1]);
  }

  if(cRegion.flags() & CmdArg::GIVEN){
    addr.SetCore(cRegion[0],cRegion[1],cRegion[2],cRegion[3]);
  }

  cout << addr.ToString() << endl;
  cout << "register 1: "<< addr.GetRegister1() << endl;
  cout << "register 2: "<< addr.GetRegister2() << endl;

  cout << "Have a nice day" << endl;
  return 0;
}

