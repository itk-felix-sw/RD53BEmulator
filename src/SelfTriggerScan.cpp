#include "RD53BEmulator/SelfTriggerScan.h"
#include "RD53BEmulator/Tools.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"

#include <iostream>
#include <chrono>
#include <signal.h>

using namespace std;
using namespace RD53B;

bool SelfTriggerScan::m_scanStop = false;

TApplication* theAppSTS;

struct chip_config_t {
  uint32_t chip_x;
  uint32_t chip_y; 
  bool flip_x;
  bool flip_y;
} typedef chip_config_t;

constexpr chip_config_t quad_map[4] = {
  {1, 1, true, false},
  {0, 1, true, false},
  {0, 0, false, true},
  {1, 0, false, true},
};

template <uint32_t chip_x, uint32_t chip_y, bool flip_x, bool flip_y>
inline std::pair<uint32_t, uint32_t> quad_transform(const std::pair<uint32_t, uint32_t>& hit) {
  return std::pair<uint32_t, uint32_t>(
				       (flip_x ? 400 - hit.first : hit.first) + (400 * chip_x),
				       (flip_y ? 384 - hit.second : hit.second) + (384 * chip_y)
				       );
}

SelfTriggerScan::SelfTriggerScan(){
  m_TriggerLatency = 4;
  m_TriggerFrequency = 40e3;
  m_colMin = 0;
  m_colMax = 400;
}

SelfTriggerScan::~SelfTriggerScan(){}

void SelfTriggerScan::StopScan(int){
  cout << endl << "You pressed ctrl+c -> quitting" << endl;
  m_scanStop = true;
}

void SelfTriggerScan::PreRun(){

  m_totalHits = new TH2I("occ_global",";Column;Row;Number of hits", Matrix::NUM_COLS*2, 0, Matrix::NUM_COLS*2, Matrix::NUM_ROWS*2, 0, Matrix::NUM_ROWS*2);

  for(auto fe : GetFEs()){
    //backup the configuration
    fe->BackupConfig();
    cout << "SelfTriggerScan: Configure" << endl;
    cout << "TRIGGER LATENCY IS : " << m_TriggerLatency << endl;
    fe->GetConfig()->SetField(Configuration::CalMode,0);
    fe->GetConfig()->SetField(Configuration::Latency,m_TriggerLatency);
  
    //Self trigger settings
    /*
    fe->GetConfig()->SetField(Configuration::SelfTrigEn,1);
    fe->GetConfig()->SetField(Configuration::EnSelfTrigTot,1);
    fe->GetConfig()->SetField(Configuration::SelfTrigTot,1);
    fe->GetConfig()->SetField(Configuration::SelfTrigMult,4);
    fe->GetConfig()->SetField(Configuration::SelfTrigDelay,45); //this might need to change
    */

    fe->GetConfig()->SetField(Configuration::DAC_TH1_L_DIFF,500);
    fe->GetConfig()->SetField(Configuration::DAC_TH1_R_DIFF,500);
    /*
    fe->GetConfig()->SetField(Configuration::DAC_TH1_M_DIFF,200);
    fe->GetConfig()->SetField(Configuration::DAC_TH1_L_DIFF,400);
    fe->GetConfig()->SetField(Configuration::DAC_TH1_R_DIFF,400);
    */

    fe->WriteGlobal();
    Send(fe);
    fe->EnableAll();

    fe->SetPixelEnable(44,146,false);
    fe->SetPixelEnable(44,145,false);
    fe->SetPixelEnable(116,251,false);
    fe->SetPixelEnable(116,334,false);
    fe->SetPixelEnable(145,0,false);
    fe->SetPixelEnable(184,169,false);
    fe->SetPixelEnable(1,33,false);
    fe->SetPixelEnable(1,44,false);
    fe->SetPixelEnable(1,47,false);
    fe->SetPixelEnable(1,63,false);
    fe->SetPixelEnable(1,50,false);
    fe->SetPixelEnable(263,182,false);
    fe->SetPixelEnable(250,198,false);
    fe->SetPixelEnable(258,198,false);
    fe->SetPixelEnable(266,198,false);
    fe->SetPixelEnable(366,267,false);
    fe->SetPixelEnable(399,277,false);
    fe->SetPixelEnable(398,160,false);
    fe->SetPixelEnable(1,1,false);
    fe->SetPixelEnable(225,347,false);

    ConfigurePixels(fe);
    
    //Create histograms
    m_occ[fe->GetName()]=new TH2I(("occ_"+fe->GetName()).c_str(),";Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_occHigh[fe->GetName()]=new TH2I(("occH_"+fe->GetName()).c_str(),";Column;Row;Number of Hi hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);

    m_enable[fe->GetName()]=new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_mask[fe->GetName()]=new TH2I(("mask_"+fe->GetName()).c_str(),"Mask map;Column;Row;Masked", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tot[fe->GetName()] =new TH1I(("tot_"+fe->GetName()).c_str(),"ToT"  , 16, -0.5, 15.5);
    m_bcid[fe->GetName()]=new TH1I(("bcid_"+fe->GetName()).c_str(),"BCID", 32, -0.5, 31.5);

    m_triggers = new TH1I("triggers", "Number of triggers", 1, 0.5, 1.5);
    m_deltaBCID[fe->GetName()]=new TH2I(("deltaBCID_"+fe->GetName()).c_str(),"DeltaBCID", 32, -0.5, 31.5, 16, -0.5, 15.5);
  }

  // GetMasker()->SetVerbose(m_verbose);
  // GetMasker()->SetRange(0,0,400,384);
  // GetMasker()->SetShape(400,384);
  // GetMasker()->Build();
  // GetMasker()->GetStep(0);

  bool enable=true;
  //ConfigMask();
  for(auto fe : GetFEs()){
    for(auto cc : GetMasker()->GetCoreColumns()){
      fe->EnableCoreColumn(cc,enable);
      fe->EnableCalCoreColumn(cc,enable);
    }
    for(auto dc : GetMasker()->GetDoubleColumns()){
      fe->EnableCalDoubleColumn(dc,enable);
    }
    fe->ProcessCommands();
    Send(fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
  }

}

void SelfTriggerScan::Run(){

  gROOT->SetBatch(false);

  theAppSTS=new TApplication("myApp",0,0);
  TCanvas* myC=new TCanvas("can","can", 1000,1000);//800*GetFEs().size(),800);
  myC->Divide(2,2);

  // RD53B is always a bit noisy at initialisation so clearing hits
  for(auto fe : GetFEs()) { 
    
    //Self trigger settings
    fe->GetConfig()->SetField(Configuration::SelfTrigEn,1);
    fe->GetConfig()->SetField(Configuration::EnSelfTrigTot,1);
    fe->GetConfig()->SetField(Configuration::SelfTrigTot,1);
    fe->GetConfig()->SetField(Configuration::SelfTrigMult,4);
    fe->GetConfig()->SetField(Configuration::SelfTrigDelay,45); //this might need to change
    fe->WriteGlobal();
    Send(fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
    //if(!fe->HasHits()) continue;
    //fe->ClearHits();
  }
  
  auto scan_start = chrono::steady_clock::now();
  
  m_scanStop = false;
  signal(SIGINT, StopScan);
  signal(SIGTERM, StopScan);
  signal(SIGILL, StopScan);
  cout << "Please press Ctrl+C to stop the scan" << endl;
  
  vector<uint32_t> vhits;
  vhits.resize(GetFEs().size(), 0);
  int feID=-1;
  
  uint32_t nhits_total = 0;
  uint32_t nhits       = 0;
  uint32_t ev=0;
  
  while(!m_scanStop){
    
    feID=-1;
    for(auto fe : GetFEs()){
      feID++;
      vector<Hit*> hits=fe->GetHits(10000);
      uint32_t nH=hits.size();
      if (nH==0) {
	this_thread::sleep_for(chrono::milliseconds( 1 ));
      } else {
	for (unsigned int h=0; h<nH; h++) {
	  Hit* hit=hits.at(h);
	  if(hit!=0 && hit->GetTOT()!=0x0){
	    m_bcid[fe->GetName()]->Fill(hit->GetBCID());
	    m_occ[fe->GetName()] ->Fill(hit->GetCol(),hit->GetRow());
	    vhits.at(feID)=vhits.at(feID)+1;
	    /*
	    if( (hit->GetCol() > 400) || (hit->GetRow() > 385) ) continue;
	    m_tot[fe->GetName()]->Fill(hit->GetTOT());
	    m_bcid[fe->GetName()]->Fill(hit->GetBCID());
	    if (hit->GetTOT()>10) m_occHigh[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	    m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	    //fill into the global histogram
	    int fe_idx = fe->GetChipID()-12;
	    std::pair<uint32_t, uint32_t> in_hits = std::make_pair<uint32_t, uint32_t>(hit->GetCol(), hit->GetRow());
	    std::pair<uint32_t, uint32_t> out_hits;
	    switch (fe_idx) {
	    case 0:
	      out_hits = quad_transform<quad_map[0].chip_x, quad_map[0].chip_y, quad_map[0].flip_x, quad_map[0].flip_y>(in_hits);
	      break;
	    case 1:
	      out_hits = quad_transform<quad_map[1].chip_x, quad_map[1].chip_y, quad_map[1].flip_x, quad_map[1].flip_y>(in_hits);
	      break;
	    case 2:
	      out_hits = quad_transform<quad_map[2].chip_x, quad_map[2].chip_y, quad_map[2].flip_x, quad_map[2].flip_y>(in_hits);
	      break;
	    case 3:
	      out_hits = quad_transform<quad_map[3].chip_x, quad_map[3].chip_y, quad_map[3].flip_x, quad_map[3].flip_y>(in_hits);
	      break;
	    default:
	      std::cerr << "Invalid chip ID!" << std::endl;
	      exit(1);
	    }
	  
	    //std::cout << "FE " << fe_idx << "In  hit: (" <<  in_hits.first << ", " <<  in_hits.second << ")  " << "Out hit: (" << out_hits.first << ", " << out_hits.second << ")" << std::endl;
	    m_totalHits->Fill(out_hits.first, out_hits.second);
	    */
	    nhits++;
	    nhits_total++;
	  }
	}
      }
      fe->ClearHits(nH);
    }
    
    ev++;
    //this_thread::sleep_for(chrono::milliseconds( 1 ));
    
    auto current_time = chrono::steady_clock::now();
  
    if( chrono::duration_cast<chrono::seconds>(current_time - scan_start).count()>5 ) {
      scan_start = chrono::steady_clock::now();
      nhits_total = 0;
      
      //myC->cd();
      //m_totalHits->SetMaximum(50);
      //m_totalHits->Draw("COLZ");

      int c=0;
      for(auto fe : GetFEs()){
       	c+=1;
       	myC->cd(c);	
       	//m_occ[fe->GetName()]->SetMaximum(50);
       	m_occ[fe->GetName()]->Draw("COLZ");
      // 	//myC->cd(2);	
      // 	//m_occHigh[fe->GetName()]->SetMaximum(50);
      // 	//m_occHigh[fe->GetName()]->Draw("COLZ");
      // 	//myC->cd(3);	
      // 	//m_tot[fe->GetName()]->Draw("HIST");
      // 	//myC->cd(4);	
      // 	//m_bcid[fe->GetName()]->Draw("HIST");
      // 	//cout << " FE_" << c << ": " << m_occ[fe->GetName()]->Integral();
      }
      //cout << endl;
      myC->Update();
      this_thread::sleep_for(chrono::milliseconds( 10 ));
      //exit(-1);
      cout << " TOTAL Hits in the last batch: " << nhits << " == PER FE: " ;
      const int feSize = GetFEs().size();
      for(feID=0; feID< feSize; feID++) {
	cout << " " << vhits.at(feID) << " || ";
      }
      std::cout<<std::endl;
      vhits.clear();
      vhits.resize(GetFEs().size(), 0);
      nhits=0;
    }
    //this_thread::sleep_for(chrono::microseconds( (int)period ));
  }
  
  //UnconfigMask();
  
  cout << "Total scan time: " << chrono::duration_cast<chrono::seconds>(chrono::steady_clock::now() - scan_start).count() << " s" << endl;
}

void SelfTriggerScan::Analysis(){
  
  gROOT->SetBatch(true);
  TCanvas* can=new TCanvas("plot","plot",800,600);

  for(auto fe : GetFEs()){

    //restore the configuration  
    fe->RestoreBackupConfig();
    fe->GetConfig()->SetField(Configuration::SelfTrigEn,0);
    fe->WriteGlobal();
    Send(fe);
    fe->ClearHits();
    
    //get total # of hits
    float thres = 1000; //m_triggers->GetBinContent(1)*8e-6; //currently threshold = 1e-6 per BC
    //create a mask for noisy pixels, mask everything > ntot/npix
    if (m_maskopt == 0){
       for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
         for(uint32_t col = m_colMin; col < m_colMax; col++){
           if (m_occ[fe->GetName()]->GetBinContent(col+1,row+1) < thres){
	     fe->SetPixelEnable(col,row,true);
	     m_enable[fe->GetName()]->SetBinContent(col+1,row+1,1);
	   }else{
	     fe->SetPixelEnable(col,row,false);
             m_mask[fe->GetName()]->SetBinContent(col+1,row+1,1);
	     //if (m_triggers->GetBinContent(1)>1e6) 
	     cout << " FE: " << fe->GetName() << " --> Masking pixel: " << col << " , " << row << endl;
	   }
         }
       }
       //m_enable=m_mask;
    }
    fe->ClearHits();
    
    m_mask[fe->GetName()]->Write();
    m_enable[fe->GetName()]->Write();
    m_occ[fe->GetName()]->Write();
    m_tot[fe->GetName()]->Write();
    m_triggers->Write();


    m_occ[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC.pdf").c_str() );

    m_enable[fe->GetName()]->SetMaximum(2);
    m_enable[fe->GetName()]->SetMinimum(0);
    m_enable[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__ENABLE.pdf").c_str() );

    m_mask[fe->GetName()]->SetMaximum(2);
    m_mask[fe->GetName()]->SetMinimum(0);
    m_mask[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__MASKED.pdf").c_str() );

    //m_tot[fe->GetName()]->Draw("hist");
    //can->Print( (m_fullOutPath+"/"+fe->GetName()+"__TOT.pdf").c_str() );

    delete m_occ[fe->GetName()];
    delete m_enable[fe->GetName()];
    delete m_mask[fe->GetName()];
    delete m_tot[fe->GetName()];
    delete m_deltaBCID[fe->GetName()];
  }

  delete can;

}
