#include "RD53BEmulator/ReadRegisterScan.h"
#include "RD53BEmulator/Tools.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "TSysEvtHandler.h"

#include <iostream>
#include <chrono>
#include <signal.h>
#include <cmath>


using namespace std;
using namespace RD53B;

bool ReadRegisterScan::m_scanStop = false;

TApplication* theAppRRS;

float adcToV(uint16_t ADC) {
  //return (float(ADC)*m_adcCalPar[1]+m_adcCalPar[0])*Unit::Milli;
  //return (float(ADC)*0.192043+5.89435)*0.001;
  return (float(ADC)*0.202+26.7)*0.001; //VD empirical
  // empirical
  // 0.867 ==> 4095 
  // 0.020 ==>    0 
  // 0.600 ==> 2853 
  
}

float adcToI(uint16_t ADC) {
  return adcToV(ADC)/10e3; //from manual QC;//4.99e3; ///m_adcCalPar[2];
}

//"NtcCalPar": [0.0007488999981433153, 0.0002769000129774213, 7.059500006789676e-8],
// need to plug numbers from Abhi 
float readNtcTemp(float R, bool in_kelvin) {
  //cout << "                  Resistance is: " << R << endl;
  float a = 7.489e-4; //m_ntcCalPar[0];
  float b = 2.769e-4; //m_ntcCalPar[1];
  float c = 7.0595e-8; //m_ntcCalPar[2];
  float logres = log(R);
  float tK = 1.0 / (a + b * logres + c * pow(logres, 3));
  if (in_kelvin) return tK;
  return tK - 273.15;
}



ReadRegisterScan::ReadRegisterScan(){
}

ReadRegisterScan::~ReadRegisterScan(){}


void ReadRegisterScan::PreRun(){

  for(auto fe : GetFEs()){
    fe->BackupConfig();
    m_tempGraphs[fe->GetName()] = new TGraph();
    m_tempGraphs[fe->GetName()]->SetTitle(fe->GetName().c_str());
    m_tempGraphs[fe->GetName()]->GetXaxis()->SetTitle("time since start [s]");
    m_tempGraphs[fe->GetName()]->GetYaxis()->SetTitle("chip ntc temperature [deg C]");
  }
}

void ReadRegisterScan::Run(){

  std::this_thread::sleep_for(std::chrono::milliseconds(2000));

  gROOT->SetBatch(false);

  theAppRRS=new TApplication("myApp",0,0);
  TCanvas* myC=new TCanvas("can","can", 1000, 1000);
  myC->Divide(2,2);

  m_scanStop = false;
  signal(SIGINT, StopScan);
  signal(SIGTERM, StopScan);
  signal(SIGILL, StopScan);
  cout << "Please press Ctrl+C to stop the scan" << endl;

  std::chrono::time_point time_start = std::chrono::steady_clock::now();
  
  while(!m_scanStop){

    for(auto fe : GetFEs()){
    
      //std::cout<<"ReadRegisterScan::Run: Comparing all the register values on the chip with the ones written in config"<<std::endl;
   
      fe->GetConfig()->SetField(Configuration::ServiceDataEnable,1);
      fe->WriteGlobal();
      Send(fe);

      for(uint32_t Register=0;Register<fe->GetConfig()->Size();Register++){
	//if (Register>2) break;
	//cout << " writing read register: " << Register << endl;
	uint32_t OldRegValue = fe->GetConfig()->GetRegisterValue(Register);
	fe->ReadRegister(Register); 
	Send(fe);
	//This wait is arbitrary and we can modify this. (Not sure what the minimum is)
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
	uint32_t NewRegValue = fe->GetConfig()->GetRegisterValue(Register);
	if (OldRegValue!=NewRegValue)
	  std::cout<<"   Stored Value for Register 0x"<<hex<<Register<<dec<<": 0x"<<hex<<NewRegValue<<dec<<"=/=0x"<<hex<<OldRegValue<<dec<<" (Old Value)"<<std::endl;
      }

      fe->GetConfig()->SetField(Configuration::ServiceDataEnable,0);
      fe->WriteGlobal();
      Send(fe);
    }
    std::cout << " " << std::endl << std::endl << std::endl << std::endl; 
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
  }      

  std::chrono::time_point time_meas = std::chrono::steady_clock::now();
  std::chrono::duration<double> time_diff = time_meas - time_start;
  double t_val = time_diff.count();

  cout << "Elapsed time: " << t_val << endl;
  /*
      fe->InitAdc();
      Send(fe);
      //std::cout << std::endl << std::endl << "Reading Temperature and Radiation sensors:  "<<std::endl;

      int def1=fe->GetConfig()->GetField(Configuration::MonitorVMuxSel)->GetValue();
      int def2=fe->GetConfig()->GetField(Configuration::MonitorIMuxSel)->GetValue();
      int def3=fe->GetConfig()->GetField(Configuration::GlobalPulseConf)->GetValue();
      //cout << "DEF3 is: " << def3 << " , " << def1 << " , " << def2 << endl;

      int tot=1;
      float AVGV=0;
      float AVGI=0;
 
      for (int c=0; c<1; c++) {
	for (int i=0; i<tot; i++) {
	  std::this_thread::sleep_for(std::chrono::milliseconds(100));
	
	  fe->SetMuxToExternalCurrent(); 
	  //fe->ReadSensor(1,false);
	  Send(fe);
	  std::this_thread::sleep_for(std::chrono::milliseconds(10));
	
	  fe->ResetAdc(); 
	  //fe->ReadSensor(4,false);
	  Send(fe);
	  std::this_thread::sleep_for(std::chrono::milliseconds(100));
	
	  fe->StartAdc(); 
	  //fe->ReadSensor(5,false);
	  Send(fe);
	  std::this_thread::sleep_for(std::chrono::milliseconds(100));
	
	  fe->ReadRegister(Configuration::ADDR_MonitoringDataADC); 
	  //fe->ReadRegister(137); 
	  Send(fe);
	  std::this_thread::sleep_for(std::chrono::milliseconds(10));
	
	  //cout << " current is: " << adcToI(fe->GetConfig()->GetField(Configuration::MonitoringDataADC)->GetValue()) << " ,, from V: " << adcToV(fe->GetConfig()->GetField(Configuration::MonitoringDataADC)->GetValue()) << endl;

	  //if (i>1) 
	  AVGI=adcToI(fe->GetConfig()->GetField(Configuration::MonitoringDataADC)->GetValue());
	}
      
	for (int i=0; i<1; i++) {
	  //std::this_thread::sleep_for(std::chrono::milliseconds(100));
	  std::this_thread::sleep_for(std::chrono::seconds(2));
	
	  fe->SetMuxToExternalVoltage(); 
	  //fe->ReadSensor(0,false);
	  Send(fe);
	  std::this_thread::sleep_for(std::chrono::milliseconds(10));
	
	  fe->ResetAdc();
	  //fe->ReadSensor(4,false);
	  Send(fe);
	  std::this_thread::sleep_for(std::chrono::milliseconds(100));
	
	  fe->StartAdc(); 
	  //fe->ReadSensor(5,false);
	  Send(fe);
	  std::this_thread::sleep_for(std::chrono::milliseconds(100));
	
	  fe->ReadRegister(Configuration::ADDR_MonitoringDataADC); 
	  //fe->ReadRegister(137); 
	  Send(fe);
	  std::this_thread::sleep_for(std::chrono::milliseconds(10));
	
	  cout << " voltage is: " << adcToV(fe->GetConfig()->GetField(Configuration::MonitoringDataADC)->GetValue()) << ", " << fe->GetConfig()->GetField(Configuration::MonitoringDataADC)->GetValue() << endl;
	  //if (i>1) 
	  AVGV=adcToV(fe->GetConfig()->GetField(Configuration::MonitoringDataADC)->GetValue());
	}
	cout << " NTC temperature for FE " << fe->GetChipID() << " is: " <<  readNtcTemp(AVGV/AVGI, false) << " ( " << AVGV << " , " << AVGI << " ) " << endl;

	//std::this_thread::sleep_for(std::chrono::seconds(10000));
	
	std::chrono::time_point time_meas = std::chrono::steady_clock::now();
	std::chrono::duration<double> time_diff = time_meas - time_start;
	double t_val = time_diff.count();
	m_tempGraphs[fe->GetName()]->AddPoint(t_val, readNtcTemp(AVGV/AVGI, false)); 
      }

      fe->ReadSensor(0,false);
      //fe->ReadSensor(6,false);
      Send(fe);
  */
      /*
	for (int i=0; i<tot; i++) {
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	cout << " " << endl;
      
      
	fe->ReadSensor(1,false);
	Send(fe);
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
	//fe->GetConfig()->GetField(Configuration::MonitorVMuxSel)->SetValue(def1);
	//fe->GetConfig()->GetField(Configuration::MonitorIMuxSel)->SetValue(def2);
	fe->GetConfig()->GetField(Configuration::GlobalPulseConf)->SetValue(def3);
	fe->WriteGlobal();
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	fe->ReadRegister(137); 
	Send(fe);
	float current=adcToI(fe->GetConfig()->GetField(Configuration::MonitoringDataADC)->GetValue());
	cout << "    current is: " << current << " , from: " << fe->GetConfig()->GetField(Configuration::MonitoringDataADC)->GetValue() << endl;
    
	fe->ReadSensor(0,false);
	Send(fe);
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
	//fe->GetConfig()->GetField(Configuration::MonitorVMuxSel)->SetValue(def1);
	//fe->GetConfig()->GetField(Configuration::MonitorIMuxSel)->SetValue(def2);
	fe->GetConfig()->GetField(Configuration::GlobalPulseConf)->SetValue(def3);
	fe->WriteGlobal();
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	fe->ReadRegister(137); 
	Send(fe);
	float voltage=adcToV(fe->GetConfig()->GetField(Configuration::MonitoringDataADC)->GetValue());
	cout << "    Voltage is: " << voltage << " , from: " << fe->GetConfig()->GetField(Configuration::MonitoringDataADC)->GetValue() << endl;
	//cout << " NTC temperature is: " <<  readNtcTemp(voltage / current, false) << endl;
	}    
      */


      /*
      //Will now read Temp/Radiation sensors
      for(uint8_t RadSensor=0;RadSensor<2;RadSensor++){ // O for Temp, 1 for Rad Sensor
      for(uint8_t Pos=0; Pos<4;Pos++){ //There are 4 sensors for each

      fe->ReadSensor(Pos,bool(RadSensor));
      Send(fe);
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      fe->ReadRegister(137); 
      Send(fe);
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      if(RadSensor==1)
      std::cout<<"  ADC Values for Radiation Sensor "<<int(Pos)<<": 0x"<<hex<<fe->GetRadiationSensor(Pos)->GetADC()<<dec<<std::endl; 
      else
      std::cout<<"  ADC Values for Temperature Sensor "<<int(Pos)<<": "<<fe->GetTemperatureSensor(Pos)->GetTemperature()<<" Kelvin, Value: "<<fe->GetTemperatureSensor(Pos)->GetADC()<<std::endl; 
      }
      }
      */
  
      //    fe->GetConfig()->SetField(Configuration::ServiceDataEnable,0);
  // fe->WriteGlobal();
  //     Send(fe);
  //  }
    


/*
    int c = 0;
    for(auto fe : GetFEs()){
      c+=1;
      myC->cd(c);	
      m_tempGraphs[fe->GetName()]->GetYaxis()->SetRangeUser(20, 40);
      m_tempGraphs[fe->GetName()]->Draw("ALP");
    }
    myC->Update();
*/

//}

//  std::this_thread::sleep_for(std::chrono::milliseconds(2000));

}

void ReadRegisterScan::Analysis(){

  gROOT->SetBatch(true);
  TCanvas* can=new TCanvas("plot","plot",800,600);

  for(auto fe : GetFEs()){
    fe->RestoreBackupConfig();
    m_tempGraphs[fe->GetName()]->Write();

    m_tempGraphs[fe->GetName()]->Draw("ALP");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__Temp.pdf").c_str() );

  }
 
  

}

void ReadRegisterScan::StopScan(int){
  cout << endl << "You pressed ctrl+c -> quitting" << endl;
  m_scanStop = true;
}
