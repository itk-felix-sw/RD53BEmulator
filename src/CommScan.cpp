#include "RD53BEmulator/CommScan.h"
#include "RD53BEmulator/Tools.h"
#include "RD53BEmulator/SCurveFitter.h"

#include <iostream>
#include <sstream>
#include <chrono>

#include "TCanvas.h"
#include "TROOT.h"
#include "TMarker.h"
#include "TStyle.h"

using namespace std;
using namespace RD53B;

CommScan::CommScan(){
  m_ntrigs         =100; 
  m_TriggerLatency = 60;

  m_par0_Min   =   0;
  m_par0_Max   =1000;
  m_par0_nSteps=  25;
  m_par1_Min   =   0;
  m_par1_Max   =1000;
  m_par1_nSteps=  25;
  m_par0_Label = "CML Bias 0";
  m_par1_Label = "CML Bias 1";
}

CommScan::~CommScan(){}

void CommScan::PreRun(){

  m_logFile << "\"m_ntrigs\": "         << m_ntrigs << endl;
  m_logFile << "\"m_TriggerLatency\": " << m_TriggerLatency << endl;
  
  if(m_verbose){
    cout << "CommScan: m_ntrigs = " << m_ntrigs << endl;
    cout << "CommScan: m_TriggerLatency = " << m_TriggerLatency << endl;
  }

  uint32_t totStep=m_par0_nSteps*m_par1_nSteps;
  float step0=(m_par0_Max-m_par0_Min)/m_par0_nSteps;
  float step1=(m_par1_Max-m_par1_Min)/m_par1_nSteps;

  for(auto fe : GetFEs()){

    fe->BackupConfig();

    cout << "AnalogScan: Configure Analog injection" << endl;
    fe->GetConfig()->SetField(Configuration::CalMode,1);
    fe->GetConfig()->SetField(Configuration::Latency,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::PIX_BCAST_EN,0);
    fe->GetConfig()->SetField(Configuration::PIX_AUTO_ROW,0);
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,4000);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,200);
    fe->GetConfig()->SetField(Configuration::InjVcalRange,0);
    
    fe->WriteGlobal();
    Send(fe);
       
    cout << "CommScan: Mask all" << endl;
    fe->DisableAll();
    ConfigurePixels(fe);

    //Create the histograms for the threshold scan of each pixel
    cout << "CommScan: Create histograms per pixel for " << fe->GetName() << endl;
    
    m_occ[fe->GetName()]=new TH2I*[totStep];
    for(uint32_t iStep=0; iStep<totStep; iStep++){
      stringstream iStep_ss;
      iStep_ss << iStep;
      m_occ[fe->GetName()][iStep] = new TH2I(("occ_"+fe->GetName()+"_"+iStep_ss.str()).c_str(),
					     (fe->GetName()+", Occupancy; Column; Row").c_str(),
					     Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    }
    
    m_results[fe->GetName()] = new TH2F(("CommScan_"+fe->GetName()).c_str(),(fe->GetName()+";"+m_par0_Label+";"+m_par1_Label).c_str(),
					m_par0_nSteps+1, m_par0_Min-step0/2, m_par0_Max+step0/2,
					m_par1_nSteps+1, m_par1_Min-step1/2, m_par1_Max+step1/2);
  }
  
  GetMasker()->SetRange(0,0,400,384);
  GetMasker()->SetShape(8,2);
  GetMasker()->SetFrequency(20);
  GetMasker()->SetVerbose(false);
  GetMasker()->Build();
}

void CommScan::Run(){

  //Prepare the trigger
  cout << "CommScan: Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency-5, 4, true, true, true, true);
  
  for(auto fe : GetFEs()){ // RD53B is always a bit noisy at initialisation so clearing hits
    if(!fe->HasHits()) continue;
    fe->ClearHits();
  }

  GetMasker()->GetStep(0);
  ConfigMask();
  
  uint32_t nH=0;
  vector<Hit*> hits;
  
  uint32_t ExpectedHits=0;
  for(auto fe : GetFEs()) ExpectedHits+=m_ntrigs*fe->GetNPixelsEnabled();

  float step0=(m_par0_Max-m_par0_Min)/m_par0_nSteps;
  float step1=(m_par1_Max-m_par1_Min)/m_par1_nSteps;
  int count=0;
  for(uint32_t iStep0=0; iStep0<m_par0_nSteps+1; iStep0++){
    float val0=(int)(m_par0_Min+step0*iStep0);
    
    for(uint32_t iStep1=0; iStep1<m_par1_nSteps+1; iStep1++){
      count+=1;
      float val1=(int)(m_par1_Min+step1*iStep1);

      //if ( ((val0+val1)>1000 ) or (val0<val1) ) continue;
      if ( val0<val1 ) continue;

      cout << "CommScan: par0= " << val0 << " , val1= " << val1 << " :::: ";
      
      //VD: this needs to be done automatically depending on whether this is in DataMerging ....
      for(auto fe : GetFEs()){
	//if ( fe->GetName()!="rd53b_quad_4" && fe->GetName()!="rd53b_quad_2"  ) {
        //if ( fe->GetName()!="rd53b_quad_4" ) {
	  fe->GetConfig()->SetField(Configuration::DAC_CML_BIAS_0,val0);
	  fe->GetConfig()->SetField(Configuration::DAC_CML_BIAS_1,val1);
	  fe->WriteGlobal();
	  Send(fe);
	  //}
      }
      
      //Trigger 
      for(uint32_t trig=0;trig<m_ntrigs; trig++) {
	std::this_thread::sleep_for(std::chrono::microseconds(2));
	Trigger();
      }
      std::this_thread::sleep_for(std::chrono::microseconds(20));
      auto start = chrono::steady_clock::now();
      
      //Readout the hit histogram hit
      bool LastLoop=false;
      uint32_t nhits=0;
      uint32_t nRhits=0;
      uint32_t nGoodHists=0;

      while(true){
	for(auto fe : GetFEs()){

	  hits=fe->GetHits(10000);
	  nH=hits.size();
	  if(nH>0) start = chrono::steady_clock::now();
	  if (nH==0) {
	    this_thread::sleep_for(chrono::milliseconds( 1 ));
	  } else {
	    for (unsigned int h=0; h<nH; h++) {
	      Hit* hit = hits.at(h);
	      if(hit==0) continue;
	      LastLoop=false;

	      if(hit->GetTOT()>0){
		if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
		  //cout << "Hit is not expected in the current mask step: " << hit->ToString() << endl;
		} else {
		  nGoodHists++;
		  //m_occ[fe->GetName()][count]->Fill(hit->GetCol(),hit->GetRow());
		  m_results[fe->GetName()]->Fill(val0,val1);
		  nRhits++;
		}
		nhits++;
	      }
	    }
	  }
	  
	  fe->ClearHits(nH);
	}
	auto end = chrono::steady_clock::now();
	uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
	if (LastLoop) break;
	if (nhits>ExpectedHits*5) {
	  cout << " too many hits" << endl;
	  break;
	}
	if(ms>20 or nGoodHists>=ExpectedHits ){LastLoop=true;}
      }
      cout << "CommScan: NHeaders in step: " << nhits <<" / "<<ExpectedHits<<" NHits: "<<nRhits<<" / "<<ExpectedHits<< endl;
       
    }
  }

  for(auto fe : GetFEs()){
    //m_results[fe->GetName()]->SetMaximum(m_ntrigs*GetMasker()->GetPixels().size());
    m_results[fe->GetName()]->Scale(100./( (float)(m_ntrigs*GetMasker()->GetPixels().size()) ) );
    m_results[fe->GetName()]->SetMaximum(102);
    m_results[fe->GetName()]->SetMinimum( 85);
  }
  UnconfigMask();
}

void CommScan::Analysis(){
  gStyle->SetOptStat(0);
  gStyle->SetPaintTextFormat("1.2g");
  TCanvas* can=new TCanvas("plot","plot",800,600);
  for(auto fe : GetFEs()){
    cout << "CommScan: Restore the original threshold value" << endl;
    
    fe->RestoreBackupConfig();

    int par0=fe->GetConfig()->GetField(Configuration::DAC_CML_BIAS_0)->GetValue();
    int par1=fe->GetConfig()->GetField(Configuration::DAC_CML_BIAS_1)->GetValue();
 
    TMarker* m=new TMarker(par0,par1,1);
    m->SetMarkerStyle(20);
    m->SetMarkerSize(2);

    m_results[fe->GetName()]->SetTitle( (fe->GetName()+" Occ").c_str() );
    m_results[fe->GetName()]->Draw("COLZTEXT");
    m->Draw("SAMEP");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__Summary.pdf").c_str() );

    delete m;
  }
  delete can;

  
}
