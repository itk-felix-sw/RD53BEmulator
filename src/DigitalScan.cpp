#include "RD53BEmulator/DigitalScan.h"

#include <iostream>
#include <chrono>
#include <unistd.h>

#include "TCanvas.h"
#include "TStyle.h"

using namespace std;
using namespace RD53B;

DigitalScan::DigitalScan(){
  m_ntrigs         = 100;
  m_TriggerLatency =  60;
  m_CommScan = false;
}

DigitalScan::~DigitalScan(){}





void DigitalScan::SetCommMode(){
  m_CommScan=true;
}




void DigitalScan::PreRun(){
  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
  m_logFile << "\"m_TriggerLatency\": " << m_ntrigs << endl;
  
  if(m_verbose){
    cout << "DigitalScan: m_ntrigs = " << m_ntrigs << endl;
    cout << "DigitalScan: m_TriggerLatency = " << m_TriggerLatency << endl;
  } 
  auto t1 = chrono::steady_clock::now();
  for(auto fe : GetFEs()){ 
 
    fe->BackupConfig();

    cout << "DigitalScan: Configure digital injection" << endl;
    fe->GetConfig()->SetField(Configuration::CalMode,1); // Digital
    ///fe->GetConfig()->SetField(Configuration::CalAnaMode,0); // Analog
    fe->GetConfig()->SetField(Configuration::Latency,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::PIX_BCAST_EN,0);
    fe->GetConfig()->SetField(Configuration::PIX_AUTO_ROW,0);
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,2000);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,100);
    fe->GetConfig()->SetField(Configuration::InjVcalRange,0);
    
    fe->WriteGlobal();
    Send(fe);

    m_occ[fe->GetName()]   =new TH2I(("occ_"+fe->GetName()).c_str(),"Occupancy map;Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS,0, Matrix::NUM_ROWS);
    m_enable[fe->GetName()]=new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable"        , Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tot[fe->GetName()]   =new TH1I(("tot_"+fe->GetName()).c_str(),(fe->GetName()+";TOT").c_str(), 16, 0, 16);
    m_tid[fe->GetName()]   =new TH1I(("tid_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger ID").c_str(), 16, 0, 16);
    m_ttag[fe->GetName()]  =new TH1I(("ttag_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger Tag").c_str(), 16, 0, 16);
    m_bcid[fe->GetName()]  =new TH1I(("bcid_"+fe->GetName()).c_str(),(fe->GetName()+";BCID").c_str(),4,-0.5,3.5); //0xFFF+1,-0.5,0xFFF+0.5);
    m_hits[fe->GetName()]  =new HitTree(("hits_"+fe->GetName()).c_str(),fe->GetName());
    m_Time     = new TH1I(("AverageTime"),"Time",200000 , 0, 200000);
    m_TimeNorm = new TH1F(("AverageTimeNorm"),"Time/Hits",2000 , 0, 1);
    cout << "DigitalScan: Mask all" << endl;
    fe->DisableAll();
    Send(fe);
    ConfigurePixels(fe);
    //Send(fe);

    /*
    // fe->GetConfig()->SetField(Configuration::EnCoreCol_0,0x0);
    // fe->GetConfig()->SetField(Configuration::EnCoreCol_1,0x0);
    // fe->GetConfig()->SetField(Configuration::EnCoreCol_2,0x0);
    // fe->GetConfig()->SetField(Configuration::EnCoreCol_3,0x0);
    fe->GetConfig()->SetField(Configuration::EnCoreColCal_0,0x0);
    fe->GetConfig()->SetField(Configuration::EnCoreColCal_1,0x0);
    fe->GetConfig()->SetField(Configuration::EnCoreColCal_2,0x0);
    fe->GetConfig()->SetField(Configuration::EnCoreColCal_3,0x0);
    // fe->GetConfig()->SetField(Configuration::HITOR_MASK_0,0xffff);
    // fe->GetConfig()->SetField(Configuration::HITOR_MASK_1,0xffff);
    // fe->GetConfig()->SetField(Configuration::HITOR_MASK_2,0xffff);
    // fe->GetConfig()->SetField(Configuration::HITOR_MASK_3,0x3f);
    fe->GetConfig()->SetField(Configuration::HITOR_MASK_0,0);
    fe->GetConfig()->SetField(Configuration::HITOR_MASK_1,0);
    fe->GetConfig()->SetField(Configuration::HITOR_MASK_2,0);
    fe->GetConfig()->SetField(Configuration::HITOR_MASK_3,0);
    */
    //fe->WriteGlobal();
  }
   
  auto t2 = chrono::steady_clock::now();
  std::cout<<"Configuration took: "<<chrono::duration_cast<chrono::milliseconds>(t2 - t1).count()<<std::endl;
  t1 = chrono::steady_clock::now();

  //Create the mask
  cout << "DigitalScan: Create the mask" << endl;
  GetMasker()->SetVerbose(m_verbose);

  GetMasker()->SetRange(0,0,400,384);

  //GetMasker()->SetRange(0,0,200,192);
  GetMasker()->SetShape(8,4);
  GetMasker()->SetFrequency(25);
 
  GetMasker()->Build();

  GetMasker()->GetStep(0);
  ConfigMask();

  t2 = chrono::steady_clock::now();
  std::cout<<"Masking took took: "<<chrono::duration_cast<chrono::milliseconds>(t2 - t1).count()<<std::endl;
  t1 = chrono::steady_clock::now();
}


void DigitalScan::Run(){

  //Prepare the trigger
  cout << "DigitalScan: Prepare the TRIGGER" << endl;
  
  PrepareTrigger(m_TriggerLatency+1, 1, true, true, true, true,1); 
   //PrepareTrigger(m_TriggerLatency-5, 4, true, true, true, true,1); //default //TT10

  //Trigger a set of triggers syncronise felix
  for(uint32_t trig=0;trig<m_ntrigs; trig++) {
    std::this_thread::sleep_for(std::chrono::microseconds(1));
    Trigger();
  }




  
  // to have TT=4 I need latency +0-->+3 ... choose+1
  uint32_t nH=0;
  vector<Hit*> hits;
  // RD53B is always a bit noisy at initialisation so clearing hits
  /*
  for(auto fe : GetFEs()){ 
    hits=fe->GetHits(20000);
    nH=hits.size();
    cout << " FE: " << fe->GetName() << " Need to clear hits: " << nH << std::endl;
    if(nH!=0) fe->ClearHits(nH);
  }
  */

  //cout << "AnalogScan: Create the mask" << endl;
  for(auto fe : GetFEs()){ // RD53B is always a bit noisy at initialisation so clearing hits
    if(!fe->HasHits()) continue;
    fe->ClearHits();
  }

  //std::this_thread::sleep_for(std::chrono::microseconds(20000));
  
  //loop over mask steps
  uint32_t NSteps=GetMasker()->GetNumSteps();
  uint32_t WaitDelays=20;
  if(m_CommScan) { //For comm scan wait longer we want to makesure all data is here
    NSteps=5;
    WaitDelays=20;
  }
  for(uint32_t st=0; st<NSteps;st++){
    //cout << "DigitalScan: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() << endl;

    //auto t1 = chrono::steady_clock::now();
    GetMasker()->GetStep(st);

    //Do the mask step
    ConfigMask();
    ConfigMask();
    //auto t2 = chrono::steady_clock::now();
    //std::cout<<"Masking took: "<<chrono::duration_cast<chrono::milliseconds>(t2 - t1).count()<<std::endl;

    uint32_t ExpectedHits=0;
    for(auto fe : GetFEs()){ 
      ExpectedHits+=m_ntrigs*fe->GetNPixelsEnabled();
    }


    //std::this_thread::sleep_for(std::chrono::microseconds(500));
    //Trigger and read-out
    //t1 = chrono::steady_clock::now();
    for(uint32_t trig=0;trig<m_ntrigs; trig++) {
      std::this_thread::sleep_for(std::chrono::microseconds(1));
      Trigger();
    }
    //std::this_thread::sleep_for(std::chrono::microseconds(1000));
    std::this_thread::sleep_for(std::chrono::microseconds(100));
    //t2 = chrono::steady_clock::now();
    //std::cout<<"All triggers took: "<<chrono::duration_cast<chrono::milliseconds>(t2 - t1).count()<<std::endl;
    
    //histogram hit
    bool LastLoop=false;
    uint32_t nHitsT=0;
    uint32_t nHitsV=0;
    uint32_t nHitsG=0;
    vector<uint32_t> vhits;
    vector<uint32_t> vhitsGood;
    vhits.resize(GetFEs().size(), 0);
    vhitsGood.resize(GetFEs().size(), 0);
    auto start = chrono::steady_clock::now();
    auto end = chrono::steady_clock::now();
    auto start_fixed = chrono::steady_clock::now();
    //t1 = chrono::steady_clock::now();
    while(true){
      int feID=-1;
      for(auto fe : GetFEs()){
	feID+=1;
	hits=fe->GetHits(20000);
	nH=hits.size();
	if(nH>0) start = chrono::steady_clock::now();
	if(m_verbose) std::cout<<"NH: "<<nH<<std::endl;
	if (nH==0) {
	  //this_thread::sleep_for(chrono::microseconds( 500 ));
	  this_thread::sleep_for(chrono::milliseconds( 1 ));
	} else {
	  for (unsigned int h=0; h<nH; h++) {
	    Hit* hit=hits.at(h);
	    nHitsT++;
	    if ( hit==0 ) continue;
	    if ( hit->GetTOT()==0x0 ) continue;
	    hit->SetEvNum(st);
	    //cout << "Hit V: " << fe->GetName() << ": " << hit->ToString() << endl;
	    m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	    nHitsV++;
	    vhits.at(feID)=vhits.at(feID)+1;   
	    
	    //LastLoop=false;
	    if( !GetMasker()->Contains(hit->GetCol(),hit->GetRow()) ) {
	      //cout << "Hit not expected in mask step: " << fe->GetName() << ": " << hit->ToString() << endl;
	    } else { //VD maybe add a check on TOT
	      nHitsG++;
	      vhitsGood.at(feID)++;   
	    }
	    // auto seg1 = chrono::steady_clock::now();
	    //m_tot[fe->GetName()] ->Fill(hit->GetTOT());
	    //m_tid[fe->GetName()] ->Fill(hit->GetTID());
	    m_ttag[fe->GetName()]->Fill(hit->GetTTag());
	    m_bcid[fe->GetName()]->Fill(hit->GetBCID());
	  }
	}
	fe->ClearHits(nH);
      }
      end = chrono::steady_clock::now();
      uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
      if(LastLoop) break;
      if(ms>WaitDelays or nHitsG>=ExpectedHits ){LastLoop=true;}
    }

    cout << "DigitalScan:: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() 
	 << " ==== allFE: " << nHitsT << " / " << nHitsV << " / " << nHitsG << " / " << ExpectedHits
	 << " ==== PER FE: ( ";


   int feID=-1;
    for(auto fe : GetFEs()) {
      feID+=1; 
      cout << " " << vhits.at(feID) << " , " << vhitsGood.at(feID) <<" / "<< m_ntrigs*fe->GetNPixelsEnabled() << " || ";

      m_Nhits[fe->GetName()]+=vhitsGood.at(feID);
      m_ExpectNhits[fe->GetName()]+=m_ntrigs*fe->GetNPixelsEnabled();


    }
    std::cout<<std::endl;
    //cout <<"Total time recieving and processing "<<ExpectedHits<<" hits are "<<chrono::duration_cast<chrono::microseconds>(end - start_fixed).count() <<" us"<<std::endl;
    m_Time->Fill(chrono::duration_cast<chrono::microseconds>(end - start_fixed).count());    
    m_TimeNorm->Fill(float(chrono::duration_cast<chrono::microseconds>(end - start_fixed).count())/ExpectedHits);    

    //t2 = chrono::steady_clock::now();
    //std::cout<<"Trigger and Data Read took: "<<chrono::duration_cast<chrono::milliseconds>(t2 - t1).count()<<std::endl;
    

    //cout << " Digital: NRealHits in step: " << nRhits 
    // <<" / "<< m_ntrigs*GetMasker()->GetPixels().size()*GetFEs().size() <<" N Orig Hits "<<nhits << endl;

    //Undo the mask step

    //t1 = chrono::steady_clock::now();

    UnconfigMask();
    //std::this_thread::sleep_for(std::chrono::microseconds(1));

    //t2 = chrono::steady_clock::now();
    //std::cout<<"Un Config Masking took: "<<chrono::duration_cast<chrono::milliseconds>(t2 - t1).count()<<std::endl;
    // if(st==10) break;
  }
}

void DigitalScan::Analysis(){
  gStyle->SetOptStat(0);
  TCanvas* can=new TCanvas("plot","plot",800,600);
  
  for(auto fe : GetFEs()){
    cout << "DigitalScan: Restore the original threshold value" << endl;
    fe->RestoreBackupConfig();
    
    m_Time->Write();
    m_TimeNorm->Write();
    std::cout<<"Mean Time "<<m_Time->GetMean()<<" +- "<<m_Time->GetRMS()<<" us, Normalized to Hit "<<m_TimeNorm->GetMean()<<" +- "<<m_TimeNorm->GetRMS()<<" us "<<std::endl; 
    m_occ[fe->GetName()]->Write();
    m_enable[fe->GetName()]->Write();
    m_tid[fe->GetName()]->Write();
    m_ttag[fe->GetName()]->Write();
    m_bcid[fe->GetName()]->Write();
    if(m_storehits){m_hits[fe->GetName()]->Write();}

    m_occ[fe->GetName()]->SetTitle( (fe->GetName()+" Occ").c_str() );
    m_occ[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC.pdf").c_str() );
    
    m_occ[fe->GetName()]->SetTitle( (fe->GetName()+" Occ zoom").c_str() );
    m_occ[fe->GetName()]->SetMinimum(m_ntrigs*0.95);
    m_occ[fe->GetName()]->SetMaximum(m_ntrigs*1.02);
    m_occ[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC_zoom.pdf").c_str() );


    //Applying Digital Mask if maskopt is not 0
    if(m_maskopt!=0){
      uint32_t nDisablePix=0;
      for(unsigned binX=0; binX<Matrix::NUM_COLS; binX++)
	for(unsigned binY=0; binY<Matrix::NUM_ROWS; binY++)
	  {
	    uint32_t BinVal= m_occ[fe->GetName()]->GetBinContent(binX+1,binY+1);
	    if( (m_ntrigs * 0.9) < BinVal && (m_ntrigs * 1.1) > BinVal ){
	      //std::cout<<"col,row: "<<binX<<","<<binY<<" = "<<BinVal<<" Accepted "<<std::endl;
	      fe->SetPixelEnable(binX, binY ,1); 
	      fe->SetPixelMask(binX, binY ,1); 
	    }
	    else {
	      nDisablePix+=1;
	      fe->SetPixelEnable(binX, binY ,0); 
	      fe->SetPixelMask(binX, binY ,0); 
	    }
	  }
      std::cout<<nDisablePix<<" Pixels have been disabled"<<std::endl;
    }    

    delete m_occ[fe->GetName()];
    delete m_enable[fe->GetName()];
    delete m_tid[fe->GetName()];
    delete m_ttag[fe->GetName()];
    delete m_bcid[fe->GetName()];
    delete m_hits[fe->GetName()];
  }
  delete can;
}



bool DigitalScan::CheckComm(){
  bool RunGood=true;
  for(auto fe : GetFEs()) {

    float Ratio= m_Nhits[fe->GetName()]/m_ExpectNhits[fe->GetName()];
    RunGood = RunGood and (Ratio>0.9);
    cout << "Ratio variable is: "<<Ratio<<endl;
  }
  cout<<"The Run is marked: "<<RunGood<<endl;
  return RunGood;
}


