#include "RD53BEmulator/Stream.h"
#include "RD53BEmulator/BinaryTree.h"
#include "RD53BEmulator/untree6.hpp"
#include "RD53BEmulator/HitMap.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cassert>
#include <chrono>

using namespace std;
using namespace RD53B;

Stream::Stream(){
  m_enable_enc=true;
  m_enable_tot=true;
  m_enable_cid=true;
  m_verbose=false;
}

Stream::~Stream(){
  Clear();
}

void Stream::Clear(){
  m_tags.clear();
  m_qcols.clear();
  m_qrows.clear();
  m_hitmaps.clear();
  m_totmaps.clear();
}

string Stream::ToString(){
  ostringstream os;
  os << "Stream "
     << "#hits:" << m_tags.size();
  for(uint32_t i=0;i<m_tags.size();i++){
    os << endl << "  " << GetHitString(i);
  }
  return os.str();
}

void Stream::EnableChipId(bool enable){
  m_enable_cid=enable;
}

bool Stream::HasChipId(){
  return m_enable_cid;
}

void Stream::EnableTot(bool enable){
  m_enable_tot=enable;
}

bool Stream::HasTot(){
  return m_enable_tot;
}

void Stream::EnableEnc(bool enable){
  m_enable_enc=enable;
}

bool Stream::HasEnc(){
  return m_enable_enc;
}

void Stream::SetVerbose(bool enable){
  m_verbose = enable;
}

void Stream::SetStreamTag(uint32_t tag){
  m_tag = tag;
}

uint32_t Stream::GetStreamTag(){
  return m_tag;
}

uint32_t Stream::readBytes(uint8_t * bytes, uint32_t pos, bool &value){
  value=0;
  //We do bit bye bit, it's lazy but safe
    //Bit possitions of 0(1,3) are not allowed in 64 bit blocks due to the structure of data
    if(pos%64==0) pos++;
    if(m_enable_cid && pos%64==1) pos++;
    if(m_enable_cid && pos%64==2) pos++;
    value|= uint32_t( (bytes[pos/8] >> (7 - (pos%8))) & 0x1 ) ;
    pos++; //We seperate index and pos as certain positions are not allowed 
  return pos;
}

uint32_t Stream::readBytes(uint8_t * bytes, uint32_t pos, uint32_t &value, uint32_t len){

  static const uint8_t LUT[9] = { 0x0, 0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F, 0x7F, 0xFF };

  value = 0;
  uint8_t index = 0; // We need keep a seperate  index and pos as certain positions are reserved and not used
  //std::cout<<m_enable_cid<<" - pos - "<<pos<<" - len - "<<len<<std::endl;;
  while(index < len){
    if(pos%64==0) pos++;
    if(m_enable_cid && pos%64==1) pos++;
    if(m_enable_cid && pos%64==2) pos++;

    uint32_t posMax= ((len-index) > (8-(pos%8))) ? (8-pos%8) : (len-index);

    value |= uint32_t((( bytes[pos/8] >> (8-(posMax+pos%8))) &  LUT[ posMax ] ) << (len-(index+posMax))); 
    //std::cout<<" posMax "<<posMax<<" bytes "<<std::hex<<int(bytes[pos/8])<<std::dec<<" val  "<<value<<" ";
    index+= posMax;
    pos+= posMax;
    //std::cout<<" index "<<int(index)<<" pos "<<pos<<std::endl;
  }
  //std::cout<<" - pos new - "<<pos<<" -- value --"<<std::hex<<value<<std::dec<<std::endl;
  return pos;
  
}


uint32_t Stream::readBytes(uint8_t * bytes, uint32_t pos, uint64_t &value, uint32_t len){

  static uint8_t LUT[9] = { 0x0, 0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F, 0x7F, 0xFF };
  value = 0;
  uint8_t index = 0; // We need keep a seperate  index and pos as certain positions are reserved and not used
  //std::cout<<m_enable_cid<<" - pos - "<<pos<<" - len - "<<len<<std::endl;;
  while(index < len){
    if(pos%64==0) pos++;
    if(m_enable_cid && pos%64==1) pos++;
    if(m_enable_cid && pos%64==2) pos++;
    uint32_t posMax= ((len-index) > (8-(pos%8))) ? (8-pos%8) : (len-index);
    value |= uint64_t(( bytes[pos/8] >> (8-(posMax+pos%8))) &  LUT[ posMax ] ) << (len-(index+posMax));   
    //std::cout<<" posMax "<<posMax<<" bytes "<<std::hex<<int(bytes[pos/8])<<" val  "<<value<<" "<<std::dec;
    index+= posMax;
    pos+= posMax;
    //std::cout<<" index "<<int(index)<<" pos "<<pos<<std::endl;
  }
  //std::cout<<" - pos new - "<<pos<<" -- value --"<<std::hex<<value<<std::dec<<std::endl;
  return pos;
  
}

uint32_t Stream::writeBytes(uint8_t * bytes, uint32_t pos, uint64_t value, uint32_t len){
  //We do bit bye bit, it's lazy but safe
  for(uint32_t index=0; index < len ; index++) {
    //if(pos%64==0) { for(uint32_t i=0;i<8;i++){bytes[pos/8+i]=0;} } //clean the next 8 bytes
    if(pos%64==0) pos++;
    if(m_enable_cid && pos%64==1) pos++;
    if(m_enable_cid && pos%64==2) pos++;
    //bytes[pos/8] &= ~(1 << (7-pos%8));
    bytes[pos/8] |= ((value >> (len - index - 1) ) & 0x1 ) << (7 - (pos%8));
    //bytes[pos/8] |= ((value >> (index) ) & 0x1 ) << (7 - (pos%8));
    pos++; //We seperate index and pos as certain positions are not allowed
  }
  return pos;
}

uint32_t Stream::writeBytes(uint8_t * bytes, uint32_t pos, bool value){
  return writeBytes(bytes,pos,(uint64_t)value,1);
}

uint32_t Stream::Pack(uint8_t * bytes){

  bool nStream=1;
  bool isfirst=1;
  bool islast=1;
  bool islastposted=0;
  bool isneighbor=0;
  uint32_t tagsize=8;
  uint32_t colsize=6;
  uint32_t rowsize=8;
  uint32_t pos=0;
  uint32_t map=0;
  uint32_t len=0;


  for(uint32_t hit=0; hit<m_tags.size(); hit++){
    
    //Add a new stream tag at the first bytes

    

    if(nStream){
      bytes[pos/8]=0x80;
      nStream=0;
    }
    //pre-tag
    //if(!isfirst){pos=writeBytes(bytes,pos,0x7,3);}

    //tag
    //Don't post the tag, if the tag is the same in the steam.
    //cout<<"Hit Id "<<hit<<" / "<<m_tags.size()<<" tag: "<<m_tags.at(hit)<<endl;
    if(hit==0 or m_tags.at(hit)!=m_tags.at(hit-1)){
      if(hit==0){
	pos=writeBytes(bytes,pos,m_tags.at(hit),tagsize);
      }else{ //we need to post 11 bit size tag instead of 8 bit where first 3 bits are 1
	//cout<<"Doing 11 bit tag: "<<std::hex<<m_tags.at(hit)<<" "<<( 0x300 +  m_tags.at(hit) )<<std::dec<<std::endl;
	pos=writeBytes(bytes,pos, ( 0x700 +  m_tags.at(hit) ),(tagsize+3));

      }
    }
    //skip if there are no hits
    //if(m_qcols.at(hit)==0) continue;


    if( islastposted==0 ){
      pos=writeBytes(bytes,pos,m_qcols.at(hit),colsize);
      islastposted=1; 
    }

    //calculate islast and isneighbor flags 
    //islast is set if this is the last qrow in the ccol and zero otherwise
    //isneighbor is set if the previous address was qrow-1 and zero otherwise
    if(hit<m_tags.size()-1){
      if(m_qcols.at(hit+1)==m_qcols.at(hit)){islast=0;}else{islast=1;}
  
      if(hit>0 and m_qcols.at(hit+1)==m_qcols.at(hit) and
         m_qrows.at(hit)==m_qrows.at(hit-1)+1){isneighbor=1;}
      else{isneighbor=0;}
      if(m_verbose){
      cout << "hit: " << hit
          << " qcol: " << m_qcols.at(hit)
          << " next-qcol: " << m_qcols.at(hit+1)
          << " qrow: " << m_qrows.at(hit)
          << " next-qrow: " << m_qrows.at(hit+1)
          << " islast: " << islast
          << " isneighbor: " << isneighbor
          << endl;
      }
    }else{
      islast=1;
      isneighbor=0;
    }

    //is-last
    pos=writeBytes(bytes,pos,islast);

    //is-neighbor
    pos=writeBytes(bytes,pos,isneighbor);

    //row
    if(!isneighbor){
    pos=writeBytes(bytes,pos,m_qrows.at(hit),rowsize);
    }
    
    //hit-map
    if(m_enable_enc){
      len=m_hm.Encode(m_hitmaps.at(hit),&map);
      pos=writeBytes(bytes,pos,map>>(30-len),len);
    }else{
      pos=writeBytes(bytes,pos,m_hitmaps.at(hit),16);
    }

    //TOT-map
    if(m_enable_tot){
      unsigned Count=0;
      for(uint32_t i=0;i<16;i++){
	if((m_hitmaps.at(hit)>>i)&0x1){
	  Count++;
	}
      }
      pos=writeBytes(bytes,pos,m_totmaps.at(hit),4*Count);
    }
    if(m_verbose){
    cout << "Add hit: "
            << "tag: " << setw(3) << m_tags.at(hit) << ", "
            << "col: " << setw(3) << m_qcols.at(hit) << ", "
            << "islast: " << islast << ", "
            << "isneighbor: " << isneighbor << ", "
            << "row: " << setw(3) << m_qrows.at(hit) << ", "
            << "hit: 0x" << hex << setw(4) << setfill('0') << m_hitmaps.at(hit) << setfill(' ') << dec << ", "
            << "tot: 0x" << hex << setw(4) << setfill('0') << m_totmaps.at(hit) << setfill(' ') << dec << ", "
            << endl;
    }
    //we are never first again
    if(isfirst){isfirst=false;}
    //if this is last, we post qcolumn in the next stream
    if(islast){islastposted=0;}
  }

  //end of stream
  pos=writeBytes(bytes,pos,0,6); 

  return pos;
}


uint32_t Stream::UnPack(uint8_t * bytes, uint32_t maxlen){
  
  //Clear();
  // auto start = chrono::steady_clock::now();
  bool islast=0;
  bool isneighbor=0;
  uint32_t pre=0;
  uint32_t tag=0;
  uint32_t col=0;
  uint32_t row=0;
  uint32_t hit=0;
  uint32_t tmphit=0;
  uint64_t tot=0;
  uint32_t pos=0;
  uint32_t tpos=0;
  uint32_t tcol=0;
  uint32_t nbits = maxlen*8;
  uint32_t NHits = 0;
  static const uint32_t tagsize=8;
  static const uint32_t colsize=6;
  static const uint32_t rowsize=8;

  pos=readBytes(bytes,pos,tag,tagsize); 
  
  SetStreamTag(tag);

  tpos=readBytes(bytes,pos,tcol,colsize);
  if(tcol<55){ //If it's a legit column
    col=tcol;
    pos=tpos;
  }


  while(pos< (nbits) ){
    
    if(islast){

      //Check if there is 
      uint32_t tcol=0;
      tpos=readBytes(bytes,pos,tcol,colsize);
      if(tcol==0x0){pos=tpos; /*cout << "BMDEBUG: tcol=0x0" << endl;*/ break;} //Collumn == 0 is a sign of end of stream
      if(tcol<55){ //If it's a legit collumn
	//cout << "BMDEBUG: AM IN THIS WEIRD PART WHERE THERE IS NO TAG" << endl;
	col=tcol;
	pos=tpos;
      }
      else { //Else means there is a 11bit tag involvded that needs reconstruction
	pos=readBytes(bytes,pos,pre,3);
	pos=readBytes(bytes,pos,tag,tagsize);  
	//cout << "BMDEBUG2: tag = " << tag << " --- " << ((tag&0xFC)>>2) << " , " <<  (tag&0x3) <<  endl;
	SetStreamTag(tag);
	pos=readBytes(bytes,pos,col,colsize);
      }
    }

    //is-last
    pos=readBytes(bytes,pos,islast);

    //is-neighbor
    pos=readBytes(bytes,pos,isneighbor);

    //row
    if(!isneighbor){
      pos=readBytes(bytes,pos,row,rowsize);
    }else{
      row+=1;
    }

    //hit-map
    if(m_enable_enc){         
      uint32_t map=0;
      uint32_t nbs=0;
      
      //Carlos hitmap
      readBytes(bytes,pos,map,30);
      nbs=m_hm.Decode((map),&hit);
      //cout<<"Decode size: "<<nbs<<" Hit: 0x"<<std::hex<<hit<<std::endl;

      //Simon method for maps
      // readBytes(bytes,pos,map,30);
      // std::pair<uint16_t,uint8_t> hm = untree_30(map);
      // hit = hm.first;
      // nbs = hm.second;


      if((pos%64)==0) pos++;
      if(m_enable_cid && (pos%64)==1) pos++;
      if(m_enable_cid && (pos%64)==2) pos++;
      if((pos%64 + nbs) <= 64)
      	pos += nbs;
      else
      	pos += (nbs + (m_enable_cid?3:1)); 

    }else{
      pos=readBytes(bytes,pos,hit,16);
    }
    

    //New TOT Extraction
    NHits=0;
    tmphit = hit;
    for (uint32_t ind=0;ind<16;ind++){
     	NHits+= (tmphit & 0x1);
	tmphit = tmphit >> 1;
    }
    
    pos=readBytes(bytes,pos,tot,4*NHits);
    //std::cout<<"Pos "<<pos<<"/"<<Nbits<<" NHits "<<NHits<<" tot "<<hex<<tot<<dec<<std::endl;
    if ((col!=0) and !(tot==0 and hit==0x8000)) 
      AddHit(tag,col,row,hit,tot);

  }


  return pos;
}

void Stream::AddHit(uint32_t tag, uint32_t qcol, uint32_t qrow, uint32_t hitmap, uint64_t totmap){

  if(m_verbose)
    cout << "Add hit: "
	 << "tag: " << tag << ", "
	 << "col: " << qcol << ", "
	 << "row: " << qrow << ", "
	 << hex << setfill('0')  
	 << "hit: 0x" << setw(4) << hitmap << ", "
	 << "tot: 0x" << setw(4) << totmap << ", "
	 << dec << endl;

  m_tags.push_back(tag);
  m_qcols.push_back(qcol);
  m_qrows.push_back(qrow);
  m_hitmaps.push_back(hitmap);
  m_totmaps.push_back(totmap);

}

string Stream::GetHitString(uint32_t hit){
  ostringstream os;
  os << "Hit: " << hit << ", "
     << "tag: " << GetTag(hit) << ", "
     << "qcol: " << GetQcol(hit) << ", "
     << "qrow: " << GetQrow(hit) << ", "
     << "Hmap: 0x" << hex << GetHitMap(hit) << dec << ", "
     << "TOT: 0x" << hex << GetTotMap(hit) << dec << " ";
  return os.str();
}

uint32_t Stream::GetNhits(){
  return m_tags.size();
}

uint32_t Stream::GetTag(uint32_t hit){
  return m_tags.at(hit);
}

uint32_t Stream::GetQcol(uint32_t hit){
  return m_qcols.at(hit);
}

uint32_t Stream::GetQrow(uint32_t hit){
  return m_qrows.at(hit);
}

uint32_t Stream::GetHitMap(uint32_t hit){
  return m_hitmaps.at(hit);
}

uint64_t Stream::GetTotMap(uint32_t hit){
  return m_totmaps.at(hit);
}
