#include "RD53BEmulator/Handler.h"
#include "RD53BEmulator/RunNumber.h"
#include "RD53BEmulator/Tools.h"
#include <json.hpp>
#include <iostream>
#include <fstream>
#include <mutex>
#include <chrono>
#include <sstream>
#include <iomanip>
#include "TFile.h"
#include "TROOT.h"

#define STOREDATA
#define SLOWDOWN 100


using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int64_t, std::uint64_t, float>;
using namespace std;
using namespace RD53B;

Handler::Handler(){
  //gROOT->SetBatch(true); //This is a must otherwise runs hang
  string itkpath = (getenv("ITK_PATH")?string(getenv("ITK_PATH")):".");
  m_config_path.push_back("./tuned/");
  m_config_path.push_back("");
  // m_config_path.push_back("");
  m_config_path.push_back("./");
  // m_config_path.push_back(itkpath+"/");
  // m_config_path.push_back(itkpath+"/tuned/");
  // m_config_path.push_back(itkpath+"/config/");
  //m_config_path.push_back(itkpath+"/installed/share/data/config/");

#ifdef STOREDATA
  m_outFile.open("RawData.dat");
  m_outFileCMD.open("RawCMD.dat");
#endif

  m_verbose = false;
  m_exit    = false;
  m_backend = "posix";
  m_retune = false;
  m_rootfile = 0;
  m_outpath = (getenv("ITK_DATA_PATH")?string(getenv("ITK_DATA_PATH")):".");
  m_rn = new RunNumber();
  m_output = true;
  m_fullOutPath = "";
  m_masker = new Masker();
  m_scan_fe = DiffFE;
  m_storehits = false;
  m_ChipId_status=0;
}

Handler::~Handler(){
  while(!m_fes.empty()){
    FrontEnd* fe=m_fes.back();
    m_fes.pop_back();
    delete fe;
  }
  m_fes.clear();
  if(m_rootfile) delete m_rootfile;
#ifdef STOREDATA
  m_outFile.close();
  m_outFileCMD.close();
#endif
  delete m_rn;
  delete m_masker;
}

void Handler::SetVerbose(bool enable){
  m_verbose=enable;
  for(auto fe: m_fes){
    fe->SetVerbose(enable);
  }
}

void Handler::SetContext(string context){
  m_backend = context;
}

void Handler::SetInterface(string interface){
  m_interface = interface;
}

void Handler::SetRetune(bool enable){
  m_retune=enable;
}

void Handler::SetEnableOutput(bool enable){
  m_output=enable;
}

bool Handler::GetEnableOutput(){
  return m_output;
}

void Handler::SetCharge(uint32_t charge){
  m_charge=charge;
}

uint32_t Handler::GetCharge(){
  return m_charge;
}

void Handler::SetMaskOpt(int maskopt){
  m_maskopt=maskopt;
 }

void Handler::SetBusPath(std::string buspath){
  m_buspath=buspath;
 }

std::string Handler::GetBusPath(){
  return m_buspath;
}

uint32_t Handler::GetMaskOpt(){
  return m_maskopt;
}

uint32_t Handler::GetToT(){
  return m_ToT;
}

void Handler::SetToT(uint32_t ToT){
  m_ToT=ToT;
}

void Handler::SetScan(string scan){
  m_scan=scan;
}

void Handler::SetScanFE(string scan_fe){
  m_scan_fe=0;
  if(scan_fe.find("left")!=string::npos){m_scan_fe=FrontEndTypes::DiffLeftFE;}
  if(scan_fe.find("right")!=string::npos){m_scan_fe=FrontEndTypes::DiffRightFE;}
  if(scan_fe.find("mid")!=string::npos){m_scan_fe=FrontEndTypes::DiffMidFE;}
  if(scan_fe.find("all")!=string::npos){m_scan_fe=FrontEndTypes::DiffFE;}
}

void Handler::StoreHits(bool enable){
  m_storehits = enable;
}

uint32_t Handler::GetThreshold(){
  return m_threshold;
}

void Handler::SetThreshold(uint32_t threshold){
  m_threshold=threshold;
}

void Handler::SetLatency(uint32_t latency){
  m_latency = latency;
}

uint32_t Handler::GetLatency(){
  return m_latency;
}

void Handler::SetOutPath(string path){
  m_outpath=path;
}

std::string Handler::GetOutPath() {
  return m_outpath;
}

std::string Handler::GetFullOutPath(){
  return m_fullOutPath;
}

void Handler::SetCommandLine(string commandLine){
  m_commandLine = commandLine;
}

void Handler::InitRun(){
  cout << "Handler::InitRun" << endl;

  //Start a new run
  cout << "Starting run number: " << m_rn->GetNextRunNumberString(6,true) << endl;

  //Create the output directory
  if(m_verbose) cout << "Handler::InitRun Creating output directory (if doesn't exist yet)" << endl;
  ostringstream cmd;
  
  string tmpPath= m_outpath + "/" + m_rn->GetRunNumberString(6) + "_" + m_scan;
  // This is not necessary, but will keep it as it is. 
  //if ( (m_scan_fe & DiffFE) ) tmpPath+="Diff";

  cmd << "mkdir -p " << tmpPath; 
  system(cmd.str().c_str());
  m_fullOutPath = tmpPath;

  //Open the output log file
  ostringstream logFilePath;
  logFilePath << tmpPath << "/" << "logFile.txt";
  
  m_logFile.open(logFilePath.str().c_str());
  if(m_verbose) cout << "Handler::InitRun Created log file " << logFilePath.str().c_str() << endl;
  m_logFile << "\"exec\": \"" << m_commandLine << "\"" << endl;
  m_logFile << "\"runNumber\": " << m_rn->GetRunNumberString(6) << endl;
  time_t startTime;
  time(&startTime);
  m_logFile << "\"startTime\": " << ctime(&startTime) << endl;

  //Copy the connectivity file to the log!!!! FIXME
  std::string line;
  std::ifstream connectivityFile(m_connectivityPath);
  while(std::getline(connectivityFile, line)){
    m_logFile << line << endl;
  }
  connectivityFile.close();

  //give up if somebody disabled the output
  if(!m_output) return;

  //Open the output ROOT file
  ostringstream opath;
  opath << tmpPath << "/" << "output.root";
  //opath << m_outpath << "/" << m_rn->GetRunNumberString(6) << "_" << m_scan << "/" << "output.root";
  if(m_verbose) cout << "Handler::InitRun Creating root file " << opath.str().c_str() << endl;
  m_rootfile=TFile::Open(opath.str().c_str(),"RECREATE");
  if(m_verbose) cout << "Handler::InitRun Created root file " << opath.str().c_str() << endl;

}

void Handler::SetMapping(string mapping, bool auto_load){

  for(string const& sdir : m_config_path){
   if(m_verbose) cout << "Handler::SetMapping" << " Testing: " << sdir << mapping << endl;
   m_connectivityPath = sdir+mapping;
   std::ifstream fr(m_connectivityPath);
   if(!fr){continue;}
   cout << "Handler::SetMapping" << " Loading: " << m_connectivityPath << endl;
   json config;
   fr >> config;
   for(auto node : config["connectivity"]){
     AddMapping(node["name"],node["config"],node["tx"],node["rx"],node["host"],node["cmd_port"],node["host"],node["data_port"]);
     if(auto_load and node["enable"]!=0){
       cout << "Handler::SetMapping Adding front end " << node["name"] << endl;
       AddFE(node["name"]);
       if(!node["dcs_node"].is_null()){
	 GetFE(node["name"])->SetDcsNode(node["dcs_node"]);}
     }
       else if(auto_load){
       cout << "Handler::SetMapping Front end is disabled: " << node["name"] << endl;
     }
   }
   fr.close();
   break;
  }

}

FrontEnd* Handler::GetFE(string name){
  return m_fe[name];
}

uint64_t Handler::GetFEDataElink(string name){
  return m_fe_rx[name];
}

uint64_t Handler::GetFECmdElink(string name){
  return m_fe_tx[name];
}

vector<FrontEnd*> Handler::GetFEs(){
  return m_fes;
}

bool Handler::GetRetune(){
  return m_retune;
}

void Handler::AddMapping(string name, string config, uint64_t cmd_elink, uint64_t data_elink, string cmd_host, uint32_t cmd_port, string data_host, uint32_t data_port){
  if(m_verbose){
    cout << "Handler::AddMapping Front-End: "
         << "name: " << name << ", "
         << "config: " << config << ", "
         << "cmd_host: " << cmd_host << ", "
         << "cmd_port: " << cmd_port << ", "
         << "cmd_elink: " << cmd_elink << ", "
         << "data_host: " << data_host << ", "
         << "data_port: " << data_port << ", "
         << "data_elink: " << data_elink << " "
         << endl;
  }
  m_fe[name]=0;
  m_enabled[name]=false;
  m_configs[name]=config;
  m_fe_rx[name]=data_elink;
  m_fe_tx[name]=cmd_elink;
  m_data_port[data_elink]=data_port;
  m_data_port[data_elink+1]=data_port; //Service Frame data_port
  m_data_host[data_elink]=data_host;
  m_data_host[data_elink+1]=data_host; //Service Frrame data_host
  m_cmd_host[cmd_elink]=cmd_host;
  m_cmd_port[cmd_elink]=cmd_port;
}

void Handler::AddFE(string name, FrontEnd* fe){

  fe->SetName(name);
  fe->SetActive(true);
  m_fe[name]=fe;
  m_fes.push_back(fe);
  m_enabled[name]=true;
}

void Handler::AddFE(string name, string path){

  if(path==""){path=m_configs[name];}
  if(path==""){path=name+".json";}

  FrontEnd * fe = 0;
  for(string const& sdir : m_config_path){
   if(m_verbose) cout << "Handler::AddFE" << " Testing: " << sdir << path << endl;
   std::ifstream fr(sdir+path);
   if(!fr){
     //if(m_verbose) cout << "Handler::AddFE" << " File not found: " << sdir << path << endl;
     continue;
   }
   cout << "Handler::AddFE" << " Loading: " << sdir << path << endl;
   json config;
   fr >> config;
   //Create the front-end
   if(m_verbose) cout << "Handler::AddFE" << " Create front-end" << endl;
   fe = new FrontEnd();
   fe->SetVerbose(m_verbose);
   fe->SetName(name);

   if(m_verbose) cout << "Handler::AddFE" << " Reading configuration file" << endl;
   
   //Actually read the file
   fe->SetChipID(config["RD53B"]["Parameter"]["ChipId"]);
   fe->SetActive(true); 

   if(m_verbose) cout << "Handler::AddFE" << " Reading global registers" << endl;
   fe->SetGlobalConfig(config["RD53B"]["GlobalConfig"]);

   if(m_verbose) cout << "Handler::AddFE" << " Reading pixel bits" << endl;

   bool EnChipID = fe->GetConfig()->GetField(Configuration::EnChnId)->GetValue(); 
   bool EnDataMerge = fe->GetConfig()->GetField(Configuration::DataMergeEn)->GetValue();
   if (m_ChipId_status==0){
     m_ChipId_status = EnChipID?1:-1;
   }
   else if ( m_ChipId_status==1 && !EnChipID){
     m_ChipId_status = -1;
   }


   if (m_ChipId_status==-1 && EnDataMerge ){
     std::cout<<"Handler::AddFE  Error!!! Chip ID status must be enabled for all FE's to use the data merging mode, either enable Chip id for all FE's or disable data-merging in all FE's"<<std::endl;
     exit(-1);
   }
   
   

   for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
     for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
       // EJS 2020-10-27, json.hpp has problems dealing with booleans, had to workaround it like this
       
       if(m_maskopt==1){ // Refresh the existing mask
       	 fe->SetPixelMask(col, row,  false);
       	 fe->SetPixelEnable(col,row, true );
       }
       else{ //Use the loaded mask
	 fe->SetPixelMask(col, row,  (config["RD53B"]["PixelConfig"][col]["Enable"][row] == 0?true:false));
	 fe->SetPixelEnable(col,row, (config["RD53B"]["PixelConfig"][col]["Enable"][row] == 0?false:true));
       }
       fe->SetPixelHitbus(col,row, (config["RD53B"]["PixelConfig"][col]["Hitbus"][row] == 0?false:true));
       //fe->SetPixelHitbus(col,row, (config["RD53B"]["PixelConfig"][col]["Hitbus"][row] == 0?false:true));
       fe->SetPixelInject(col,row, (config["RD53B"]["PixelConfig"][col]["InjEn"][row] == 0?false:true));
       fe->SetPixelThreshold(col,row, config["RD53B"]["PixelConfig"][col]["TDAC"][row]);
     }
   }

   //Force the global registers are compatible with FELIX
   //fe->GetConfig()->SetField(Configuration::SER_CLK,0);

   m_fe[name]=fe;
   m_fes.push_back(fe);
   m_enabled[name]=true;

   if(m_fe_rx.count(name)==0 or m_fe_tx.count(name)==0){
     cout << "Handler::AddFE Configuration error. Connectivity file does not contains FE: " << name << endl;
     m_enabled[name]=false;
   }

   //File was found, no need to keep looking for it
   break;
  }
  if(!fe){cout << "Handler::AddFE Front-end not added. File not found: " << path << endl;}
  else{
    if(m_verbose) cout << "Handler::AddFE File correctly loaded: " << path << endl;
  }
}

void Handler::AddFEbyString(string name, string config_str){
  cout << "Handler::AddFEbyString" << " Json config given as string --> Parsing..."<< endl;
  
  json config;
  std::stringstream(config_str) >> config;

  //Create the front-end
  if(m_verbose) cout << "Handler::AddFE" << " Create front-end" << endl;
  FrontEnd * fe = new FrontEnd();
  fe->SetVerbose(m_verbose);
  fe->SetName(name);

  if(m_verbose) cout << "Handler::AddFE" << " Reading configuration file" << endl;

  //Actually read the file
  fe->SetChipID(config["RD53B"]["Parameter"]["ChipId"]);
  fe->SetActive(true);

  if(m_verbose) cout << "Handler::AddFE" << " Reading global registers" << endl;
  fe->SetGlobalConfig(config["RD53B"]["GlobalConfig"]);

  if(m_verbose) cout << "Handler::AddFE" << " Reading pixel bits" << endl;

  for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
    for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
      // EJS 2020-10-27, json.hpp has problems dealing with booleans, had to workaround it like this
      fe->SetPixelMask(col, row,  (config["RD53B"]["PixelConfig"][col]["Enable"][row] == 0?false:true));
      fe->SetPixelEnable(col,row, (config["RD53B"]["PixelConfig"][col]["Enable"][row] == 0?false:true));
      fe->SetPixelHitbus(col,row, (config["RD53B"]["PixelConfig"][col]["Hitbus"][row] == 0?false:true));
      fe->SetPixelInject(col,row, (config["RD53B"]["PixelConfig"][col]["InjEn"][row] == 0?false:true));
      fe->SetPixelThreshold(col,row, config["RD53B"]["PixelConfig"][col]["TDAC"][row]);
    }
  }

  m_fe[name]=fe;
  m_fes.push_back(fe);
  m_enabled[name]=true;

  if(m_fe_rx.count(name)==0 or m_fe_tx.count(name)==0){
    cout << "Handler::AddFE Configuration error. Connectivity file does not contains FE: " << name << endl;
    m_enabled[name]=false;
  }
}

void Handler::SaveFE(FrontEnd * fe, string path){

  cout << "Handler::SaveFE " << fe->GetName() << " in: " << path << endl;
  json config;
  config["RD53B"]["name"]=fe->GetName();
  config["RD53B"]["Parameter"]["ChipId"]=fe->GetChipID();
  config["RD53B"]["GlobalConfig"]=fe->GetGlobalConfig();

  for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
    config["RD53B"]["PixelConfig"][col]["Col"]=col;
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      config["RD53B"]["PixelConfig"][col]["Enable"][row]=(uint32_t)fe->GetPixelEnable(col,row);
      config["RD53B"]["PixelConfig"][col]["Hitbus"][row]=(uint32_t)fe->GetPixelHitbus(col,row);
      config["RD53B"]["PixelConfig"][col]["InjEn"][row]=(uint32_t)fe->GetPixelInject(col,row);
      config["RD53B"]["PixelConfig"][col]["TDAC"][row]=fe->GetPixelThreshold(col,row);
    }
  }

  ofstream fw(path);
  fw << setw(4) << config;
  fw.close();

}

void Handler::Connect(){

  bool DataMerging=m_ChipId_status==1?1:0;
  std::cout<<"DataMerging "<<DataMerging<<std::endl;
  //Connect to FELIX
  cout << "Handler::Connect Create the context" << endl;

  std::vector<uint64_t> vec_elinks;

  //TX
  for(auto it : m_fe_tx){
    if(m_enabled[it.first]==false){continue;}
    uint64_t tx_elink = it.second;
    if(std::find(vec_elinks.begin(), vec_elinks.end(), tx_elink) == vec_elinks.end()){
      vec_elinks.push_back(tx_elink);
    }
    m_tx_fes[tx_elink].push_back(m_fe[it.first]);
  }
  std::cout<<"Vec_elinks size:"<<vec_elinks.size()<<endl;;
  for(auto it: vec_elinks){
    std::cout<<"Elinks: 0x"<<std::hex<<it<<std::dec<<endl;
  }



  for(auto it : m_fe_rx){
    if(m_enabled[it.first]==false){continue;}
    uint64_t rx_elink = it.second;
    
    if(std::find(vec_elinks.begin(), vec_elinks.end(), rx_elink) == vec_elinks.end()){
      vec_elinks.push_back(rx_elink);
      #ifdef ITKFELIXSTAR
      vec_elinks.push_back(rx_elink+(0x10000)); //Service frames
      #endif
    }

    if (DataMerging){
      if(m_verbose) std::cout<<"DataMerging, adding ChipID: "<<int((m_fe[it.first]->GetChipID() & 0x3))<<endl;
      m_rx_fes[rx_elink][ (m_fe[it.first]->GetChipID() & 0x3)  ] = m_fe[it.first];
      #ifdef ITKFELIXSTAR
      m_rx_fes[rx_elink+(0x10000)][ (m_fe[it.first]->GetChipID() & 0x3)  ] = m_fe[it.first];
      #endif
    } else{
      m_rx_fes[rx_elink][0] = m_fe[it.first];
      #ifdef ITKFELIXSTAR
      m_rx_fes[rx_elink+(0x10000)][0] = m_fe[it.first];
      #endif
    }
    //The service frames comes with e_linkID + 1, so we subscribe to the +1 socket as well. 
    
  }


  //All Possible setting
  // LOCAL_IP_OR_INTERFACE = "local_ip_or_interface"
  // LOG_LEVEL = "log_level"
  // BUS_INTERFACE = "bus_interface"
  // BUS_DIR = "bus_dir"
  // BUS_GROUP_NAME = "bus_group_name"
  // VERBOSE_BUS = "verbose_bus"
  // VERBOSE_ZYRE = "verbose_zyre"
  // TIMEOUT = "timeout"
  // NETIO_PAGES = "netio_pages"
  // NETIO_PAGESIZE = "netio_pagesize"
  // THREAD_AFFINITY = "thread_affinity"

  std::cout<<"Handler::Connect Interface to be used is:"<<m_interface<<endl;
  std::cout<<"Handler::Connect bus dir   to be used is:"<<m_buspath<<endl; 

  m_NextConfig = new felix_proxy::ClientThread::Config();
  m_NextConfig->property["local_ip_or_interface"] = m_interface;//"127.0.0.1";
  
  if(m_verbose) // Error level is defined by verbosity level
    m_NextConfig->property["log_level"] = "trace";
  else
     m_NextConfig->property["log_level"] = "error";

  m_NextConfig->property["bus_dir"] = m_buspath;
  m_NextConfig->property["bus_group_name"] = "FELIX";
  // m_NextConfig.property["verbose_bus"] = "True";
  // m_NextConfig.property["netio_pages"] = "256";
  // m_NextConfig.property["timeout"] = "2";
  // m_NextConfig.property["netio_pagesize"] = "";
  // m_NextConfig.property["thread_affinity"] = "";

  

  m_NextConfig->on_init_callback = []() {
    std::cout<<"Handler::ClientThread::Initialiasing connection"<<endl;
  };

  m_NextConfig->on_connect_callback = [](const std::uint64_t fid){
    std::cout<<"Handler::ClientThread::Connecting to FID: 0x"<<std::hex<<fid<<std::dec<<endl;

  };

  m_NextConfig->on_disconnect_callback = [](const std::uint64_t fid){
    std::cout<<"Handler::ClientThread::Disconnecting to FID: 0x"<<std::hex<<fid<<std::dec<<endl;

  };




  //Shouldn't handle service frames yet
  m_NextConfig->on_data_callback =  [&,DataMerging](const std::uint64_t rx_elink,
						   const std::uint8_t* data,
						   const std::size_t size,
						   const std::uint8_t status){
    

    if(m_rx_fes.find(rx_elink) != m_rx_fes.end()){
      if(m_verbose) cout << "Handler::Connect Received data from " << rx_elink << " size:" << size << endl;
      bool IsServiceFrame = (rx_elink & 0x10000);
      	  
      if(size%8!=0) {

	cout<< "Non 64-bit data packet arrived. Something is broken, aborting the run packet size was "<<int(size)<<endl;

	return;// 0;
      }

      

#ifdef STOREDATA
      
	  if(IsServiceFrame==0){
	    //cout<<"!!!!!!!!!!!!!!!!!!! data frame is here !!!!!!!!!!!!"<<endl;
	    for(uint32_t BInd=0; BInd<size; BInd+=1){
               m_outFile<<data[BInd];
	    }
	   
	  }
#endif




      // We need to have switch this enable disables this and we need the correct elink mapping 
      if (DataMerging){
	for(uint32_t BInd=0; BInd<size; BInd+=8){
	      

	  uint8_t cid = ((data[BInd+3] >> 5) & 0x3); //We read from the 4 Byte because every 32 bytes is inverted
	  if(IsServiceFrame) //For service frames chip ID is stroed later so we shift by a byte
	    cid = ((data[BInd+2] >> 6) & 0x3); //We read from the 4 Byte because every 32 bytes is inverted
	  //std::cout<<"CID: "<<int(cid)   <<endl;
	  if(m_verbose)  std::cout<<"CID recovered here is: "<<int(cid)<<"from data "<<int(data[BInd+3])<<endl;
	  if(m_rx_fes[rx_elink].find(cid) != m_rx_fes[rx_elink].end()){
	    m_mutex[rx_elink][cid].lock();
	    m_rx_fes[rx_elink][cid]->HandleData(&data[BInd],8, IsServiceFrame); 
	    m_mutex[rx_elink][cid].unlock(); 
	  }
	  else{
	    if(m_verbose){
	      std::cout<<"Handler::Connect: ERROR!CID recovered here is broken CID: "<<int(cid)<<endl;
	      std::cout<<"Byte Stream for Broken Was"<<int(data[BInd+3])<<" "<<int(data[BInd+2])<<" "<<int(data[BInd+1])<<" "<<int(data[BInd])<<" "<<int(data[BInd+7])<<" "<<int(data[BInd+6])<<" "<<int(data[BInd+5])<<" "<<int(data[BInd+4])<<endl;
	    }
	  }

	}
      }
      else{
	m_mutex[rx_elink][0].lock();
	m_rx_fes[rx_elink][0]->HandleData(&data[0],size, IsServiceFrame); 
	m_mutex[rx_elink][0].unlock();
      } 
    }
    else{
      cout<<"Handler::data_callback::ERROR! RX_Elink: "<<rx_elink<<" not defined among channels listened"<<endl;
    }
  
  };

  m_NextClient = new felix_proxy::ClientThread(*m_NextConfig);
  // m_NextClient_thread = thread([&](){m_NextClient->event_loop()->run_forever();});

  m_NextClient->subscribe(vec_elinks);
}


void Handler::Config(){

  cout<< "Handler::Config" << endl;

  for(auto fe : m_fes){


    //Reset the front-end in steps
    for (int step = 0; step <= 3; step++){
      fe->Reset(step);
      Send(fe);
      #ifdef SLOWDOWN
      std::this_thread::sleep_for(std::chrono::microseconds(SLOWDOWN));
      #endif
    }

    for (unsigned i=0; i<16; i++) {
      fe->GetConfig()->SetField(Configuration::EnCoreCol_0,1<<i);
      fe->GetConfig()->SetField(Configuration::EnCoreCol_1,1<<i);
      fe->GetConfig()->SetField(Configuration::EnCoreCol_2,1<<i);
      fe->GetConfig()->SetField(Configuration::EnCoreCol_3,1<<i);
      fe->GetConfig()->SetField(Configuration::EnCoreColReset_0,1<<i);
      fe->GetConfig()->SetField(Configuration::EnCoreColReset_1,1<<i);
      fe->GetConfig()->SetField(Configuration::EnCoreColReset_2,1<<i);
      fe->GetConfig()->SetField(Configuration::EnCoreColReset_3,1<<i);


      fe->WriteRegister(Configuration::ADDR_EnCoreColReset_0);
      fe->WriteRegister(Configuration::ADDR_EnCoreColReset_1);
      fe->WriteRegister(Configuration::ADDR_EnCoreColReset_2);
      fe->WriteRegister(Configuration::ADDR_EnCoreColReset_3);
      fe->WriteRegister(Configuration::ADDR_EnCoreCol_0);
      fe->WriteRegister(Configuration::ADDR_EnCoreCol_1);
      fe->WriteRegister(Configuration::ADDR_EnCoreCol_2);
      fe->WriteRegister(Configuration::ADDR_EnCoreCol_3);

      Send(fe);

      //std::this_thread::sleep_for(std::chrono::milliseconds(1));

      fe->AddClear();
      Send(fe);
      #ifdef SLOWDOWN
      std::this_thread::sleep_for(std::chrono::microseconds(SLOWDOWN));
      #endif
    }

    fe->GetConfig()->SetField(Configuration::EnCoreCol_0,0xffff);
    fe->GetConfig()->SetField(Configuration::EnCoreCol_1,0xffff);
    fe->GetConfig()->SetField(Configuration::EnCoreCol_2,0xffff);
    fe->GetConfig()->SetField(Configuration::EnCoreCol_3,0x3f);
    // fe->GetConfig()->SetField(Configuration::EnCoreCol_0,0xffff);
    // fe->GetConfig()->SetField(Configuration::EnCoreCol_1,0xffff);
    // fe->GetConfig()->SetField(Configuration::EnCoreCol_2,0xffff);
    // fe->GetConfig()->SetField(Configuration::EnCoreCol_3,0x3f);

    fe->GetConfig()->SetField(Configuration::EnCoreColReset_0,0);
    fe->GetConfig()->SetField(Configuration::EnCoreColReset_1,0);
    fe->GetConfig()->SetField(Configuration::EnCoreColReset_2,0);
    fe->GetConfig()->SetField(Configuration::EnCoreColReset_3,0);


    fe->WriteRegister(Configuration::ADDR_EnCoreColReset_0);
    fe->WriteRegister(Configuration::ADDR_EnCoreColReset_1);
    fe->WriteRegister(Configuration::ADDR_EnCoreColReset_2);
    fe->WriteRegister(Configuration::ADDR_EnCoreColReset_3);
    fe->WriteRegister(Configuration::ADDR_EnCoreCol_0);
    fe->WriteRegister(Configuration::ADDR_EnCoreCol_1);
    fe->WriteRegister(Configuration::ADDR_EnCoreCol_2);
    fe->WriteRegister(Configuration::ADDR_EnCoreCol_3);

    Send(fe);
    //std::this_thread::sleep_for(std::chrono::milliseconds(1));

    fe->GetConfig()->SetField(Configuration::EnCoreCol_0,0x0);
    fe->GetConfig()->SetField(Configuration::EnCoreCol_1,0x0);
    fe->GetConfig()->SetField(Configuration::EnCoreCol_2,0x0);
    fe->GetConfig()->SetField(Configuration::EnCoreCol_3,0x0);
    Send(fe);

    
    fe->AddClear();
    Send(fe);
    #ifdef SLOWDOWN
    std::this_thread::sleep_for(std::chrono::microseconds(SLOWDOWN));
    #endif

    uint32_t ThrDiff = fe->GetGlobalThreshold(Pixel::Diff);
    uint32_t ThrDiffR = fe->GetGlobalThreshold(Pixel::DiffRight);
    uint32_t ThrDiffL = fe->GetGlobalThreshold(Pixel::DiffLeft);

    cout<<"Getting vlaue here: "<<ThrDiff<<" "<<ThrDiff<<" "<<ThrDiffR<<" "<<ThrDiffL<<endl;

    //Gloabal Threshold needs to be set high before configuring. 
    fe->SetGlobalThreshold(Pixel::Diff,500);
    fe->SetGlobalThreshold(Pixel::DiffRight,500);
    fe->SetGlobalThreshold(Pixel::DiffLeft,500);
    fe->WriteRegister(Configuration::ADDR_DAC_TH1_L_DIFF);
    fe->WriteRegister(Configuration::ADDR_DAC_TH1_R_DIFF);
    fe->WriteRegister(Configuration::ADDR_DAC_TH1_M_DIFF);
    fe->WriteRegister(Configuration::ADDR_DAC_TH2_DIFF);
    Send(fe);
    #ifdef SLOWDOWN
    std::this_thread::sleep_for(std::chrono::microseconds(SLOWDOWN));
    #endif


    cout << "Global configuration: " << fe->GetName() << endl;
    //Write the global registers one by one
    for(uint32_t address = Configuration::REGISTER0; address < Configuration::NUM_REGISTERS; address++){
      //if (address >= 119 && address <= 137) continue;
      

      //We don't write the Diff thresholds till the end, and we write them at the end. 
      if (address>=Configuration::ADDR_DAC_TH1_L_DIFF && address<= Configuration::ADDR_DAC_TH2_DIFF)
	continue;
	
      fe->WriteRegister(address);
      Send(fe);
      fe->GetConfig()->GetRegister(address)->Update(false); // Since the registers are written, we can set the register to updated
      //Add this line to slow down the configuration
      //std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }  
    //std::this_thread::sleep_for(std::chrono::milliseconds(10));

    
    /*
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    cout << "Pixel configuration: " << fe->GetName() << endl;
    //configure pixels in pairs
    for(uint32_t dcol=0;dcol<Matrix::NUM_DOUBLE_COLS;dcol++){
      for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
        fe->WritePixelPair(dcol,row);
        if((row+1)%20==0){
	  Send(fe);
	  //std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
      }
    }
    Send(fe);
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
    */

    // for(uint32_t address = Configuration::ADDR_DAC_TH1_L_DIFF; address <= Configuration::ADDR_DAC_TH2_DIFF; address++){
    //   fe->WriteRegister(address);
    //   Send(fe);
    //   fe->GetConfig()->GetRegister(address)->Update(false); // Since the registers are written, we can set the register to updated
    //   //Add this line to slow down the configuration
    //   std::this_thread::sleep_for(std::chrono::milliseconds(10));
    // }
 
    fe->SetGlobalThreshold(Pixel::Diff,ThrDiff);
    fe->SetGlobalThreshold(Pixel::DiffRight,ThrDiffR);
    fe->SetGlobalThreshold(Pixel::DiffLeft,ThrDiffL);
    fe->WriteGlobal();
    Send(fe);


  }

}

void Handler::ConfigurePixels(){
  cout<< "Handler::ConfigurePixels" << endl;
  for(auto fe : m_fes){
    ConfigurePixels(fe);
  }
}

void Handler::ConfigurePixels(FrontEnd * fe){
  cout << "Handler::ConfigurePixels " << fe->GetName() << endl;
  //configure pixels in pairs 
  for(uint32_t dcol=0;dcol<Matrix::NUM_DOUBLE_COLS;dcol++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      fe->WritePixelPair(dcol,row);
      if((row+1)%50==0) {
	Send(fe);
	#ifdef SLOWDOWN
	std::this_thread::sleep_for(std::chrono::microseconds(SLOWDOWN));
        #endif 
      }
    }
    Send(fe);
  }
}

void Handler::PrepareTrigger(uint32_t cal_delay, uint32_t triggerMultiplier, bool bc1, bool bc2, bool bc3, bool bc4, uint32_t PulseInj){
  cout << "Handler::PrepareTrigger: Preparing trigger with delay " << cal_delay << endl;
  for(auto it : m_tx_fes){
    cout << "Handler::PrepareTrigger: Composing trigger message for tx " << it.first << endl;
    FrontEnd * fe = it.second.at(0);
    
    fe->Trigger(cal_delay, triggerMultiplier, bc1, bc2, bc3, bc4, PulseInj);
    fe->Encode();
    m_trigger_msgs[it.first]=vector<uint8_t>();
    for(uint32_t i=0;i<fe->GetLength();i++){m_trigger_msgs[it.first].push_back(fe->GetBytes()[i]);}
    fe->Clear();
  }
}

void Handler::PrepareTrigger(uint32_t latency, bool pulse_inj, vector<uint32_t> trigger){

  Encoder encoder;
  encoder.AddCommand(new RD53B::Sync());
  encoder.AddCommand(new RD53B::Sync());
  if(pulse_inj){
    encoder.AddCommand(new RD53B::Cal(16,1,0,20,0,0)); // This is prefersed for digital                                                                                                                           
  }else{
    encoder.AddCommand(new RD53B::Cal(16,0,0,1,0,0));
  }

  for(uint32_t i=0;i<(latency/4-1);i++){
    encoder.AddCommand(new RD53B::PllLock());
  }
  for(uint32_t i=0;i<trigger.size()/4;i++){
    encoder.AddCommand(new RD53B::Trigger(trigger[i], trigger[i+1], trigger[i+2], trigger[i+3], 8+i));
  }
  for(uint32_t i=0;i<8;i++){
    encoder.AddCommand(new RD53B::PllLock());
  }
  if(!pulse_inj){
    encoder.AddCommand(new RD53B::Cal(16,1,0,0,0,0));
  }
  PrepareTrigger(&encoder);
}

void Handler::PrepareTrigger(Encoder * encoder){
  cout << "Handler::PrepareTrigger" << endl;
  for(auto it : m_tx_fes){
    if(m_verbose) cout << "Handler::PrepareTrigger: Composing trigger message for tx " << it.first << endl;
    encoder->Encode();
    m_trigger_msgs[it.first]=vector<uint8_t>();
    for(uint32_t i=0;i<encoder->GetLength();i++){m_trigger_msgs[it.first].push_back(encoder->GetBytes()[i]);}
  }
}

void Handler::Trigger(){

  for(auto it : m_tx_fes){
    
    if(m_verbose) cout << "Handler::Trigger: Trigger! for tx " << it.first << endl;

    m_NextClient->send_data(it.first, m_trigger_msgs[it.first].data(), m_trigger_msgs[it.first].size(), true ); 
    
  }

}

void Handler::Send(FrontEnd * fe){
  //process the commands from the front-end
  fe->Encode();
  //quick return
  if(fe->GetLength()==0){return;}
  if(!m_enabled[fe->GetName()]){return;}
  //figure out the tx_elink
  uint64_t tx_elink=m_fe_tx[fe->GetName()];
  //copy the message
  vector<uint8_t> payload(fe->GetLength());
  for(uint32_t i=0;i<fe->GetLength();i++){payload[i]=fe->GetBytes()[i];};
#ifdef STOREDATA
  for(uint32_t i=0;i<payload.size();i++){m_outFileCMD<<payload[i];}
#endif  
  
  for(unsigned iTry=0;iTry<3;iTry++){
    try{
      m_NextClient->send_data(tx_elink, payload.data(), payload.size(), true ); 
      break;
    }
    catch(int Error){
      std::cout<<"Error caught," <<Error<<" re-trying"<<std::endl;
      std::this_thread::sleep_for(std::chrono::microseconds(1));
      continue;
    }
    
  }


  fe->Clear();

}

void Handler::Disconnect(){
  
}

void Handler::PreRun(){}

void Handler::SaveConfig(std::string configuration){
  if(!m_output) return;

  if(configuration=="tuned"){
    system("mkdir -p ./tuned");
  }

  for(auto fe: m_fes){
    if(!fe->IsActive()){continue;}
    ostringstream os;
    if(configuration!="tuned"){
      string path = m_configs[fe->GetName()].substr(m_configs[fe->GetName()].find_last_of("/"),-1);
      os << m_fullOutPath << "/" << path << "_" << configuration;
      //os << m_fullOutPath << "/" << m_configs[fe->GetName()] << "_" << configuration;
    }else{
      string path = m_configs[fe->GetName()].substr(0, m_configs[fe->GetName()].find_last_of("/"));
      if(path != m_configs[fe->GetName()]){
	system(("mkdir -p ./tuned/"+path).c_str());
      }
      os << "./tuned/" << m_configs[fe->GetName()];
    }
    if (m_configs[fe->GetName()].find(".json")==string::npos)
      os<<".json";
    SaveFE(fe,os.str());
  }
}

void Handler::Run(){
  cout << __PRETTY_FUNCTION__ << "FIXME: Handler::Run is supposed to be extended" << endl;
}

void Handler::Analysis(){}

void Handler::Save(){
  if(!m_output) return;


  SaveConfig("tuned");

  time_t endTime;
  time(&endTime);
  m_logFile << "\"endTime\": " << ctime(&endTime) << endl;
  m_logFile.close();
  cout << "Save log file: " << m_fullOutPath + "/logFile.txt" << endl;
  
  cout << "Save ROOT file: " << m_rootfile->GetName() << endl;
  m_rootfile->Close();
    
}

Masker * Handler::GetMasker(){
  return m_masker;
}

void Handler::ConfigMask(bool enable, bool doPrecisionToT){

  /**
   * broadcasting the commands speeds up the scan 33%
   * We need to find a way that speeds up the scan 
   * and keeps the memory representation of the fe
   **/
  Masker::MaskAuraType AuraMask = m_masker->GetMaskAuora();
  for(auto fe : GetFEs()){
    unsigned Counter=0;
    
    for(auto pixel : m_masker->GetAuraPixels()){
      if(m_verbose){cout<< "Handler::ConfigMask Enabling Aura Pixel Col "<<pixel.first<<" Row "<<pixel.second<<endl;}
      Counter++;
      fe->SetPixelEnable(pixel.first, pixel.second,0); //This is always set to false in Cross talk aura
      fe->SetPixelInject(pixel.first, pixel.second,enable);
      fe->SetPixelHitbus(pixel.first, pixel.second,0);//This is always set to false in Cross talk aura
      if(Counter%100==0){
    	fe->ProcessCommands();
    	Send(fe);
	#ifdef SLOWDOWN
	std::this_thread::sleep_for(std::chrono::microseconds(SLOWDOWN));
	#endif 
      }
    }
    
    
    for(auto pixel : m_masker->GetPixels()){
      Counter++;
      if(m_verbose){cout<< "Handler::ConfigMask Enabling Pixel Col "<<pixel.first<<" Row "<<pixel.second<<endl;}

      if (AuraMask==Masker::Cross)
	fe->SetPixelInject(pixel.first,pixel.second,0);
      else
	fe->SetPixelInject(pixel.first,pixel.second,enable);

      fe->SetPixelEnable(pixel.first,pixel.second,enable);
      fe->SetPixelHitbus(pixel.first,pixel.second,enable);

      if(Counter%100==0){
    	fe->ProcessCommands();
    	Send(fe);
        #ifdef SLOWDOWN
	std::this_thread::sleep_for(std::chrono::microseconds(SLOWDOWN));
	#endif 

      }
    }
    fe->ProcessCommands();
    Send(fe);
    #ifdef SLOWDOWN
    std::this_thread::sleep_for(std::chrono::microseconds(SLOWDOWN));
    #endif 
 
    Counter=0;
    for(auto cc : m_masker->GetCoreColumns()){
      Counter++;
      if(m_verbose){cout<< "Handler::ConfigMask Enabling core column: "<<cc<<endl;}
      fe->EnableCoreColumn(cc,enable);
      fe->EnableCalCoreColumn(cc,enable);
      // fe->EnableHitOrCoreColumn(cc,!enable);
      if( doPrecisionToT ) fe->EnablePrecisionToTCoreColumn(cc,enable);
      if(Counter%100==0){
    	fe->ProcessCommands();
    	Send(fe);
        #ifdef SLOWDOWN
	std::this_thread::sleep_for(std::chrono::microseconds(SLOWDOWN));
	#endif 

      }
    }
    fe->ProcessCommands();
    Send(fe);
    #ifdef SLOWDOWN
    std::this_thread::sleep_for(std::chrono::microseconds(SLOWDOWN));
    #endif 
  }

  //std::this_thread::sleep_for(std::chrono::milliseconds(1));

}

void Handler::UnconfigMask(){
  ConfigMask(false);
}


std::string Handler::GetConfig(std::string configuration){
  //if(!m_output) return ;

  json config;
  std::string strConfig;
  std::string name;

  for(auto fe: m_fes){
    if(!fe->IsActive()){continue;}

    name = "RD53B_"+fe->GetName();
    //config["RD53B"]["name"]=fe->GetName() + configuration;
    config[name]["Parameter"]["ChipId"]=fe->GetChipID();
    config[name]["GlobalConfig"]=fe->GetGlobalConfig();

    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
          config[name]["PixelConfig"][col]["Enable"][row]=(uint32_t)fe->GetPixelEnable(col,row);
          config[name]["PixelConfig"][col]["Hitbus"][row]=(uint32_t)fe->GetPixelHitbus(col,row);
          config[name]["PixelConfig"][col]["InjEn"][row]=(uint32_t)fe->GetPixelInject(col,row);
          config[name]["PixelConfig"][col]["TDAC"][row]=fe->GetPixelThreshold(col,row);
        }
      }
      std::string str = config.dump();
      //std::string str;
      //config.dump(str);
      strConfig.append(str);
    }
  return strConfig;
}

std::string Handler::GetResults(){
  if(!m_output) return "";
  return RD53B::Tools::RootToJson(m_rootfile);    
}

float Handler::GetElapsedTime(string frontEndName){
  return m_elapsedTime[frontEndName];
}

void Handler::SetElapsedTime(string frontEndName, float elapsedTime){
  m_elapsedTime[frontEndName] = elapsedTime;
}

void Handler::ResetAllTagCounters(){
  for(auto fe: m_fes){
    if(!fe->IsActive()){continue;}
    fe->ResetTagCounter();
  }
}
