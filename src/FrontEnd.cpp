#include "RD53BEmulator/FrontEnd.h"

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <thread>
#include <math.h> 

using namespace std;
using namespace RD53B;

FrontEnd::FrontEnd(){
  m_decoder = new Decoder();
  m_decoder_reg = new Decoder();
  m_decoder_reg->SetMode(1);
  m_encoder = new Encoder();
  m_config  = new Configuration();
  m_matrix  = new Matrix();
  m_verbose = 1;
  m_chipid = 16;
  m_name = "RD53B";
  m_active=true;
  m_dcs_node = "";
  for(uint32_t i=0;i<4;i++){
    m_ntcs.push_back(new TemperatureSensor());
    m_bjts.push_back(new RadiationSensor());
  }
  m_hit = new Hit();
  m_refBCID = 0;
  m_tagCounter = 0;
}

FrontEnd::~FrontEnd(){
  delete m_decoder;
  delete m_decoder_reg;
  delete m_encoder;
  delete m_config;
  delete m_matrix;
  delete m_hit;
  for(uint32_t i=0;i<4;i++){
    delete m_ntcs[i];
    delete m_bjts[i];
  }
}

void FrontEnd::SetVerbose(bool enable){
  m_verbose = enable;
}

void FrontEnd::SetChipID(uint32_t chipid){
  m_chipid=chipid;
}

uint32_t FrontEnd::GetChipID(){
  return m_chipid;
}

void FrontEnd::SetName(string name){
  m_name=name;
}

string FrontEnd::GetName(){
  return m_name;
}

void FrontEnd::SetDcsNode(string dcs_node){
  m_dcs_node = dcs_node;
}

string FrontEnd::GetDcsNode(){
  return m_dcs_node;
}

Configuration * FrontEnd::GetConfig(){
  return m_config;
}

Matrix * FrontEnd::GetMatrix(){
  return m_matrix;
}

void FrontEnd::BackupConfig(){
  m_config->Backup();
  m_matrix->Backup();
}

void FrontEnd::RestoreBackupConfig(){ 
  m_config->Restore();
  m_matrix->Restore();
}

void FrontEnd::Clear(){
  m_encoder->Clear();
}

void FrontEnd::Reset(uint32_t res){
  // Taking the seqeunce directly from YARR
  if( res == 0){
    for(int c=0;c<128;c++){
      m_encoder->AddCommand(new Sync());
    }
  }
  else if(res == 1) {
    //Write the global pulse route to reset a bunch of things
    uint32_t val = 0xAC75;
    m_encoder->AddCommand(new WrReg(m_chipid, Configuration::ADDR_GCR_DEFAULT_CONFIG, val));
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new PllLock());
    m_config->SetField(Configuration::GCR_DEFAULT_CONFIG,val);
    val = 0x538A;
    m_encoder->AddCommand(new WrReg(m_chipid, Configuration::ADDR_GCR_DEFAULT_CONFIG_B, val));
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new PllLock());
    m_config->SetField(Configuration::GCR_DEFAULT_CONFIG_B,val);
  }
  else if (res == 2) {
    //Send the global pulse to reset what was configured before
    uint32_t val = 0x0FFF;
    m_encoder->AddCommand(new WrReg(m_chipid, Configuration::ADDR_GlobalPulseConf, val));
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new PllLock());

    m_config->SetField(Configuration::GlobalPulseConf,val);
    val = 10;
    m_encoder->AddCommand(new WrReg(m_chipid, Configuration::ADDR_GlobalPulseWidth, val));
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new PllLock());
    m_config->SetField(Configuration::GlobalPulseWidth,val);
  }
  else if (res == 3) {
    //Send a sequence of 32 sync commands
    uint32_t val = 0x0;
    m_encoder->AddCommand(new WrReg(m_chipid, Configuration::ADDR_GlobalPulseConf, val));
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new PllLock());
    m_config->SetField(Configuration::GlobalPulseConf,val);
  }
}

bool FrontEnd::IsActive(){
  return m_active;
}

void FrontEnd::SetActive(bool active){
  m_active=active;
}

void FrontEnd::EnableAll(){
  for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      m_matrix->GetPixel(col,row)->SetEnable(true);
      m_matrix->GetPixel(col,row)->SetInject(true);
      m_matrix->GetPixel(col,row)->SetHitbus(true);
    }
  }
}


void FrontEnd::DisableAll(){
  for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      m_matrix->GetPixel(col,row)->SetEnable(false);
      m_matrix->GetPixel(col,row)->SetInject(false);
      m_matrix->GetPixel(col,row)->SetHitbus(false);
    }
  }
}


uint32_t FrontEnd::GetNPixelsEnabled(){

  uint32_t EnabledPixels=0;
  for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      EnabledPixels+=uint32_t(m_matrix->GetPixel(col,row)->GetEnable());
    }
  }
  return EnabledPixels;

}



uint32_t FrontEnd::GetGlobalThreshold(uint32_t col, uint32_t /*row*/){

  uint32_t ThresholdHigh =0;
  uint32_t ThresholdLow =m_config->GetField(Configuration::DAC_TH2_DIFF)->GetValue();

  if(col>=Matrix::LEFT_COL_START && col <= Matrix::LEFT_COL_END){
    ThresholdHigh =m_config->GetField(Configuration::DAC_TH1_L_DIFF)->GetValue();
  }
  else if(col>=Matrix::LEFT_COL_START && col <= Matrix::LEFT_COL_END){
    ThresholdHigh =m_config->GetField(Configuration::DAC_TH1_R_DIFF)->GetValue();
  }
  else {
    ThresholdHigh =m_config->GetField(Configuration::DAC_TH1_M_DIFF)->GetValue();
  }

  return ThresholdHigh-ThresholdLow;
}

uint32_t FrontEnd::GetGlobalThreshold(uint32_t type){
  uint32_t ThresholdHigh =0;
  uint32_t ThresholdLow =m_config->GetField(Configuration::DAC_TH2_DIFF)->GetValue();

  switch (type){
  case Pixel::DiffLeft:  ThresholdHigh = m_config->GetField(Configuration::DAC_TH1_L_DIFF)->GetValue(); break;
  case Pixel::DiffRight: ThresholdHigh = m_config->GetField(Configuration::DAC_TH1_R_DIFF)->GetValue(); break;
  case Pixel::Diff:      ThresholdHigh = m_config->GetField(Configuration::DAC_TH1_M_DIFF)->GetValue(); break;
  }
  if(ThresholdHigh>=ThresholdLow)
    return ThresholdHigh-ThresholdLow;
  else
    return 0;
}

void FrontEnd::SetGlobalThreshold(uint32_t type, uint32_t threshold){
  
  uint32_t ThresholdLow = m_config->GetField(Configuration::DAC_TH2_DIFF)->GetValue();
  uint32_t ThresholdVal = ThresholdLow + threshold;
  
  switch (type){
  case Pixel::DiffLeft:  return m_config->SetField(Configuration::DAC_TH1_L_DIFF,ThresholdVal); break;
  case Pixel::DiffRight: return m_config->SetField(Configuration::DAC_TH1_R_DIFF,ThresholdVal); break;
  case Pixel::Diff:      return m_config->SetField(Configuration::DAC_TH1_M_DIFF,ThresholdVal); break;
  }
}

bool FrontEnd::GetPixelEnable(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetEnable();
}

void FrontEnd::SetPixelEnable(uint32_t col, uint32_t row, bool enable){
  return m_matrix->GetPixel(col,row)->SetEnable(enable);
  
}

void FrontEnd::ApplyMasking(){
 for (uint32_t col=0;col<Matrix::NUM_COLS;col++){
    for (uint32_t row=0;row<Matrix::NUM_ROWS;row++){
       if ( m_matrix->GetPixel(col,row)->GetMask() ){
	 cout << "Masking: " << col << " , " << row << endl;
	 m_matrix->GetPixel(col, row)->SetEnable(false);
       }
    }
 } 
 return;  
}

bool FrontEnd::GetPixelInject(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetInject();
}

void FrontEnd::SetPixelInject(uint32_t col, uint32_t row, bool enable){
  return m_matrix->GetPixel(col,row)->SetInject(enable);
}

bool FrontEnd::GetPixelHitbus(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetHitbus();
}

void FrontEnd::SetPixelHitbus(uint32_t col, uint32_t row, bool enable){
  return m_matrix->GetPixel(col,row)->SetHitbus(enable);
}

int32_t FrontEnd::GetPixelThreshold(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetThreshold();
}

void FrontEnd::SetPixelThreshold(uint32_t col, uint32_t row, int32_t threshold){
  m_matrix->GetPixel(col,row)->SetThreshold(threshold);
}

void FrontEnd::SetPixelMask(uint32_t col, uint32_t row, uint32_t mask){
  return m_matrix->GetPixel(col,row)->SetMask(mask);
}

uint32_t FrontEnd::GetPixelMask(uint32_t col, uint32_t row){
  return m_matrix->GetPixel(col,row)->GetMask();
}

void FrontEnd::SetGlobalConfig(map<string,uint32_t> config){
  m_config->SetRegisters(config);
  m_decoder->EnableChipId(m_config->GetField(Configuration::EnChnId)->GetValue());
  m_decoder_reg->EnableChipId(m_config->GetField(Configuration::EnChnId)->GetValue());
}

map<string,uint32_t> FrontEnd::GetGlobalConfig(){
  return m_config->GetRegisters();
}

TemperatureSensor * FrontEnd::GetTemperatureSensor(uint32_t index){
  return m_ntcs[index];
}

RadiationSensor * FrontEnd::GetRadiationSensor(uint32_t index){
  return m_bjts[index];
}

void FrontEnd::ProcessCommands(){
  for(uint32_t addr=Configuration::REGISTER0;addr<Configuration::NUM_REGISTERS;addr++){
    if(!m_config->GetRegister(addr)->IsUpdated()){continue;}
    WriteRegister(addr);
    m_config->GetRegister(addr)->Update(false);
  }
  for(uint32_t dcol=0;dcol<Matrix::NUM_DOUBLE_COLS;dcol++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      if(!m_matrix->IsPairUpdated(dcol,row)){continue;}
      WritePixelPair(dcol,row);
      m_matrix->UpdatePair(dcol,row,false);
    }
  }
}

void FrontEnd::WriteGlobal(){
  for(auto addr : m_config->GetUpdatedRegisters()){
    m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegisterValue(addr)));
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new PllLock());
  }
}

void FrontEnd::WriteRegister(uint32_t addr){
  m_encoder->AddCommand(new WrReg(m_chipid,addr,m_config->GetRegisterValue(addr,true)));
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new PllLock());

}

void FrontEnd::ReadRegister(uint32_t addr){
  m_encoder->AddCommand(new RdReg(m_chipid,addr));
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new PllLock());
}


void FrontEnd::ReadGlobal(){
  for(uint32_t addr=Configuration::REGISTER0;addr<=Configuration::NUM_REGISTERS;addr++){
    m_encoder->AddCommand(new RdReg(m_chipid,addr));
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new PllLock());
  }
}

void FrontEnd::WritePixels(){
  for(uint32_t dcol=0;dcol<Matrix::NUM_DOUBLE_COLS;dcol++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      WritePixelPair(dcol,row);
    }
  }
}





void FrontEnd::WritePixelPair(uint32_t double_col, uint32_t row){
  uint32_t value = m_matrix->GetPair(double_col,row);
  if(!m_matrix->IsPairUpdated(double_col,row)) return;
  
  if(m_config->GetRegisterValue(Configuration::ADDR_REGION_COL)!=double_col){
    m_config->SetRegisterValue(Configuration::ADDR_REGION_COL,double_col);
    WriteRegister(Configuration::ADDR_REGION_COL);
  }
  if(m_config->GetRegisterValue(Configuration::ADDR_REGION_ROW)!=row){
    m_config->SetRegisterValue(Configuration::ADDR_REGION_ROW,row);
    WriteRegister(Configuration::ADDR_REGION_ROW);
  }
  m_config->SetRegisterValue(Configuration::ADDR_PIX_PORTAL,value);

  WriteRegister(Configuration::ADDR_PIX_PORTAL);
  m_matrix->UpdatePair(double_col,row,false);
}

void FrontEnd::ReadPixels(){
  for(uint32_t dcol=0;dcol<Matrix::NUM_DOUBLE_COLS;dcol++){
    for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
      ReadPixelPair(dcol,row);
    }
  }
}

void FrontEnd::ReadPixelPair(uint32_t double_col, uint32_t row){

  m_config->SetField(Configuration::REGION_COL,double_col);
  m_config->SetField(Configuration::REGION_ROW,row);
  m_encoder->AddCommand(new WrReg(m_chipid,Configuration::ADDR_REGION_COL,m_config->GetRegisterValue(Configuration::ADDR_REGION_COL,true)));
  m_encoder->AddCommand(new WrReg(m_chipid,Configuration::ADDR_REGION_ROW,m_config->GetRegisterValue(Configuration::ADDR_REGION_ROW,true)));
  m_encoder->AddCommand(new RdReg(m_chipid,Configuration::ADDR_PIX_PORTAL));
}

void FrontEnd::EnableCoreColumn(uint32_t ccol, bool enable){
  uint32_t reg=0;
  uint32_t off=0;
  if     (ccol>= 0 and ccol<=15){ reg=Configuration::EnCoreCol_0; off=00; }
  else if(ccol>=16 and ccol<=31){ reg=Configuration::EnCoreCol_1; off=16; }
  else if(ccol>=32 and ccol<=47){ reg=Configuration::EnCoreCol_2; off=32; }
  else if(ccol>=48 and ccol<=53){ reg=Configuration::EnCoreCol_3; off=48; }
  uint32_t value = m_config->GetField(reg)->GetValue();
  if(enable) value |= (1<<(ccol-off));
  else       value &= ~(1<<(ccol-off));
  m_config->SetField(reg,value);
}

void FrontEnd::EnableCalCoreColumn(uint32_t core_col, bool enable){
  uint32_t reg=0;
  uint32_t off=0;
  if     (core_col>=  0 and core_col<= 15)  { reg=Configuration::EnCoreColCal_0;   off=  0; }
  else if(core_col>=  16 and core_col<= 31) { reg=Configuration::EnCoreColCal_1;   off=  16; }
  else if(core_col>=  32 and core_col<= 47) { reg=Configuration::EnCoreColCal_2;   off=  32; }
  else if(core_col>=  48 and core_col<= 53) { reg=Configuration::EnCoreColCal_3;   off=  48; }
  uint32_t value = m_config->GetField(reg)->GetValue();
  if(enable) value |= (1<<(core_col-off));
  else       value &= ~(1<<(core_col-off));
  m_config->SetField(reg,value);

}


void FrontEnd::EnableHitOrCoreColumn(uint32_t core_col, bool enable){
  uint32_t reg=0;
  uint32_t off=0;

  if     (core_col>=  0 and core_col<= 15)  { reg=Configuration::HITOR_MASK_0;   off=  0; }
  else if(core_col>=  16 and core_col<= 31) { reg=Configuration::HITOR_MASK_1;   off=  16; }
  else if(core_col>=  32 and core_col<= 47) { reg=Configuration::HITOR_MASK_2;   off=  32; }
  else if(core_col>=  48 and core_col<= 53) { reg=Configuration::HITOR_MASK_3;   off=  48; }
  uint32_t value = m_config->GetField(reg)->GetValue();
  if(enable) value |= (1<<(core_col-off));
  else       value &= ~(1<<(core_col-off));
  m_config->SetField(reg,value);

}


void FrontEnd::EnablePrecisionToTCoreColumn(uint32_t core_col, bool enable){
  uint32_t reg=0;
  uint32_t off=0;

  if     (core_col>=  0 and core_col<= 15)  { reg=Configuration::PrecisionToTEnable_0;   off=  0; }
  else if(core_col>=  16 and core_col<= 31) { reg=Configuration::PrecisionToTEnable_1;   off=  16; }
  else if(core_col>=  32 and core_col<= 47) { reg=Configuration::PrecisionToTEnable_2;   off=  32; }
  else if(core_col>=  48 and core_col<= 53) { reg=Configuration::PrecisionToTEnable_3;   off=  48; }
  uint32_t value = m_config->GetField(reg)->GetValue();
  if(enable) value |= (1<<(core_col-off));
  else       value &= ~(1<<(core_col-off));
  m_config->SetField(reg,value);
}




void FrontEnd::EnableCalDoubleColumn(uint32_t double_col, bool enable){
  EnableCalCoreColumn(double_col/4, enable);
}


void FrontEnd::InitAdc(){
  m_config->SetField(Configuration::MonitorPinEn,1);
  WriteGlobal();
}

void FrontEnd::SetMuxToExternalCurrent(bool enable){
  if(enable){
    m_config->SetField(Configuration::MonitorVMuxSel,Configuration::VMUX_PAD_VOLTAGE);
    m_config->SetField(Configuration::MonitorIMuxSel,Configuration::IMUX_NTC_PAD_CURRENT);
  }else{
    m_config->SetField(Configuration::MonitorVMuxSel,Configuration::VMUX_NTC_PAD_VOLTAGE);
    m_config->SetField(Configuration::MonitorIMuxSel,Configuration::IMUX_HIGHZ);
  }
  m_config->SetField(Configuration::MonitorPinEn,1);
  WriteGlobal();
}

void FrontEnd::SetMuxToExternalVoltage(bool enable){
  SetMuxToExternalCurrent(false);
}

void FrontEnd::ResetAdc(){
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_config->SetField(Configuration::GlobalPulseConf,64); //bit6 Reset ADC
  WriteGlobal();
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  
  m_encoder->AddCommand(new Pulse(m_chipid));
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());  
}

void FrontEnd::StartAdc(){
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_config->SetField(Configuration::GlobalPulseConf,4096); //2^12 // Start ADC Conversion
  WriteGlobal();
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Pulse(m_chipid));
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
}

void FrontEnd::ReadSensor(uint32_t pos, bool read_radiation_sensor){
  if (read_radiation_sensor) {
    cout << "THERE IS NO WAY I AM GOING TO IMPLEMENT THIS!!!" << endl;
    return;
  }

  if (pos==0) {        // ntc voltage
    m_config->SetField(Configuration::MonitorVMuxSel,2);
    m_config->SetField(Configuration::MonitorIMuxSel,63);
    m_config->SetField(Configuration::MonitorPinEn,1);
    WriteGlobal();
    return;
  } else if (pos==1) { // ntc current
    m_config->SetField(Configuration::MonitorVMuxSel,1);
    m_config->SetField(Configuration::MonitorIMuxSel,9);
    m_config->SetField(Configuration::MonitorPinEn,1);
    WriteGlobal();
    return;
  }
  
  if (pos==3) {
    m_config->SetField(Configuration::MonitorVMuxSel,2);
    m_config->SetField(Configuration::MonitorIMuxSel,63);
    m_config->SetField(Configuration::MonitorPinEn,1); //2^12 // enable ADC Conversion
    WriteGlobal();
    return ;
  }

  if (pos==6) {
    m_config->SetField(Configuration::MonitorVMuxSel,1);
    m_config->SetField(Configuration::MonitorIMuxSel,9);
    m_config->SetField(Configuration::MonitorPinEn,1); //2^12 // enable ADC Conversion
    WriteGlobal();
    return ;
  }

  if (pos==7) {
    m_config->SetField(Configuration::MonitorVMuxSel,0);
    m_config->SetField(Configuration::MonitorIMuxSel,63);
    
    //m_config->SetField(Configuration::MonitorVMuxSel,1);
    //m_config->SetField(Configuration::MonitorIMuxSel,63);

    m_config->SetField(Configuration::MonitorPinEn,1); //2^12 // enable ADC Conversion
    WriteGlobal();
    return ;
  }


  /*
  // ??? Not essential will be written later ???
  if(pos==0){
    //m_config->SetField(Configuration::SENSOR_BIAS_0,1);
    //m_config->SetField(Configuration::SENSOR_CURRENT_0,1);
    //m_config->SetField(Configuration::SENSOR_ENABLE_0,1);
    if(read_radiation_sensor == true)   m_bjts[pos]->SetPower(true);
    else                                m_ntcs[pos]->SetPower(true);
  }else if(pos==1){
    //m_config->SetField(Configuration::SENSOR_BIAS_1,1);
    //m_config->SetField(Configuration::SENSOR_CURRENT_1,1);
    //m_config->SetField(Configuration::SENSOR_ENABLE_1,1);
    if(read_radiation_sensor == true)   m_bjts[pos]->SetPower(true);
    else                                m_ntcs[pos]->SetPower(true);
  }else if(pos==2){
    //m_config->SetField(Configuration::SENSOR_BIAS_2,1);
    //m_config->SetField(Configuration::SENSOR_CURRENT_2,1);
    //m_config->SetField(Configuration::SENSOR_ENABLE_2,1);
    if(read_radiation_sensor == true)   m_bjts[pos]->SetPower(true);
    else                                m_ntcs[pos]->SetPower(true);
  }else if(pos==3){
    //m_config->SetField(Configuration::SENSOR_BIAS_3,1);
    //m_config->SetField(Configuration::SENSOR_CURRENT_3,1);
    //m_config->SetField(Configuration::SENSOR_ENABLE_3,1);
    if(read_radiation_sensor == true)   m_bjts[pos]->SetPower(true);
    else                                m_ntcs[pos]->SetPower(true);
  }
  */  

  /*
  if(pos==0){
    if(read_radiation_sensor == true) m_config->SetField(Configuration::MonitorVMuxSel,4);
    else                              m_config->SetField(Configuration::MonitorVMuxSel,3);
  }else if(pos==1){
    if(read_radiation_sensor == true) m_config->SetField(Configuration::MonitorVMuxSel,6);
    else                              m_config->SetField(Configuration::MonitorVMuxSel,5);
  }else if(pos==2){
    if(read_radiation_sensor == true) m_config->SetField(Configuration::MonitorVMuxSel,14);
    else                              m_config->SetField(Configuration::MonitorVMuxSel,15);
  }else if(pos==3){
    if(read_radiation_sensor == true) m_config->SetField(Configuration::MonitorVMuxSel,8);
    else                              m_config->SetField(Configuration::MonitorVMuxSel,7);
  }
  */ 
  

  if (pos==4) {
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new Sync());
    m_config->SetField(Configuration::GlobalPulseConf,64); //bit6 Reset ADC
    WriteGlobal();
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new Sync());
    
    m_encoder->AddCommand(new Pulse(m_chipid));
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new Sync());
    return;
  } 
  if (pos==5) {
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new Sync());
    
    m_config->SetField(Configuration::GlobalPulseConf,4096); //2^12 // Start ADC Conversion
    WriteGlobal();
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new Pulse(m_chipid));
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new Sync());
    m_encoder->AddCommand(new Sync());
    return;
  }

  m_config->SetField(Configuration::MonitorPinEn,1); //2^12 // enable ADC Conversion
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_config->SetField(Configuration::GlobalPulseConf,64); //bit6 Reset ADC
  WriteGlobal();
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
 
  m_encoder->AddCommand(new Pulse(m_chipid));
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
 
  m_config->SetField(Configuration::GlobalPulseConf,8); //2^3 Clear Monitoring data
  WriteGlobal();
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Pulse(m_chipid));
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());

  m_config->SetField(Configuration::GlobalPulseConf,4096); //2^12 // Start ADC Conversion
  WriteGlobal();
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Sync());
  m_encoder->AddCommand(new Pulse(m_chipid));
  //for (int ind=0; ind<20;ind++) m_encoder->AddCommand(new Sync());

  // m_encoder->AddCommand(new RdReg(m_chipid,136));
  // m_encoder->AddCommand(new Noop());
}


void FrontEnd::AddClear(){

  m_encoder->AddCommand(new RD53B::Clear(m_chipid));
  m_encoder->AddCommand(new RD53B::Sync());
  m_encoder->AddCommand(new RD53B::PllLock());
}

void FrontEnd::Trigger(uint32_t delay, uint32_t trigMult, bool bc1, bool bc2, bool bc3, bool bc4, uint32_t PulseInj){


  // m_encoder->AddCommand(new RD53B::Clear(16));
  // //YARR waits 200micosec here

  m_encoder->AddCommand(new RD53B::Sync());
  m_encoder->AddCommand(new RD53B::Sync());
  if(PulseInj)
    m_encoder->AddCommand(new RD53B::Cal(16,1,0,20,0,0)); // This is prefersed for digital
  else
    m_encoder->AddCommand(new RD53B::Cal(16,0,0,1,0,0));
  

  for(uint32_t i=0;i<(delay/4-1);i++){
    m_encoder->AddCommand(new RD53B::PllLock());
  }
  for(uint32_t iTrig = 0; iTrig < trigMult; iTrig++){
    //m_encoder->AddCommand(new RD53B::Trigger(bc1, bc2, bc3, bc4,16));
    m_encoder->AddCommand(new RD53B::Trigger(bc1, bc2, bc3, bc4,4+iTrig));
    
  }
  for(uint32_t i=0;i<8;i++){
    m_encoder->AddCommand(new RD53B::PllLock());
  }
  if(!PulseInj)
    m_encoder->AddCommand(new RD53B::Cal(16,1,0,0,0,0));
}

//This is the barebone function that YARR has, that resets the ToT of a scan. Not sure it should be here or in Handler
// vod FrontEnd::ZeroTot (){

//         // uint16_t latency = rd53b->Latency.read();
//         // uint16_t injDigEn = rd53b->InjDigEn.read();
//         // rd53b->writeRegister(&Rd53b::Latency, 500);
//         // rd53b->writeRegister(&Rd53b::InjDigEn, 1);

// 	uint16_t latency = m_config->GetField(Configuration::Latency)->GetValue();
// 	uint16_t injDigEn = m_config->GetField(Configuration::CalMode)->GetValue();
// 	m_config->SetField(Configuration::Latency,500);
// 	m_config->SetField(Configuration::CalMode,1);

// 	m_encoder->AddCommand(new RD53B::Sync());
// 	m_encoder->AddCommand(new RD53B::Sync());
// 	for(uint32_t i=0;i<32;i++){
// 	  m_encoder->AddCommand(new RD53B::PllLock());
// 	}

       
// 	//Trigger( here )  ??? ToDo ???

// 	m_encoder->AddCommand(new RD53B::Sync());
// 	m_encoder->AddCommand(new RD53B::Sync());
// 	for(uint32_t i=0;i<32;i++){
// 	  m_encoder->AddCommand(new RD53B::PllLock());
// 	}

// 	m_config->SetField(Configuration::Latency,500);
// 	m_config->SetField(Configuration::CalMode,1);
	
// }



Hit * FrontEnd::GetHit(){
  Hit * tmp = NULL;
  m_mutex.lock();
  tmp = m_hits.front();
  m_mutex.unlock();
  return tmp;
}

bool FrontEnd::NextHit(){
  if (m_hits.empty()) return false;
  m_mutex.lock();
  Hit * hit = m_hits.front();
  m_hits.pop_front();
  delete hit;
  m_mutex.unlock();
  return (!m_hits.empty());
}

bool FrontEnd::HasHits(){
  return (not m_hits.empty());
}

void FrontEnd::ClearHits(){
  while(NextHit());
}


uint32_t FrontEnd::GetNHits(){
  return m_hits.size();
}

vector<Hit *> FrontEnd::GetHits(uint32_t max_hits){
  vector<Hit *> tmp;
  m_mutex.lock();
  if(max_hits>m_hits.size())max_hits=m_hits.size();
  for(uint32_t i=0;i<max_hits;i++){
    tmp.push_back(m_hits.at(i));
  }
  m_mutex.unlock();
  return tmp;
}

void FrontEnd::ClearHits(uint32_t max_hits){
  m_mutex.lock();
  if(max_hits>m_hits.size())max_hits=m_hits.size();
  for(uint32_t i=0;i<max_hits;i++){
    Hit * hit = m_hits.front();
    m_hits.pop_front();
    delete hit;
  }
  m_mutex.unlock();
}

void FrontEnd::SetNumberOfEvents(uint32_t totevnum){
  m_totalHits = totevnum;
}

uint32_t FrontEnd::GetTotalEventsNumber(){
  return m_totalHits;
}

// This needs to be re-written from scratch
void FrontEnd::HandleData(const uint8_t *recv_data, const size_t recv_size, bool register_data){

  if(m_verbose) cout << "FrontEnd::HandleData" <<endl;
  if(m_verbose)  cout<< "FrontEnd::HandleData is being set to handle Normal(0) or RegisterData(1): "<<register_data<<endl;
  
  //Swap between register data and normal data
  
  Decoder * TmpDec=0;
  //m_decoder->SetBytes(recv_data,recv_size,true);
  if(register_data){
    TmpDec=m_decoder_reg;
  }
  else{
    TmpDec=m_decoder;
  }

  TmpDec->AddBytes(recv_data,0,recv_size,true);
  TmpDec->Decode(m_verbose);

  if(m_verbose) cout << "FrontEnd::HandleData Decode bytestream: ChipId: "<<m_chipid<<"; "<< TmpDec->GetByteString() << endl;

  
  for(auto frame: TmpDec->GetFrames()){
    
    if(m_verbose) cout << "FrontEnd::HandleData: ChipID: " << m_chipid <<" " << frame->ToString() << endl;

    if(frame->GetType()==Frame::REGISTER){
      RegisterFrame * reg=dynamic_cast<RegisterFrame*>(frame);
      //std::string RegFrameString = reg->ToString();
      if(reg->GetAuroraCode()!=0xD2 ) cout << "FrontEnd::HandleData: " << reg->ToString() << endl;
      

      if( reg->GetAuroraCode()==0x55 || reg->GetAuroraCode()==0x99  || reg->GetAuroraCode()==0xB4 ){
        for(uint32_t i=0;i<2;i++){
          if(reg->GetAddress(i)==Configuration::PIX_PORTAL){
            uint32_t reg_col=m_config->GetField(Configuration::REGION_COL)->GetValue();
            uint32_t reg_row=m_config->GetField(Configuration::REGION_ROW)->GetValue();
            m_matrix->SetPair(reg_col,reg_row,reg->GetValue(i));
          }
          else if(reg->GetAddress(i)>Configuration::PIX_PORTAL and reg->GetAddress(i)<=0x1FF){
            m_config->SetRegisterValue(reg->GetAddress(i),reg->GetValue(i));
          }

          if(reg->GetAddress(i) == 137){
	    for(int j=0; j < 4; j++){
              if(m_ntcs[j]->GetPower() == true && m_ntcs[j]->isUpdated() == false && reg->GetAuto(i) == 0){
                m_ntcs[j]->SetADC(reg->GetValue(i));
                m_ntcs[j]->Update(true);
              }
              else if(m_bjts[j]->GetPower() == true && m_bjts[j]->isUpdated() == false && reg->GetAuto(i) == 0){
                m_bjts[j]->SetADC(reg->GetValue(i));
                m_bjts[j]->Update(true);
  	      }
  	    }
          }
        }
      }
    }else if(frame->GetType()==Frame::DATA){
      DataFrame * dat=dynamic_cast<DataFrame*>(frame);
      if(m_verbose) cout << "FrontEnd::HandleData: Getting streams  "<< frame->ToString() << endl;
      for (unsigned iStream=0; iStream< dat->GetNStreams(); iStream++){ 
	Stream * pStream = dat->GetStream(iStream);
	if(m_verbose) cout << "FrontEnd::HandleData: Got streams  "<< iStream << " with NHits"<<pStream->GetNhits()<<endl;
	//if there are no hits we need to set the right event number
	//FIXME: this need to change as soon as the tag reset works properly
	// uint32_t tag = pStream->GetStreamTag();
     	// cout << "BMDEBUG1: chipid = " << m_chipid << ", name = " << m_name << ", tag = " << tag << " --- " << ((tag&0xFC)>>2) << " , " <<  (tag&0x3) <<  endl;
	m_tagCounter ++;
	m_hit->SetBCID(m_tagCounter); //FIXME: this is not correct because BXs are missing
	if( (m_tagCounter%16 == 0) ){
	  m_hit->SetEvNum(m_hit->GetEvNum()+1);
	  m_tagCounter = 0;
	  //cout << "BMDEBUG: event number = " << m_hit->GetEvNum() << endl;
	}
		
	if(m_verbose){
	  if(pStream->GetNhits() == 0){
	    cout << "-->this stream has no hits " << endl;
	  }
	}
	for(unsigned iHit=0; iHit < pStream->GetNhits(); iHit++){
	  uint32_t HitMap = pStream->GetHitMap(iHit);
	  uint64_t ToTMap=0x7777777777777777; //If ToT value is not recovered from data-stream. Assume ToT=14
	  if (pStream->HasTot())
	    ToTMap = pStream->GetTotMap(iHit);
	 
	  if(m_verbose) cout << "FrontEnd::HandleData: Got HitMap "<<hex<< HitMap <<" and ToTMap "<<hex<<ToTMap<<dec<<endl;
 //Hit Map needs to be converted to individual hits
	  //Must double check this conversion
	  uint32_t ToTInd=0;
	  for (short iRow=0;iRow<2;iRow++){
	    for(short iCol=0;iCol<8;iCol++){
	      if (( (HitMap >> (iRow*8+iCol)) & 0x1) == 0x1 ){
		
		Hit * nhit = m_hit->Clone();
		nhit->SetTTag(pStream->GetTag(iHit)>>2); //The first 2 bits in the tag are the corresponding bunch cross ID (We might want to save that)
		nhit->SetBCID(pStream->GetTag(iHit)&0x3); // Temprorarly setting the tag for where in the bunch crossing here
		nhit->Set( (pStream->GetQcol(iHit)-1)*8 + iCol ,(pStream->GetQrow(iHit))*2  + iRow, ((ToTMap>>(4*ToTInd))&0xF)   ); //I am not sure of ToT Map order
		ToTInd++;
		if(m_verbose) cout << "FrontEnd::HandleData: Formed the NHit  "<<endl;
		m_mutex.lock();
		m_hits.push_back(nhit);
		m_mutex.unlock();
	      }
	    }
	  }// End of Hit Map Translation
	}//End of Stream Hits
      } //End of streams
    }
  }
}

void FrontEnd::Encode(){
  if(m_verbose){
    for(auto cmd: m_encoder->GetCommands()){
      cout << "FronEnd::Encode: Command: " << cmd->ToString() << endl;
    }
  }
  m_encoder->Encode();
  if(m_verbose){
    cout << "FrontEnd::Encode: Byte Stream: " << m_encoder->GetByteString() << endl;
  }
}

uint32_t FrontEnd::GetLength(){
  return m_encoder->GetLength();
}

uint8_t * FrontEnd::GetBytes(){
  return m_encoder->GetBytes();
}

void FrontEnd::ResetTagCounter(){
  m_tagCounter = 0;
  m_hit->SetEvNum(0);
}
