#include "RD53BEmulator/ToTScan.h" 
#include "RD53BEmulator/Tools.h"

#include <iostream>
#include <chrono>

#include "TCanvas.h"

using namespace std;
using namespace RD53B;

ToTScan::ToTScan(){
  m_ntrigs = 10;
  m_TriggerLatency = 60; //48;
  m_vcalMed  = 200;
  m_vcalDiff =3800;
  m_vcalHigh = m_vcalMed + m_vcalDiff;
} 

ToTScan::~ToTScan(){}

void ToTScan::PreRun(){

  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
  m_logFile << "\"m_TriggerLatency\": " << m_TriggerLatency << endl;

  if(m_verbose){
    cout << __PRETTY_FUNCTION__ << ": m_ntrigs = " << m_ntrigs << endl;
    cout << __PRETTY_FUNCTION__ << ": m_TriggerLatency = " << m_TriggerLatency << endl;
  }

  m_vcalDiff = Tools::injToVcal(m_charge);
  cout << "Injecting Charge of " << m_charge << " with Med: " << m_vcalMed << " and Diff: " << m_vcalDiff  << endl;

  for(auto fe : GetFEs()){

    fe->BackupConfig();
    
    //Configure the options for a ToT scan
    cout << "TotScan: Configure analog injection for " << fe->GetName() << endl;
    //fe->GetConfig()->SetField(Configuration::INJ_MODE_DIG,0);
    fe->GetConfig()->SetField(Configuration::CalMode,0);
    fe->GetConfig()->SetField(Configuration::Latency,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::PIX_BCAST_EN,0);
    fe->GetConfig()->SetField(Configuration::PIX_AUTO_ROW,0);

    fe->GetConfig()->SetField(Configuration::VCAL_MED , 200); //m_vcalMed);
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH, 500); //m_vcalMed+m_vcalDiff);
    fe->GetConfig()->SetField(Configuration::InjVcalRange,0);

    fe->WriteGlobal();
    Send(fe);

    cout << "ToTScan: Mask all" << endl;
    fe->DisableAll();
    ConfigurePixels(fe);
    Send(fe);

    m_totT[fe->GetName()]       = new TH1I(("totT_"+fe->GetName()).c_str(),(fe->GetName()+";ToT [bc]").c_str(), 16, -0.5, 15.5);
    m_occ[fe->GetName()]        = new TH2I(("occ_"+fe->GetName()).c_str(),(fe->GetName()+";Column;Row").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tot[fe->GetName()]        = new TH2I(("tot_"+fe->GetName()).c_str(),(fe->GetName()+";Column;Row;ToT [bc]").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tot2[fe->GetName()]       = new TH2I(("tot2_"+fe->GetName()).c_str(),(fe->GetName()+";Column;Row;ToT^2 [bc^2]").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    
    m_meantot[fe->GetName()]    = new TH2F(("meantot_"+fe->GetName()).c_str(),(fe->GetName()+";Column;Row;Mean ToT [bc]").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_sigmatot[fe->GetName()]   = new TH2F(("sigmatot_"+fe->GetName()).c_str(),(fe->GetName()+";Column;Row; #sigma(ToT) [bc]").c_str(), Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_meantot_1d[fe->GetName()] = new TH1F(("meantot_1d_" +fe->GetName()).c_str(),(fe->GetName()+";Mean ToT [bc]").c_str()    , 16, -0.5, 15.5);
    m_sigmatot_1d[fe->GetName()]= new TH1F(("sigmatot_1d_"+fe->GetName()).c_str(),(fe->GetName()+"; #sigma(ToT) [bc]").c_str(), 40, 0, 1.1);

    if(m_verbose) std::cout << __PRETTY_FUNCTION__ << ": added front-end with chip ID " << fe->GetChipID() << std::endl;
  }

  GetMasker()->SetVerbose(m_verbose);
  GetMasker()->SetRange(0,0,Matrix::NUM_COLS,Matrix::NUM_ROWS);
  GetMasker()->SetShape(8,4);
  GetMasker()->SetFrequency(25);
  GetMasker()->Build();
  //GetMasker()->SetFrequency((Matrix::DIF_COLN+1-Matrix::DIF_COL0)/4);

}

void ToTScan::Run(){

  cout << "ToTScan: Prepare the TRIGGER" << endl;
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // The Analog scans requirs many more triggers due to timming issues
  PrepareTrigger(m_TriggerLatency-5, 4, true, true, true, true);
  /*
  cout << "TotScan: Prepare the mask" << endl;
  GetMasker()->SetShape(8,2);
  GetMasker()->Build();
  */
  
  for(auto fe : GetFEs()){ // RD53B is always a bit noisy at initialisation so clearing hits 
    if(!fe->HasHits()) continue;
    fe->ClearHits();
  }

  for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
    //cout << "TotScan: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() << endl;
    GetMasker()->GetStep(st);

    //Dp the mask step
    ConfigMask();

    uint32_t ExpectedHits=0;
    for(auto fe : GetFEs()){ 
      ExpectedHits+=m_ntrigs*fe->GetNPixelsEnabled();
    }


    uint32_t nhits2=0;

    for(uint32_t trig=0;trig<m_ntrigs; trig++) Trigger();
   
    auto start = chrono::steady_clock::now();
    
    //histogram hit
    uint32_t tmpTOT=0;
    bool LastLoop=false;

    vector<Hit*> hits;
    int nH=0;
    while(true){
      
      for(auto fe : GetFEs()){


	hits=fe->GetHits(10000);
	nH=hits.size();
	if(nH>0) start = chrono::steady_clock::now();
	if (nH==0) {
	  this_thread::sleep_for(chrono::milliseconds( 1 ));
	
	} else {
	  for (int h=0; h<nH; h++) {

	    Hit* hit = hits.at(h);
	    if(hit==0) continue;


	    LastLoop=false;
	    start = chrono::steady_clock::now();
	    if(m_verbose)cout << "Hit V: " << fe->GetName() << ": " << hit->ToString() << endl;
	    tmpTOT=hit->GetTOT();
	  
	    //if( tmpTOT == 0xF) {
	    //  fe->NextHit();
	    //  continue;
	    //}
 
	    if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
	      cout << "Hit not expected in mask step: " << fe->GetName() << ": " << hit->ToString() << endl;
	      //fe->NextHit();
	      //continue;
	    }

	    m_totT[fe->GetName()]->Fill(tmpTOT);
	    m_occ [fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	    m_tot [fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),tmpTOT);
	    m_tot2[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),pow(tmpTOT,2));
	    nhits2++;
	  }
	}
	fe->ClearHits(nH);
      }
      auto end = chrono::steady_clock::now();
      uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
      if(LastLoop) break;
      if(ms>50){LastLoop=true;}
    }

    cout << "AnalogScan: NRealHits in step: " << nhits2 <<" / "<< ExpectedHits   <<std::endl;

    //Undo the mask step
    UnconfigMask();
  }
}

void ToTScan::Analysis(){
  TCanvas* can=new TCanvas("plot","plot",800,600);
  double sumtot = 0.;
  uint32_t totalhit = 0;
  double sumtot2 = 0.;
  double meantot2 = 0.;
  
  for(auto fe : GetFEs()){
    cout << "ToTScan: Restore the original threshold value" << endl;
    fe->RestoreBackupConfig();

    m_occ[fe->GetName()]->Write();
    for(uint32_t row=0; row<Matrix::NUM_ROWS; row++){
      for(uint32_t col=0; col<Matrix::NUM_COLS; col++){
	sumtot = m_tot[fe->GetName()]->GetBinContent(col+1,row+1);
	totalhit = m_occ[fe->GetName()]->GetBinContent(col+1,row+1);
	if (totalhit == 0) m_meantot[fe->GetName()]->Fill(col,row,0.);
	else {
	  m_meantot[fe->GetName()]->Fill(col,row,sumtot/totalhit);
	  m_meantot_1d[fe->GetName()]->Fill(sumtot/totalhit);
	  meantot2 = pow(m_meantot[fe->GetName()]->GetBinContent(col+1,row+1),2); 
	  sumtot2 = m_tot2[fe->GetName()]->GetBinContent(col+1,row+1);
	  m_sigmatot[fe->GetName()]->Fill(col,row,pow((sumtot2/totalhit - meantot2),0.5));
	  m_sigmatot_1d[fe->GetName()]->Fill(pow((sumtot2/totalhit - meantot2),0.5));
	}
      }
    }
    m_meantot[fe->GetName()]->Write();
    m_sigmatot[fe->GetName()]->Write();
    m_meantot_1d[fe->GetName()]->Write();
    m_sigmatot_1d[fe->GetName()]->Write();

    m_occ[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC_2D.pdf").c_str() );

    m_totT[fe->GetName()]->Draw("HIST");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__TOT_1D.pdf").c_str() );

    m_meantot_1d[fe->GetName()]->Draw("HIST");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__TOTmean_1D.pdf").c_str() );
    
    m_sigmatot_1d[fe->GetName()]->Draw("HIST");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__TOTsigma_1D.pdf").c_str() );

    delete m_occ[fe->GetName()];
    delete m_meantot[fe->GetName()];
    delete m_sigmatot[fe->GetName()];
  }
  delete can;
}
