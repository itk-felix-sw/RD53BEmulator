#include "RD53BEmulator/RdTrigger.h"
#include <sstream>
#include <iomanip>

using namespace std;
using namespace RD53B;

RdTrigger::RdTrigger(){
  m_symbol = 0x69;
  m_chipid = 0;
  m_tag = 0;
}

RdTrigger::~RdTrigger(){}

Command * RdTrigger::Clone(){
  RdTrigger * clone = new RdTrigger();
  clone->m_chipid = m_chipid;
  clone->m_tag = m_tag;
  return clone;
}

string RdTrigger::ToString(){
  ostringstream os;
  os << "RdTrigger 0x" << hex << m_symbol << dec
     << " ChipId: " << m_chipid
     << " (0x" << hex << m_chipid << dec << ")"
     << " Tag: " << m_tag
     << " (0x" << hex << m_tag << dec << ")";
  return os.str();
}

bool RdTrigger::FromString(string str){
  istringstream is(str);
  uint32_t hsym;
  string tmp;
  is >> tmp >> hex >> hsym >> dec;
  if(hsym!=m_symbol){return false;}
  is >> tmp >> m_chipid >> tmp 
     >> tmp >> m_tag >> tmp;
  return true;
}

uint32_t RdTrigger::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(maxlen<4) return 0;
  m_symbol   = bytes[0];
  if(m_symbol!=0x69) return 0;
  m_chipid   = m_symbol2data[bytes[1]];
  m_tag      = m_symbol2data[bytes[2]]<<5;
  m_tag     |= m_symbol2data[bytes[3]];
  return 6;
}

uint32_t RdTrigger::Pack(uint8_t * bytes){
  bytes[0]=((m_symbol>>0)&0xFF);
  bytes[1]=m_data2symbol[m_chipid];
  bytes[2]=m_data2symbol[m_tag>>5];
  bytes[3]=m_data2symbol[m_tag&0xF];
  return 4;
}

uint32_t RdTrigger::GetType(){
  return Command::RDTRG;
}

void RdTrigger::SetChipId(uint32_t chipid){
  m_chipid=chipid&0x1F;
}

uint32_t RdTrigger::GetChipId(){
  return m_chipid;
}

void RdTrigger::SetTag(uint32_t tag){
  m_tag=tag&0xFF;
}

uint32_t RdTrigger::GetTag(){
  return m_tag;
}



