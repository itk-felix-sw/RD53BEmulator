#include "RD53BEmulator/PixelThresholdTune.h"
#include "RD53BEmulator/Tools.h"

#include <iostream>
#include <sstream>
#include <chrono>

using namespace std;
using namespace RD53B;

PixelThresholdTune::PixelThresholdTune(){


  m_TriggerLatency = 48;
  m_ntrigs = 100;
  m_vcalMed  = 200;
  m_vcalHigh = 200;
  m_vcalDiff = 0;
  // m_nSteps = 8;
  // m_Steps = {8,4,2,1,1,1,1};
  // m_Acceptance = 0.65;

  
  if(m_verbose){
    cout << __PRETTY_FUNCTION__ << ": m_ntrigs = " << m_ntrigs << endl;
    cout << __PRETTY_FUNCTION__ << ": m_nSteps = " << m_nSteps << endl;
  }
}

PixelThresholdTune::~PixelThresholdTune(){}

void PixelThresholdTune::PreRun(){


  m_nSteps = 8;
  m_Steps = {8,4,2,1,1,1,1};
  m_Acceptance = 0.8;

  if(m_retune){
    m_nSteps = 5;
    m_Steps = {1,1,1,1};
    m_Acceptance = 0.6;
  }

  m_vcalDiff = Tools::injToVcal(m_threshold);
  m_vcalHigh = m_vcalMed + m_vcalDiff;
  cout << " PRECONFIG: " << m_threshold << " correspond to VCAL diff: " << m_vcalDiff << endl;
  
  cout << "ntrigs: " << m_ntrigs << endl;
  cout << "latency: " << m_TriggerLatency << endl;
  cout << "vcalMed: " << m_vcalMed << endl;
  cout << "vcalHigh: " << m_vcalHigh << endl;


  int FECount=-1;
  m_occ.resize(GetFEs().size());
  m_sign.resize(GetFEs().size());
  for(auto fe : GetFEs()){
    FECount++;
    fe->BackupConfig();

    //Configure the options for an threshold scan                                                                                     
    cout << "GlobalThresholdTune: Configure analog injection for " << fe->GetName() << endl;
    fe->GetConfig()->SetField(Configuration::CalMode,0); //was INJ_MODE_DIG
    fe->GetConfig()->SetField(Configuration::Latency, m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::PIX_BCAST_EN,0);
    fe->GetConfig()->SetField(Configuration::PIX_AUTO_ROW,0);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,m_vcalMed);
    fe->GetConfig()->SetField(Configuration::InjVcalRange,1);    
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,m_vcalHigh);




    fe->WriteGlobal();
    Send(fe);
    
    fe->DisableAll();
    ConfigurePixels(fe);
    
    for(uint32_t Step=0; Step < m_nSteps; Step++){
      
      m_occ[FECount].push_back(new TH2I(("occ_"+to_string(Step)+"_"+fe->GetName()).c_str(),
					    ("Occupancy map, Step: "+to_string(Step)+";Column;Row;Number of hits").c_str(), 
					      Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS));
      m_sign[FECount].push_back(new TH2I(("sign_"+to_string(Step)+"_"+fe->GetName()).c_str(),
					    ("sign map, Step: "+to_string(Step)+";Column;Row;Number of hits").c_str(), 
					       Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS));


    }

  }

  
  
  //Setting up the masking according to the Scan Type
  GetMasker()->SetVerbose(m_verbose);
  GetMasker()->SetRange(0,0,Matrix::NUM_COLS,Matrix::NUM_ROWS);
  //GetMasker()->SetRange(0,0,100,100);
  GetMasker()->SetShape(8,4);
  GetMasker()->SetFrequency(10);

  GetMasker()->Build();



}

void PixelThresholdTune::Run(){
  cout << __PRETTY_FUNCTION__ << "Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency-5, 4, true, true, true, true);

  for(auto fe : GetFEs()){ // RD53B is always a bit noisy at initialisation so clearing hits
    if(!fe->HasHits()) continue;
    fe->ClearHits();
  }

  
  for(uint32_t iStep = 0; iStep < m_nSteps; iStep++){
    int FECount= -1;
    for(auto fe : GetFEs()){
      FECount++;
      unsigned Counter=0;
      for(uint32_t row = 0; row < (Matrix::NUM_ROWS); row++){
    	for(uint32_t col = 0; col < (Matrix::NUM_COLS); col++){
	  Counter++;
	  int32_t adjBit = 0; //adjBit = fe->GetPixelThreshold(col, row);;
	  if(m_retune) adjBit = fe->GetPixelThreshold(col, row);
	  if (iStep > 0) { //Update the threshold according to previous run. 
	    int8_t sign = m_sign[FECount][iStep-1]->GetBinContent(col+1,row+1);
	    adjBit = fe->GetPixelThreshold(col, row) + m_Steps.at(iStep-1)*sign;
	    // if(row==0 and col==5){
	    //   std::cout<<"Early report  step: "<<iStep<<" Occ "<<occupancy<<" hits "<<hits<<" Thr: "<<fe->GetPixelThreshold(col, row)<<" Sign "<<m_sign[FECount][iStep]->GetBinContent(col+1,row+1)<<std::endl; 
	    // }

	    
	    if (adjBit>15) adjBit=15;
	    if (adjBit<-15) adjBit=-15;
	    if (col==1 and row==1) cout<<"sign "<<int(sign)<<" bit "<<int(adjBit)<<std::endl;
      	  }
	  

	  fe->SetPixelThreshold(col, row, adjBit);
 	  if(Counter%50==0){
	    fe->ProcessCommands();
	    Send(fe);
 	  }    	
    	}
      }
      fe->ProcessCommands();
      Send(fe);
    }

    for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
      
      cout << "PixelTune: Step: " << (st) << "/" << GetMasker()->GetNumSteps() << endl;
      GetMasker()->GetStep(st);
      ConfigMask();


      uint32_t ExpectedHits=0;
      for(auto fe : GetFEs()){ 
	ExpectedHits+=m_ntrigs*fe->GetNPixelsEnabled();
      }


      for(uint32_t trig=0;trig<m_ntrigs; trig++) {
	Trigger();
      }


      uint32_t nHitsT=0;
      uint32_t nHitsV=0;
      uint32_t nHitsG=0;
      vector<uint32_t> vhits;
      vector<uint32_t> vhitsGood;
      vhits.resize(GetFEs().size(),0);
      vhitsGood.resize(GetFEs().size(),0);

      vector<Hit*> hits;
      int nH=0;
      bool LastLoop=false;
      auto start = chrono::steady_clock::now();
      //t1 = chrono::steady_clock::now();
      while(true){
	int feID=-1;
	for(auto fe : GetFEs()){
	  feID+=1;
	  hits=fe->GetHits(10000);
	  nH=hits.size();
	  if(nH>0) start = chrono::steady_clock::now();
	  if(m_verbose) std::cout<<"NH: "<<nH<<std::endl;
	  if (nH==0) {
	    this_thread::sleep_for(chrono::milliseconds( 1 ));
	
	  } else {
	    for (int h=0; h<nH; h++) {
	      Hit* hit=hits.at(h);
	      nHitsT++;
	      if ( hit==0 ) continue;
	      if ( hit->GetTOT()==0x0 ) continue;
	      hit->SetEvNum(st);
	      //cout << "Hit V: " << fe->GetName() << ": " << hit->ToString() << endl;
	      m_occ[feID][iStep]->Fill(hit->GetCol(),hit->GetRow());
	      nHitsV++;
	      vhits.at(feID)=vhits.at(feID)+1;   
	    
	      //LastLoop=false;
	      if( !GetMasker()->Contains(hit->GetCol(),hit->GetRow()) ) {
		//cout << "Hit not expected in mask step: " << fe->GetName() << ": " << hit->ToString() << endl;
	      } else { //VD maybe add a check on TOT
		nHitsG++;
		vhitsGood.at(feID)++;   
	      }
	    }
	  }
	  fe->ClearHits(nH);
	}
	auto end = chrono::steady_clock::now();
	uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
	if(LastLoop) break;
	if(ms>20 or nHitsG>=ExpectedHits ){LastLoop=true;}
      }

      cout << "Pixel Tune:: ConfigStep"<< iStep <<" / "<<m_nSteps <<" Size "<< (iStep>0?m_Steps[iStep-1]:0) <<" Mask Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() 
	   << " ==== allFE: " << nHitsT << " / " << nHitsV << " / " << nHitsG << " / " << ExpectedHits
	   << " ==== PER FE: ( ";
      int feID=-1;
      for(auto fe : GetFEs()) {
	feID+=1; 
	cout << " " << vhits.at(feID) << " , " << vhitsGood.at(feID) <<" / "<< m_ntrigs*fe->GetNPixelsEnabled() << " || ";
      }
      std::cout<<std::endl;


      UnconfigMask();
    }
  

    // Results
    FECount=-1;
    for(auto fe : GetFEs()){
      FECount++;

      m_occ[FECount][iStep]->Write();

      for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
	for(uint32_t col = 0; col < Matrix::NUM_COLS; col++){
	  if(fe->GetPixelMask(col,row)) { //If masked we ignore it
	    m_sign[FECount][iStep]->SetBinContent(col+1,row+1, 0);
	    continue;
	  }

	  uint32_t hits = m_occ[FECount][iStep]->GetBinContent(col+1,row+1);
	  double occupancy = double(hits)/m_ntrigs;   
	  //cout<<"Occupancy is: "<<occupancy<<endl;


	  if (occupancy < (1-m_Acceptance) )
	    m_sign[FECount][iStep]->SetBinContent(col+1,row+1, -1);
	  else if (occupancy > m_Acceptance )
	    m_sign[FECount][iStep]->SetBinContent(col+1,row+1, +1);
	  else
	    m_sign[FECount][iStep]->SetBinContent(col+1,row+1, 0);

	  // if((row==5 and col==0) or (row==0 and col==5) or (row==0 and col==0)){
	  //   std::cout<<row<<" "<<col<<" report  step: "<<iStep<<" Occ "<<occupancy<<" hits "<<hits<<" Thr: "<<fe->GetPixelThreshold(col, row)<<" Sign "<<m_sign[FECount][iStep]->GetBinContent(col+1,row+1)<<std::endl; 
	  // }




	}
      }

      m_sign[FECount][iStep]->Write();
    }
  }
}

void PixelThresholdTune::Analysis(){
  std::cout<<" PixelThresholdTune::Analysis Starting"<<std::endl;
  int FECount=-1;
  for(auto fe : GetFEs()){
    FECount++;
    TH2I* Thresholds = new TH2I(("Thresholds_"+fe->GetName()).c_str(),("Thresholds_"+fe->GetName()).c_str(),  Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);

    std::cout<<"PixelThresholdTune::Analysis Saving Thresholds "<<std::endl;
    for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){                                                	for(uint32_t col = 0; col < Matrix::NUM_COLS; col++){

	Thresholds->SetBinContent(col+1,row+1, fe->GetPixelThreshold(col,row));
	// if ((col==0 and row==5)  or (col==5 and row==0) or (col==0 and row==0) )
	//   std::cout<<"New bin threshold :"<<col<<" "<<row<<" - "<<fe->GetPixelThreshold(col,row)<<std::endl;
      }
    }
    std::cout<<"PixelThresholdTune::Analysis Restoring Backups "<<std::endl;
    fe->RestoreBackupConfig();

    std::cout<<"PixelThresholdTune::Analysis Saving new thresholds "<<std::endl;
    for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){                                                	for(uint32_t col = 0; col < Matrix::NUM_COLS; col++){
	fe->SetPixelThreshold(col, row, Thresholds->GetBinContent(col+1,row+1) );
	if (col==1 and row==1) std::cout<<"Set bin threshold "<<fe->GetPixelThreshold(col,row)<<std::endl;
      }
    }
    std::cout<<"PixelThresholdTune::Analysis Saving Histogram "<<std::endl;
    Thresholds->Write();
    // for(uint32_t iStepBit = 0; iStepBit < m_nSteps; iStepBit++){  
    //   delete m_occ[FECount][iStepBit];
    //   delete m_sign[FECount][iStepBit];
    //   delete Thresholds;
    // }
  }
}

