#include "RD53BEmulator/Decoder.h"

#include <iostream>
#include <ctime>
#include <vector>
#include <iomanip>
#include <fstream>
#include <bitset>
#include <cmdl/cmdargs.h>

using namespace std;
using namespace RD53B;

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# decode_rd53b_data               #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose(   'v',"verbose","turn on verbose mode");
  CmdArgStr  cFilePath(  'f',"file","path","file to parse", CmdArg::isREQ);
  CmdArgStr  cFileFormat('F',"format","format","text,binary. Default text");

  CmdLine cmdl(*argv,&cVerbose,&cFilePath,&cFileFormat,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  ifstream fr(cFilePath);
  fr.seekg(0, std::ios::end);
  cout << "File size (bytes): " << fr.tellg() << endl;
  fr.seekg(0, std::ios::beg);

  cout << "Create new decoder object" << endl;
  Decoder *decoder=new Decoder();

  cout << "Create a byte stream" << endl;
  vector<uint8_t> bytes;

  cout << "Loop over file contents" << endl;

  char * buff = new char[8];
  uint8_t byte;

  while(fr){
    fr.read(buff,8);
    bitset<8> bstr(buff);
    byte=bstr.to_ulong()&0xFF;
    bytes.push_back(byte);
    if(cVerbose) cout << "byte: 0x" << hex << (uint32_t) byte << dec << endl;
  }

  delete[] buff;
  fr.close();

  cout << "Set bytes: size: " << bytes.size() << endl;
  decoder->SetBytes(&bytes[0],bytes.size());

  if(cVerbose) cout << "Byte stream: 0x" << decoder->GetByteString() << endl;

  cout << "Decode" << endl;
  decoder->Decode();

  cout << "Contents" << endl;
  for(uint32_t i=0;i<decoder->GetFrames().size();i++){
    cout << decoder->GetFrames().at(i)->ToString() << endl;
  }

  delete decoder;

  return 0;
}
