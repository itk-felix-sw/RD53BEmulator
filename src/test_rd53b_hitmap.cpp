#include "RD53BEmulator/Tools.h"
#include "RD53BEmulator/HitMap.h"
#include <iostream>
#include <iomanip>
#include <bitset>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cmdl/cmdargs.h>
#include <chrono>
#include <json.hpp>

using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int64_t, std::uint64_t, float>;
using namespace RD53B;
using namespace std;

string to_string(uint32_t val, uint32_t len){
  ostringstream os;
  for(uint32_t i=0;i<len;i++){
    os << ((val>>(len-1-i))&1);
  }
  return os.str();
}

void encode(uint32_t decoded, uint32_t *encoded, bool verbose){
  
  HitMap hm;
  uint32_t len=hm.Encode(decoded,encoded);
  
  cout << " dec(16): 0b" << to_string(decoded,16) 
       << " dec(0x): 0x" << hex << setw(8) << setfill('0') << decoded << dec 
       << " => "
       << " enc(" << setw(2) << len << "): 0b" << to_string(*encoded>>(30-len),len) 
       << " enc(0x): 0x" << hex << setw(4) << setfill('0') << *encoded << dec 
       << endl;

  if(verbose) cout << "explain: " << hm.to_string() << endl;

}

void decode(uint32_t encoded, uint32_t *decoded, bool verbose){
  
  HitMap hm;
  uint32_t len=hm.Decode(encoded,decoded);
  
  cout << " enc(" << setw(2) << len << "): 0b" << to_string(encoded>>(30-len),len) 
       << " enc(0x): 0x" << hex << setw(4) << setfill('0') << encoded << dec 
       << " => "
       << " dec(16): 0b" << to_string(*decoded,16) 
       << " dec(0x): 0x" << hex << setw(8) << setfill('0') << *decoded << dec 
       << endl;

  if(verbose) cout << "explain: " << hm.to_string() << endl;

}

int main(int argc, char** argv){
 
  cout << "#################################" << endl
       << "# test_rd53b_hitmap             #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgBool cDecode( 'd',"decode","enable decoding");
  CmdArgBool cEncode( 'e',"encode","enable encoding");
  CmdArgStr  cStream( 's',"stream","stream","stream of bytes"); 
  CmdArgBool cPerf(   'p',"perf","enable performance testing"); 
  CmdLine cmdl(*argv,&cVerbose,&cDecode,&cEncode,&cStream,&cPerf,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  uint32_t encoded=0;
  uint32_t decoded=0;
  if(cStream){
    string str(cStream);
    uint32_t val=0;
    if(str.find("0x")!=str.npos){val=stoul(str.substr(2).c_str(),NULL,16);}
    else if(str.find("0b")!=str.npos){val=stoul(str.substr(2).c_str(),NULL,2);}
    else{val=strtoul(str.c_str(),NULL,10);}
    if(cDecode){
      encoded=val;
      decode(encoded,&decoded,cVerbose);
      encode(decoded,&encoded,cVerbose);
    }
    if(cEncode){
      decoded=val;
      encode(decoded,&encoded,cVerbose);
      decode(encoded,&decoded,cVerbose);
    }
  }


  cout<<"Test Charge: "<<1000<<" Vcal "<<Tools::injToVcal(1000)<<" Charge: "<<Tools::injToCharge(Tools::injToVcal(1000))<<endl;


  if(cPerf){
    json out=json::array();
    uint32_t ntries=1e4;
    HitMap hm;
    //There are 0xFFFF different patterns
    for(uint32_t pat=1;pat<=0xFFFF;pat++){
      uint32_t len=hm.Encode(pat,&encoded);
      auto start = chrono::steady_clock::now();
      for(uint32_t c=0;c<ntries;c++){
	hm.Decode(encoded,&decoded);
      }
      auto stop = chrono::steady_clock::now();
      double duration = chrono::duration_cast<chrono::microseconds>(stop - start).count();
      double perf=duration/(float)ntries;
      cout << "Pattern: 0x" << hex << setw(4) << setfill('0') << pat << dec << ", "
	   << "Len: " << setw(2) << len << ", "
	   << "Encoded: 0b" << to_string(encoded>>(30-len),len) << setw(30-len+2) << ", "
	   << "NTries: " << ntries << ", "
	   << "Time [us]: " << duration << ", "
	   << "Perf [us/wrd]: " << perf << ", "
	   << "Perf [us/bit]: " << perf/len << ", "
	   << "Speed [Mb/s]: " << 1./(perf/len) << ", "
	   << (decoded!=pat?", ERROR decoding!!":"")	   
	   << endl;
      json res;
      res["Pattern"]=pat;
      res["Encoded"]=encoded;
      res["Len"]=len;
      res["Ntries"]=ntries;
      res["Time_us"]=duration;
      out.push_back(res);
    }
    cout << "Saving performance file: test_rd53b_hitmap.txt" << endl;
    ofstream fw("test_rd53b_hitmap.txt");
    fw << setw(4) << out;
    fw.close();
  }  
  
  return 0;  
  
}
