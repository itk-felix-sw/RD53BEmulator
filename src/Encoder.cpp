#include "RD53BEmulator/Encoder.h"
#include <iostream>
#include <iomanip>

using namespace std;
using namespace RD53B;

Encoder::Encoder(){
  m_bytes.resize(100000,0);
  m_length = 0;
  m_Cal=new Cal();
  m_Cle=new RD53B::Clear();
  m_Pll=new PllLock();
  m_Pul=new Pulse();
  m_RdR=new RdReg();
  m_RdT=new RdTrigger();
  m_Syn=new Sync();
  m_Trg=new Trigger();
  m_WrR=new WrReg();
}

Encoder::~Encoder(){
  Clear();
  delete m_Cal;
  delete m_Cle;
  delete m_Pll;
  delete m_Pul;
  delete m_RdR;
  delete m_RdT;
  delete m_Syn;
  delete m_Trg;
  delete m_WrR;
}

void Encoder::SetBytes(uint8_t *bytes, uint32_t len){
  m_length=0;
  m_bytes.resize(len);
  for(uint32_t i=0;i<len;i++){
    m_bytes[m_length]=bytes[i];
    m_length++;
  }
}

void Encoder::AddCommand(Command *command){
  m_commands.push_back(command);
}
  
void Encoder::ClearBytes(){
  m_length=0;
}
  
void Encoder::ClearCommands(){
  while(!m_commands.empty()){
    Command * command = m_commands.back();
    delete command;
    m_commands.pop_back();
  }
}

void Encoder::Clear(){
  ClearBytes();
  ClearCommands();
}
  
string Encoder::GetByteString(){
  ostringstream os;
  os << hex; 
  for(uint32_t pos=0; pos<m_length; pos++){
    os << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << " ";
  }
  return os.str();
}

uint8_t * Encoder::GetBytes(){
  return m_bytes.data();
}
  
uint32_t Encoder::GetLength(){
  return m_length;
}
  
vector<Command*> & Encoder::GetCommands(){
  return m_commands;
}

void Encoder::Encode(){
  //@FIXME we need to multiplex the service 1/N
  uint32_t pos=0;
  for(uint32_t i=0;i<m_commands.size();i++){
    //If position commes to the limit  of m_bytes, re-size the m_bytes array permenantly.
    //This should only happen in extremum cases
    if(pos%100000 >= 99990 and (m_bytes.size()-pos) <= 10) {
      m_bytes.resize(100000 + m_bytes.size());
      std::cout<<" Encoder::Encode !!!! Warning !!!!, You are trying to send/encode too many packets at once. This is supported but not recommended."<<std::endl; 
      
    }
    pos+=m_commands[i]->Pack(&m_bytes[pos]);
  }
  m_length=pos;
}

void Encoder::Decode(){
  ClearCommands();
  uint32_t pos=0;
  uint32_t nb=0;
  uint32_t tnb=m_length;
  while(pos!=tnb){
    Command * command=0;
    if     ((nb=m_Cal->UnPack(&m_bytes[pos],tnb-pos))>0){command=m_Cal; m_Cal=new Cal();}
    else if((nb=m_Cle->UnPack(&m_bytes[pos],tnb-pos))>0){command=m_Cle; m_Cle=new RD53B::Clear();}
    else if((nb=m_Pll->UnPack(&m_bytes[pos],tnb-pos))>0){command=m_Pll; m_Pll=new PllLock();}
    else if((nb=m_Pul->UnPack(&m_bytes[pos],tnb-pos))>0){command=m_Pul; m_Pul=new Pulse();}
    else if((nb=m_RdR->UnPack(&m_bytes[pos],tnb-pos))>0){command=m_RdR; m_RdR=new RdReg();}
    else if((nb=m_RdT->UnPack(&m_bytes[pos],tnb-pos))>0){command=m_RdT; m_RdT=new RdTrigger();}
    else if((nb=m_Syn->UnPack(&m_bytes[pos],tnb-pos))>0){command=m_Syn; m_Syn=new Sync();}
    else if((nb=m_Trg->UnPack(&m_bytes[pos],tnb-pos))>0){command=m_Trg; m_Trg=new Trigger();}
    else if((nb=m_WrR->UnPack(&m_bytes[pos],tnb-pos))>0){command=m_WrR; m_WrR=new WrReg();}
    else{
      cout << "Cannot decode byte sequence: "
           << "0x" << hex << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << dec 
           << " at index: " << pos 
           << " ...skipping" << endl;
      pos++;
      continue;
    }
    if(!command){pos++; continue;}
    m_commands.push_back(command);
    pos+=nb;
  }
  //Check if there is bytes left
}

void Encoder::AddCommand(string str){
  Command * command=0;
  if     (m_Cal->FromString(str)){command=m_Cal; m_Cal=new Cal();}
  else if(m_Cle->FromString(str)){command=m_Cle; m_Cle=new RD53B::Clear();}
  else if(m_Pll->FromString(str)){command=m_Pll; m_Pll=new PllLock();}
  else if(m_Pul->FromString(str)){command=m_Pul; m_Pul=new Pulse();}
  else if(m_RdR->FromString(str)){command=m_RdR; m_RdR=new RdReg();}
  else if(m_RdT->FromString(str)){command=m_RdT; m_RdT=new RdTrigger();}
  else if(m_Syn->FromString(str)){command=m_Syn; m_Syn=new Sync();}
  else if(m_WrR->FromString(str)){command=m_WrR; m_WrR=new WrReg();}
  else if(m_Trg->FromString(str)){command=m_Trg; m_Trg=new Trigger();}
  else{
    cout << "Cannot decode string: " << str << endl;
  }
  if(command){m_commands.push_back(command);}
}

