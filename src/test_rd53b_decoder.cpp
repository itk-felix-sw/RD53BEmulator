#include "RD53BEmulator/Stream.h"
#include "RD53BEmulator/DataFrame.h"
#include "RD53BEmulator/Decoder.h"

#include <iostream>
#include <ctime>
#include <vector>
#include <cmdl/cmdargs.h>
#include <string>
#include <fstream>
#include "TCanvas.h"
#include "TH2I.h"
#include "TStyle.h"
#include "chrono"

using namespace std;
using namespace RD53B;

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# test_rd53b_decoder            #" << endl
       << "#################################" << endl;


  CmdArgBool cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgStr  cRawData( 'f',"rawdata","file","raw data file that contains all the data packets");
  CmdArgBool cNoTot(   'T',"no-tot","turn off tot");
  CmdArgBool cNoEnc(   'E',"no-enc","turn off encoding");
  CmdArgBool cEnChipId(   'C',"enableChipId","Enable Chip Id");
  CmdLine cmdl(*argv,&cVerbose,&cRawData,&cNoTot,&cNoEnc,&cEnChipId,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  bool verbose = (cVerbose.flags()&CmdArg::GIVEN)?1:0;
  bool EnChipId = (cEnChipId.flags()&CmdArg::GIVEN)?1:0;
  string file(cRawData.flags()&CmdArg::GIVEN?cRawData:"");
  if( file == ""){
    std::cout<<"File name not specified, closing the run"<<std::endl;
    return 0;
  }

  std::ifstream RawData;
  RawData.open(file);

  if (! RawData.is_open() ) {
    
    std::cout<<"The provided raw data file is incorrect, closing the run"<<std::endl;
    return 0;
  }

  vector<uint8_t> Bytes;
  vector<uint8_t> BytesReversed;
  char Byte;
  while(RawData.get(Byte)){
    Bytes.push_back(uint8_t(Byte));
  }

  for(unsigned ind=0;ind<Bytes.size();ind+=4){
    BytesReversed.push_back( Bytes[ind+3]); 
    BytesReversed.push_back( Bytes[ind+2]);
    BytesReversed.push_back( Bytes[ind+1]);
    BytesReversed.push_back( Bytes[ind]);
  }



  
  RawData.close();
  std::cout<<"Loaded the RawData of lenght "<<Bytes.size() <<" Bytes "<<std::endl; 

  

  Decoder * deco = new Decoder(); 
  deco->EnableChipId(EnChipId);
  deco->Clear();
  deco->AddBytes( Bytes.data(), 0, Bytes.size(),true);

  cout << "Decoding performance" << endl;

  double duration, frequency, freqBit;
  auto start = chrono::steady_clock::now();
  
  deco->Decode(verbose);
  
  auto end = chrono::steady_clock::now();
  duration = chrono::duration_cast<chrono::microseconds>(end - start).count();
  frequency = (double) Bytes.size() / duration;
  freqBit = (double) Bytes.size()*8 / duration;
  
  cout << "CPU time  [s]  : " << duration/1e6 << endl;
  cout << "Frequency [MBytes/s]: " << frequency << endl;
  cout << "Frequency [MBits/s]: " << freqBit << endl;


  cout<<"Checking the decoded data"<<endl;

  TH2I * m_occ = new TH2I("occ","Occupancy map;Column;Row;Number of hits", 400, 0, 400,  384, 0, 384);

  for(auto frame: deco->GetFrames()){
    DataFrame * dat=dynamic_cast<DataFrame*>(frame);
    for (unsigned iStream=0; iStream< dat->GetNStreams(); iStream++){ 
      Stream * pStream = dat->GetStream(iStream);
      for(unsigned iHit=0; iHit < pStream->GetNhits(); iHit++){
	uint32_t HitMap = pStream->GetHitMap(iHit);
	// uint64_t ToTMap=0x7777777777777777; //If ToT value is not recovered from data-stream. Assume ToT=14
	// if (pStream->HasTot())
	//   ToTMap = pStream->GetTotMap(iHit);
	uint32_t ToTInd=0;
	for (short iRow=0;iRow<2;iRow++){
	  for(short iCol=0;iCol<8;iCol++){
	    if (( (HitMap >> (iRow*8+iCol)) & 0x1) == 0x1 ){
	      	
		unsigned col = (pStream->GetQcol(iHit)-1)*8 + iCol;
		unsigned row = (pStream->GetQrow(iHit))*2  + iRow;
		//uint8_t ToT = ((ToTMap>>(4*ToTInd))&0xF) ;
		//cout<<"col "<<col<<" row "<<row<<" ToT "<<std::hex<<int(ToT)<<std::dec<<std::endl;
		//if(ToT==0x7)
		m_occ->Fill(col,row);
		ToTInd++;
	    }
	  }
	}
      }
    }
  }

  gStyle->SetOptStat(0);
  TCanvas* can=new TCanvas("plot","plot",800,600);
  m_occ->SetTitle("" );
  m_occ->Draw("COLZ");
  can->Print("DataDecode_Occ.pdf" );
  can->Print("DataDecode_Occ.png" );




  cout<<"Checking stream performance "<<endl;
  DataFrame * m_fD=new DataFrame(); 
  m_fD->EnableChnID(EnChipId);
  uint32_t nb=0;
  


  start = chrono::steady_clock::now();

  for (unsigned c=0;c<1000;c++)
    nb+=m_fD->UnPack(&Bytes[nb],  Bytes.size());
  // nb+=m_fD->UnPack(&Bytes[nb],  Bytes.size());
  // nb+=m_fD->UnPack(&Bytes[nb],  Bytes.size());
  // nb+=m_fD->UnPack(&Bytes[nb],  Bytes.size());
  end = chrono::steady_clock::now();
  std::cout<<"Unpacked packages with total lenght of "<<nb<<" Bytes "<<std::endl;
  duration = chrono::duration_cast<chrono::microseconds>(end - start).count();
  frequency = (double) nb / duration;
  freqBit = (double) nb*8 / duration;
  
  cout << "CPU time  [s]  : " << duration/1e6 << endl;
  cout << "Frequency [MBytes/s]: " << frequency << endl;
  cout << "Frequency [MBits/s]: " << freqBit << endl;
  




  cout << "Have a nice day" << endl;
  return 0;
}

