#include "RD53BEmulator/Decoder.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <chrono>


using namespace std;
using namespace RD53B;

Decoder::Decoder(){
  m_bytes.reserve(1000000);
  m_size = 1000000;
  m_bytes.resize(m_size,0);
  m_length = 0;
  m_enable_cid=1;
  m_mode = MODE_DATA;
  m_fD=new DataFrame();
  m_fD->EnableChnID(m_enable_cid);
  m_fI=new IdleFrame();
  m_fR=new RegisterFrame();
}

Decoder::~Decoder(){
  m_bytes.clear();
  ClearFrames();
  delete m_fD;
  delete m_fR;
  delete m_fI;
}


uint32_t Decoder::GetMode(){
  return m_mode;
}
  
void Decoder::EnableChipId(bool Mode){
  m_enable_cid = Mode;
  m_fD->EnableChnID(m_enable_cid);
}

bool Decoder::GetChipId(){
  return m_enable_cid;
}
  
void Decoder::SetMode(uint32_t Mode){
  m_mode = Mode;
}


void Decoder::AddBytes(const uint8_t *bytes, uint32_t pos, uint32_t len, bool reversed){

  //std::cout<<"Decoder::AddBytes Adding "<<len<<" bytes to the stream starting at"<<m_length<<std::endl;
  //Resize the m_bytes vector if too much data is flowing
  if(m_size<(m_length+len)){
    m_bytes.resize(m_length+len,0);
    m_size=m_length+len;
  }
  
  if(!reversed){
    for(uint32_t i=pos;i<pos+len;i++){
      m_bytes[m_length]=bytes[i];
      m_length++;
    }
  }
  // else         {for(uint32_t i=pos;i<len+pos;i++){m_bytes[len-i-1]=bytes[i];}}
  else{
    for(uint32_t i=pos;i<pos+len && (pos+len-i)>=4;i+=4){
      
      uint32_t j=m_length-pos; 
      //std::cout<<pos<<" "<<i<<" "<<len<<" "<<j<<endl;
      m_bytes[j+3]=bytes[i];
      m_bytes[j+2]=bytes[i+1];
      m_bytes[j+1]=bytes[i+2];
      m_bytes[j]=bytes[i+3];
      m_length+=4; 
    }
  }
}

void Decoder::SetBytes(const uint8_t *bytes, uint32_t len, bool reversed){

  m_length=len;
  if(len!=m_bytes.size()){m_bytes.resize(len);}
  if(!reversed){for(uint32_t i=0;i<len;i++){m_bytes[i]=bytes[i];}}
  // else         {for(uint32_t i=0;i<len;i++){m_bytes[len-i-1]=bytes[i];}
  else         {
    for(uint32_t i=0;i<len;i=i+4){ 
      m_bytes[i+3]=bytes[i];
      m_bytes[i+2]=bytes[i+1];
      m_bytes[i+1]=bytes[i+2];
      m_bytes[i]=bytes[i+3];
    }
  }
}

void Decoder::AddFrame(Frame *frame){
  m_frames.push_back(frame);
}
  
void Decoder::ClearBytes(){
  m_length=0;
}
  
void Decoder::ClearFrames(){
  while(!m_frames.empty()){
    Frame* frame = m_frames.back();
    delete frame;
    m_frames.pop_back();
  }
}

void Decoder::Clear(){
  ClearBytes();
  ClearFrames();
}
  
string Decoder::GetByteString(){
  ostringstream os;
  os << hex; 
  for(uint32_t pos=0; pos<m_length; pos++){
    os << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << " ";
  }
  return os.str();
}

void Decoder::GetBytes(uint8_t * bytes, uint32_t & length, bool reversed){
  length=m_length;
  if(!reversed){for(uint32_t i=0;i<m_length;i++){bytes[i]=m_bytes[i];}}
  else         {for(uint32_t i=0;i<m_length;i++){bytes[m_length-i-1]=m_bytes[i];}}
}

uint8_t * Decoder::GetBytes(bool reversed){
  if(!reversed){return m_bytes.data();}
  m_rbytes.resize(m_length);
  for(uint32_t i=0;i<m_length;i++){
    m_rbytes[m_length-i-1]=m_bytes[i];
  }
  return m_rbytes.data();
}
  
uint32_t Decoder::GetLength(){
  return m_length;
}
  
vector<Frame*> & Decoder::GetFrames(){
  return m_frames;
}

void Decoder::Encode(){
  //@FIXME we need to multiplex the service 1/N
  uint32_t pos=0;
  for(uint32_t i=0;i<m_frames.size();i++){
    pos+=m_frames[i]->Pack(&m_bytes[pos]);
  }
  m_length=pos;
}

void Decoder::Decode(bool verbose){
  const bool Vakhtang64BitCheck=true;

  if(verbose) cout << "Decoder::Decode Starting Decoding in mode: "<<m_mode << endl;
  ClearFrames();
  uint32_t pos=0;
  uint32_t nb=0;
  uint32_t tnb=m_length;
 
  if(m_mode==MODE_DATA){
    while(pos<tnb){
      if(verbose)cout << "Decoder::Decode: Decode frame at: " << (pos) << endl;
      // Frame * frame=0;
      //if     ((nb=m_fI->UnPack(&m_bytes[pos],tnb-pos))>0){frame=m_fI; m_fI=new IdleFrame();}
      //else if((nb=m_fR->UnPack(&m_bytes[pos],tnb-pos))>0){frame=m_fR; m_fR=new RegisterFrame();}
      //else 
      // auto seg1 = chrono::steady_clock::now();
      if((nb=m_fD->UnPack(&m_bytes[pos],tnb-pos))>0){} //frame=m_fD; m_fD=new DataFrame(); m_fD->EnableChnID(m_enable_cid);}
      else{
	nb=0;
	if(verbose || ((Vakhtang64BitCheck) && ((tnb-pos)%8==4))) cout << "Decoder::Decode: Cannot decode byte sequence: "
			 << "0x" << hex << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << dec 
					       << " at index: " << pos << "Data Size: "<<tnb
			 << " ...Gathering more data" << endl;
	//continue;
	break;
      }
      // auto seg2 = chrono::steady_clock::now();
      
      // std::cout<<"Unpacking Data: "<<chrono::duration_cast<chrono::nanoseconds>(seg2 - seg1).count()<<" ns "<<chrono::duration_cast<chrono::microseconds>(seg2 - seg1).count()<<" micros "<<std::endl;

      // m_frames.push_back(frame);
      if( (8-nb%8)%8 != 0) std::cout<<"Decoder::Decoder: Wierd Posittion Count"<<std::endl;
      if( (pos-nb%8)%8 != 0) std::cout<<"Decoder::Decoder: Wierd Package size"<<std::endl;
      pos+=nb + (8-nb%8)%8; // (Here the Mod part is to make sure we follow 64b packet structure as new frames always comes in 64b packets)
    }
    m_frames.push_back(m_fD);
    m_fD=new DataFrame(); 
    m_fD->EnableChnID(m_enable_cid);

    if(verbose) std::cout<<"Decoder::Decode: Sequence stopped at: "<<pos<<"/"<<tnb<<std::endl;
    //Whatever something not decoded keep the delete the rest 
    for(uint32_t i=pos;i<m_length;i++){
      m_bytes[i-pos]=m_bytes[i];
    }
    m_length-=pos;
    if(verbose) std::cout<<"Decoder::Decode: New length is "<<m_length<<std::endl;

  }
  else if(m_mode==MODE_REGISTER){
    while(pos<tnb){
      if(verbose)cout << "Decoder::Decode: Decode frame at: " << (pos) << endl;
      Frame * frame=0;
      if ((nb=m_fR->UnPack(&m_bytes[pos],tnb-pos))>0){
        if(verbose) cout << "Decoder::Decode new register frame" << endl;
	// if(m_fR->GetAuroraCode()!=0xD2) {
	//   cout<<"Found new register frame "<<m_fR->ToString();
	
	// }
	frame=m_fR;
	m_fR=new RegisterFrame();
      }
      else{
        cout << __PRETTY_FUNCTION__ << "Cannot decode byte sequence: "
            << "0x" << hex << setw(2) << setfill('0') << (uint32_t) m_bytes[pos] << dec
            << " at index: " << pos
            << " ...skipping" << endl;
        pos++;
        continue;
      }
      
      m_frames.push_back(frame);
      pos+=nb;
      if(verbose) {cout << "Decoder::Decode pos=" << pos << " tnb=" << tnb << endl;}

      for(uint32_t i=pos;i<m_length;i++){
	m_bytes[i-pos]=m_bytes[i];
      }
      m_length-=pos;
      if(verbose) std::cout<<"Decoder::Decode: New length is "<<m_length<<std::endl;


    }
  }


}
