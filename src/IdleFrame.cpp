#include "RD53BEmulator/IdleFrame.h"

#include <sstream>
#include <iomanip>

using namespace std;
using namespace RD53B;

IdleFrame::IdleFrame(){
  m_aheader = 0x2;
  m_kcode = 0x00;
  m_acode = 0x00;
}

IdleFrame::IdleFrame(IdleFrame *copy){
  m_aheader = copy->m_aheader;
  m_kcode = copy->m_kcode;
  m_acode = copy->m_acode;
}

IdleFrame::~IdleFrame(){}

IdleFrame * IdleFrame::Clone(){
  IdleFrame * copy=new IdleFrame();
  copy->m_aheader = m_aheader;
  copy->m_kcode = m_kcode;
  copy->m_acode = m_acode;
  return copy;
}

string IdleFrame::ToString(){
  ostringstream os;
  os << "IdleFrame " << hex
     << "Code: 0x" << (uint32_t)m_acode << " "
     << dec;
  return os.str();
}

uint32_t IdleFrame::UnPack(uint8_t * bytes, uint32_t maxlen){
  if(bytes[0]!=m_kcode) return 0;
  m_acode=bytes[1];
  return 8;
}

uint32_t IdleFrame::Pack(uint8_t * bytes){
  bytes[0] = m_kcode;
  bytes[1] = m_acode;
  for(uint32_t i=2;i<8;i++){bytes[i]=0;}
  return 8;
}


uint32_t IdleFrame::GetType(){
  return Frame::IDLE;
}

void IdleFrame::SetAuroraCode(uint32_t code){
  m_acode=code&0xFF;
}

uint32_t IdleFrame::GetAuroraCode(){
  return m_acode;
}

void IdleFrame::SetCC(bool enable){
  if(enable) m_acode |= kCC;
  else       m_acode &= ~kCC;
}

bool IdleFrame::GetCC(){
  return (m_acode&kCC);
}

void IdleFrame::SetCB(bool enable){
  if(enable) m_acode |= kCB;
  else       m_acode &= ~kCB;
}

bool IdleFrame::GetCB(){
  return (m_acode&kCB);
}

void IdleFrame::SetNR(bool enable){
  if(enable) m_acode |= kNR;
  else       m_acode &= ~kNR;
}

bool IdleFrame::GetNR(){
  return (m_acode&kNR);
}

void IdleFrame::SetSA(bool enable){
  if(enable) m_acode |= kSA;
  else       m_acode &= ~kSA;
}

bool IdleFrame::GetSA(){
  return (m_acode&kSA);
}


