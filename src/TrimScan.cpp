#include "RD53BEmulator/TrimScan.h"
#include "RD53BEmulator/Tools.h"
#include "RD53BEmulator/SCurveFitter.h"

#include <iostream>
#include <sstream>
#include <chrono>

#include "TCanvas.h"
#include "TROOT.h"
#include "TMarker.h"
#include "TStyle.h"

using namespace std;
using namespace RD53B;

TrimScan::TrimScan(){
  m_ntrigs         = 50; 
  m_TriggerLatency = 60;

  m_par0_Min   = 0;
  m_par0_Max   =15;
  m_par0_nSteps=15;
  m_par1_Min   = 0;
  m_par1_Max   =15;
  m_par1_nSteps=15;
  m_par0_Label = "Trim A";
  m_par1_Label = "Trim D";
}

TrimScan::~TrimScan(){}

void TrimScan::PreRun(){

  m_logFile << "\"m_ntrigs\": "         << m_ntrigs << endl
	    << "\"m_TriggerLatency\": " << m_TriggerLatency << endl
	    << "\"m_par0_Min\": "       << m_par0_Min << endl
	    << "\"m_par0_Max\": "       << m_par0_Max << endl
	    << "\"m_par0_nSteps\": "    << m_par0_nSteps << endl
	    << "\"m_par1_Min\": "       << m_par1_Min << endl
	    << "\"m_par1_Max\": "       << m_par1_Max << endl
	    << "\"m_par1_nSteps\": "    << m_par1_nSteps << endl;
  
  if(m_verbose){
    cout << "TrimScan: m_ntrigs = " << m_ntrigs << endl;
    cout << "TrimScan: m_TriggerLatency = " << m_TriggerLatency << endl;
  }

  int totStep=m_par0_nSteps*m_par1_nSteps;
  float step0=(m_par0_Max-m_par0_Min)/m_par0_nSteps;
  float step1=(m_par1_Max-m_par1_Min)/m_par1_nSteps;

  for(auto fe : GetFEs()){

    fe->BackupConfig();

    cout << "AnalogScan: Configure Analog injection" << endl;
    fe->GetConfig()->SetField(Configuration::CalMode,1);
    fe->GetConfig()->SetField(Configuration::Latency,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::PIX_BCAST_EN,0);
    fe->GetConfig()->SetField(Configuration::PIX_AUTO_ROW,0);
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,4000);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,200);
    fe->GetConfig()->SetField(Configuration::InjVcalRange,0);
    
    fe->WriteGlobal();
    Send(fe);
       
    cout << "TrimScan: Mask all" << endl;
    fe->DisableAll();
    ConfigurePixels(fe);

    //Create the histograms for the threshold scan of each pixel
    cout << "TrimScan: Create histograms per pixel for " << fe->GetName() << endl;
    
    m_occ[fe->GetName()]=new TH2I*[totStep];
    for(int iStep=0; iStep<totStep; iStep++){
      stringstream iStep_ss;
      iStep_ss << iStep;
      m_occ[fe->GetName()][iStep] = new TH2I(("occ_"+fe->GetName()+"_"+iStep_ss.str()).c_str(),
					     (fe->GetName()+", Occupancy; Column; Row").c_str(),
					     Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    }
    
    m_results[fe->GetName()] = new TH2F(("TrimScan_"+fe->GetName()).c_str(),(fe->GetName()+";"+m_par0_Label+";"+m_par1_Label).c_str(),
					m_par0_nSteps+1, m_par0_Min-step0/2, m_par0_Max+step0/2,
					m_par1_nSteps+1, m_par1_Min-step1/2, m_par1_Max+step1/2);
  }

  GetMasker()->SetRange(0,0,400,384);
  GetMasker()->SetShape(8,2);
  GetMasker()->SetFrequency(10);
  GetMasker()->SetVerbose(false);
  GetMasker()->Build();
}

void TrimScan::Run(){

  //Prepare the trigger
  cout << "TrimScan: Prepare the TRIGGER" << endl;
  PrepareTrigger(m_TriggerLatency-5, 4, true, true, true, true);
  
  for(auto fe : GetFEs()){ // RD53B is always a bit noisy at initialisation so clearing hits
    if(!fe->HasHits()) continue;
    fe->ClearHits();
  }

  GetMasker()->GetStep(0);
  ConfigMask();

  float step0=(m_par0_Max-m_par0_Min)/m_par0_nSteps;
  float step1=(m_par1_Max-m_par1_Min)/m_par1_nSteps;
  int count=0;
  for(uint32_t iStep0=0; iStep0<m_par0_nSteps+1; iStep0++){
    float val0=(int)(m_par0_Min+step0*iStep0);
    
    for(uint32_t iStep1=0; iStep1<m_par1_nSteps+1; iStep1++){
      count+=1;
      float val1=(int)(m_par1_Min+step1*iStep1);

      cout << "TrimScan: par0= " << val0 << " , val1= " << val1 << endl;
    
      for(auto fe : GetFEs()){
	if ( fe->GetName()!="rd53b_quad_4" && fe->GetName()!="rd53b_quad_2"  ) {
	  //if ( fe->GetName()!="rd53b_quad_4" ) {
	  fe->GetConfig()->SetField(Configuration::DAC_SHUNT_ANA,val0);
	  fe->GetConfig()->SetField(Configuration::DAC_SHUNT_DIG,val1);
	}
	fe->WriteGlobal();
	Send(fe);
      }
      	

      uint32_t ExpectedHits=0;
      for(auto fe : GetFEs()){ 
	ExpectedHits+=m_ntrigs*fe->GetNPixelsEnabled();
      }

      
      //Trigger 
      for(uint32_t trig=0;trig<m_ntrigs; trig++) {
	std::this_thread::sleep_for(std::chrono::microseconds(100));
	Trigger();
      }
      auto start = chrono::steady_clock::now();
      
      //Readout the hit histogram hit
      bool LastLoop=false;
      uint32_t nhits=0;
      uint32_t nRhits=0;
      uint32_t nGoodHists=0;
      
      vector<Hit*> hits;
      int nH=0;
      while(true){
	for(auto fe : GetFEs()){

	  hits=fe->GetHits(10000);
	  nH=hits.size();
	  if(nH>0) start = chrono::steady_clock::now();
	  if (nH==0) {
	    this_thread::sleep_for(chrono::milliseconds( 1 ));
	
	  } else {
	    for (int h=0; h<nH; h++) {
	
	      Hit* hit = hits.at(h);
	      if(hit==0) continue;
	      LastLoop=false;
	      if(hit->GetTOT()>0){
		if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
		  //cout << "Hit is not expected in the current mask step: " << hit->ToString() << endl;
		} else {
		  nGoodHists++;
		  //m_occ[fe->GetName()][count]->Fill(hit->GetCol(),hit->GetRow());
		  m_results[fe->GetName()]->Fill(val0,val1);
		  nRhits++;
		}
		nhits++;
	      }
	    }
	  }
	  fe->ClearHits(nH);
	}
	auto end = chrono::steady_clock::now();
	uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
	if (LastLoop) break;
	if (nhits>100000) {
	  cout << " too many hits" << endl;
	  break;
	}
	if(ms>100 or nGoodHists>=ExpectedHits ){LastLoop=true;}
      }
      cout << "TrimScan: NHeaders in step: " << nhits <<" / "<<ExpectedHits<<" NHits: "<<nRhits<<" / "<<ExpectedHits<< endl;
      
    }
  }
  for(auto fe : GetFEs()){
    //m_results[fe->GetName()]->SetMaximum(m_ntrigs*GetMasker()->GetPixels().size());
    m_results[fe->GetName()]->Scale(100./( (float)(m_ntrigs*GetMasker()->GetPixels().size()) ) );
    m_results[fe->GetName()]->SetMaximum(105);
    m_results[fe->GetName()]->SetMinimum( 85);
  }
  UnconfigMask();
}

void TrimScan::Analysis(){
  gStyle->SetOptStat(0);
  gStyle->SetPaintTextFormat("1.2g");
  TCanvas* can=new TCanvas("plot","plot",800,600);
  for(auto fe : GetFEs()){
    cout << "TrimScan: Restore the original threshold value" << endl;
    
    fe->RestoreBackupConfig();

    int par0=fe->GetConfig()->GetField(Configuration::DAC_SHUNT_ANA)->GetValue();
    int par1=fe->GetConfig()->GetField(Configuration::DAC_SHUNT_DIG)->GetValue();
     
    TMarker* m=new TMarker(par0,par1,1);
    m->SetMarkerStyle(20);
    m->SetMarkerSize(2);

    /*
    for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
      for(uint32_t col = 0; col < Matrix::NUM_COLS; col++){
	//	m_tot[fe->GetName()]->SetBinContent(col+1,row+1,m_tot[fe->GetName()]->GetBinContent(col+1,row+1)/m_ntrigs);
        if (m_occ[fe->GetName()]->GetBinContent(col+1,row+1) == m_ntrigs){
          m_enable[fe->GetName()]->SetBinContent(col+1,row+1,1);
	  fe->SetPixelEnable(col,row,true);
	}
        else fe->SetPixelEnable(col,row,false);
      }
    }
    */

    /*
    m_occ[fe->GetName()]->Write();
    m_occC[fe->GetName()]->Write();
    m_tot[fe->GetName()]->Write();
    m_enable[fe->GetName()]->Write();
    m_tid[fe->GetName()]->Write();
    m_ttag[fe->GetName()]->Write();
    m_bcid[fe->GetName()]->Write();
    m_ttagmap[fe->GetName()]->Write();
    m_bcidmap[fe->GetName()]->Write();
    */

    m_results[fe->GetName()]->SetTitle( (fe->GetName()+" Occ").c_str() );
    m_results[fe->GetName()]->Draw("COLZTEXT");
    m->Draw("SAMEP");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__Summary.pdf").c_str() );

    //m_occC[fe->GetName()]->Draw("COLZ");
    //can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCCcut.pdf").c_str() );    

    /*
    delete m_occ[fe->GetName()];
    delete m_tot[fe->GetName()];
    delete m_enable[fe->GetName()];
    delete m_tid[fe->GetName()];
    delete m_ttag[fe->GetName()];
    delete m_bcid[fe->GetName()];
    delete m_ttagmap[fe->GetName()];
    delete m_bcidmap[fe->GetName()];
    delete m_hits[fe->GetName()];
    */
    delete m;
  }
  delete can;

  
}
