#include "RD53BEmulator/Address.h"
#include <sstream>
#include <iomanip>

using namespace std;
using namespace RD53B;

Address::Address(){}

Address::~Address(){}

string Address::ToString(){
  ostringstream os;
  os << "Address "
     << " Pixel:"
     << " col=" << m_col << ", "
     << " row=" << m_row << ", "
     << " Region:"
     << " core_col=" << m_core_col << ", "
     << " core_row=" << m_core_row << ", "
     << " region=" << m_region << ", "
     << " pair=" << m_pair;
     return os.str();
}

void Address::unpack(){
  m_core_row = m_row >> 3;
  m_core_col = m_col >> 3;
  m_region = ((m_row & 0x7) << 1) | (m_col & 0x1);
  m_pair = (m_col & 0x7) >> 2;
}

void Address::pack(){
  m_row = (m_core_row << 3) | ((m_region >> 1) & 0x7);
  m_col = (m_core_col << 3) | ((m_region << 2) & 0x4) | ((m_pair << 1) & 0x2);
}

void Address::SetPixel(uint32_t col, uint32_t row){
  m_col=col;
  m_row=row;
  unpack();
}

void Address::SetRow(uint32_t row){
  m_row=row;
  unpack();
}

void Address::SetCol(uint32_t col){
  m_col=col;
  unpack();
}

void Address::SetCore(uint32_t core_col, uint32_t core_row, uint32_t region, uint32_t pair){
  m_core_col=core_col;
  m_core_row=core_row;
  m_region=region;
  m_pair=pair;
  pack();
}

void Address::SetCoreRow(uint32_t core_row){
  m_core_row=core_row;
  pack();
}

void Address::SetCoreCol(uint32_t core_col){
  m_core_col=core_col;
  pack();
}

void Address::SetRegion(uint32_t region){
  m_region=region;
  pack();
}

void Address::SetRegister1(uint32_t register1){
  m_core_col=(register1>>2)&0x3F;
  m_region&=~0x1;
  m_region|=(register1>>1)&0x1;
  m_pair=register1 & 0x1;
}

void Address::SetRegister2(uint32_t register2){
  m_core_row=(register2>>3)&0x3F;
  m_region&=~0xE;
  m_region|=(register2<<1)&0xE;
}

void Address::SetPair(uint32_t pair){
  m_pair=pair;
  pack();
}

uint32_t Address::GetRow(){
  return m_row;
}

uint32_t Address::GetCol(){
  return m_col;
}

uint32_t Address::GetCoreRow(){
  return m_core_row;
}

uint32_t Address::GetCoreCol(){
  return m_core_col;
}

uint32_t Address::GetRegion(){
  return m_region;
}

uint32_t Address::GetPair(){
  return m_pair;
}

uint32_t Address::GetRegister1(){
  uint32_t register1 = 0;
  register1 |= m_pair & 0x1;
  register1 |= (m_region & 0x1)<<1;
  register1 |= (m_core_col & 0x3F)<<2;
  return register1;
}

uint32_t Address::GetRegister2(){
  uint32_t register2 = 0;
  register2 |= (m_region & 0xE)>>1;
  register2 |= (m_core_row & 0x3F)<<3;
  return register2;
}

