#include "RD53BEmulator/Masker.h"
#include "RD53BEmulator/FrontEnd.h"
#include <set>
#include <algorithm>
#include <iostream>

using namespace std;
using namespace RD53B;

Masker::Masker(){
  m_px0=0;
  m_pxN=Matrix::NUM_COLS;
  m_py0=0;
  m_pyN=Matrix::NUM_ROWS;
  m_size_x=8;
  m_size_y=2;
  m_gap=8;
  m_freq=1;
  m_cur_step=0;
  m_num_steps=0;
  m_verbose=true;
  m_doPrecisionToT=false;
  m_auraType=Masker::Basic;
}

Masker::~Masker(){
  m_double_cols.clear();
  m_core_cols.clear();
  m_pixels.clear();
  m_auraPixels.clear();
}

void Masker::SetVerbose(bool verbose){
  m_verbose=verbose;
}

void Masker::SetRange(uint32_t px0, uint32_t py0, uint32_t pxN, uint32_t pyN){
  m_px0=px0;
  m_pxN=pxN;
  m_py0=py0;
  m_pyN=pyN;
}

void Masker::SetShape(uint32_t size_x, uint32_t size_y){
  if(size_x==0)  size_x=1;
  if(size_x> (m_pxN-m_px0)) size_x=(m_pxN-m_px0);
  if(size_y==0)  size_y=1;
  if(size_y> (m_pyN-m_py0)) size_y=(m_pyN-m_py0);
  m_size_x = size_x;
  m_size_y = size_y;
}

void Masker::SetFrequency(uint32_t freq){
  m_freq=freq;
}

void Masker::SetGap(uint32_t gap){
  m_gap=gap;
}




void Masker::SetMaskAuora(Masker::MaskAuraType Type ){
  m_auraType = Type;
}

Masker::MaskAuraType Masker::GetMaskAuora(){
  return m_auraType;
}






void Masker::Build(){
  if(m_verbose){cout << "Masker::Build" << endl;}
  m_num_steps_y = (m_pyN-m_py0)/(float)m_size_y;
  m_num_steps_x = (m_pxN-m_px0)/(float)m_size_x;
  if(m_num_steps_y<1) m_num_steps_y=1;
  if(m_num_steps_x<1) m_num_steps_x=1;
  m_num_steps=m_num_steps_y*m_num_steps_x/m_freq;
  m_cur_step=0;
}

uint32_t Masker::GetNumSteps(){
  return m_num_steps;
}

void Masker::GetStep(uint32_t step){
  if(m_verbose) {cout << "Masker::GetStep" << endl;}
  m_pixels.clear();
  m_auraPixels.clear();
  m_double_cols.clear();
  m_core_cols.clear();
  set<uint32_t> ccs;
  set<uint32_t> dcs;
  for(uint32_t f=0;f<m_freq;f++){
    uint32_t step2=step+f*m_num_steps;
    uint32_t py1 = m_size_y*(step2%m_num_steps_y)+m_py0;
    uint32_t px1 = m_size_x*(step2/m_num_steps_y)+m_px0;
    if(m_verbose) cout << "Masker::GetStep start at px1:" << px1 << ", py1:" << py1 << endl;
    for(uint32_t dy=0; dy<m_size_y; dy++){
      for(uint32_t dx=0; dx<m_size_x; dx++){
	if(m_verbose) cout << "Masker::GetStep add pixel:" << (px1+dx) << ", " << (py1+dy) << endl;
	m_pixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx,py1+dy));
	dcs.insert((px1+dx)/2);
	ccs.insert((px1+dx)/8);

	if (m_auraType==Masker::Cross){
	  if( px1+dx +1 < m_pxN){
	    m_auraPixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx +1, py1+dy));
	    dcs.insert((px1+dx+1)/2);
	    ccs.insert((px1+dx+1)/8);
	  }
	  if( px1+dx  > 0) {
	    m_auraPixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx -1, py1+dy));
	    dcs.insert((px1+dx-1)/2);
	    ccs.insert((px1+dx-1)/8);
	  }
	  if( py1+dy +1 < m_pyN){
	    m_auraPixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx , py1+dy +1));
	  }
	  if( py1+dy  > 0){
	    m_auraPixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx , py1+dy -1));
	  }
	}

	
      }
    }
  }
  for(auto cc : ccs){
    if(m_verbose) cout << "Masker::GetStep add cc:" << cc << endl;
    m_core_cols.push_back(cc);
  }
  for(auto dc : dcs){
    if(m_verbose) cout << "Masker::GetStep add dc:" << dc << endl;
    m_double_cols.push_back(dc);
  }
}

void Masker::GetNextStep(){
  if(m_cur_step>=m_num_steps) return;
  m_cur_step++;
  GetStep(m_cur_step);
}

vector<uint32_t> & Masker::GetCoreColumns(){
  return m_core_cols;
}

vector<uint32_t> & Masker::GetDoubleColumns(){
  return m_double_cols;
}

vector<pair<uint32_t,uint32_t> > & Masker::GetPixels(){
  return m_pixels;
}
vector<pair<uint32_t,uint32_t> > & Masker::GetAuraPixels(){
  return m_auraPixels;
}


// void Masker::MaskFE(FrontEnd*  fe, bool enable){
//   for(auto cc : GetCoreColumns()){
//     if(m_verbose){cout<< "Masker::MaskFE Enabling core column: "<<cc<<std::endl;}
//     //fe->EnableCoreColumn(cc,enable);
//     fe->EnableCalCoreColumn(cc,enable);
//     //fe->EnableHitOrCoreColumn(cc,enable);
//     if( m_doPrecisionToT ) fe->EnablePrecisionToTCoreColumn(cc,enable);    
//   }

//   //we only enable injection for Aura pixels, we don't enable the pixel them selves
//   //These are not using in the default scans and practically only used for disconnected bump and cross-talk scans. 
//   for(auto pixel : GetAuraPixels()){
//     if(m_verbose){cout<< "Masker::MaskFE Enabling Pixel Col "<<pixel.first<<" Row "<<pixel.second<<std::endl;}

//     fe->SetPixelEnable(pixel.first, pixel.second,0); //This is always set to false in Cross talk aura
//     fe->SetPixelInject(pixel.first, pixel.second,enable);
//     fe->SetPixelHitbus(pixel.first, pixel.second,0);//This is always set to false in Cross talk aura

//   }

//   for(auto pixel : GetPixels()){
//     if(m_verbose){cout<< "Masker::MaskFE Enabling Pixel Col "<<pixel.first<<" Row "<<pixel.second<<std::endl;}
//     fe->SetPixelEnable(pixel.first, pixel.second,enable);
//     if (m_auraType==Masker::Cross) // In cross talk scans, we don't inject in the main pixel it-self but around the pixel (Aura pixels get injected)
//       fe->SetPixelInject(pixel.first, pixel.second,0);  //This is always set to false in Cross talk aura
//     else
//       fe->SetPixelInject(pixel.first, pixel.second,enable);
//     fe->SetPixelHitbus(pixel.first, pixel.second,enable);
//   }

//   //if(m_verbose){cout << "Masker::MaskFE ProcessCommands" << endl;}
//   fe->ProcessCommands();
  
// }

// void Masker::UnMaskFE(FrontEnd* fe){
//   MaskFE(fe,false);
// }

bool Masker::Contains(uint32_t col, uint32_t row){
  pair<uint32_t,uint32_t> pp(col,row);
  return (std::find(m_pixels.begin(), m_pixels.end(), pp) != m_pixels.end());
}
