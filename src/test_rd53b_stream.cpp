#include "RD53BEmulator/Stream.h"

#include <iostream>
#include <ctime>
#include <vector>
#include <cmdl/cmdargs.h>
#include <chrono>
#include <json.hpp>
#include <fstream>

using json=nlohmann::basic_json<std::map, std::vector, std::string, bool, std::int64_t, std::uint64_t, float>;
using namespace std;
using namespace RD53B;

string bytes_to_string(uint8_t * bytes, uint32_t len){
  ostringstream os;
  os << hex;
  for(uint32_t i=0;i<len;i++){os << setfill('0') << setw(2) << (uint32_t) bytes[i];}
  os << dec;
  return os.str();
}

string bytes_to_bitstring(uint8_t * bytes, uint32_t len){
  ostringstream os;
  for(uint32_t i=0;i<len;i++){
    for(uint32_t b=0;b<8;b++){
      os << ((bytes[i]>>(7-b))&1);
    }
  }
  return os.str();
}

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# test_rd53b_stream             #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cStream('s',"stream","stream","stream of bytes to decode"); 
  CmdArgInt  cCid('c',"cid","enable", "0=disable,1=enable the channel id, default=0");
  CmdArgInt  cTot('t',"tot","enable", "0=disable,1=enable the hit tot, default=1");
  CmdArgInt  cEnc('e',"enc","enable", "0=disable,1=enable hitmap encoding, default=1");
  CmdArgBool cPerf('p',"perf","enable performance testing");
  CmdLine cmdl(*argv,&cVerbose,&cStream,&cCid,&cTot,&cEnc,&cPerf,NULL);

  cCid=0;
  cTot=0;
  cEnc=0;
  cPerf=0;

  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  //create a Stream
  cout << "Create a new stream" << endl;
  Stream * stream = new Stream();
  stream->SetVerbose(cVerbose);
  stream->EnableEnc(cEnc);
  stream->EnableTot(cTot);
  stream->EnableChipId(cCid);

  //create the byte array
  vector<uint8_t> bytes;
    
  if(cStream){
    //convert the cmdarg into a string
    string str(cStream);
    //check the base and the starting position
    uint32_t pos=0;
    uint32_t base=16;
    uint32_t skip=2;
    if     (str.find("0x")!=str.npos){base=16;pos=2;skip=2;}
    else if(str.find("0b")!=str.npos){base= 2;pos=2;skip=8;}
    
    //fill the byte array
    for(uint32_t i=pos;i<str.length();i+=skip){
      uint8_t val=stoul(str.substr(i,skip).c_str(),NULL,base);
      //cout << hex << (uint32_t) val << dec << endl;
      bytes.push_back(val);
    }
    cout << "Stream: 0x" << bytes_to_string(&bytes[0],bytes.size()) << " 0b" << bytes_to_bitstring(&bytes[0],bytes.size()) << endl;

    cout << "Decode" << endl;
    stream->UnPack(&bytes[0],bytes.size());

    cout << "To String" << endl;
    cout << stream->ToString() << endl;
  
    cout << "Encode" << endl;
    stream->Pack(&bytes[0]);

    cout << "To String" << endl;
    cout << stream->ToString() << endl;

    cout << "Stream: 0x" << bytes_to_string(&bytes[0],bytes.size()) << endl;
	
  }

  if(cPerf){
  
    uint32_t ntries=1E4;
    uint32_t qrow=1;
    uint32_t qcol=1;
    uint32_t tag=0xF;
    uint64_t tot=0xFFFFFFFFFFFFFFFF;
    bytes.resize(1000,0);
     
    json out=json::array();

    cout << "Decoding performance" << endl;
    
    for(uint32_t pat=1;pat<=0xFFFF;pat++){
      cout << "New pattern" << endl;
      stream->Clear();
      stream->AddHit(tag,qrow,qcol,pat,tot);
      uint32_t nbits=stream->Pack(&bytes[0]);
      uint32_t nbytes=ceil(nbits/64.)*8;
      cout << stream->ToString() << endl;
      cout << "Stream: 0x" << bytes_to_string(&bytes[0],nbytes) << " 0b" << bytes_to_bitstring(&bytes[0],nbytes) << endl;
    
      //decoding performance
      auto start = chrono::steady_clock::now();
      for(uint32_t c=0;c<ntries;c++){
	stream->UnPack(&bytes[0],bytes.size());
      }
      auto stop = chrono::steady_clock::now();
      double duration = chrono::duration_cast<chrono::microseconds>(stop - start).count();
      //double perf=duration/(float)ntries;
      cout << "Pattern: 0x" << hex << setw(4) << setfill('0') << pat << dec << ", "
	   << "NTries: " << ntries << ", "
	   << "Time [us]: " << duration << " "
	   << "Encoded: 0x" << bytes_to_string(&bytes[0],nbytes)
	   << endl;
      json res;
      res["Pattern"]=pat;
      res["Ntries"]=ntries;
      res["Len"]=nbits;
      res["Time_us"]=duration;
      out.push_back(res);     
    }
    cout << "Saving performance file: test_rd53b_stream.txt" << endl;
    ofstream fw("test_rd53b_stream.txt");
    fw << setw(4) << out;
    fw.close();

   
  }

  cout << "Cleaning the house" << endl;
  delete stream;
  
  cout << "Have a nice day" << endl;
  return 0;
}

