#include "RD53BEmulator/Encoder.h"

#include <iostream>
#include <ctime>
#include <vector>
#include <iomanip>
#include <fstream>
#include <bitset>
#include <cmdl/cmdargs.h>

using namespace std;
using namespace RD53B;

int main(int argc, char ** argv) {

  cout << "#################################" << endl
       << "# encode_rd53b_commands         #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose(   'v',"verbose","turn on verbose mode");
  CmdArgStr  cFilePath(  'f',"file","path","file to parse", CmdArg::isREQ);

  CmdLine cmdl(*argv,&cVerbose,&cFilePath,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);


  cout << "Create new encoder object" << endl;
  Encoder *encoder=new Encoder();

  cout << "Loop over file contents" << endl;

  ifstream fr(cFilePath);
  string line; 
  while(getline(fr,line)){
    if(line.length()==0){break;}
    cout << line << endl;
    if(fr.eof())break;
    encoder->AddCommand(line);
  }
  fr.close();

  cout << "Encode" << endl;
  encoder->Encode();

  cout << "Byte stream: 0x" << encoder->GetByteString() << endl;

  cout << "Contents" << endl;
  for(uint32_t i=0;i<encoder->GetCommands().size();i++){
    cout << encoder->GetCommands().at(i)->ToString() << endl;
  }

  delete encoder;

  return 0;
}
