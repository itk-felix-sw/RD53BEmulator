
#include "RD53BEmulator/DisconnectedBumpScan.h"
#include "RD53BEmulator/Masker.h"
#include "RD53BEmulator/Tools.h"
#include "TCanvas.h"
#include "TStyle.h"

#include <iostream>
#include <chrono>

using namespace std;
using namespace RD53B;

DisconnectedBumpScan::DisconnectedBumpScan(){
  m_ntrigs = 100;
  m_TriggerLatency = 60;
  m_colMin = 0; 
  m_colMax = 400;
}

DisconnectedBumpScan::~DisconnectedBumpScan(){}

void DisconnectedBumpScan::PreRun(){

  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
  m_logFile << "\"m_TriggerLatency\": " << m_TriggerLatency << endl;
  
  if(m_verbose){
    cout << "DisconnectedBumpScan: m_ntrigs = " << m_ntrigs << endl;
    cout << "DisconnectedBumpScan: m_TriggerLatency = " << m_TriggerLatency << endl;
  }

  for(auto fe : GetFEs()){

    fe->BackupConfig();

    cout << "DisconnectedBumpScan: Configure Analog injection" << endl;
    fe->GetConfig()->SetField(Configuration::CalMode,0); // Analog Mode
    //fe->GetConfig()->SetField(Configuration::CalAnaMode,1); // Analog fine delay
    fe->GetConfig()->SetField(Configuration::Latency,m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::PIX_BCAST_EN,0);
    fe->GetConfig()->SetField(Configuration::PIX_AUTO_ROW,0);

    //fe->SetGlobalThreshold(Pixel::Diff,Tools::chargeToThr(100,2));

    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,4000);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,200);
    fe->GetConfig()->SetField(Configuration::InjVcalRange,0);
    //fe->GetConfig()->SetField(Configuration::DiffTh1M,0);


    fe->WriteGlobal();
    Send(fe);
       
    cout << "DisconnectedBumpScan: Mask all" << endl;
    fe->DisableAll();
    ConfigurePixels(fe);
    //Send(fe);

    m_occ[fe->GetName()]    =new TH2I(("occ_"+fe->GetName()).c_str(),"Occupancy map;Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS,0, Matrix::NUM_ROWS);
    m_occC[fe->GetName()]   =new TH2I(("occC_"+fe->GetName()).c_str(),"Occupancy map (with cut);Column;Row;Number of hits", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS,0, Matrix::NUM_ROWS);

    m_tot[fe->GetName()]    =new TH1I(("tot_"+fe->GetName()).c_str(),(fe->GetName()+";TOT").c_str(), 16, 0, 16);
    m_enable[fe->GetName()] =new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_tid[fe->GetName()]    =new TH1I(("tid_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger ID").c_str(), 16, 0, 16);
    m_ttag[fe->GetName()]   =new TH1I(("ttag_"+fe->GetName()).c_str(),(fe->GetName()+";Trigger Tag").c_str(), 16, 0, 16);
    m_bcid[fe->GetName()]   =new TH1I(("bcid_"+fe->GetName()).c_str(),(fe->GetName()+";BCID").c_str(),0xFFFF+1,-0.5,0xFFFF+0.5);
    m_ttagmap[fe->GetName()]=new TH2I(("ttagmap_"+fe->GetName()).c_str(),"TTag map;Column;Row;TTag accumulated", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_bcidmap[fe->GetName()]=new TH2I(("bcidmap_"+fe->GetName()).c_str(),"BCID map;Column;Row;BCID accumulated", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_hits[fe->GetName()]   =new HitTree(("hits_"+fe->GetName()).c_str(),fe->GetName());

  }

  //GetMasker()->SetRange(40,40,48,42);
  //GetMasker()->SetRange(0,0,200,200);
  //GetMasker()->SetRange(0,0,10,10);
  // //GetMasker()->SetShape(1,1); //8,2

  GetMasker()->SetVerbose(false);
  GetMasker()->SetRange(0,0,400,384);
  //GetMasker()->SetRange(0,0,100,100);
  GetMasker()->SetShape(1,1);
  GetMasker()->SetFrequency(300); //50
  GetMasker()->SetMaskAuora(Masker::Cross); // This enables the Cross injection pattern
  GetMasker()->Build();

}

void DisconnectedBumpScan::Run(){

  //Prepare the trigger
  cout << "DisconnectedBumpScan: Prepare the TRIGGER" << endl;
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // This is needed for diff in the disconnected bump config.
  PrepareTrigger(m_TriggerLatency-5, 4, true, true, true, true);
  
  //cout << "DisconnectedBumpScan: Create the mask" << endl;
  for(auto fe : GetFEs()){ // RD53B is always a bit noisy at initialisation so clearing hits
    if(!fe->HasHits()) continue;
    fe->ClearHits(); 
  }

  //GetMasker()->GetStep(0);
  //ConfigMask();

  for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
    GetMasker()->GetStep(st);
    
    if (st==0) {
      for(auto fe : GetFEs()){ 
	if(!fe->HasHits()) continue;
	fe->ClearHits();
      }
    }

    //Do the mask step
    ConfigMask();
    //std::this_thread::sleep_for(std::chrono::milliseconds(50)); //100
    
    //accumulate hits
    uint32_t expected=GetMasker()->GetPixels().size();
    
    //histogram hit
    bool LastLoop=false;
    uint32_t nhits=0;
    uint32_t nRhits=0;
    uint32_t nGoodHists=0;


    //Trigger and read-out
    for(uint32_t trig=0;trig<m_ntrigs; trig++) Trigger();

    auto start = chrono::steady_clock::now();
    
    while(true){
	
      for(auto fe : GetFEs()){
	if(!fe->HasHits()) continue;
	LastLoop=false;
	if ( nhits%1000==0 ) start = chrono::steady_clock::now();
	Hit* hit = fe->GetHit();
	if(hit!=0){
	  hit->SetEvNum(st);
	  //cout << "Hit V: " << fe->GetName() << ": " << hit->ToString() << endl;
	  
	  if(hit->GetTOT()>0)  { //added by VD to control the noise
	    m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	    nRhits++;
	    
	    //cout << " ::: " << hit->ToString() << endl;
	    if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
	      //cout << "Hit not expected in mask step: " << fe->GetName() << ": " << hit->ToString() << endl;
	      // fe->NextHit();
	      // continue;
	    } else {
	      m_occC[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
	      nGoodHists++;
	    }
	    m_tot[fe->GetName()] ->Fill(hit->GetTOT());
	    m_tid[fe->GetName()] ->Fill(hit->GetTID());
	    m_ttag[fe->GetName()]->Fill(hit->GetTTag());
	    m_bcid[fe->GetName()]->Fill(hit->GetBCID());
	    m_bcidmap[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),hit->GetBCID());
	    m_ttagmap[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow(),hit->GetTTag());
	  } else {
	  }
	  nhits++;
	  //tree->Set(hit);
	  //tree->Fill();
	}
	fe->NextHit();
	//
      }
      auto end = chrono::steady_clock::now();
      uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
      if(LastLoop) break;
      if(ms>50 or nGoodHists>=m_ntrigs*expected*GetFEs().size() ){LastLoop=true;}
      //if(ms>300) {LastLoop=true;}
    }
    cout << "DisconnectedBumpScan:: Step: " << (st+1) << "/" << GetMasker()->GetNumSteps() 
	 << " ==== NRealHits in step: " << nRhits << " / " << nGoodHists << " / Expected Hits 0 "
	 <<" :: ALL Hits: " << nhits << endl;
    
    //Undo the mask step
    UnconfigMask();
    //////std::this_thread::sleep_for(std::chrono::microseconds(1000000));
  }
  //UnconfigMask();
}
 
void DisconnectedBumpScan::Analysis(){
  gStyle->SetOptStat(0);
  TCanvas* can=new TCanvas("plot","plot",800,600);
  for(auto fe : GetFEs()){
    cout << "DisconnectedBumpScan: Restore the original threshold value" << endl;
    fe->RestoreBackupConfig();

    
    for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
      for(uint32_t col = 0; col < Matrix::NUM_COLS; col++){
	//	m_tot[fe->GetName()]->SetBinContent(col+1,row+1,m_tot[fe->GetName()]->GetBinContent(col+1,row+1)/m_ntrigs);
        if (m_occ[fe->GetName()]->GetBinContent(col+1,row+1) == m_ntrigs){
          m_enable[fe->GetName()]->SetBinContent(col+1,row+1,1);
	  fe->SetPixelEnable(col,row,true);
	}
        else fe->SetPixelEnable(col,row,false);
      }
    }

    m_occ[fe->GetName()]->Write();
    m_occC[fe->GetName()]->Write();
    m_tot[fe->GetName()]->Write();
    m_enable[fe->GetName()]->Write();
    m_tid[fe->GetName()]->Write();
    m_ttag[fe->GetName()]->Write();
    m_bcid[fe->GetName()]->Write();
    m_ttagmap[fe->GetName()]->Write();
    m_bcidmap[fe->GetName()]->Write();
    if(m_storehits){m_hits[fe->GetName()]->Write();}

    m_occ[fe->GetName()]->Draw("COLZ");
    m_occ[fe->GetName()]->SetTitle( (fe->GetName()+" Occ").c_str() );
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC.pdf").c_str() );
    m_occC[fe->GetName()]->SetTitle( (fe->GetName()+" Occ (cut)").c_str() );
    m_occC[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCCcut.pdf").c_str() );    

    delete m_occ[fe->GetName()];
    delete m_tot[fe->GetName()];
    delete m_enable[fe->GetName()];
    delete m_tid[fe->GetName()];
    delete m_ttag[fe->GetName()];
    delete m_bcid[fe->GetName()];
    delete m_ttagmap[fe->GetName()];
    delete m_bcidmap[fe->GetName()];
    delete m_hits[fe->GetName()];
    
  }
  delete can;
}
