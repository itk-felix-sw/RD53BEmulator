#include "RD53BEmulator/ThresholdScan.h"
#include "RD53BEmulator/Tools.h"
#include "RD53BEmulator/SCurveFitter.h"

#include <iostream>
#include <sstream>
#include <chrono>

#include "TCanvas.h"
#include "TROOT.h"

using namespace std;
using namespace RD53B;

ThresholdScan::ThresholdScan(){
  m_ntrigs = 100; //100
  m_TriggerLatency = 50;//48; //52

  m_vcalMed = 500;

  m_vcalDiffStart=   00.;
  m_vcalDiffEnd  =  300.; //used to be 600, then 400
  m_vcalStep     =   5.; //used to be 5

  m_chargeMin = Tools::injToCharge(m_vcalDiffStart);
  m_chargeMax = Tools::injToCharge(m_vcalDiffEnd);
  m_chargeStep= Tools::injToCharge(m_vcalStep);
  m_nSteps = (m_vcalDiffEnd - m_vcalDiffStart) / m_vcalStep;
  }

ThresholdScan::~ThresholdScan(){}

void ThresholdScan::PreRun(){
  
  GetMasker()->SetVerbose(false);
  GetMasker()->SetRange(0,0,Matrix::NUM_COLS,Matrix::NUM_ROWS);
  GetMasker()->SetShape(8,64);
  GetMasker()->SetFrequency(1);
  GetMasker()->Build();

  for(auto fe : GetFEs()){

    fe->BackupConfig();
    
    /*
    cout << "ThresholdScan: Configure for " << fe->GetName() << endl;
    m_vcalDiffStart=000 ; //here or in the constructor?
    m_vcalDiffEnd  =500 ; //here or in the constructor?
    m_vcalStep     = 10.; //here or in the constructor?
    */
    
    /*
    //Disable SYN & LIN //needed? these registers don't exist anymore, just renamed?
    fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_1,0);
    fe->GetConfig()->SetField(Configuration::EN_CORE_COL_LIN_2,0);
    fe->GetConfig()->SetField(Configuration::EN_CORE_COL_SYNC ,0);
    */

    //Configure the options for an threshold scan
    cout << "ThresholdScan: Configure analog injection for " << fe->GetName() << endl;

    /*
    m_nSteps = (m_vcalDiffEnd - m_vcalDiffStart) / m_vcalStep; //here or in the constructor?
    m_chargeMin = Tools::injToCharge(m_vcalDiffStart); //here or in the constructor?
    m_chargeMax = Tools::injToCharge(m_vcalDiffEnd); //here or in the constructor?
    m_chargeStep= Tools::injToCharge(m_vcalStep); //here or in the constructor?
    */

    fe->GetConfig()->SetField(Configuration::CalMode,0); //was INJ_MODE_DIG
    fe->GetConfig()->SetField(Configuration::Latency, m_TriggerLatency);
    fe->GetConfig()->SetField(Configuration::PIX_BCAST_EN,0);
    fe->GetConfig()->SetField(Configuration::PIX_AUTO_ROW,0);
    fe->GetConfig()->SetField(Configuration::VCAL_MED,m_vcalMed);
    fe->GetConfig()->SetField(Configuration::VCAL_HIGH,m_vcalMed+m_vcalDiffStart);
    fe->GetConfig()->SetField(Configuration::InjVcalRange,1);    

       
    fe->WriteGlobal();
    Send(fe);

    cout << "ThresholdScan: Mask all" << endl;
    fe->DisableAll();
    ConfigurePixels(fe);
    Send(fe); //new

    //Create the histograms for the threshold scan of each pixel
    cout << "ThresholdScan: Create histograms per pixel for " << fe->GetName() << endl;
    
    m_tot[fe->GetName()]    =new TH1I(("tot_"+fe->GetName()).c_str(),(fe->GetName()+";TOT").c_str(), 16, 0, 16); //FIXME DELETE!!

    m_scur[fe->GetName()]=new TH1I( ("scur_"+fe->GetName()).c_str(),
				    ("scur_"+fe->GetName()+";#DeltaVcal;FE occupancy").c_str(),
				    m_nSteps,m_vcalDiffStart,m_vcalDiffEnd);

    m_scur2D[fe->GetName()]=new TH2F( ("scur2D_"+fe->GetName()).c_str(),
				      ("scur2D_"+fe->GetName()+";#DeltaVcal;pixel occupancy").c_str(),
    				      m_nSteps,m_vcalDiffStart,m_vcalDiffEnd,
    				      m_ntrigs+1,-0.5,m_ntrigs+0.5);

    for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
      for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
        
	m_curves[fe->GetName()][col][row]= new TH1I(("scurve_"+fe->GetName()+std::to_string(col)+"_"+std::to_string(row)).c_str(),
						   ("scurve_"+fe->GetName()+std::to_string(col)+"_"+std::to_string(row)).c_str(),
									  m_nSteps,m_vcalDiffStart,m_vcalDiffEnd);
      }
    }
    m_occ[fe->GetName()]=new TH2I*[m_nSteps];
    for(uint32_t iStep=0; iStep<m_nSteps; iStep++){
      stringstream iStep_ss;
      iStep_ss << iStep;
      stringstream charge_ss;
      //charge_ss << (m_chargeMin + iStep * m_vcalStep);
      charge_ss << (iStep * m_vcalStep);
      m_occ[fe->GetName()][iStep] = new TH2I(("occ_"+fe->GetName()+"_"+iStep_ss.str()).c_str(),
					     (fe->GetName()+", #Delta VCAL="+charge_ss.str()+";Column;Row").c_str(),
					     Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);

    }
    
    //Store hits
    m_hits[fe->GetName()]= new HitTree("tree_"+fe->GetName(),"HitTree");

  }

  m_logFile << "\"m_ntrigs\": " << m_ntrigs << endl;
  m_logFile << "\"m_chargeMin\": " << m_chargeMin << endl;
  m_logFile << "\"m_chargeMax\": " << m_chargeMax << endl;
  m_logFile << "\"m_vcalStep\": " << m_chargeStep << endl;
  m_logFile << "\"m_nSteps\": " << m_nSteps << endl;


}

void ThresholdScan::Run(){

  //Prepare the trigger
  cout << "ThresholdScan: Prepare the TRIGGER" << endl;
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // The Analog scans requirs many more triggers due to timming issues
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // This is needed for diff in the disconnected bump config.
  PrepareTrigger(m_TriggerLatency-10, 4, true, true, true, true,1);
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}); // This is ok for Analog synlindiff
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1,1,1,1,1});
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,0,0,0,0,0,0}); // This is the minimum needed for lin
  //PrepareTrigger(m_TriggerLatency,{1,1,1,1,1,1,1,1}); // does not work with lin 
  for(auto fe : GetFEs()){ // RD53B is always a bit noisy at initialisation so clearing hits
    if(!fe->HasHits()) continue;
    fe->ClearHits();
  }


  /*
  cout << "ThresholdScan: Prepare the mask" << endl;
  GetMasker()->SetShape(8,2);
  GetMasker()->Build();
  bool cutBCID=( m_scan_fe & SyncFE ); 
  */


  cout << "ThresholdScan: Loop over " << m_nSteps << " vcal steps" << endl;

  for(uint32_t st=0; st<GetMasker()->GetNumSteps();st++){
    GetMasker()->GetStep(st);
    ConfigMask();
    ConfigMask();



    uint32_t ExpectedHits=0;
    for(auto fe : GetFEs()){ 
      ExpectedHits+=m_ntrigs*fe->GetNPixelsEnabled();
    }



    for(uint32_t iStep=0; iStep<m_nSteps; iStep++){
      unsigned vcalDiff = m_vcalDiffStart + iStep * m_vcalStep;
      unsigned vcalHigh = m_vcalMed + vcalDiff;
      double charge = Tools::injToCharge(vcalDiff);
      ///cout << "ThresholdScan: injecting charge Q=" << charge << "e-" << " vcalMed "<<m_vcalMed<<" vcalHigh "<<vcalHigh<< " DeltaVCal: " << m_vcalDiffStart + iStep * m_vcalStep  << endl;

      // setting VCAL
      for(auto fe : GetFEs()){
	fe->GetConfig()->SetField(Configuration::VCAL_HIGH,vcalHigh);
	fe->WriteGlobal();
	Send(fe);
	//std::this_thread::sleep_for(std::chrono::milliseconds(10)); 
	if(m_verbose) std::cout << __PRETTY_FUNCTION__ << ": written inj charge " << charge << " for chip ID " << fe->GetChipID() << std::endl;
	if(m_storehits) m_hits[fe->GetName()]->setVcal(vcalDiff);
      }

      //Trigger 
      for(uint32_t trig=0;trig<m_ntrigs; trig++) Trigger();
      
      auto start = chrono::steady_clock::now();
      
      //Readout the hit histogram hit
      bool LastLoop=false;
      uint32_t nhits=0;
      uint32_t nRhits=0;
      uint32_t nGoodHists=0;

      vector<Hit*> hits;
      uint32_t nH=0;
      while(true){
	for(auto fe : GetFEs()){
	  hits=fe->GetHits(10000);
	  nH=hits.size();
	  if(nH>0) start = chrono::steady_clock::now();
	  if (nH==0) {
	    this_thread::sleep_for(chrono::milliseconds( 1 ));
	
	  } else {
	    for (unsigned int h=0; h<nH; h++) {   
	      Hit* hit = hits.at(h);
	      if(hit==0) continue;

	      LastLoop=false;

	      if(hit->GetTOT()>0){
	    
		if(!GetMasker()->Contains(hit->GetCol(),hit->GetRow())){
		  //cout << "Hit is not expected in the current mask step: " << hit->ToString() << endl;
		} else {
		  nGoodHists++;
		  m_tot[fe->GetName()] ->Fill(hit->GetTOT());
		  //if( !fe->GetPixelMask(hit->GetCol(),hit->GetRow())){
		  m_occ[fe->GetName()][iStep]->Fill(hit->GetCol(),hit->GetRow());
		  //m_scur[fe->GetName()]->Fill(vcalDiff);
		  m_curves[fe->GetName()][hit->GetCol()][hit->GetRow()]->Fill(vcalDiff);
		  //Store the hits!
		  if(m_storehits) m_hits[fe->GetName()]->Fill(hit);
		  nRhits++;
		}
		nhits++;
	      }
	    }
	  }
	  fe->ClearHits(nH);
	}
	auto end = chrono::steady_clock::now();
	uint32_t ms=chrono::duration_cast<chrono::milliseconds>(end - start).count();
	if(LastLoop) break;
	if(ms>5 or nGoodHists>=ExpectedHits ){LastLoop=true;}
      }
      cout << "ThresholdScan: NHeaders in step " <<   (st+1) << "/" << GetMasker()->GetNumSteps() << " and DVcal: " 
	   << m_vcalDiffStart + iStep * m_vcalStep 
	   << " :: " << nhits <<" / "<<ExpectedHits<<" NHits: "<<nRhits<<" / "<<ExpectedHits<< endl;
    }
    
    //Undo the mask step
    UnconfigMask();
    UnconfigMask();
  }
}

void ThresholdScan::Analysis(){

  gROOT->SetBatch();
  TCanvas* can=new TCanvas("plot","plot",800,600);
  cout << "Analysis" << endl;
  
  time_t start_time;
  time(&start_time);
  m_logFile << "\"start time analyis\": " << ctime(&start_time) << endl;

  map<string,TH2F*> thres;
  map<string,TH2F*> noise;
  map<string,TH1F*> thres_1d;
  map<string,TH1F*> noise_1d;
  map<string,TH2F*> thres_Q;
  map<string,TH2F*> noise_Q;
  map<string,TH1F*> thres_Q_1d;
  map<string,TH1F*> noise_Q_1d;


  map<string,TH2I*> occ_vs_vcal;
  for(auto fe : GetFEs()){
    fe->RestoreBackupConfig();

    thres[fe->GetName()]=new TH2F(("thres_Vcal_"+fe->GetName()).c_str(),";Column;Row", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    noise[fe->GetName()]=new TH2F(("noise_Vcal_"+fe->GetName()).c_str(),";Column;Row", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    thres_1d[fe->GetName()]=new TH1F(("thres_Vcal_1d_"+fe->GetName()).c_str(),";VCalDiff",m_nSteps*2,m_vcalDiffStart,m_vcalDiffEnd);
    noise_1d[fe->GetName()]=new TH1F(("noise_Vcal_1d_"+fe->GetName()).c_str(),";VcalDiff",40,0,30);

    thres_Q[fe->GetName()]=new TH2F(("thres_Q_"+fe->GetName()).c_str(),";Column;Row", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    noise_Q[fe->GetName()]=new TH2F(("noise_Q_"+fe->GetName()).c_str(),";Column;Row", Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    thres_Q_1d[fe->GetName()]=new TH1F(("thres_Q_1d_"+fe->GetName()).c_str(),";Charge",m_nSteps*2, m_chargeMin, m_chargeMax);
    //noise_Q_1d[fe->GetName()]=new TH1F(("noise_Q_1d_"+fe->GetName()).c_str(),";Charge",30,50,400);
    noise_Q_1d[fe->GetName()]=new TH1F(("noise_Q_1d_"+fe->GetName()).c_str(),";Charge",40, 0, Tools::injToCharge(30));
  }

  //fitting of s-curves
  Fitter::SCurveFitter scf;

  for(auto fe : GetFEs()){
    for(uint32_t col=0;col<Matrix::NUM_COLS;col++){
      for(uint32_t row=0;row<Matrix::NUM_ROWS;row++){
	if (m_curves[fe->GetName()][col][row]->GetEntries()<1){continue;}

	scf.setDataFromHist(m_curves[fe->GetName()][col][row]);
	scf.guessInitialParameters();
	scf.fit();

        float mean = scf.getSCurve().mu;
        float sigma = scf.getSCurve().sigma;

        float mean_Q=Tools::injToCharge(mean);
	float sigma_Q=Tools::injToCharge(mean+sigma) - Tools::injToCharge(mean); //injToCharge is a non-linear transformation

        thres[fe->GetName()]->Fill(col,row,mean);
        noise[fe->GetName()]->Fill(col,row,sigma);
        thres_1d[fe->GetName()]->Fill(mean);
        noise_1d[fe->GetName()]->Fill(sigma);

        thres_Q[fe->GetName()]->Fill(col,row,mean_Q);
        noise_Q[fe->GetName()]->Fill(col,row,sigma_Q);
        thres_Q_1d[fe->GetName()]->Fill(mean_Q);
        noise_Q_1d[fe->GetName()]->Fill(sigma_Q);
	
	for (int binV=0; binV<=m_curves[fe->GetName()][col][row]->GetNbinsX()+1; binV++) {
	  m_scur2D[fe->GetName()]->Fill( m_curves[fe->GetName()][col][row]->GetXaxis()->GetBinCenter(binV), 
					 m_curves[fe->GetName()][col][row]->GetBinContent(binV) );
	}
	
        if (((col*Matrix::NUM_ROWS)+row)%3200==0){
          m_curves[fe->GetName()][col][row]->Write();
        }
      }
    }

    time_t stop_time;
    time(&stop_time);
    m_logFile << "\"stop time analyis\": " << ctime(&stop_time) << endl;

    //m_tot[fe->GetName()]->Write();
    thres[fe->GetName()]->Write();
    noise[fe->GetName()]->Write();
    thres_1d[fe->GetName()]->Write();
    noise_1d[fe->GetName()]->Write();
    thres_Q[fe->GetName()]->Write();
    noise_Q[fe->GetName()]->Write();
    thres_Q_1d[fe->GetName()]->Write();
    noise_Q_1d[fe->GetName()]->Write();

    m_scur[fe->GetName()]->Write();
    m_scur2D[fe->GetName()]->Write();

    //Write the hits
    if(m_storehits) m_hits[fe->GetName()]->Write();

    thres_Q_1d[fe->GetName()]->Draw("HIST");
    //can->SetLogy(false);
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__Thres_1D.pdf").c_str() );

    noise_Q_1d[fe->GetName()]->Draw("HIST");
    //can->SetLogy(false);
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__Noise_1D.pdf").c_str() );

    m_scur2D[fe->GetName()]->Draw("COLZ");
    can->SetLogz();
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__Scurve_2D.pdf").c_str() );

  }


  for(auto fe : GetFEs()){
    for(uint32_t iStep=0; iStep<m_nSteps; iStep++){
      m_occ[fe->GetName()][iStep]->Write();
      delete m_occ[fe->GetName()][iStep];
    }
    delete[] m_occ[fe->GetName()];
    delete m_scur[fe->GetName()];
    delete m_scur2D[fe->GetName()];

    delete m_tot[fe->GetName()];
    delete thres[fe->GetName()];
    delete noise[fe->GetName()];
    delete thres_1d[fe->GetName()];
    delete noise_1d[fe->GetName()];
    delete thres_Q[fe->GetName()];
    delete noise_Q[fe->GetName()];
    delete thres_Q_1d[fe->GetName()];
    delete noise_Q_1d[fe->GetName()];

    // if this ever causes problems, write brian.moser@cern.ch

    // for( auto i : m_curves[fe->GetName()] ){
    //   for(auto j : i.second){
    // 	delete j.second;
    //   }
    // }

  }
  
}
