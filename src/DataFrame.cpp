#include "RD53BEmulator/DataFrame.h"
#include <sstream>
#include <iomanip>
//#include <bitset>
#include <iostream>

using namespace std;
using namespace RD53B;

DataFrame::DataFrame(){
  m_enable_eos = true;
  m_enable_tot = true;
  m_enable_cid = true;
  m_verbose = false;
  m_streams=vector<Stream*>();
}

DataFrame::~DataFrame(){
  Clear();
}

string DataFrame::ToString(){
  ostringstream os;
  for(uint32_t i=0;i<m_streams.size();i++){
    os << m_streams.at(i)->ToString() << endl;
  }
  return os.str();
}

uint32_t DataFrame::UnPack(uint8_t * bytes, uint32_t maxlen){
  //We need at least one Aurora Frame to build a Stream
  if(maxlen<8){return 0;}
  uint32_t b=0;

  //Identify all new Frames in the block
  uint32_t NewFrameBitLocation[2]={0,0};
  uint8_t Ind=0;
  
  std::vector<uint32_t> nStreams; 
  while(b<maxlen){
    if (bytes[b]&0x80) {NewFrameBitLocation[Ind]=b; Ind++;}
    b+=8;
    if(Ind==3) break; //UnPack only between one set of NS
  }
  
  //If new frame is not found, look for an end frame signature of 8 zeros, we allow catching anywhere here trying to recover broken data 
  if(Ind!=3){
    for(b=maxlen; ((Ind==0 and b>0 ) or ( Ind==1 and b>NewFrameBitLocation[0])); b--){
  
      //Look for 6 consecutive zeros in the data. 
      if( ((   bytes[b]       & 0x3F ) == 0) ||
	  (( ( bytes[b] >> 1) & 0x3F ) == 0) ||
      	  (( ( bytes[b] >> 2) & 0x3F ) == 0) ) {

	NewFrameBitLocation[Ind]=b; Ind++;

      }

    }

  }
  



  if(Ind!=3){
    if(m_verbose) cout << "DataFrame: didn't find an end-stream, setting end-frame to the maxlen" << endl;


    if(Ind==1)
      return NewFrameBitLocation[0];
    else
      return 0;
  }

  if(m_verbose){
    if(NewFrameBitLocation[0]!=0) std::cout<<"DataFrame::UnPack Warning!!! Decoding stream didn't start from begging, there might be data-loss"<<std::endl;
    std::cout<<"Decoding new stream from :"<<NewFrameBitLocation[0]<<" to "<<NewFrameBitLocation[1]<<std::endl;
  }

  Stream * ss = new Stream();
  ss->EnableChipId(m_enable_cid);
  ss->EnableTot(m_enable_tot);
  ss->EnableEnc(true);
  //ss->EnableEos(m_enable_eos);
  ss->UnPack(&bytes[NewFrameBitLocation[0]], NewFrameBitLocation[1]-NewFrameBitLocation[0]);
  if(m_verbose) cout <<"DataFrame::UnPack Unpacked data: "<<ss->ToString() << endl;
  m_streams.push_back(ss);

  return NewFrameBitLocation[1];
}


uint32_t DataFrame::Pack(uint8_t * bytes){
  uint32_t nb=0; 
  for(uint32_t i=0; i<m_streams.size();i++){
    nb=m_streams.at(i)->Pack(&bytes[nb]);
    //skip the next bytes until the next Frame
    nb+=nb%8;
  }
  return nb;
}

uint32_t DataFrame::GetType(){
  return Frame::DATA;
}

string DataFrame::ToByteStream(uint8_t * bytes, uint32_t len){
  ostringstream os;
  for(uint32_t i=0;i<len;i++){cout << setw(2) << setfill('0') << hex << (uint32_t)(bytes[i]) << dec << "|";}
  return os.str();
}

void DataFrame::Clear(){
  while(!m_streams.empty()){
    Stream *s=m_streams.back();
    m_streams.pop_back();
    delete s;
  }
}

// void DataFrame::SetNumEventsPerStream(uint32_t nes){
//   m_nes=nes;
// }

void DataFrame::SetEOS(bool enable){
  m_enable_eos=enable;
}

void DataFrame::SetEnc(bool enable){
  m_enable_enc=enable;
}

void DataFrame::SetChnID(bool enable){
  m_enable_cid=enable;
}

void DataFrame::EnableEOS(bool enable){
  m_enable_eos=enable;
}

void DataFrame::EnableEnc(bool enable){
  m_enable_enc=enable;
}

void DataFrame::EnableChnID(bool enable){
  m_enable_cid=enable;
}

void DataFrame::EnableTOT(bool enable){
  m_enable_tot=enable;
}

void DataFrame::AddStream(Stream * stream){
  m_streams.push_back(stream);
}
  
uint32_t DataFrame::GetNStreams(){
  return m_streams.size();
}

Stream * DataFrame::GetStream(uint32_t index){
  return m_streams.at(index);
}

void DataFrame::SetVerbose(bool enable){
  m_verbose=enable;
}
