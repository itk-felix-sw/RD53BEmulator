#include "RD53BEmulator/HitMap.h"
#include <sstream>

using namespace RD53B;
using namespace std;

HitMap::HitMap(){}
HitMap::~HitMap(){}

uint32_t HitMap::Decode(uint32_t encoded, uint32_t *decoded){

  //Resetting the Mbll/Mbrr variables 
  Mbll=0;Mbrl=0;Mblr=0;Mbrr=0;
  Mtll=0;Mtrl=0;Mtlr=0;Mtrr=0;

  uint32_t pos=0;
  pos+=readTwo(encoded,pos,&S1);
  switch(S1){
  case 0b10:
    S2b=0;S3bl=0;S3br=0;Mbll=0;Mbrl=0;Mblr=0;Mbrr=0;
    pos+=readTwo(encoded,pos,&S2t);
    switch(S2t){
    case 0b10:
      S3tr=0;Mtrl=0;Mtrr=0;
      pos+=readTwo(encoded,pos,&S3tl);
      if     (S3tl==0b10){ pos+=readTwo(encoded,pos,&Mtll); Mtlr=0;                          }
      else if(S3tl==0b01){ Mtll=0;                          pos+=readTwo(encoded,pos,&Mtlr); }
      else if(S3tl==0b11){ pos+=readTwo(encoded,pos,&Mtll); pos+=readTwo(encoded,pos,&Mtlr); }
      break;
    case 0b01:
      S3tl=0;Mtll=0;Mtlr=0;
      pos+=readTwo(encoded,pos,&S3tr);
      if     (S3tr==0b10){ pos+=readTwo(encoded,pos,&Mtrl); Mtrr=0;                          }
      else if(S3tr==0b01){ Mtrl=0;                          pos+=readTwo(encoded,pos,&Mtrr); }
      else if(S3tr==0b11){ pos+=readTwo(encoded,pos,&Mtrl); pos+=readTwo(encoded,pos,&Mtrr); }
      break;
    case 0b11:
      pos+=readTwo(encoded,pos,&S3tl);
      pos+=readTwo(encoded,pos,&S3tr);
      if     (S3tl==0b10){ pos+=readTwo(encoded,pos,&Mtll); Mtlr=0;                          }
      else if(S3tl==0b01){ Mtll=0;                          pos+=readTwo(encoded,pos,&Mtlr); }
      else if(S3tl==0b11){ pos+=readTwo(encoded,pos,&Mtll); pos+=readTwo(encoded,pos,&Mtlr); }
      if     (S3tr==0b10){ pos+=readTwo(encoded,pos,&Mtrl); Mtrr=0;                          }
      else if(S3tr==0b01){ Mtrl=0;                          pos+=readTwo(encoded,pos,&Mtrr); }
      else if(S3tr==0b11){ pos+=readTwo(encoded,pos,&Mtrl); pos+=readTwo(encoded,pos,&Mtrr); }
      break;
    }
    break;
  case 0b01:
    S2t=0;S3tl=0;S3tr=0;Mtll=0;Mtrl=0;Mtlr=0;Mtrr=0;
    pos+=readTwo(encoded,pos,&S2b);
    switch(S2b){
    case 0b10:
      S3br=0;Mbrl=0;Mbrr=0;
      pos+=readTwo(encoded,pos,&S3bl);
      if     (S3bl==0b10){ pos+=readTwo(encoded,pos,&Mbll); Mblr=0;                          }
      else if(S3bl==0b01){ Mbll=0;                          pos+=readTwo(encoded,pos,&Mblr); }
      else if(S3bl==0b11){ pos+=readTwo(encoded,pos,&Mbll); pos+=readTwo(encoded,pos,&Mblr); }
      break;
    case 0b01:
      S3bl=0;Mbll=0;Mblr=0;
      pos+=readTwo(encoded,pos,&S3br);
      if     (S3br==0b10){ pos+=readTwo(encoded,pos,&Mbrl); Mbrr=0;                          }
      else if(S3br==0b01){ Mbrl=0;                          pos+=readTwo(encoded,pos,&Mbrr); }
      else if(S3br==0b11){ pos+=readTwo(encoded,pos,&Mbrl); pos+=readTwo(encoded,pos,&Mbrr); }
      break;
    case 0b11:
      pos+=readTwo(encoded,pos,&S3bl);
      pos+=readTwo(encoded,pos,&S3br);
      if     (S3bl==0b10){ pos+=readTwo(encoded,pos,&Mbll); Mblr=0;                          }
      else if(S3bl==0b01){ Mbll=0;                          pos+=readTwo(encoded,pos,&Mblr); }
      else if(S3bl==0b11){ pos+=readTwo(encoded,pos,&Mbll); pos+=readTwo(encoded,pos,&Mblr); }
      if     (S3br==0b10){ pos+=readTwo(encoded,pos,&Mbrl); Mbrr=0;                          }
      else if(S3br==0b01){ Mbrl=0;                          pos+=readTwo(encoded,pos,&Mbrr); }
      else if(S3br==0b11){ pos+=readTwo(encoded,pos,&Mbrl); pos+=readTwo(encoded,pos,&Mbrr); }
      break;
    }  
    break;
  case 0b11:
    pos+=readTwo(encoded,pos,&S2t);
    switch(S2t){
    case 0b10:
      S3tr=0;Mbrl=0;Mtrr=0;
      pos+=readTwo(encoded,pos,&S3tl);
      if     (S3tl==0b10){ pos+=readTwo(encoded,pos,&Mtll); Mtlr=0;                          }
      else if(S3tl==0b01){ Mtll=0;                          pos+=readTwo(encoded,pos,&Mtlr); }
      else if(S3tl==0b11){ pos+=readTwo(encoded,pos,&Mtll); pos+=readTwo(encoded,pos,&Mtlr); }
      break;
    case 0b01:
      S3tl=0;Mtll=0;Mtlr=0;
      pos+=readTwo(encoded,pos,&S3tr);
      if     (S3tr==0b10){ pos+=readTwo(encoded,pos,&Mtrl); Mtrr=0;                          }
      else if(S3tr==0b01){ Mtrl=0;                          pos+=readTwo(encoded,pos,&Mtrr); }
      else if(S3tr==0b11){ pos+=readTwo(encoded,pos,&Mtrl); pos+=readTwo(encoded,pos,&Mtrr); }
      break;
    case 0b11:
      pos+=readTwo(encoded,pos,&S3tl);
      pos+=readTwo(encoded,pos,&S3tr);
      if     (S3tl==0b10){ pos+=readTwo(encoded,pos,&Mtll); Mtlr=0;                          }
      else if(S3tl==0b01){ Mtll=0;                          pos+=readTwo(encoded,pos,&Mtlr); }
      else if(S3tl==0b11){ pos+=readTwo(encoded,pos,&Mtll); pos+=readTwo(encoded,pos,&Mtlr); }
      if     (S3tr==0b10){ pos+=readTwo(encoded,pos,&Mtrl); Mtrr=0;                          }
      else if(S3tr==0b01){ Mtrl=0;                          pos+=readTwo(encoded,pos,&Mtrr); }
      else if(S3tr==0b11){ pos+=readTwo(encoded,pos,&Mtrl); pos+=readTwo(encoded,pos,&Mtrr); }
      break;
    }
    pos+=readTwo(encoded,pos,&S2b);
    switch(S2b){
    case 0b10:
      S3br=0;Mbrl=0;Mbrr=0;
      pos+=readTwo(encoded,pos,&S3bl);
      if     (S3bl==0b10){ pos+=readTwo(encoded,pos,&Mbll); Mblr=0;                          }
      else if(S3bl==0b01){ Mbll=0;                          pos+=readTwo(encoded,pos,&Mblr); }
      else if(S3bl==0b11){ pos+=readTwo(encoded,pos,&Mbll); pos+=readTwo(encoded,pos,&Mblr); }
      break;
    case 0b01:
      S3bl=0;Mbll=0;Mblr=0;
      pos+=readTwo(encoded,pos,&S3br);
      if     (S3br==0b10){ pos+=readTwo(encoded,pos,&Mbrl); Mbrr=0;                          }
      else if(S3br==0b01){ Mbrl=0;                          pos+=readTwo(encoded,pos,&Mbrr); }
      else if(S3br==0b11){ pos+=readTwo(encoded,pos,&Mbrl); pos+=readTwo(encoded,pos,&Mbrr); }
      break;
    case 0b11:
      pos+=readTwo(encoded,pos,&S3bl);
      pos+=readTwo(encoded,pos,&S3br);
      if     (S3bl==0b10){ pos+=readTwo(encoded,pos,&Mbll); Mblr=0;                          }
      else if(S3bl==0b01){ Mbll=0;                          pos+=readTwo(encoded,pos,&Mblr); }
      else if(S3bl==0b11){ pos+=readTwo(encoded,pos,&Mbll); pos+=readTwo(encoded,pos,&Mblr); }
      if     (S3br==0b10){ pos+=readTwo(encoded,pos,&Mbrl); Mbrr=0;                          }
      else if(S3br==0b01){ Mbrl=0;                          pos+=readTwo(encoded,pos,&Mbrr); }
      else if(S3br==0b11){ pos+=readTwo(encoded,pos,&Mbrl); pos+=readTwo(encoded,pos,&Mbrr); }
      break;
    }  
    break;
  }
  
  //build the decoded
  *decoded = 0;
  *decoded |= ((Mtll >> 1) & 0x1) << 0;
  *decoded |= ((Mtll >> 0) & 0x1) << 1;
  *decoded |= ((Mtlr >> 1) & 0x1) << 2;
  *decoded |= ((Mtlr >> 0) & 0x1) << 3;
  *decoded |= ((Mtrl >> 1) & 0x1) << 4;
  *decoded |= ((Mtrl >> 0) & 0x1) << 5;
  *decoded |= ((Mtrr >> 1) & 0x1) << 6;
  *decoded |= ((Mtrr >> 0) & 0x1) << 7;
  *decoded |= ((Mbll >> 1) & 0x1) << 8;
  *decoded |= ((Mbll >> 0) & 0x1) << 9;
  *decoded |= ((Mblr >> 1) & 0x1) << 10;
  *decoded |= ((Mblr >> 0) & 0x1) << 11;
  *decoded |= ((Mbrl >> 1) & 0x1) << 12;
  *decoded |= ((Mbrl >> 0) & 0x1) << 13;
  *decoded |= ((Mbrr >> 1) & 0x1) << 14;
  *decoded |= ((Mbrr >> 0) & 0x1) << 15;
  
  return pos;
}
uint32_t HitMap::Encode(uint32_t decoded, uint32_t *encoded){
  //build the maps
  Mbrr = ((decoded >> 14) & 0x1) << 1 | ((decoded >> 15) & 0x1);
  Mbrl = ((decoded >> 12) & 0x1) << 1 | ((decoded >> 13) & 0x1);
  Mblr = ((decoded >> 10) & 0x1) << 1 | ((decoded >> 11) & 0x1);
  Mbll = ((decoded >>  8) & 0x1) << 1 | ((decoded >>  9) & 0x1);
  Mtrr = ((decoded >>  6) & 0x1) << 1 | ((decoded >>  7) & 0x1);
  Mtrl = ((decoded >>  4) & 0x1) << 1 | ((decoded >>  5) & 0x1);
  Mtlr = ((decoded >>  2) & 0x1) << 1 | ((decoded >>  3) & 0x1);
  Mtll = ((decoded >>  0) & 0x1) << 1 | ((decoded >>  1) & 0x1);
  
  S1=(bool(Mtll or Mtlr or Mtrl or Mtrr) << 1) | bool(Mbll or Mblr or Mbrl or Mbrr);
  S2t=(bool(Mtll or Mtlr) << 1) | bool(Mtrl or Mtrr); 
  S2b=(bool(Mbll or Mblr) << 1) | bool(Mbrl or Mbrr);
  S3tl=(bool(Mtll) << 1) | bool(Mtlr);
  S3tr=(bool(Mtrl) << 1) | bool(Mtrr);
  S3bl=(bool(Mbll) << 1) | bool(Mblr);
  S3br=(bool(Mbrl) << 1) | bool(Mbrr);
  
  uint32_t pos=0;
  *encoded = 0;

  if(S1) pos+=writeTwo(S1,pos,encoded);
  if(S2t) pos+=writeTwo(S2t,pos,encoded);
  if(S3tl) pos+=writeTwo(S3tl,pos,encoded);
  if(S3tr) pos+=writeTwo(S3tr,pos,encoded);
  if(Mtll) pos+=writeTwo(Mtll,pos,encoded);
  if(Mtlr) pos+=writeTwo(Mtlr,pos,encoded);
  if(Mtrl) pos+=writeTwo(Mtrl,pos,encoded);
  if(Mtrr) pos+=writeTwo(Mtrr,pos,encoded);
  if(S2b) pos+=writeTwo(S2b,pos,encoded);
  if(S3bl) pos+=writeTwo(S3bl,pos,encoded);
  if(S3br) pos+=writeTwo(S3br,pos,encoded);
  if(Mbll) pos+=writeTwo(Mbll,pos,encoded);
  if(Mblr) pos+=writeTwo(Mblr,pos,encoded);
  if(Mbrl) pos+=writeTwo(Mbrl,pos,encoded);
  if(Mbrr) pos+=writeTwo(Mbrr,pos,encoded);

  return pos;
}

string HitMap::to_string(){
  ostringstream os;
  if(S1) os << " S1:" << to_string(S1);
  if(S2t) os << " S2t:" << to_string(S2t);
  if(S3tl) os << " S3tl:" << to_string(S3tl);
  if(S3tr) os << " S3tr:" << to_string(S3tr);
  if(Mtll) os << " Mtll:" << to_string(Mtll);
  if(Mtlr) os << " Mtlr:" << to_string(Mtlr);
  if(Mtrl) os << " Mtrl:" << to_string(Mtrl);
  if(Mtrr) os << " Mtrr:" << to_string(Mtrr);
  if(S2b) os << " S2b:" << to_string(S2b);
  if(S3bl) os << " S3bl:" << to_string(S3bl);
  if(S3br) os << " S3br:" << to_string(S3br);
  if(Mbll) os << " Mbll:" << to_string(Mbll);
  if(Mblr) os << " Mblr:" << to_string(Mblr);
  if(Mbrl) os << " Mbrl:" << to_string(Mbrl);
  if(Mbrr) os << " Mbrr:" << to_string(Mbrr);
  return os.str();
}

uint32_t HitMap::readTwo(uint32_t src, uint32_t pos, uint32_t * dst){
  uint32_t val=( src >> (28-pos) ) & 0x3;
  if(val==0b00 or val==0b01){*dst=0b01;return 1;}
  else{*dst=val; return 2;}
}

uint32_t HitMap::writeTwo(uint32_t src, uint32_t pos, uint32_t * dst){
  if(src==0b01){*dst|=(0b0)<<(28-pos); return 1;}
  else{*dst|=(src&0x3)<<(28-pos); return 2;}
}

string HitMap::to_string(uint32_t src){
  if      (src==0b10){return "10";}
  else if (src==0b01){return "01";}
  else if (src==0b11){return "11";}
  else return "00";
}
