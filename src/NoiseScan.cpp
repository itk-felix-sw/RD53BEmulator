#include "RD53BEmulator/NoiseScan.h"

#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"

#include <iostream>
#include <chrono>
#include <signal.h>

using namespace std;
using namespace RD53B;

bool stopScan = false;

TApplication* theAppSTS2;

void Stop(int){
  std::cout << std::endl << "You pressed ctrl+c -> quitting" << std::endl;
  stopScan = true;
}

NoiseScan::NoiseScan(){
  m_TriggerLatency = 80;
  m_duration = 500; //s
  m_frequency = 0.2; //Hz
  m_maxRate = 1.0e-6; //0.000001;
  m_ntrigs = 0;
  m_TriggerMultiplier = 4;
  m_bc.resize(4,true);
  m_bcTriggered = 0;
  m_realDuration = m_duration;
  if(m_verbose){
    std::cout << ": m_duration = " << m_duration << std::endl;
    std::cout << ": m_frequency = " << m_frequency << std::endl;
  }
}

NoiseScan::~NoiseScan(){}

void NoiseScan::PreRun(){
  m_logFile << "\"m_frequency\": " << m_frequency << " Hz" << endl;
  m_logFile << "\"m_maxRate\": " << m_maxRate << " hit/bc" << endl;
  m_logFile << "\"m_TriggerMultiplier\": " << m_TriggerMultiplier << endl;
  m_logFile << "\"m_bc\": {" << m_bc[0] << ", " << m_bc[1] << ", " << m_bc[2] << ", " << m_bc[3] << "}" << endl;

  for(auto fe : GetFEs()){
    fe->GetConfig()->SetField(Configuration::CalMode,0);
    fe->GetConfig()->SetField(Configuration::Latency,m_TriggerLatency);

    fe->GetConfig()->SetField(Configuration::DAC_TH1_L_DIFF,500);
    fe->GetConfig()->SetField(Configuration::DAC_TH1_R_DIFF,500);

    fe->WriteGlobal();
    //fe->SetMask(0,0);
    Send(fe);
    
    m_occ[fe->GetName()]      =new TH2I(("occ_"+fe->GetName()).c_str(), ";Column;Row;Number of hits", 
					Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_occInBC[fe->GetName()]  =new TH2F(("occInBC_"+fe->GetName()).c_str(),";Column;Row;#frac{Number of hits}{bc}", 
					Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    m_enableMap[fe->GetName()]=new TH2I(("enable_"+fe->GetName()).c_str(),"Enable map;Column;Row;Enable", 
					Matrix::NUM_COLS, 0, Matrix::NUM_COLS, Matrix::NUM_ROWS, 0, Matrix::NUM_ROWS);
    
    if(m_verbose) std::cout << ": added front-end with chip ID " << fe->GetChipID() << std::endl;

    //maybe we need to enable all and configure pixels
    fe->EnableAll();


    fe->SetPixelEnable(44,146,false);
    fe->SetPixelEnable(44,145,false);
    fe->SetPixelEnable(116,251,false);
    fe->SetPixelEnable(116,334,false);
    
    fe->SetPixelEnable(145,0,false);
    fe->SetPixelEnable(184,169,false);
    fe->SetPixelEnable(1,33,false);
    fe->SetPixelEnable(1,44,false);
    fe->SetPixelEnable(1,47,false);
    fe->SetPixelEnable(1,63,false);
    fe->SetPixelEnable(1,50,false);
    fe->SetPixelEnable(263,182,false);
    fe->SetPixelEnable(250,198,false);
    fe->SetPixelEnable(258,198,false);
    fe->SetPixelEnable(266,198,false);

    fe->SetPixelEnable(366,267,false);

    fe->SetPixelEnable(399,277,false);
    fe->SetPixelEnable(398,160,false);
    


    fe->SetPixelEnable(1,1,false);
    fe->SetPixelEnable(225,347,false);
    
    ConfigurePixels(fe);
  }
}

void NoiseScan::Run(){

  gROOT->SetBatch(false);
  theAppSTS2=new TApplication("myApp",0,0);
  TCanvas* myC2=new TCanvas("can","can", 1000,1000);//800*GetFEs().size(),800);
  myC2->Divide(2,2);

  
  //Prepare the trigger
  uint32_t period = 1; //round(1 / m_frequency);
  cout << "Prepare the TRIGGER and period is: " << period << endl;
  PrepareTrigger(m_TriggerLatency, m_TriggerMultiplier, m_bc[0], m_bc[1], m_bc[2], m_bc[3], 0);
  

  // RD53B is always a bit noisy at initialisation so clearing hits
  for(auto fe : GetFEs()){ 
    if(!fe->HasHits()) continue;
    fe->ClearHits();
  }

  auto start_scan = chrono::steady_clock::now();

  cout << "Run for " << m_duration << " seconds" << endl;
  vector<uint32_t> vhits;
  vhits.resize(GetFEs().size(), 0);
  uint32_t nhits = 0;
  int feID=-1;

  signal(SIGINT, Stop);
  while(!stopScan){
    auto elapsed_time = chrono::steady_clock::now();
    uint32_t diff = chrono::duration_cast<chrono::microseconds>(elapsed_time - start_scan).count();

    if(diff % period == 0) {
      for (int t=0; t<100; ++t) Trigger();
      m_ntrigs++;
      auto start_read = chrono::steady_clock::now();

      //histogram hit

      vector<Hit*> hits;
      uint32_t nH=0;
      int totH=0;
      while(true){
	totH=0;
	feID=-1;
	for(auto fe : GetFEs()) {
	  feID+=1;
	  hits=fe->GetHits(10000);
	  nH=hits.size();
	  if(nH>0) start_read = chrono::steady_clock::now();
	  if (nH==0) {
	    this_thread::sleep_for(chrono::microseconds( 1 ));
	    //break;
	  } else {
	    for (unsigned int h=0; h<nH; h++) {
	      Hit* hit = hits.at(h);
	      if(hit==0) continue;
	      if (hit->GetTOT()!=0x0) {
		//cout << " hit from FE: " << fe->GetName() << " , " << hit->ToString() << endl;
		m_occ[fe->GetName()]->Fill(hit->GetCol(),hit->GetRow());
		vhits.at(feID)=vhits.at(feID)+1;   
		nhits++;
		totH++;
	      }
	    }
	  }
	  fe->ClearHits(nH);
	}
	if (totH==0) {
	  this_thread::sleep_for(chrono::microseconds( 1000 ));
	  break;
	}
	//auto end_read = chrono::steady_clock::now();
	//uint32_t ms = chrono::duration_cast<chrono::milliseconds>(end_read - start_read).count();
	//if(ms > 1) break;
      }
      if (m_ntrigs%100==0) {
	cout << " NTrig: " << m_ntrigs << " , TOTAL Hits in the last batch: " << nhits << " == PER FE: " ;
	const int feSize = GetFEs().size();
	for(feID=0; feID< feSize; feID++) {
	  cout << " " << vhits.at(feID) << " || ";
	}
	std::cout<<std::endl;
	vhits.clear();
	vhits.resize(GetFEs().size(), 0);
	nhits=0;
      }
      if (m_ntrigs%500==0) {
	std::cout << " plotting " << std::endl;
	int c=0;
	for(auto fe : GetFEs()) {
	  c++;
	  myC2->cd(c);
	  m_occ[fe->GetName()]->Draw("COLZ");
	}
	myC2->Update();
      }

      //for(auto fe : GetFEs())   Send(fe); // what is this for???
      
    }

    auto end_scan = chrono::steady_clock::now();
    m_realDuration = chrono::duration_cast<chrono::seconds>(end_scan - start_scan).count();
    if(m_realDuration > m_duration) break;
  }
  std::cout << "m_realDuration: " << m_realDuration << " s" << endl;
}

void NoiseScan::Analysis(){

  gROOT->SetBatch(true);
  TCanvas* can=new TCanvas("plot","plot",800,600);
  m_maxRate=100;
  
  for(auto fe : GetFEs()){
    if(stopScan) m_duration = m_realDuration;
    m_occ[fe->GetName()]->SetTitle(("Occupancy map (in "+to_string(m_duration)+" s)").c_str());
    m_occInBC[fe->GetName()]->SetTitle(("Occupancy/bc map (in "+to_string(m_duration)+" s)").c_str());
    m_bcTriggered = m_ntrigs * m_TriggerMultiplier * (m_bc[0] + m_bc[1] + m_bc[2] + m_bc[3]);
    m_logFile << "\"m_duration\": " << m_duration << " s" << endl;
    m_logFile << "\"m_bcTriggered\": " << m_bcTriggered << endl;
    for(uint32_t row = 0; row < Matrix::NUM_ROWS; row++){
      for(uint32_t col = 0; col < Matrix::NUM_COLS; col++){
	uint32_t occupancy = m_occ[fe->GetName()]->GetBinContent(col+1,row+1);
	float occupancyInBC = ((float)occupancy); ///((float)m_bcTriggered);
	m_occInBC[fe->GetName()]->SetBinContent(col+1,row+1,occupancyInBC);
        if (occupancyInBC >= m_maxRate){
          m_enableMap[fe->GetName()]->SetBinContent(col+1,row+1,1);
	  fe->SetPixelEnable(col,row,false);
	  cout << " FE: " << fe->GetName() << " --> Masking pixel: " << col << " , " << row << endl;
        }
      }
    }

    m_occ[fe->GetName()]->Draw("COLZ");
    can->Print( (m_fullOutPath+"/"+fe->GetName()+"__OCC.pdf").c_str() );

    m_occ[fe->GetName()]->Write();
    m_enableMap[fe->GetName()]->Write();
    m_occInBC[fe->GetName()]->Write();
    delete m_occ[fe->GetName()];
    delete m_enableMap[fe->GetName()];
    delete m_occInBC[fe->GetName()];
  }

  delete can;
}
