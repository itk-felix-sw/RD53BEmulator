#!/usr/bin/env python
##################################################
# Create Look Up Tables for RD53B 
# binary tree encoding and decoding
#
# The pixels in the quad-core-region can be 
# numbered like the following
# 
# |     | Column                     ||||||||
# | Row | 1 | 2 | 3 | 4 |  5 |  6 |  7 |  8 |
# | --- | - | - | - | - | -- | -- | -- | -- | 
# | 1   | 1 | 2 | 3 | 4 |  9 | 10 | 11 | 12 |
# | 2   | 5 | 6 | 7 | 8 | 13 | 14 | 15 | 16 |
#
# And assigned the following code that can be obtained
# by first splitting the region left to right, and
# assigning a 1 to the region where the selected pixel
# is located, and a 0 otherwise, keeping left first,
# then splitting top and bottom, and assigning a 1 to
# the region with the hit, and finally left right again,
# keeping left first. 
# 
#
# | Pix | Code     |
# | --- | -------- |
# |   1 | 10101010 |
# |   2 | 10101001 |
# |   3 | 10100110 |
# |   4 | 10100101 |
# |   5 | 10011010 |
# |   6 | 10011001 |
# |   7 | 10010110 |
# |   8 | 10010101 |
# |   9 | 01101010 |
# |  10 | 01101001 |
# |  11 | 01100110 |
# |  12 | 01100101 |
# |  13 | 01011010 |
# |  14 | 01011001 |
# |  15 | 01010110 |
# |  16 | 01010101 |
#
#
# It can be reproduced by using a 16-bit array,
# and a breadth-first-search algorithm that
# splits the array in half and returns a 1 if
# the any half of the array contains a 1, or
# a 0 otherwise. The return values are sorted
# by increasing bit number (LSB half first).
# If the search returns a 1, the corresponding
# half is added to the list of arrays to search,
# and the return values are appended in pairs.
# This is the purpose of the hitmap function.
#
# This code produces the resulting hitmap patterns
# for all 16-bit array combinations, and are
# written into a c++ class file as look-up-tables.
# The position corresponds to the 16-bit value of
# the original bit map.
#
# Carlos.Solans@cern.ch
# May 2020
##################################################

import os
import sys
import argparse

parser=argparse.ArgumentParser("Create LUTs for RD53B")
parser.add_argument("-v","--verbose",help="enable verbose mode", action="store_true")
parser.add_argument("-c","--classname",help="Class name", default="BinaryTree")
parser.add_argument("-s","--src",help="source dir (default=src)", default="src")
parser.add_argument("-i","--include",help="include dir (default=same as classname)")
parser.add_argument("-o","--outdir",help="Output directory (default=.)", default=".")
args=parser.parse_args()

if not args.include: args.include=args.classname

def pattern(val,bits):
    top='0'
    bot='0'
    halfbits=int(bits>>1);
    for i in range(halfbits):
        if val[i]=='1': 
            top='1'
            break
        pass
    for i in range(halfbits):
        if val[i+halfbits]=='1': 
            bot='1'
            break
        pass
    ret=[bot+top,[]]
    if bot=='1' and bits>1: 
        ret[1].append((val[halfbits:],halfbits))
        pass
    if top=='1' and bits>1:
        ret[1].append((val[0:halfbits],halfbits))
        pass
    return ret


# def patternS3Map(vals,bits):
#     Left='0'
#     Right='0'
#     for i in range(2):
#         if val[i]=='1': 
#             Left='1'
#             break
#         pass
#     for i in range(2):
#         if val[i+2]=='1': 
#             Right='1'
#             break
#         pass
    
#     ret = [Left+Right,[]]
#     if Left = '1':
#         ret[0] += val[0]
#         ret[0] += val[1]
#     if Right = '1':
#         ret[0] += val[2]
#         ret[0] += val[3]




def hitmap(needle):
    hm=''
    S3=[]
    Map=[]
    qlist=[]
    qlist.append(needle)
    listSize = len(qlist)
    listInd=0;
    # print (qlist)
    while listInd<listSize:
        q = qlist[listInd]
        ret=pattern(q[0],q[1])
        if not (ret[0]=='00'): 
            retSize=ret[1][0][1]
            if(retSize==8):
                hm+=ret[0]
            elif(retSize==4):
                #print("Before join:",hm,S3,Map)
                hm+="".join(S3)
                hm+="".join(Map)
                #print("After join:",hm)
                hm+=ret[0]
                S3=[]
                Map=[]
            elif(retSize==2):
                S3.append(ret[0])
            elif(retSize==1):
                Map.append(ret[0])
            qlist = qlist[:listInd+1] + ret[1] + qlist[listInd+1:]
            listSize = len(qlist) 

        # print(qlist)
        # print ("S3",S3,"map",Map)
        # print(listInd,listSize,hm)
        listInd+=1

        pass
    hm+="".join(S3)
    hm+="".join(Map)
    return hm
     
def compress(val):
    ret=''
    for i in range(0,len(val),2):
        v=val[i:i+2]
        if v=='01': ret+='0'
        else: ret+=v
        pass
    return ret

sss={}

enc2hit={}
hit2enc={}
sz2encs={}
cc2pos={}
bb2pos={}
bbs=[]
ccs=[]
szs=[]
for n in range(2**16):
    if n==0: continue
    bb='{0:016b}'.format(n)
    #print('n: %5i bb: %s' % (n,bb))
    pp=hitmap((bb,16))
    cc=compress(pp)
    sz=len(cc)
    print('n: %5i, bb: %s, s: %2i, p: %30s, s: %2i, c: %30s' % (n,bb,len(pp),pp,len(cc),cc))
    hit2enc[bb]=cc
    enc2hit[cc]=bb
    if not sz in sz2encs: sz2encs[sz]=[]
    sz2encs[sz].append(cc)
    cc2pos[cc]=n
    bb2pos[bb]=n
    ccs.append(cc)
    bbs.append(bb)
    szs.append(sz)
    if not sz in sss: sss[sz]=0
    sss[sz]+=1
    pass

print("Stats: %s" % sss)

print("Write out files")
if not os.path.exists("%s/%s"%(args.outdir,args.include)): os.system("mkdir %s/%s"%(args.outdir,args.include))
if not os.path.exists("%s/%s"%(args.outdir,args.src)):     os.system("mkdir %s/%s"%(args.outdir,args.src))

fw=open("%s/%s/%s.h"%(args.outdir,args.include,args.classname),"w+")
fw.write("#ifndef RD53B_BINARYTREE_H\n")
fw.write("#define RD53B_BINARYTREE_H\n")
fw.write("\n")
fw.write("#include <map>\n")
fw.write("#include <vector>\n")
fw.write("#include <cstdint>\n")
fw.write("\n")
fw.write("namespace RD53B{\n")
fw.write("\n")
fw.write("/**\n")
fw.write(" * The hit maps in a quarter-core region of the RD53B are encoded into\n")
fw.write(" * patterns (%s::ccs) of different sizes (%s::szs).\n" % (args.classname,args.classname))
fw.write(" * The position of the hitmap in the look-up-table corresponds to the binary\n")
fw.write(" * representation of the hit following the numbering of the pixels in the matrix\n")
fw.write(" * (row increases from top to bottom down, and column from left to right).\n")
fw.write(" * \n")
fw.write(" * \n")
fw.write(" * The pixels in the quad-core-region can be numbered like the following:\n")
fw.write(" * \n")
fw.write(" * | R/C | 1 | 2 | 3 | 4 |  5 |  6 |  7 |  8 |\n")
fw.write(" * | --- | - | - | - | - | -- | -- | -- | -- |\n")
fw.write(" * | 1   | 1 | 2 | 3 | 4 |  9 | 10 | 11 | 12 |\n")
fw.write(" * | 2   | 5 | 6 | 7 | 8 | 13 | 14 | 15 | 16 |\n")
fw.write(" *\n")
fw.write(" * And assigned the following code that can be obtained\n")
fw.write(" * by first splitting the region left to right, and\n")
fw.write(" * assigning a 1 to the region where the selected pixel\n")
fw.write(" * is located, and a 0 otherwise, keeping left first,\n")
fw.write(" * then splitting top and bottom, and assigning a 1 to\n")
fw.write(" * the region with the hit, and finally left right again,\n")
fw.write(" * keeping left first. \n")
fw.write(" *\n")
fw.write(" *\n")
fw.write(" * | Pix | Code     |\n")
fw.write(" * | --- | -------- |\n")
fw.write(" * |   1 | 10101010 |\n")
fw.write(" * |   2 | 10101001 |\n")
fw.write(" * |   3 | 10100110 |\n")
fw.write(" * |   4 | 10100101 |\n")
fw.write(" * |   5 | 10011010 |\n")
fw.write(" * |   6 | 10011001 |\n")
fw.write(" * |   7 | 10010110 |\n")
fw.write(" * |   8 | 10010101 |\n")
fw.write(" * |   9 | 01101010 |\n")
fw.write(" * |  10 | 01101001 |\n")
fw.write(" * |  11 | 01100110 |\n")
fw.write(" * |  12 | 01100101 |\n")
fw.write(" * |  13 | 01011010 |\n")
fw.write(" * |  14 | 01011001 |\n")
fw.write(" * |  15 | 01010110 |\n")
fw.write(" * |  16 | 01010101 |\n")
fw.write(" * \n")
fw.write(" * @brief RD53B %s look-up-tables for quarter-core hitmap encoding and decoding.\n" % args.classname)
fw.write(" * @author Carlos.Solans@cern.ch\n")
fw.write(" * @date May 2020\n")
fw.write(" **/\n")
fw.write("\n")
fw.write("class %s{\n" % args.classname)
fw.write("  public:\n");
fw.write("  /** @brief Compressed bit-maps **/\n")
fw.write("  static const uint32_t ccs[65536];\n")
fw.write("  /** @brief Uncompressed bit-maps **/\n")
fw.write("  static const uint32_t bbs[65536];\n")
fw.write("  /** @brief Size of the compressed bit-maps **/\n")
fw.write("  static const uint32_t szs[65536];\n")
fw.write("};\n")
fw.write("\n")
fw.write("}\n")
fw.write("#endif\n")
fw.close()

fw=open("%s/%s/%s.cpp"%(args.outdir,args.src,args.classname),"w+")

fw.write("#include <%s/%s.h>\n"%(args.include,args.classname))
fw.write("\n")
fw.write("using namespace RD53B;\n")
fw.write("\n")

fw.write("const uint32_t %s::ccs[65536]={\n"%args.classname)
i=1
for cc in ccs:
    fw.write("0x%08x,"%int(cc,2)) 
    if i%8==0: fw.write("\n");
    i+=1
    pass
fw.write("};\n\n")

fw.write("const uint32_t %s::bbs[65536]={\n"%args.classname)
i=1
for bb in bbs:
    fw.write("0x%08x,"%int(bb,2)) 
    if i%8==0: fw.write("\n");
    i+=1
    pass
fw.write("};\n\n")

fw.write("const uint32_t %s::szs[65536]={\n"%args.classname)
i=1
for sz in szs:
    fw.write("%2i,"%sz) 
    if i%32==0: fw.write("\n");
    i+=1
    pass
fw.write("};\n\n")

fw.close()
