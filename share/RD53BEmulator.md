# RD53B software for ITK-FELIX {#PackageRD53BEmulator}

This package contains all the software necesary to control, read-out and emulate the RD53B pixel detector.

## The RD53B pixel detector

The RD53B is a Pixel detector with 400 columns and 384 rows that succeeds the RD53A. It has a single single differential front-end design. The RD53B::FrontEnd class represents the RD53B. It uses several classes to represent the functionality of the chip. The global register configuration is stored in the RD53B::Configuration. The pixel configuration is stored in the RD53B::Pixel. And the pixels are organized in rows and columns in the RD53B::Matrix.

The command path of the RD53B can handle up to 160 Mbps of encoded commands. The RD53B::Encoder can decode a byte array into RD53B::Command objects, and encodes any RD53B::Command into a byte array. The RD53B::WrReg is a command to write a register. The RD53B::RdReg is a command to request the read-back of a register. This includes the pixel registers that are written by sending a RD53B::WrReg command to the RD53B::Configuration::PIX_PORTAL address. The corresponding pixel pair is selected by the RD53B::Configuration::REGION_COL and RD53B::Configuration::REGION_ROW. Other commands include the RD53B::Cal that controls the gernation of the charge injection, the RD53B::Clear that clears the entire data path, the RD53B::PllLock that allows the front-end to lock the correct clock speed, the RD53B::Pulse that generates a signal inside the chip for syncrhonised configuration, the RD53B::Sync that is used as a spacer between other commands, and the RD53B::Trigger that specifies which of the next four bunch-crossings should be triggered.

The data path of the RD53B is made out of 64-bit Aurora frames. The RD53B::Decoder class can decode any byte array into RD53B::Frame objects. The frames can be RD53B::DataFrame for frames containing hit information, RD53B::RegisterFrame for frames containing read-back register information, or RD53B::IdleFrame with debug information that is used as a spacer in the Aurora protocol. One or many RD53B::DataFrame frames build one RD53B::Stream that actually contain the hit information that is represented by the RD53B::Hit. Part of the stream containing the hit map can be binary compressed. The RD53B::HitMap is a tool to decompress and compress the hit maps. 

The RD53B::Emulator is an emulator of the RD53B that implements the same classes as the RD53B::FrontEnd but in this case emulates the behaviour of the detector, and can be used to test the code.

## Implementation paradigm

The RD53B::Handler is an example on how to interact with the RD53B::FrontEnd objects. It implements the communication layer with FELIX through NETIO/NETIO-NEXT for any number of RD53B::FrontEnd objects. The methods of the RD53B::Handler represent the ATLAS TDAQ FSM transition commands with the exception of the connect transition.

- RD53B::Handler::Connect: connect to FELIX through NETIO/NETIO-NEXT before it is possible to configure the front-ends. 
- RD53B::Handler::InitRun: read the configuration into memory
- RD53B::Handler::Config: configure the RD53B::FrontEnd objects
- RD53B::Handler::PreRun: prepare the histograms and trigger sequences for the next run
- RD53B::Handler::Run: loop over the trigger sequences, acquire the data, and histogram
- RD53B::Handler::Analysis: analogous to the StopEB transition, perform checks on the histograms
- RD53B::Handler::Disconnect: disconnect from FELIX

The methods in the RD53B::Handler are single threaded, thus smart grouping of RD53B::FrontEnd objects into multiple RD53B::Handler instances allow concurrent processing. Since the calibration procedure of a pixel detector module is a sequence of tuning steps, but the tuning of many modules is a completely independent process, that can be highly parallelized. The available calibration scans and tuning procedures are those that extend from the RD53B::Handler.


## How to run a scan

1. Prepare the configuration file for example for emu-rd53b-1.json
2. Prepare the connectivity file: make sure the emu-rd53b-1.json is referenced inside as emu-rd53b-1
3. Run the scan_manager_rd53b
   
   ```
   scan_manager_rd53b -n connectivity-file.json -s scan-name
   ```

### Connectivity file for NETIO

```
{
  "connectivity" : [
    {"name": "emu-rd53b-1", "config" : "emu-rd53b-1.json", "rx" :  0, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 0, "locked" : 0},
    {"name": "emu-rd53b-2", "config" : "emu-rd53b-2.json", "rx" :  4, "tx" :  4, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 0, "locked" : 0},
    ]
}
```

### Configuration files 

See package @PackageRD53BConfigs
   
### Available calibration scans

- RD53B::CommScan
- RD53B::DigitalScan
- RD53B::AnalogScan
- RD53B::NoiseScan
- RD53B::ReadRegisterScan
- RD53B::ThresholdScan
- RD53B::GlobalThresholdTune
- RD53B::PixelThresholdTune
- RD53B::ToTScan
- RD53B::TrimScan
- RD53B::CrossTalkScan
- RD53B::DisconnectedBumpScan
- RD53B::SelfTriggerScan
- RD53B::ExternalTriggerScan

## How to run the software emulator

1. Run the felix_emulator_rd53b

```
felix_emulator_rd53b
```


   
