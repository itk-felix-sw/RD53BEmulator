#!/usr/bin/env python

import os
import argparse

if __name__=="__main__":
    
    parser=argparse.ArgumentParser()
    parser.add_argument('-v','--verbose',help='enable verbose mode',action='store_true')
    parser.add_argument('-t','--trg-dir',help='itk felix sw directory',default=os.environ['ITK_PATH'])
    parser.add_argument('-f','--flx-dir',help='Felix directory',required=True)
    args=parser.parse_args()

    libs=['libfelix-client-lib.so']

    paths=[os.environ['CMTCONFIG']+"/felix-client/",""]
    dst=args.trg_dir+"/installed/external/"+os.environ["CMTCONFIG"]+"/lib";

    if not os.path.exists(dst):
        print("Create destination directory: %s" % dst)
        os.system("mkdir -p %s" % dst);
        pass

    for lib in libs:
        for p in paths:
            if os.path.exists("%s/%s/%s" % (args.flx_dir,p,lib)):
                os.system("cp -v %s/%s/%s %s/." % (args.flx_dir,p,lib,dst))
                pass
            pass
        pass

    print("Have a nice day")
