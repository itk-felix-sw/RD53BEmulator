#!/usr/bin/env python

import os
import sys
import ROOT
import argparse

def pattern(val,bits):
    top='0'
    bot='0'
    for i in range(bits/2):
        if val[i]=='1': 
            top='1'
            break
        pass
    for i in range(bits/2):
        if val[i+bits/2]=='1': 
            bot='1'
            break
        pass
    ret=[bot+top,[]]
    if bot=='1' and bits>1: 
        ret[1].append((val[bits/2:],bits/2))
        pass
    if top=='1' and bits>1:
        ret[1].append((val[0:bits/2],bits/2))
        pass
    return ret

def hitmap(needle):
    hm=''
    qlist=[]
    qlist.append(needle)
    for q in qlist:
        ret=pattern(q[0],q[1])
        if ret[0]=='00': continue
        hm+=ret[0]
        qlist.extend(ret[1])
        pass
    return hm
     
def compress(val):
    ret=''
    for i in range(0,len(val),2):
        v=val[i:i+2]
        if v=='01': ret+='0'
        else: ret+=v
        pass
    return ret

parser=argparse.ArgumentParser()
parser.add_argument('-v','--verbose',help='enable verbose mode',action='store_true')
parser.add_argument('-d','--dirname',help='directory name',default='.')
parser.add_argument('-p','--pattern',help='pattern name',default='profile')
args=parser.parse_args()


print("Parse directory")
dd=[]
for fname in os.listdir(args.dirname):
    if not args.pattern in fname: continue
    if not fname.startswith(args.pattern): continue
    fr = open("%s/%s"%(args.dirname,fname))
    for line in fr.readlines():
        line=line.strip()
        if len(line)==0: continue
        if args.verbose: print("Parse: %s"%line)
        chk=line.split(",")
        ele={}
        for ch in chk:
            cc=ch.split(":")
            if "hit" in cc[0]  : ele["hits"]=int(cc[1])
            elif "Pat" in cc[0]: ele["pattern"]=int(cc[1],16)
            elif "Enc" in cc[0]: ele["encoding"]=float(cc[1])
            elif "Dec" in cc[0]: ele["decoding"]=float(cc[1])
            pass
        ele["hibits"]=bin(ele["pattern"]).count("1")
        ele["hitmap"]=hitmap(('{0:016b}'.format(ele["pattern"]),16))
        ele["encmap"]=compress(ele["hitmap"])
        ele["size"]=len(ele["encmap"])
        dd.append(ele)
        pass
    pass

print("Plot")
hh={}

hh["enc"]=ROOT.TH1F("hEnc",";Encoding freq [MHz]",100,1,30)
hh["dec"]=ROOT.TH1F("hDec",";Decoding freq [MHz]",100,1,20)
hh["size"]=ROOT.TH1F("hSize",";Size",40,0,40)
hh["encvsbits"]=ROOT.TH2F("hEncVsBits",";Bits at 1; Encoding freq [MHz]",17,0,17,100,1,30)
hh["decvsbits"]=ROOT.TH2F("hDecVsBits",";Bits at 1; Decoding freq [MHz]",17,0,17,100,1,20)
hh["encvspatt"]=ROOT.TH2F("hEncVsPatt",";Pattern; Encoding freq [MHz]",0x10000,0,0x10000,100,1,30)
hh["decvspatt"]=ROOT.TH2F("hDecVsPatt",";Pattern; Decoding freq [MHz]",0x10000,0,0x10000,100,1,20)
hh["encvssize"]=ROOT.TH2F("hEncVsSize",";Enc size; Encoding freq [MHz]",40,0,40,100,1,30)
hh["decvssize"]=ROOT.TH2F("hDecVsSize",";Enc size; Decoding freq [MHz]",40,0,40,100,1,20)
hh["decvsenc"]=ROOT.TH2F("hDecVsEnc",";Encoding freq [MHz]; Decoding freq [MHz]",100,1,30,100,1,20)

for k in dd:
    hh["enc"].Fill(k["encoding"])
    hh["dec"].Fill(k["decoding"])
    hh["size"].Fill(k["size"])
    hh["encvsbits"].Fill(k["hibits"],k["encoding"])
    hh["decvsbits"].Fill(k["hibits"],k["decoding"])
    hh["encvspatt"].Fill(k["pattern"],k["encoding"])
    hh["decvspatt"].Fill(k["pattern"],k["decoding"])
    hh["encvssize"].Fill(k["size"],k["encoding"])
    hh["decvssize"].Fill(k["size"],k["decoding"])
    hh["decvsenc"].Fill(k["encoding"],k["decoding"])
    pass

cc={}
for k in hh:
    cc[k]=ROOT.TCanvas("c%s"%k,"",800,600)
    hh[k].Draw()
    cc[k].Update()
    pass

raw_input("press any key")

