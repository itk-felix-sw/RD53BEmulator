#!/usr/bin/env python
import os
import sys
import time
import threading
import subprocess
import argparse

def startProfile(pos, cmds):
    idx=pos
    while idx < len(cmds):
        cmd=cmds[idx]
        print("Thread %2i Start job: %s"%(pos,cmd))
        proc = subprocess.Popen(cmd,shell=True)
        proc.wait()
        idx=idx+16
        pass
    pass

if __name__=="__main__":
    
    parser=argparse.ArgumentParser()
    parser.add_argument('-v','--verbose',help='enable verbose mode',action='store_true')
    parser.add_argument('-d','--dirname',help='directory name',default='.')
    args=parser.parse_args()

    if not os.path.exists(args.dirname): os.system("mkdir -p %s" % args.dirname)
    
    occs={}
    occs[1]="-r 31 -c 31"
    occs[2]="-r 31 32 -c 31 32"
    occs[3]="-r 31 32 33 -c 31 32 33"
    occs[5]="-r 31 32 33 34 35 -c 31 32 33 34 35"
    occs[7]="-r 31 32 33 34 35 36 37 -c 31 32 33 34 35 36 37"
    encs={}
    encs[0]="-B"
    encs[1]="  "
    tots={}
    tots[0]="-T"
    tots[1]="  "

    cmds=[]    
    
    for occi in occs:
        for evts in (1, 3, 10):
            for pat in (0x0001, 0x0005, 0x000F, 0x00FF, 0x0FFF, 0xFFFF):
                for enci in encs:
                    for toti in tots:
                        fn="%s/out_%02i_%04x_%02i_E%i_T%i.txt" % (args.dirname, occi, pat, evts, toti, enci)
                        cmds.append("profile_rd53b_data %s -p 0x%04x -n %i %s %s -o %s" % (occs[occi], pat, evts, encs[enci], tots[toti], fn))
                        pass
                    pass
                pass
            pass
        pass
    
    tts={}
    ntt=16
    for i in range(ntt):
        print("Start thread: %i" %i)
        tts[i]=threading.Thread(target=startProfile,args=(i,cmds))
        tts[i].start()
        time.sleep(1)
        pass
    print ("Monitor")
    running=True
    while running:
        running=False
        for i in tts:
            if tts[i].is_alive():
                running=True
                print("Thread %i is still running" % i)
                pass
            else:
                print("Thread %i is done" % i)
                pass
            pass
        time.sleep(3)
        pass
    print("Have a nice day")
    pass

