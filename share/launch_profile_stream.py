#!/bin/env/python
import os
import sys
import time
import threading
import subprocess
import argparse

def makeCmd(start, stop, enc, tot):
    fn="profile_"
    if enc: fn+="enc_"
    else: fn+="noenc_"    
    if tot: fn+="tot_"
    else: fn+="notot_"
    fn+="%04x_%04x" % (start, stop)
    ss="profile_rd53b_stream -p 0x%04x -P 0x%04x -n 1 -o %s" % (start, stop, fn)
    if not enc: ss+=" -E"
    if not tot: ss+=" -T"
    return ss

def startProfile(pos,enc,tot):
    first = 0x1000*pos
    num = 0xFFF
    steps = 16
    start = first
    for i in range(steps):
        stop=start+((num+1)/steps)-1
        cmd=makeCmd(start,stop,enc,tot)
        print("Start job: %s"%cmd)
        proc = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
        proc.wait()
        start=stop+1
        pass
    pass

if __name__=="__main__":
    
    parser=argparse.ArgumentParser()
    parser.add_argument('-v','--verbose',help='enable verbose mode',action='store_true')
    parser.add_argument('-d','--dirname',help='directory name',default='.')
    parser.add_argument('--no-enc',help='disable encoding',action='store_true')
    parser.add_argument('--no-tot',help='disable tot',action='store_true')
    args=parser.parse_args()

    if not os.path.exists(args.dirname): os.system("mkdir -p %s" % args.dirname)
    os.chdir(args.dirname)
    
    tts={}
    ntt=16
    for i in range(ntt):
        print("Start thread: %i" %i)
        tts[i]=threading.Thread(target=startProfile,args=(i,not args.no_enc,not args.no_tot))
        tts[i].start()
        time.sleep(1)
        pass
    print ("Monitor")
    running=True
    while running:
        running=False
        for i in tts:
            if tts[i].is_alive():
                running=True
                print("Thread %i is still running" % i)
                pass
            else:
                print("Thread %i is done" % i)
                pass
            pass
        time.sleep(3)
        pass
    print("Have a nice day")
    pass

